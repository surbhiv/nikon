 //
//  MenuViewController.swift
//  NikonApp
//
//  Created by Surbhi on 20/12/16.
//  Copyright © 2016 Neuronimbus. All rights reserved.
//

import UIKit
import GGLSignIn
import GoogleSignIn
import Google
import FBSDKCoreKit
import FBSDKLoginKit


class MenuViewController: UITableViewController{
    
    var menuArray : NSMutableArray = [ "Home","My Gallery","My Workshop", "About Us", "Contact Us", "Workshop", "Loyalty", "Gallery", "Blog", "Ask a Question","FAQ's","Add a Camera","Contest", "Logout", "Rate our App","Share App","Events & Promotions","Privacy Policy","License Agreement"]
    
    var menuArrayWithOutLogin : NSMutableArray = [ "Home", "About Us", "Contact Us", "Workshop", "Loyalty", "Gallery", "Blog", "Ask a Question","FAQ's","Add a Camera","Contest", "Login", "Rate our App","Share App","Events & Promotions","Privacy Policy","License Agreement"]
    
    var menuImageArrayWithOutLogin : NSMutableArray = [ "Home_icon", "aboutUs_Icon", "contactus_Icon", "Workshop_MenuIcon", "Loyalty_MenuIcon", "Gallery_MenuIcon", "Blog_MenuIcon", "askQuestion_Icon","faq_Icon","add_camera_menuicon", "contest_menuicon","logout_Icon", "rate_Icon","share_White","events_White","privacy_Icon","licenseAgreement"]
    var menuImageArray : NSMutableArray = [ "Home_icon","Gallery_MenuIcon","Workshop_MenuIcon", "aboutUs_Icon", "contactus_Icon", "Workshop_MenuIcon", "Loyalty_MenuIcon", "Gallery_MenuIcon", "Blog_MenuIcon", "askQuestion_Icon","faq_Icon","add_camera_menuicon", "contest_menuicon","logout_Icon", "rate_Icon","share_White","events_White","privacy_Icon","licenseAgreement"]
    
    var selectedMenuItem : Int = 0
    var lastSelectedMenuItem : Int = 0
    var username = String()
    var containImage = Bool()
    var profileImageStr = String()

    
    override func viewDidLoad() {
        super.viewDidLoad()

        username = ""
        if (UserDefaults.standard.integer(forKey: Constants.USERID) != 0) {
            self.getUserDetail(String(UserDefaults.standard.integer(forKey: Constants.USERID)) as NSString)
            
        }
        else
        {
            username = "Guest Login"
        }
        
        
        DispatchQueue.main.async
        {
            self.tableView.reloadData()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    //MARK:- GET USER DETAIL API
    func getUserDetail(_ userId : NSString)  {
        if Reachability.isConnectedToNetwork()
        {
            let method = "getuserprofile?emp_id=\(userId)"
            
            Constants.appDelegate.startIndicator()
            Server.getRequestWithURL(Constants.BASEURL+method, completionHandler: { (response) in
                
                
                // if response[Constants.STATE] != nil && response[Constants.STATE] as! String == Constants.SUCC {
                let responseDict = response //as NSDictionary
                
                DispatchQueue.main.async
                {
                    Constants.appDelegate.stopIndicator()
                    let userName = "\(responseDict["User_Title"] as! String) \(responseDict["User_Fname"] as! String) \(responseDict["User_Lname"] as! String)"
                    self.username = userName
                    self.profileImageStr = responseDict["ProfileImage"] as! String
                    self.containImage = true
                    UserDefaults.standard .set(userName, forKey: Constants.USERNAME)
					UserDefaults.standard .set(Helper.getDOBStringFromDate(response["User_Dob"] as? String ?? "" ), forKey: Constants.USERDOB)
                    
                    UserDefaults.standard .synchronize()
                    
                    DispatchQueue.main.async
                    {
                       
                        self.tableView.reloadData()
                    }
                  
                    
                }
                
                
            })
        }
        else
        {
            Constants.appDelegate.stopIndicator()
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
        
        
    }

    
    func viewProfileButton()  {
         if (UserDefaults.standard.integer(forKey: Constants.USERID) != 0) {
        
            let destViewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "ProfileViewController")
            //self.navigationController?.pushViewController(destViewController, animated: true)
            sideMenuController()?.setContentViewController(destViewController)
        }
        else
         {
            self.loginAlert()
        }
        
        
    }
    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
            return 150
    }
    
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headervw = UIView(frame: CGRect(x: 0,y: 0,width: tableView.frame.size.width,height: 150))
        headervw.backgroundColor = Colors.textPlaceHolderGray
        
        let bg = UIImageView.init(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 150))
        bg.image = UIImage.init(named: "slide_top_new.jpg")
        headervw.addSubview(bg)
        
        let profile = UIImageView(frame: CGRect(x: (headervw.frame.size.width - 65)/2, y: 39, width: 65, height: 65))
        profile.layer.cornerRadius = 32.5
        profile.layer.borderWidth = 2
        profile.layer.borderColor = UIColor.white.cgColor
        profile.layer.backgroundColor = UIColor.clear.cgColor
        profile.image = UIImage.init(named: "profilePlaceHolder")
        if (containImage)
        {
            profile.sd_setImage(with: URL.init(string: profileImageStr), placeholderImage: UIImage.init(named: "profilePlaceHolder"))
            profile.setShowActivityIndicator(true)
        }
        profile.contentMode = UIViewContentMode.scaleAspectFill
        profile.clipsToBounds = true
        
        let nameLabel = UIButton(frame: CGRect (x: 10,y: 112,width: tableView.frame.size.width-20,height: 30))
        nameLabel.setImage(UIImage.init(named: "editProfile"), for:UIControlState())
        nameLabel.setTitleColor(UIColor.white, for: UIControlState())
        nameLabel.titleLabel?.font = UIFont(name: "Opensans-Semibold", size: 15)!
        nameLabel.contentHorizontalAlignment = UIControlContentHorizontalAlignment.center
        nameLabel.semanticContentAttribute = UISemanticContentAttribute.forceRightToLeft
        
        nameLabel.setTitle(username, for: UIControlState())
        nameLabel.addTarget(self, action: #selector(viewProfileButton), for: .touchUpInside)
        
        if DeviceType.IS_IPHONE_6 || DeviceType.IS_IPHONE_6P
        {
            nameLabel.titleLabel?.font = UIFont(name: "Opensans-Semibold", size: 16)!
        }
        
        headervw.addSubview(profile)
        headervw.addSubview(nameLabel)

        return headervw
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (UserDefaults.standard.integer(forKey: Constants.USERID) != 0)
        {

            if (indexPath.row == 5){
                return 10
            }
            else{
                return 50
            }
        }
        else
        {
            if (indexPath.row == 3){
                return 10
            }
            else{
                return 50
            }
            
        }
    }
    
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (UserDefaults.standard.integer(forKey: Constants.USERID) != 0)
        {
            return menuArray.count + 1
        }
        else
        {
            return menuArrayWithOutLogin.count+1
        }
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        tableView.separatorStyle = UITableViewCellSeparatorStyle.none
         if (UserDefaults.standard.integer(forKey: Constants.USERID) != 0) {
            
            if (indexPath.row == 5){
                var cell = tableView.dequeueReusableCell(withIdentifier: "CELL1")
                if (cell == nil) {
                    cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "CELL1")
                    cell!.backgroundColor = Colors.textPlaceHolderGray
                }
                cell?.textLabel?.text = ""
//              cell?.imageView?.image = UIImage.init(named: menuImageArray[indexPath.row] as! String)

                return cell!
            }
            else if (indexPath.row == 14 || indexPath.row == 15 || indexPath.row == 16 || indexPath.row == 17 || indexPath.row == 18 || indexPath.row == 19){
                var cell = tableView.dequeueReusableCell(withIdentifier: "CELL2")
                if (cell == nil) {
                    cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "CELL2")
                    cell!.backgroundColor = UIColor.gray
                    cell!.textLabel?.textColor = UIColor.white
                    cell!.textLabel?.font = UIFont(name: "Opensans-Semibold", size: 13.5)!
                
                    if DeviceType.IS_IPHONE_6 || DeviceType.IS_IPHONE_6P
                    {
                        cell!.textLabel?.font = UIFont(name: "Opensans-Semibold", size: 15)!
                    }
                }
            
                if (indexPath.row == 14){
                    cell!.textLabel!.text = menuArray[13] as? String
                    cell?.imageView?.image = UIImage.init(named: menuImageArray[13] as! String)

                }
                else if (indexPath.row == 15){
                    cell!.textLabel!.text = menuArray[14] as? String
                    cell?.imageView?.image = UIImage.init(named: menuImageArray[14] as! String)

                }
                else if (indexPath.row == 16){
                    cell!.textLabel!.text = menuArray[15] as? String
                    cell?.imageView?.image = UIImage.init(named: menuImageArray[15] as! String)

                
                }
                else if (indexPath.row == 17){
                    cell!.textLabel!.text = menuArray[16] as? String
                    cell?.imageView?.image = UIImage.init(named: menuImageArray[16] as! String)
                
                
                }
                else if (indexPath.row == 18){
                    cell!.textLabel!.text = menuArray[17] as? String
                    cell?.imageView?.image = UIImage.init(named: menuImageArray[17] as! String)
                    
                    
                }

                else
                {
                    cell!.textLabel!.text = menuArray[18] as? String
                    cell?.imageView?.image = UIImage.init(named: menuImageArray[18] as! String)

                
                }
            
                if (cell?.viewWithTag(21)) != nil
                {
                    let vw = cell?.viewWithTag(21)
                    vw?.removeFromSuperview()
                }
            
                let line = UILabel.init(frame: CGRect(x: 0, y: (cell?.frame.size.height)! - 1, width: (cell?.frame.size.width)!, height: 1))
                line.tag = 21
                line.backgroundColor = UIColor.white
                cell?.addSubview(line)

            
                return cell!
            }
            else{
            
            var cell = tableView.dequeueReusableCell(withIdentifier: "CELL3")
                if (cell == nil) {
                    cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "CELL3")
                    cell!.backgroundColor = UIColor.white
                    cell!.textLabel?.textColor = UIColor.black
                    cell!.textLabel?.font = UIFont(name: "Opensans-Light", size: 13.5)!
                
                    if DeviceType.IS_IPHONE_6 || DeviceType.IS_IPHONE_6P
                    {
                        cell!.textLabel?.font = UIFont(name: "Opensans-Light", size: 15)!
                    }
                }
            
                if (cell?.viewWithTag(22)) != nil
                {
                    let vw = cell?.viewWithTag(22)
                    vw?.removeFromSuperview()
                }
                let line = UILabel.init(frame: CGRect(x: 0, y: (cell?.frame.size.height)! - 1, width: (cell?.frame.size.width)!, height: 1))
                line.tag = 22
                line.backgroundColor = Colors.textPlaceHolderGray
                cell?.addSubview(line)

            
                if (indexPath.row < 5){
                    cell!.textLabel!.text = menuArray[indexPath.row] as? String
                    cell?.imageView?.image = UIImage.init(named: menuImageArray[indexPath.row] as! String)

                }
                else if (indexPath.row > 5 && indexPath.row < 14) {
                    cell!.textLabel!.text = menuArray[indexPath.row - 1] as? String
                    cell?.imageView?.image = UIImage.init(named: menuImageArray[indexPath.row - 1] as! String)

                }
            
                return cell!
            }
        }
        else
         {
            if (indexPath.row == 3){
                var cell = tableView.dequeueReusableCell(withIdentifier: "CELL1")
                if (cell == nil) {
                    cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "CELL1")
                    cell!.backgroundColor = Colors.textPlaceHolderGray
                }
                cell?.textLabel?.text = ""
                //              cell?.imageView?.image = UIImage.init(named: menuImageArray[indexPath.row] as! String)
                
                return cell!
            }
            else if (indexPath.row == 12 || indexPath.row == 13 || indexPath.row == 14 || indexPath.row == 15 || indexPath.row == 16  || indexPath.row == 17 ){
                var cell = tableView.dequeueReusableCell(withIdentifier: "CELL2")
                if (cell == nil) {
                    cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "CELL2")
                    cell!.backgroundColor = UIColor.gray
                    cell!.textLabel?.textColor = UIColor.white
                    cell!.textLabel?.font = UIFont(name: "Opensans-Semibold", size: 13.5)!
                    
                    if DeviceType.IS_IPHONE_6 || DeviceType.IS_IPHONE_6P
                    {
                        cell!.textLabel?.font = UIFont(name: "Opensans-Semibold", size: 15)!
                    }
                }
                
                if (indexPath.row == 12){
                    cell!.textLabel!.text = menuArrayWithOutLogin[11] as? String
                    cell?.imageView?.image = UIImage.init(named: menuImageArrayWithOutLogin[11] as! String)
                    
                }
                else if (indexPath.row == 13){
                    cell!.textLabel!.text = menuArrayWithOutLogin[12] as? String
                    cell?.imageView?.image = UIImage.init(named: menuImageArrayWithOutLogin[12] as! String)
                    
                }
                else if (indexPath.row == 14){
                    cell!.textLabel!.text = menuArrayWithOutLogin[13] as? String
                    cell?.imageView?.image = UIImage.init(named: menuImageArrayWithOutLogin[13] as! String)
                    
                    
                }
                else if (indexPath.row == 15){
                    cell!.textLabel!.text = menuArrayWithOutLogin[14] as? String
                    cell?.imageView?.image = UIImage.init(named: menuImageArrayWithOutLogin[14] as! String)
                    
                    
                }
                else if (indexPath.row == 16){
                    cell!.textLabel!.text = menuArrayWithOutLogin[15] as? String
                    cell?.imageView?.image = UIImage.init(named: menuImageArrayWithOutLogin[15] as! String)
                    
                    
                }
                else
                {
                    cell!.textLabel!.text = menuArrayWithOutLogin[16] as? String
                    cell?.imageView?.image = UIImage.init(named: menuImageArrayWithOutLogin[16] as! String)
                    
                    
                }
                
                if (cell?.viewWithTag(21)) != nil
                {
                    let vw = cell?.viewWithTag(21)
                    vw?.removeFromSuperview()
                }
                
                let line = UILabel.init(frame: CGRect(x: 0, y: (cell?.frame.size.height)! - 1, width: (cell?.frame.size.width)!, height: 1))
                line.tag = 21
                line.backgroundColor = UIColor.white
                cell?.addSubview(line)
                
                
                return cell!
            }
            else{
                
                var cell = tableView.dequeueReusableCell(withIdentifier: "CELL3")
                if (cell == nil) {
                    cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "CELL3")
                    cell!.backgroundColor = UIColor.white
                    cell!.textLabel?.textColor = UIColor.black
                    cell!.textLabel?.font = UIFont(name: "Opensans-Light", size: 13.5)!
                    
                    if DeviceType.IS_IPHONE_6 || DeviceType.IS_IPHONE_6P
                    {
                        cell!.textLabel?.font = UIFont(name: "Opensans-Light", size: 15)!
                    }
                }
                
                if (cell?.viewWithTag(22)) != nil
                {
                    let vw = cell?.viewWithTag(22)
                    vw?.removeFromSuperview()
                }
                let line = UILabel.init(frame: CGRect(x: 0, y: (cell?.frame.size.height)! - 1, width: (cell?.frame.size.width)!, height: 1))
                line.tag = 22
                line.backgroundColor = Colors.textPlaceHolderGray
                cell?.addSubview(line)
                
                
                if (indexPath.row < 3){
                    cell!.textLabel!.text = menuArrayWithOutLogin[indexPath.row] as? String
                    cell?.imageView?.image = UIImage.init(named: menuImageArrayWithOutLogin[indexPath.row] as! String)
                    
                }
                else if (indexPath.row > 3 && indexPath.row < 12) {
                    cell!.textLabel!.text = menuArrayWithOutLogin[indexPath.row - 1] as? String
                    cell?.imageView?.image = UIImage.init(named: menuImageArrayWithOutLogin[indexPath.row - 1] as! String)
                    
                }
                
                return cell!
            }
        }
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
//       
        
    if (UserDefaults.standard.integer(forKey: Constants.USERID) != 0)
    {

        
//        if var topController = UIApplication.sharedApplication().keyWindow?.rootViewController {
//            while let presentedViewController = topController.presentedViewController {
//                topController = presentedViewController
//            }
//                   }
        var destViewController : UIViewController
        
        switch (indexPath.row) {
        case 0: // Dashboard
            destViewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "Dashboard")
            lastSelectedMenuItem = selectedMenuItem
            selectedMenuItem = indexPath.row
            sideMenuController()?.setContentViewController(destViewController)
            break
            
        case 1: // User Gallery
            let destVC = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "ContestUserMyGallery")as! ContestUserMyGallery//ContestUserMyGallery
        lastSelectedMenuItem = selectedMenuItem
        selectedMenuItem = indexPath.row
        destVC.isFromMenu = true
        sideMenuController()?.setContentViewController(destVC)
            //self.navigationController?.pushViewController(destVC, animated: true)
        
        case 2: // My Workshop
            let destVC = Constants.mainStoryboard.instantiateViewController(withIdentifier: "MyWorkShopVC")as! MyWorkShopVC//ContestUserMyGallery
        lastSelectedMenuItem = selectedMenuItem
        selectedMenuItem = indexPath.row
        sideMenuController()?.setContentViewController(destVC)
            
        case 3: // About us
            destViewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "WebVC")
            lastSelectedMenuItem = selectedMenuItem
            selectedMenuItem = indexPath.row
            destViewController.title = "ABOUT US"
//            "TERMS & CONDITIONS"
            sideMenuController()?.setContentViewController(destViewController)
            break
            
        case 4: // Constact us
            destViewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "WebVC")
            lastSelectedMenuItem = selectedMenuItem
            selectedMenuItem = indexPath.row
            destViewController.title = "CONTACT US"
            sideMenuController()?.setContentViewController(destViewController)
            break
            
        case 6: // Workshop
           let destVC = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "WorkshopVC")as! WorkshopVC
           
            destVC.isFromMenu = true
            lastSelectedMenuItem = selectedMenuItem
            selectedMenuItem = indexPath.row
            sideMenuController()?.setContentViewController(destVC)
            break

        case 7: // loyalty
            let destVC = Constants.mainStoryboard.instantiateViewController(withIdentifier: "LoyaltyVC") as! LoyaltyVC
            lastSelectedMenuItem = selectedMenuItem
            selectedMenuItem = indexPath.row
            destVC.isFromMenu = true
            sideMenuController()?.setContentViewController(destVC)
            break
            
        case 8: //Gallery
            let destViewController = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "GalleryMainVC")as! GalleryMainVC
            lastSelectedMenuItem = selectedMenuItem
            destViewController.isFromMenu = true
            selectedMenuItem = indexPath.row
            sideMenuController()?.setContentViewController(destViewController)
            break
            
        case 9: // Blog
            let destViewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "BlogMainVC")as! BlogMainVC
            lastSelectedMenuItem = selectedMenuItem
            destViewController.isFromMenu = true
            selectedMenuItem = indexPath.row
            sideMenuController()?.setContentViewController(destViewController)
            break
            
        case 10: // AskAQuestion
            let destViewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "AskQuestionVC")as! AskQuestionVC
            lastSelectedMenuItem = selectedMenuItem
             destViewController.isFromMenu = true
            selectedMenuItem = indexPath.row
            sideMenuController()?.setContentViewController(destViewController)
            break
            
        case 11: // FAQ
            let destViewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "FAQVC")as! FAQVC
            lastSelectedMenuItem = selectedMenuItem
             destViewController.isFromMenu = true
            selectedMenuItem = indexPath.row
            sideMenuController()?.setContentViewController(destViewController)
            break
        case 12: // camera and lense
            let destViewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "CameraLenseMainScroll")as! CameraLenseMainScroll
            lastSelectedMenuItem = selectedMenuItem
             destViewController.isFromMenu = true
            selectedMenuItem = indexPath.row
            sideMenuController()?.setContentViewController(destViewController)
            break
        case 13: // contest
            let destViewController = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "ContestChallengesListVC")as! ContestChallengesListVC
            lastSelectedMenuItem = selectedMenuItem
             destViewController.isFromMenu = true
            selectedMenuItem = indexPath.row
            sideMenuController()?.setContentViewController(destViewController)
            break
        case 15: // rate app
            
            NotificationCenter.default.post(name: Notification.Name(rawValue: "RatingViewCanBeShowm"), object: UIApplication.shared)
            break

        case 16: // SHARE APP
            //todo
            let shareUrl = "https://itunes.apple.com/us/app/nikon-india/id1228039296?ls=1&mt=8"
            let myWebsite = URL.init(string: shareUrl)
            
            let activityVC = UIActivityViewController.init(activityItems: [myWebsite!], applicationActivities: nil)
            let excludeActivities = [UIActivityType.airDrop,UIActivityType.print,UIActivityType.assignToContact,UIActivityType.saveToCameraRoll,UIActivityType.addToReadingList]
            activityVC.excludedActivityTypes = excludeActivities;
            
            
            if UIApplication.shared.keyWindow?.rootViewController?.isKind(of: SideMenuNavigation.self) == true {
                
                let baseVC = UIApplication.shared.keyWindow?.rootViewController?.childViewControllers.first
                
                if DeviceType.IS_IPAD {
                    activityVC.popoverPresentationController!.sourceView = baseVC!.view;
                }
                
                baseVC?.present(activityVC, animated: true, completion: {
                    self.toggleSideMenuView()
                })
                
            }
            
            break
        case 17 : //Events and promotion
            let destViewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "PDFOpenVC")as! PDFOpenVC
            destViewController.pdfURLStr = "http://www.nikon.co.in/en_IN/about/events_and_promotions"
            destViewController.titleStr = "Events & Promotions"
            destViewController.isFromMenu = true
            sideMenuController()?.setContentViewController(destViewController)
            break
        case 18:
            let destViewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "WebVC")as! WebVC
            lastSelectedMenuItem = selectedMenuItem
            selectedMenuItem = indexPath.row
             destViewController.isFromMenu = true
            destViewController.title = "PRIVACY POLICY"
            sideMenuController()?.setContentViewController(destViewController)
            break
            
        case 19:
            let destViewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "PDFOpenVC")as! PDFOpenVC
            destViewController.pdfURLStr = "http://nikonschool.in/UploadImage/AppImages/170327_Nikon_India_Mobile_APP_for_iOS.pdf"
            destViewController.titleStr = "License Agreement"
            destViewController.isFromMenu = true
            sideMenuController()?.setContentViewController(destViewController)
            break

        case 14:
            lastSelectedMenuItem = selectedMenuItem
            selectedMenuItem = indexPath.row
            
            if self.menuArray[indexPath.row-1] as! String == "Login"
            {
                DispatchQueue.main.async
                {
                    let destinationController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "SideMenuNavigation")
                    //self.presentViewController(destinationController, animated: true, completion: nil);
                    Constants.appDelegate.window?.rootViewController = destinationController
                }
            }
            else
            {
                let alertController = UIAlertController.init(title: "Nikon", message: "Are you sure, you want to logout?", preferredStyle: UIAlertControllerStyle.alert)
                let  yesButton = UIAlertAction.init(title: "OK", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                    
                    GIDSignIn.sharedInstance().signOut()
                    let loginManager: FBSDKLoginManager = FBSDKLoginManager()
                    loginManager.logOut()
                    
                    UserDefaults.standard.removeObject(forKey: Constants.USERHAVEPASSWORD)
                    UserDefaults.standard.removeObject(forKey: Constants.USERID)
                    UserDefaults.standard.synchronize()
                    let destinationController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "SideMenuNavigation")
                    //self.presentViewController(destinationController, animated: true, completion: nil);
                    Constants.appDelegate.window?.rootViewController = destinationController
                    
//
                })
                let noButton = UIAlertAction.init(title: "CANCEL", style: UIAlertActionStyle.cancel, handler: { (action:UIAlertAction) in
                                   })
                alertController.addAction(noButton)
                alertController.addAction(yesButton)
                
                self.present(alertController, animated: true, completion: {
                })
            }
            break
//
        default:
            break
            
            }
        }
        else
        {
        
            var destViewController : UIViewController
            
            switch (indexPath.row) {
            case 0: // Dashboard
                destViewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "Dashboard")
                lastSelectedMenuItem = selectedMenuItem
                selectedMenuItem = indexPath.row
                sideMenuController()?.setContentViewController(destViewController)
                break
                
                
            case 1: // About us
                destViewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "WebVC")
                lastSelectedMenuItem = selectedMenuItem
                selectedMenuItem = indexPath.row
                destViewController.title = "ABOUT US"
                //            "TERMS & CONDITIONS"
                sideMenuController()?.setContentViewController(destViewController)
                break
                
            case 2: // Constact us
                destViewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "WebVC")
                lastSelectedMenuItem = selectedMenuItem
                selectedMenuItem = indexPath.row
                destViewController.title = "CONTACT US"
                sideMenuController()?.setContentViewController(destViewController)
                break
                
            case 4: // Workshop
                let destVC = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "WorkshopVC")as! WorkshopVC
                
                destVC.isFromMenu = true
                lastSelectedMenuItem = selectedMenuItem
                selectedMenuItem = indexPath.row
                sideMenuController()?.setContentViewController(destVC)
                
                
                break
                
            case 5: // loyalty
                let destVC = Constants.mainStoryboard.instantiateViewController(withIdentifier: "LoyaltyVC") as! LoyaltyVC
                lastSelectedMenuItem = selectedMenuItem
                selectedMenuItem = indexPath.row
                destVC.isFromMenu = true
                sideMenuController()?.setContentViewController(destVC)
                break
                
            case 6: //Gallery
                let destViewController = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "GalleryMainVC")as! GalleryMainVC
                lastSelectedMenuItem = selectedMenuItem
                destViewController.isFromMenu = true
                selectedMenuItem = indexPath.row
                sideMenuController()?.setContentViewController(destViewController)
                break
                
            case 7: // Blog
                let destViewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "BlogMainVC")as! BlogMainVC
                lastSelectedMenuItem = selectedMenuItem
                destViewController.isFromMenu = true
                selectedMenuItem = indexPath.row
                sideMenuController()?.setContentViewController(destViewController)
                break
                
            case 8: // AskAQuestion
                let destViewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "AskQuestionVC")as! AskQuestionVC
                lastSelectedMenuItem = selectedMenuItem
                destViewController.isFromMenu = true
                selectedMenuItem = indexPath.row
                sideMenuController()?.setContentViewController(destViewController)
                break
                
            case 9: // FAQ
                let destViewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "FAQVC")as! FAQVC
                lastSelectedMenuItem = selectedMenuItem
                destViewController.isFromMenu = true
                selectedMenuItem = indexPath.row
                sideMenuController()?.setContentViewController(destViewController)
                break
            case 10: // camera and lense
//                let destViewController = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("CameraLenseMainScroll")as! CameraLenseMainScroll
//                lastSelectedMenuItem = selectedMenuItem
//                destViewController.isFromMenu = true
//                selectedMenuItem = indexPath.row
//                sideMenuController()?.setContentViewController(destViewController)
                self.loginAlert()
                break
            case 11: // contest
                let destViewController = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "ContestChallengesListVC")as! ContestChallengesListVC
                lastSelectedMenuItem = selectedMenuItem
                destViewController.isFromMenu = true
                selectedMenuItem = indexPath.row
                sideMenuController()?.setContentViewController(destViewController)
                break
            case 13: // rate app
                
                NotificationCenter.default.post(name: Notification.Name(rawValue: "RatingViewCanBeShowm"), object: UIApplication.shared)
                break
                
            case 14: // SHARE APP
                //todo
                let shareUrl = "https://itunes.apple.com/us/app/nikon-india/id1228039296?ls=1&mt=8"
                let myWebsite = URL.init(string: shareUrl)
                
                let activityVC = UIActivityViewController.init(activityItems: [myWebsite!], applicationActivities: nil)
                let excludeActivities = [UIActivityType.airDrop,UIActivityType.print,UIActivityType.assignToContact,UIActivityType.saveToCameraRoll,UIActivityType.addToReadingList]
                activityVC.excludedActivityTypes = excludeActivities;
                
                
                if UIApplication.shared.keyWindow?.rootViewController?.isKind(of: SideMenuNavigation.self) == true {
                    
                    let baseVC = UIApplication.shared.keyWindow?.rootViewController?.childViewControllers.first
                    
                    if DeviceType.IS_IPAD {
                        activityVC.popoverPresentationController!.sourceView = baseVC!.view;
                    }
                    
                    baseVC?.present(activityVC, animated: true, completion: {
                        self.toggleSideMenuView()
                    })
                    
                }
                break
            case 15 : //Events and promotion
                let destViewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "PDFOpenVC")as! PDFOpenVC
                destViewController.pdfURLStr = "http://www.nikon.co.in/en_IN/about/events_and_promotions"
                destViewController.titleStr = "Events & Promotions"
                destViewController.isFromMenu = true
                sideMenuController()?.setContentViewController(destViewController)
                break

                
                
            case 16:
                let destViewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "WebVC")as! WebVC
                lastSelectedMenuItem = selectedMenuItem
                selectedMenuItem = indexPath.row
                destViewController.isFromMenu = true
                destViewController.title = "PRIVACY POLICY"
                sideMenuController()?.setContentViewController(destViewController)
                break
                
            case 17:
                let destViewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "PDFOpenVC")as! PDFOpenVC
                destViewController.pdfURLStr = "http://nikonschool.in/UploadImage/AppImages/170327_Nikon_India_Mobile_APP_for_iOS.pdf"
                destViewController.titleStr = "License Agreement"
                destViewController.isFromMenu = true
                sideMenuController()?.setContentViewController(destViewController)
                break
                
            case 12:
                lastSelectedMenuItem = selectedMenuItem
                selectedMenuItem = indexPath.row
                
                if self.menuArrayWithOutLogin[indexPath.row-1] as! String == "Login"
                {
                    DispatchQueue.main.async
                    {
                        let destinationController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "SideMenuNavigation")
                        //self.presentViewController(destinationController, animated: true, completion: nil);
                        Constants.appDelegate.window?.rootViewController = destinationController
                    }
                }
                else
                {
                    let alertController = UIAlertController.init(title: "Nikon", message: "Are you sure, you want to logout?", preferredStyle: UIAlertControllerStyle.alert)
                    let  yesButton = UIAlertAction.init(title: "OK", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                        
                        GIDSignIn.sharedInstance().signOut()
                        let loginManager: FBSDKLoginManager = FBSDKLoginManager()
                        loginManager.logOut()
                        
                        UserDefaults.standard.removeObject(forKey: Constants.USERHAVEPASSWORD)
                        UserDefaults.standard.removeObject(forKey: Constants.USERID)
                        UserDefaults.standard.synchronize()
                        let destinationController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "SideMenuNavigation")
                        //self.presentViewController(destinationController, animated: true, completion: nil);
                        Constants.appDelegate.window?.rootViewController = destinationController
                        
                        //
                    })
                    let noButton = UIAlertAction.init(title: "CANCEL", style: UIAlertActionStyle.cancel, handler: { (action:UIAlertAction) in
                    })
                    alertController.addAction(noButton)
                    alertController.addAction(yesButton)
                    
                    self.present(alertController, animated: true, completion: {
                    })
                }
                break
            //
            default:
                break
                
            }

        }
    }
    //MARK:- LoginAlert
    
    func loginAlert()  {
        let alert = UIAlertController.init(title: "Nikon", message: "Please login to continue.", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
            
            DispatchQueue.main.async
            {
                let destinationController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "SideMenuNavigation")
                //self.presentViewController(destinationController, animated: true, completion: nil);
                Constants.appDelegate.window?.rootViewController = destinationController
            }
        }))
        alert.addAction(UIAlertAction.init(title: "Cancel", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
            self.dismiss(animated: true, completion: nil)
        }))
        self.present(alert, animated: true, completion: nil)
    }
}
