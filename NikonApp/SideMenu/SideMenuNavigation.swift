//
//  SideMenuNavigation.swift
//  TestSwiftApp
//
//  Created by Surbhi on 24/10/16.
//  Copyright © 2016 Neuronimbus. All rights reserved.
//

import UIKit

class SideMenuNavigation: ENSideMenuNavigationController, ENSideMenuDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        sideMenu = ENSideMenu(sourceView: self.view, menuViewController: MenuViewController(), menuPosition:.right)
        //sideMenu?.delegate = self //optional
        
        if DeviceType.IS_IPHONE_6 || DeviceType.IS_IPHONE_6P {
            sideMenu?.menuWidth = 220.0
        }
        else
        {
            sideMenu?.menuWidth = 205.0
        }
        
        view.bringSubview(toFront: navigationBar)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - ENSideMenu Delegate
    func sideMenuWillOpen() {
        print("sideMenuWillOpen")
    }
    
    func sideMenuWillClose() {
        print("sideMenuWillClose")
    }
    
    func sideMenuDidClose() {
        print("sideMenuDidClose")
    }
    
    func sideMenuDidOpen() {
        print("sideMenuDidOpen")
    }
}
