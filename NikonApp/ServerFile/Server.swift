//
//  Server.swift
//  TestSwiftApp
//
//  Created by Surbhi on 21/10/16.
//  Copyright © 2016 Neuronimbus. All rights reserved.
//

import UIKit
import Network
import Foundation


class Server: NSObject {
    
    static func getRequestWithURL_PRODUCTCATALOGUE(_ urlString: String, completionHandler:@escaping (_ response: Dictionary <String, AnyObject>) -> Void) {
        
        
        let defaultConfigObject = URLSessionConfiguration.default
        let defaultSession = URLSession(configuration: defaultConfigObject, delegate: nil, delegateQueue: nil)
        var URLString = String()
        URLString = urlString
        URLString = URLString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        var urlRequest = URLRequest(url: URL(string: URLString)!)
        urlRequest.httpMethod = "GET"
        
//        // Handling Basic HTTPS Authorization
        let authString = "nind-api:5m4JDtCLgx6XQWn"
        let authData = authString.data(using: String.Encoding.utf8)
        let authValue = "Basic \(authData!.base64EncodedString(options: NSData.Base64EncodingOptions.lineLength64Characters))"
        urlRequest.setValue(authValue, forHTTPHeaderField: "Authorization")
        
        let task = defaultSession.dataTask(with: urlRequest, completionHandler: { (data, response, error) -> Void in
            
            if error != nil {
                print("Error occurred: "+(error?.localizedDescription)!)
                Constants.appDelegate.stopIndicator()
                return;
            }
            
            do {
                let responseObjc = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! [String: AnyObject]
                completionHandler(responseObjc)
            
            }
            catch {
                print("Error occurred parsing data: \(error)")
                completionHandler([:])
            }
        }) 
        task.resume()
    }
    
    
    static func getRequestWithURL(_ urlString: String, completionHandler:@escaping (_ response: Dictionary <String, AnyObject>) -> Void) {
        
        
        let defaultConfigObject = URLSessionConfiguration.default
        let defaultSession = URLSession(configuration: defaultConfigObject, delegate: nil, delegateQueue: nil)
        var URLString = String()
        URLString = urlString
        URLString = URLString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        var urlRequest = URLRequest(url: URL(string: URLString)!)
        urlRequest.httpMethod = "GET"
        
        // Handling Basic HTTPS Authorization
        let authString = "Neuron:Neuro@4321"
        let authData = authString.data(using: String.Encoding.utf8)
        let authValue = "Basic \(authData!.base64EncodedString(options: NSData.Base64EncodingOptions.lineLength64Characters))"
        urlRequest.setValue(authValue, forHTTPHeaderField: "Authorization")
        
        let task = defaultSession.dataTask(with: urlRequest, completionHandler: { (data, response, error) -> Void in
            
            if error != nil {
                print("Error occurred: "+(error?.localizedDescription)!)
                 Constants.appDelegate.stopIndicator()
                return;
            }
            
            do {
                let responseObjc = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! [String: AnyObject]
                completionHandler(responseObjc)
                
            }
            catch {
                print("Error occurred parsing data: \(error)")
                completionHandler([:])
            }
        }) 
        task.resume()
    }

    
    
    static func getRequestWithURL_Contest(_ urlString: String , completionHandler:@escaping (_ response: Dictionary <String, AnyObject>) -> Void) {
        
        
        let defaultConfigObject = URLSessionConfiguration.default
        let defaultSession = URLSession(configuration: defaultConfigObject, delegate: nil, delegateQueue: nil)
        var URLString = String()
        URLString = urlString
        URLString = URLString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        var urlRequest = URLRequest(url: URL(string: URLString)!)
        urlRequest.httpMethod = "GET"
        
        //        // Handling Basic HTTPS Authorization
        let authString = "nikonapi:nikonApi123"
        let authData = authString.data(using: String.Encoding.utf8)
        let authValue = "Basic \(authData!.base64EncodedString(options: NSData.Base64EncodingOptions.lineLength64Characters))"
        urlRequest.setValue(authValue, forHTTPHeaderField: "Authorization")
        
        let task = defaultSession.dataTask(with: urlRequest, completionHandler: { (data, response, error) -> Void in
            
            if error != nil {
                print("Error occurred: "+(error?.localizedDescription)!)
                Constants.appDelegate.stopIndicator()
                return;
            }
            
            do {
                let responseObjc = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! [String: AnyObject]
                completionHandler( responseObjc)
                
            }
            catch {
                print("Error occurred parsing data: \(error)")
                completionHandler( [:])
            }
        }) 
        task.resume()
    }

    
    static func getRequestWithURLANDParam(_ urlString: String, param: String, completionHandler:@escaping (_ response: Dictionary <String, AnyObject>) -> Void) {
        
        let defaultConfigObject = URLSessionConfiguration.default
        let defaultSession = URLSession(configuration: defaultConfigObject, delegate: nil, delegateQueue: nil)
        
        var urlRequest = URLRequest(url: URL(string: urlString)!)
        urlRequest.httpMethod = "GET"
        
        // Handling Basic HTTPS Authorization
//        let authString = "whirlpoolapps@gmail.com:Neuro@1009#@!"
//        let authData = authString.dataUsingEncoding(NSUTF8StringEncoding)
//        let authValue = "Basic \(authData!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions.Encoding64CharacterLineLength))"
//        urlRequest.setValue(authValue, forHTTPHeaderField: "Authorization")
        
        
//        let urlRequest = NSMutableURLRequest(URL: NSURL(string: urlString)!)
//        urlRequest.setValue( "application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
//        urlRequest.HTTPMethod = "POST"
//        urlRequest.HTTPBody = paramString.dataUsingEncoding(NSUTF8StringEncoding)
//        print(paramString)

        
        let task = defaultSession.dataTask(with: urlRequest, completionHandler: { (data, response, error) -> Void in
            
            if error != nil {
                print("Error occurred: "+(error?.localizedDescription)!)
                Constants.appDelegate.stopIndicator()
                return;
            }
            
            do {
                let responseObjc = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! [String: AnyObject]
                completionHandler( responseObjc)
            }
            catch {
                print("Error occurred parsing data: \(error)")
                completionHandler( [:])
            }
        }) 
        task.resume()
    }
    
    static func postRequestWithURL(_ urlString: String, paramString: String, completionHandler:@escaping (_ response: Dictionary <String, AnyObject>) -> Void) {
        
        let defaultConfigObject = URLSessionConfiguration.default
        let defaultSession = URLSession(configuration: defaultConfigObject, delegate: nil, delegateQueue: nil)

        var urlRequest = URLRequest(url: URL(string: urlString)!)
        
        let headers = [
            "content-type": "application/x-www-form-urlencoded",
            "cache-control": "no-cache",
        ]
        urlRequest.allHTTPHeaderFields = headers
        let authString = "Neuron:Neuro@4321"
        let authData = authString.data(using: String.Encoding.utf8)
        let authValue = "Basic \(authData!.base64EncodedString(options: NSData.Base64EncodingOptions.lineLength64Characters))"
        urlRequest.setValue(authValue, forHTTPHeaderField: "Authorization")

        urlRequest.httpMethod = "POST"
        
        let postData = NSData.init(data: paramString.data(using: String.Encoding.utf8)!) as Data
        
        urlRequest.httpBody = postData//paramString.dataUsingEncoding(NSUTF8StringEncoding)
        
        
        let task = defaultSession.dataTask(with: urlRequest, completionHandler: { (data, response, error) -> Void in
            
            if error != nil {
                print("Error occurred: "+(error?.localizedDescription)!)
                Constants.appDelegate.stopIndicator()
                return;
            }
            
            do {
                let responseObjc = try JSONSerialization.jsonObject(with: data!, options: []) as! [String: AnyObject]
                completionHandler( responseObjc)
            }
            catch {
                print("Error occurred parsing data: \(error)")
                completionHandler( [:])
            }
        }) 
        task.resume()
    }
    
    static func postRequestWithURL_Contest(_ urlString: String, paramString: String, completionHandler:@escaping (_ response: Dictionary <String, AnyObject>) -> Void) {
        
        let defaultConfigObject = URLSessionConfiguration.default
        let defaultSession = URLSession(configuration: defaultConfigObject, delegate: nil, delegateQueue: nil)
        
        var urlRequest = URLRequest(url: URL(string: urlString)!)
        
        let headers = [
            "content-type": "application/x-www-form-urlencoded",
            "cache-control": "no-cache",
            ]
        //        urlRequest.setValue( "application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        
        // Handling Basic HTTPS Authorization
        urlRequest.allHTTPHeaderFields = headers
        let authString = "nikonapi:nikonApi123"
        let authData = authString.data(using: String.Encoding.utf8)
        let authValue = "Basic \(authData!.base64EncodedString(options: NSData.Base64EncodingOptions.lineLength64Characters))"
        urlRequest.setValue(authValue, forHTTPHeaderField: "Authorization")
        
        urlRequest.httpMethod = "POST"
        
        let postData = NSData.init(data: paramString.data(using: String.Encoding.utf8)!) as Data
        
        urlRequest.httpBody = postData//paramString.dataUsingEncoding(NSUTF8StringEncoding)
        
        
        let task = defaultSession.dataTask(with: urlRequest, completionHandler: { (data, response, error) -> Void in
            
            if error != nil {
                print("Error occurred: "+(error?.localizedDescription)!)
                Constants.appDelegate.stopIndicator()
                return;
            }
            
            do {
                let responseObjc = try JSONSerialization.jsonObject(with: data!, options: []) as! [String: AnyObject]
                completionHandler( responseObjc)
            }
            catch {
                print("Error occurred parsing data: \(error)")
                completionHandler( [:])
            }
        }) 
        task.resume()
    }

    
    static func postRequestWithURL_PRODUCTCATALOGUE(_ urlString: String, paramString: String, completionHandler:@escaping (_ response: Dictionary <String, AnyObject>) -> Void) {
        
        let defaultConfigObject = URLSessionConfiguration.default
        let defaultSession = URLSession(configuration: defaultConfigObject, delegate: nil, delegateQueue: nil)
        
        var urlRequest = URLRequest.init(url: URL(string: urlString)!)
        
        let headers = [
            "content-type": "application/x-www-form-urlencoded",
            "cache-control": "no-cache",
            ]
        //        urlRequest.setValue( "application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        
        // Handling Basic HTTPS Authorization
        urlRequest.allHTTPHeaderFields = headers
        let authString = "nind-api:5m4JDtCLgx6XQWn"
        let authData = authString.data(using: String.Encoding.utf8)
        let authValue = "Basic \(authData!.base64EncodedString(options: NSData.Base64EncodingOptions.lineLength64Characters))"
        urlRequest.setValue(authValue, forHTTPHeaderField: "Authorization")
        
        urlRequest.httpMethod = "POST"
        
        let postData = paramString.data(using: String.Encoding.utf8)
        
        urlRequest.httpBody = postData//paramString.dataUsingEncoding(NSUTF8StringEncoding)
        
        
        let task = defaultSession.dataTask(with: urlRequest, completionHandler: { (data, response, error) -> Void in
            
            if error != nil {
                print("Error occurred: "+(error?.localizedDescription)!)
                Constants.appDelegate.stopIndicator()
                return;
            }
            
            do {
                let responseObjc = try JSONSerialization.jsonObject(with: data!, options: []) as! [String: AnyObject]
                completionHandler( responseObjc)
            }
            catch {
                print("Error occurred parsing data: \(error)")
                completionHandler( [:])
            }
        }) 
        task.resume()
    }

    
    static func UploadPicture(_ urlString: String, paramString: String, ImageString: String, completionHandler:@escaping (_ response: Dictionary <String, AnyObject>) -> Void) {
        
        let defaultConfigObject = URLSessionConfiguration.default
        let defaultSession = URLSession(configuration: defaultConfigObject, delegate: nil, delegateQueue: nil)
        
        var urlRequest = URLRequest.init(url: URL(string: urlString)!, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 100.0)
        
        let headers = [
            "content-type": "application/x-www-form-urlencoded",
            "cache-control": "no-cache",
            ]
        urlRequest.allHTTPHeaderFields = headers
        let authString = "Neuron:Neuro@4321"
        let authData = authString.data(using: String.Encoding.utf8)
        let authValue = "Basic \(authData!.base64EncodedString(options: NSData.Base64EncodingOptions.lineLength64Characters))"
        urlRequest.setValue(authValue, forHTTPHeaderField: "Authorization")
        urlRequest.httpMethod = "POST"
        
        var postData = NSData.init(data: paramString.data(using: String.Encoding.utf8)!) as Data
        postData.append(ImageString.data(using: String.Encoding.utf8)!)
        
        
        urlRequest.httpBody = postData //paramString.dataUsingEncoding(NSUTF8StringEncoding)
        
        
        let task = defaultSession.dataTask(with: urlRequest, completionHandler: { (data, response, error) -> Void in
            
            if error != nil {
                print("Error occurred: "+(error?.localizedDescription)!)
                Constants.appDelegate.stopIndicator()
                return;
            }
            
            do {
                let responseObjc = try JSONSerialization.jsonObject(with: data!, options: []) as! [String: AnyObject]
                completionHandler( responseObjc)
            }
            catch {
                print("Error occurred parsing data: \(error)")
                completionHandler( [:])
            }
        }) 
        task.resume()
    }

    static func UploadPictureForContest(_ urlString: String, paramString: String, ImageString: String, completionHandler:@escaping (_ response: Dictionary <String, AnyObject>) -> Void) {
        
        let defaultConfigObject = URLSessionConfiguration.default
        let defaultSession = URLSession(configuration: defaultConfigObject, delegate: nil, delegateQueue: nil)
        
        var urlRequest = URLRequest.init(url: URL(string: urlString)!, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 100.0)
        
        let headers = [
            "content-type": "application/x-www-form-urlencoded",
            "cache-control": "no-cache",
            ]
        urlRequest.allHTTPHeaderFields = headers
        let authString = "nikonapi:nikonApi123"
        let authData = authString.data(using: String.Encoding.utf8)
        let authValue = "Basic \(authData!.base64EncodedString(options: NSData.Base64EncodingOptions.lineLength64Characters))"
        urlRequest.setValue(authValue, forHTTPHeaderField: "Authorization")

        urlRequest.httpMethod = "POST"
        
    
        
        
        let postData = NSData.init(data: paramString.data(using: String.Encoding.utf8)!) as Data
//        postData.appendData(ImageString.dataUsingEncoding(NSUTF8StringEncoding)!)
        
        
        urlRequest.httpBody = postData//paramString.dataUsingEncoding(NSUTF8StringEncoding)
        
        
        let task = defaultSession.dataTask(with: urlRequest, completionHandler: { (data, response, error) -> Void in
            
            if error != nil {
                print("Error occurred: "+(error?.localizedDescription)!)
                Constants.appDelegate.stopIndicator()
                return;
            }
            
            do {
                let responseObjc = try JSONSerialization.jsonObject(with: data!, options: []) as! [String: AnyObject]
                completionHandler( responseObjc)
            }
            catch {
                print("Error occurred parsing data: \(error)")
                completionHandler( [:])
            }
        }) 
        task.resume()
    }


    static func LoginToApp(_ email : String, pass : String , completionHandler:@escaping (_ response: Dictionary <String, AnyObject>) -> Void)
    {
        
        let defaultConfigObject = URLSessionConfiguration.default
        let defaultSession = URLSession(configuration: defaultConfigObject, delegate: nil, delegateQueue: nil)
        var paramString = String()
        
        paramString = "email=\(email)&password=\(pass)&device_token=\(UserDefaults.standard.object(forKey: Constants.USERDEVICETOKEN)!)&device_type=IOS&device_id=\(AppDelegate.getUniqueDeviceIdentifierAsString())&device_version=\(iOSVersion.SYS_VERSION_FLOAT)"
        
        
        let loginMethod = "Login?"
        let urlString = Constants.BASEURL+loginMethod+paramString

        var urlRequest = URLRequest(url: URL(string: urlString)!)
        
        // Handling Basic HTTPS Authorization
        let authString = "Neuron:Neuro@4321"
        let authData = authString.data(using: String.Encoding.utf8)
        let authValue = "Basic \(authData!.base64EncodedString(options: NSData.Base64EncodingOptions.lineLength64Characters))"
        urlRequest.setValue(authValue, forHTTPHeaderField: "Authorization")
        
        urlRequest.httpMethod = "GET"
        
        let task = defaultSession.dataTask(with: urlRequest, completionHandler: { (data, response, error) -> Void in
            
            if error != nil {
                print("Error occurred: "+(error?.localizedDescription)!)
                Constants.appDelegate.stopIndicator()
                return;
            }
                        
            do {
                let responseObjc = try JSONSerialization.jsonObject(with: data!, options: [JSONSerialization.ReadingOptions.allowFragments]) as! [String: AnyObject]
                completionHandler( responseObjc)
            }
            catch {
                print("Error occurred parsing data: \(error)")
                completionHandler( [:])
            }
        }) 
        task.resume()
    }
    
    
    static func RegisterToApp(_ fname: String, lname : String, mobile: String, email : String, pass : String, gender : String ,dob : String , completionHandler:@escaping (_ response: Dictionary <String, AnyObject>) -> Void)
    {
        
        let defaultConfigObject = URLSessionConfiguration.default
        let defaultSession = URLSession(configuration: defaultConfigObject, delegate: nil, delegateQueue: nil)
        var paramString = String()
        paramString = "User_Fname=\(fname)&User_Lname=\(lname)&User_Email=\(email)&User_Pass=\(pass)&User_Dob=\(dob)&user_phone=\(mobile)&User_Gender=\(gender)&device_token=\(UserDefaults.standard.object(forKey: Constants.USERDEVICETOKEN)!)&device_type=IOS&device_id=\(AppDelegate.getUniqueDeviceIdentifierAsString())"
        
        let loginMethod = "UserRegistration"
        let urlString = Constants.BASEURL+loginMethod

        var urlRequest = URLRequest(url: URL(string: urlString)!)
        // Handling Basic HTTPS Authorization
        let authString = "Neuron:Neuro@4321"
        let authData = authString.data(using: String.Encoding.utf8)
        let authValue = "Basic \(authData!.base64EncodedString(options: NSData.Base64EncodingOptions.lineLength64Characters))"
        urlRequest.setValue(authValue, forHTTPHeaderField: "Authorization")

        urlRequest.setValue( "application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        urlRequest.httpMethod = "POST"
        urlRequest.httpBody = paramString.data(using: String.Encoding.utf8)

        let task = defaultSession.dataTask(with: urlRequest, completionHandler: { (data, response, error) -> Void in
            
            if error != nil {
                print("Error occurred: "+(error?.localizedDescription)!)
                Constants.appDelegate.stopIndicator()
                return;
            }
            
            do {
                let responseObjc = try JSONSerialization.jsonObject(with: data!, options: [JSONSerialization.ReadingOptions.allowFragments]) as! [String: AnyObject]
                completionHandler( responseObjc)
            }
            catch {
                print("Error occurred parsing data: \(error)")
                completionHandler( [:])
            }
        }) 
        task.resume()
    }
    
    //MARK:- Social Registration
    static func RegisterToAppWithSocialID(_ fname: String, lname : String, Id: String, email : String, type : String , completionHandler:@escaping (_ response: Dictionary <String, AnyObject>) -> Void)
    {
        
        let defaultConfigObject = URLSessionConfiguration.default
        let defaultSession = URLSession(configuration: defaultConfigObject, delegate: nil, delegateQueue: nil)
        var paramString = String()
        var loginType = type
        var  loginMethod = String()
        
        if loginType == "facebook"
        {
            loginType = "facebookId"
            loginMethod = "SignUpWithFacebook"
        }
        else
        {
            loginType = "GmailId"
            loginMethod = "SignUpWithGmail"
        }
        paramString = "User_Fname=\(fname)&User_Lname=\(lname)&User_Email=\(email)&\(loginType)=\(Id)&device_token=\(UserDefaults.standard.object(forKey: Constants.USERDEVICETOKEN)!)&device_type=IOS&device_id=\(AppDelegate.getUniqueDeviceIdentifierAsString())&device_version=\(iOSVersion.SYS_VERSION_FLOAT)"
        paramString = paramString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        
        let urlString = Constants.BASEURL+loginMethod

        var urlRequest = URLRequest.init(url:  URL(string: urlString)!)
        // Handling Basic HTTPS Authorization
        let authString = "Neuron:Neuro@4321"
        let authData = authString.data(using: String.Encoding.utf8)
        let authValue = "Basic \(authData!.base64EncodedString(options: NSData.Base64EncodingOptions.lineLength64Characters))"
        urlRequest.setValue(authValue, forHTTPHeaderField: "Authorization")

        urlRequest.setValue( "application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        urlRequest.httpMethod = "POST"
        urlRequest.httpBody = paramString.data(using: String.Encoding.utf8)
        
        let task = defaultSession.dataTask(with: urlRequest, completionHandler: { (data, response, error) -> Void in
            
            if error != nil {
                print("Error occurred: "+(error?.localizedDescription)!)
                Constants.appDelegate.stopIndicator()
                return;
            }
            
            do {
                let responseObjc = try JSONSerialization.jsonObject(with: data!, options: [JSONSerialization.ReadingOptions.allowFragments]) as! [String: AnyObject]
                completionHandler(responseObjc)
            }
            catch {
                print("Error occurred parsing data: \(error)")
                completionHandler([:])
            }
        }) 
        task.resume()
    }

    
//    static func RegisterationForApp(name : String, email : String, mobile: String, authType : String , authId : String , pass : String ,profileImage : String, completionHandler:(response: Dictionary <String, AnyObject>) -> Void)
//    {
//        
//        
//        let defaultConfigObject = NSURLSessionConfiguration.defaultSessionConfiguration()
//        let defaultSession = NSURLSession(configuration: defaultConfigObject, delegate: nil, delegateQueue: nil)
//        var paramString = String()
//        
//        let customAllowedSet =  NSCharacterSet(charactersInString:"&").invertedSet
//        let escapedString = profileImage.stringByAddingPercentEncodingWithAllowedCharacters(customAllowedSet)
//        
//        if authType == "1" // FaceBook
//        {
//            paramString = "name=\(name)&email=\(email)&mobile=&password=&auth_type=1&auth_id=\(authId)&device_type=ios&device_id=\(AppDelegate.getUniqueDeviceIdentifierAsString())&device_token=\(AppDelegate.getDeviceToken())&device_model=\(UIDevice.currentDevice().model)&device_version=\(UIDevice.currentDevice().systemVersion)&profile_pic=\(escapedString!)"
//        }
//        else if authType == "2" // Google
//        {
//            paramString = "name=\(name)&email=\(email)&mobile=&password=&auth_type=2&auth_id=\(authId)&device_type=ios&device_id=\(AppDelegate.getUniqueDeviceIdentifierAsString())&device_token=\(AppDelegate.getDeviceToken())&device_model=\(UIDevice.currentDevice().model)&device_version=\(UIDevice.currentDevice().systemVersion)&profile_pic=\(escapedString!)"
//        }
//        else // Normal
//        {
//            let passwordStr = Helper.md5(string: pass)
//            paramString = "name=\(name)&email=\(email)&mobile=\(mobile)&password=\(passwordStr)&auth_type=0&auth_id=&device_type=ios&device_id=\(AppDelegate.getUniqueDeviceIdentifierAsString())&device_token=\(AppDelegate.getDeviceToken())&device_model=\(UIDevice.currentDevice().model)&device_version=\(UIDevice.currentDevice().systemVersion)&profile_pic="
//        }
//        
//        let signUpMethod = "users/create"
//        let urlString = Constants.BASEURL+signUpMethod
//
//        let urlRequest = NSMutableURLRequest(URL: NSURL(string: urlString)!)
//        urlRequest.HTTPMethod = "POST"
//        urlRequest.HTTPBody = paramString.dataUsingEncoding(NSUTF8StringEncoding)
//        
//        // Handling Basic HTTPS Authorization
//        let authString = "whirlpoolapps@gmail.com:Neuro@1009#@!"
//        let authData = authString.dataUsingEncoding(NSUTF8StringEncoding)
//        let authValue = "Basic \(authData!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions.Encoding64CharacterLineLength))"
//        urlRequest.setValue(authValue, forHTTPHeaderField: "Authorization")
//        urlRequest.setValue( "application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
//
//        let task = defaultSession.dataTaskWithRequest(urlRequest) { (let data, let response, let error) -> Void in
//            
//            if error != nil {
//                print("Error occurred: "+(error?.localizedDescription)!)
//                Constants.appDelegate.stopIndicator()
//                return;
//            }
//            
//            do {
//                let responseObjc = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as! [String: AnyObject]
//                completionHandler(response: responseObjc)
//            }
//            catch {
//                print("Error occurred parsing data: \(error)")
//                completionHandler(response: [:])
//            }
//        }
//        task.resume()
//    }

    
    
//    static func getNotificationForApp( completionHandler:(response: Dictionary <String, AnyObject>) -> Void)
//    {
//        let defaultConfigObject = NSURLSessionConfiguration.defaultSessionConfiguration()
//        let defaultSession = NSURLSession(configuration: defaultConfigObject, delegate: nil, delegateQueue: nil)
//        
//        let urlRequest = NSMutableURLRequest(URL: NSURL(string: "https://app.whirlpoolindiawstore.com/api/notifications")!)
//        urlRequest.HTTPMethod = "GET"
//        
//        // Handling Basic HTTPS Authorization
//        let authString = "whirlpoolapps@gmail.com:Neuro@1009#@!"
//        let authData = authString.dataUsingEncoding(NSUTF8StringEncoding)
//        let authValue = "Basic \(authData!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions.Encoding64CharacterLineLength))"
//        urlRequest.setValue(authValue, forHTTPHeaderField: "Authorization")
//        
//        let task = defaultSession.dataTaskWithRequest(urlRequest) { (let data, let response, let error) -> Void in
//            
//            if error != nil {
//                print("Error occurred: "+(error?.localizedDescription)!)
//                Constants.appDelegate.stopIndicator()
//                return;
//            }
//            
//            do {
//                let responseObjc = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers) as! [String: AnyObject]
//                
//                completionHandler(response: responseObjc)
//            }
//            catch {
//                print("Error occurred parsing data: \(error)")
//            }
//        }
//        task.resume()
//    }
    
}



