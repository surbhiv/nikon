//
//  Helper.swift
//  NBA
//
//  Created by HiteshDhawan on 12/04/16.
//  Copyright © 2016 Neuronimbus. All rights reserved.
//

import UIKit


class Helper: NSObject {

    static func isValidEmail(_ emailString: String) -> Bool {
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPredicate.evaluate(with: emailString)
        
    }
    
    static func isValidPassword(_ passwordString: String) -> Bool {
        
        let emailRegEx = "^.*(?=.{6,20})(?=.*\\d)(?=.*[a-zA-Z]).*$" //"^[a-zA-Z]+[0-9]{6,20}$"//"^(?=.*[a-z])(?=.*[0-9])$"  "^.*(?=.{6,20})(?=.*\\d)(?=.*[a-zA-Z]).*$"

        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPredicate.evaluate(with: passwordString)
    }
    
    static func isValidPhoneNumber(_ phoneNumber: String) -> Bool {
        
        if phoneNumber.characters.count == 10 {
            return true
        }
        return false
    }
    
    static func setImageOntextField(_ textField: UITextField){
        
        let imageV = UIImageView(frame:CGRect(x: 0, y: 0, width: 15, height: 15))
        imageV.image = UIImage.init(named: "downArrow")
        textField.rightViewMode = UITextFieldViewMode.always
        textField.rightView = imageV
    }
    
    static func activatetextField(_ lineLabel:UILabel, height : NSLayoutConstraint, textlabel : UILabel ){
        
        lineLabel.backgroundColor = Colors.lineYello
        textlabel.textColor = Colors.textYello
        textlabel.font = UIFont.init(name: Fonts.boldFONTname, size: 12)
        height.constant = 2.0
    }
    
    static func deActivatetextField(_ lineLabel:UILabel, height : NSLayoutConstraint, textlabel : UILabel ){
        
        lineLabel.backgroundColor = Colors.textPlaceHolderGray
        textlabel.textColor = Colors.textPlaceHolderGray
        textlabel.font = UIFont.init(name: Fonts.regFONTname, size: 12)
        height.constant = 1.0
    }
    
    static func activatetextFieldForWorkShop(_ lineLabel:UILabel, height : NSLayoutConstraint, textlabel : UILabel ){
        
        lineLabel.backgroundColor = Colors.lineYello
        textlabel.textColor = Colors.textYello
        textlabel.font = UIFont.init(name: Fonts.boldFONTname, size: 12)
        height.constant = 2.0
    }
    
    static func deActivatetextFieldForWorkShop(_ lineLabel:UILabel, height : NSLayoutConstraint, textlabel : UILabel ){
        
        lineLabel.backgroundColor =  UIColor.black
        textlabel.textColor = Colors.textPlaceHolderGray
        textlabel.font = UIFont.init(name: Fonts.regFONTname, size: 12)
        height.constant = 1.0
    }

    
    static func activatetextFieldOnly(_ lineLabel:UILabel, height : NSLayoutConstraint, textlabel : UILabel, color: UIColor ){
        
//        lineLabel.backgroundColor = Colors.lineYello
        textlabel.textColor = color
        textlabel.font = UIFont.init(name: Fonts.boldFONTname, size: 12)
        height.constant = 2.0
    }
    static func deActivatetextFieldOnly(_ lineLabel:UILabel, height : NSLayoutConstraint, textlabel : UILabel, color: UIColor  ){
        
//        lineLabel.backgroundColor = Colors.textPlaceHolderGray
        textlabel.textColor = color
        textlabel.font = UIFont.init(name: Fonts.regFONTname, size: 12)
        height.constant = 1.0
    }
    
    //MARK:- get string from date
    static func removeHTMLTagFromString(_ htmlString:String) -> String
    {
    let str = htmlString.replacingOccurrences(of: "<[^>]+>", with: "", options: .regularExpression, range: nil)
    //print(str)
        return str
    }
    
    //MARK:- Convert string to dictionary
    static func convertStringToDictionary(_ text: String) -> [String:AnyObject]? {
        if let data = text.data(using: String.Encoding.utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String:AnyObject]
            } catch let error as NSError {
                print(error)
            }
        }
        return nil
    }

    //MARK:- get string from date
    static func sendingDateString(_ dateStr:String) -> String
    {
        let myDate = dateStr
        let dateFormatter = DateFormatter()
         dateFormatter.dateFormat = " dd MMM, yyyy"
        //dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        let date = dateFormatter.date(from: myDate)!
        dateFormatter.dateFormat = "MM/dd/yyyy"//"EEE, MMM dd, yyyy"
        let dateString = dateFormatter.string(from: date)
        
        return dateString
    }
    static func getStringFromDate(_ dateStr:String) -> String
    {
        let myDate = dateStr
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        let date = dateFormatter.date(from: myDate)!
        dateFormatter.dateFormat = "EEE, MMM dd, yyyy"
        let dateString = dateFormatter.string(from: date)
        
       return dateString
    }
   
    
    //MARK:- get DOBstring from date
    static func getDOBStringFromDate(_ dateStr:String) -> String
    {
		if dateStr.count==0{
			return dateStr
		}
        let myDate = dateStr
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
		let date = dateFormatter.date(from: myDate)!
//        dateFormatter.dateFormat = " dd MMM, yyyy"
        dateFormatter.dateFormat = " dd/MM/yyyy"
        let dateString = dateFormatter.string(from: date)
        
        return dateString
    }
    static func getDateFromString(_ dateStr:String) -> String
    {
        let myDate = dateStr
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        let date = dateFormatter.date(from: myDate)!
        dateFormatter.dateFormat = " dd/MM/yyyy"
        let dateString = dateFormatter.string(from: date)
        
        return dateString
    }

    //MARK:- get height from label
    
   static func getHeightForTextView(_ text : String ,label : UITextView , fon : UIFont) -> CGFloat{
        
//        //let wide = ScreenSize.SCREEN_WIDTH - 20
//       // let label:UILabel = UILabel(frame: CGRectMake(0, 0, wide, CGFloat.max))
//        //label.numberOfLines = 0
//        label.font = fon
//        label.text = text
//        label.sizeToFit()
//        return label.frame.height
    let wide = ScreenSize.SCREEN_WIDTH - 40
    let label_new:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: wide, height: CGFloat.greatestFiniteMagnitude))
    label_new.numberOfLines = 0
    label_new.lineBreakMode = NSLineBreakMode.byTruncatingTail
    label_new.font = fon
    label_new.text = text
    label_new.sizeToFit()
    return label_new.frame.height

    }

    static func getHeightForTextView_WithLine(_ text : String ,label : UITextView , fon : UIFont) -> CGFloat{
        
        let wide = ScreenSize.SCREEN_WIDTH - 40
        let label_new:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: wide, height: 100))
        label_new.numberOfLines = 3
        label_new.lineBreakMode = NSLineBreakMode.byTruncatingTail
        label_new.font = fon
        label_new.text = text
        label_new.sizeToFit()
        return label_new.frame.height
    }
    static func getHeightForText(_ text : String , fon : UIFont) -> CGFloat{
        
        let wide = ScreenSize.SCREEN_WIDTH - 40
        let label_new:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: wide, height: CGFloat.greatestFiniteMagnitude))
        label_new.numberOfLines = 0
        label_new.lineBreakMode = NSLineBreakMode.byTruncatingTail
        label_new.font = fon
        label_new.text = text
        label_new.sizeToFit()
        return label_new.frame.height
        
    }
    static func getWidthForText(_ text : String , fon : UIFont) -> CGFloat{
        
        let wide = ScreenSize.SCREEN_WIDTH - 40
        let label_new:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: wide, height: CGFloat.greatestFiniteMagnitude))
        label_new.numberOfLines = 0
        label_new.lineBreakMode = NSLineBreakMode.byTruncatingTail
        label_new.font = fon
        label_new.text = text
        label_new.sizeToFit()
        return label_new.frame.width
        
    }
    

    
    static func loadImageFromUrl(_ urls: String, view: UIImageView, width:CGFloat){
        
        // Create Url from string    
        
        if  urls == ""{
            
            view.image = UIImage.init(named: "no_image.png")
        }
        else
        {
            let url = URL(string: urls)!
            let task = URLSession.shared.dataTask(with: url, completionHandler: { (responseData, responseUrl, error) -> Void in

                if let data = responseData{
                    
                    // execute in UI thread
                    DispatchQueue.main.async(execute: { () -> Void in
                        let img = UIImage(data: data)

                        if img != nil
                        {
                            view.image = img
                        }
                        else
                        {
                            view.image = UIImage.init(named: "profilePlaceTemp")
                        }
                    })
                }
            }) 
            
            // Run task
            task.resume()
        }
    }
    
    
    static func loadImageFromUrlndPlaceHolder(_ urls: String, view: UIImageView, placeholder:String){
        
        // Create Url from string
        
        if  urls == ""{
            
            view.image = UIImage.init(named: "no_image.png")
        }
        else
        {
            let url = URL(string: urls)!
            
            
            let task = URLSession.shared.dataTask(with: url, completionHandler: { (responseData, responseUrl, error) -> Void in
                
                if let data = responseData{
                    
                    // execute in UI thread
                    DispatchQueue.main.async(execute: { () -> Void in
                        let img = UIImage(data: data)
                        
                        if img != nil
                        {
                            view.image = img
                        }
                        else
                        {
                            view.image = UIImage.init(named: placeholder)
                        }
                    })
                }
            }) 
            
            // Run task
            task.resume()
        }
    }
    
    
    static func loadImageFromUrlWithIndicator(_ url: String, view: UIImageView, indic : UIActivityIndicatorView){
        
        // Create Url from string
        let url = URL(string: url)!
        
        // Download task:
        // - sharedSession = global NSURLCache, NSHTTPCookieStorage and NSURLCredentialStorage objects.
        let task = URLSession.shared.dataTask(with: url, completionHandler: { (responseData, responseUrl, error) -> Void in
            // if responseData is not null...
            if let data = responseData{
                
                // execute in UI thread
                DispatchQueue.main.async(execute: { () -> Void in
                    
//                    let img = UIImage(data: data)
                    view.image = UIImage(data: data)
                    indic.stopAnimating()
                })
            }
        }) 
        
        // Run task
        task.resume()
    }

    
    static func loadImageFromUrlWithIndicatorAndSize(_ url: String, view: UIImageView, indic : UIActivityIndicatorView, size: CGSize){
        
        // Create Url from string
        let url = URL(string: url)!
        
        // Download task:
        // - sharedSession = global NSURLCache, NSHTTPCookieStorage and NSURLCredentialStorage objects.
        let task = URLSession.shared.dataTask(with: url, completionHandler: { (responseData, responseUrl, error) -> Void in
            // if responseData is not null...
            if let data = responseData{
                
                // execute in UI thread
                DispatchQueue.main.async(execute: { () -> Void in
                    
                    let img = UIImage(data: data)
                    if img != nil
                    {
                        let newImage = self.resizeImage(img!, newWidth: size.width)
                        view.image = newImage
                    }
                    else
                    {
                        view.image = UIImage.init(named: "no_image.png")

                    }
                    indic.stopAnimating()
                })
            }
        }) 
        
        // Run task
        task.resume()
    }

    
    
//    static func md5(string string: String) -> String {
//        var digest = [UInt8](count: Int(CC_MD5_DIGEST_LENGTH), repeatedValue: 0)
//        if let data = string.dataUsingEncoding(NSUTF8StringEncoding) {
//            CC_MD5(data.bytes, CC_LONG(data.length), &digest)
//        }
//        
//        var digestHex = ""
//        for index in 0..<Int(CC_MD5_DIGEST_LENGTH) {
//            digestHex += String(format: "%02x", digest[index])
//        }
//        
//        return digestHex
//    }
    
    static func resizeImage(_ image: UIImage, newWidth: CGFloat) -> UIImage {
        
        let scale = newWidth / image.size.height
        let newHeight = image.size.width * scale
//        UIGraphicsBeginImageContext(CGSizeMake(newHeight,newWidth))
        UIGraphicsBeginImageContextWithOptions(CGSize(width: newHeight,height: newWidth), true, 0.25)
        image.draw(in: CGRect(x: 0, y: 0, width: newHeight, height: newWidth))
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    static func saveDataInNsDefault(_ object:AnyObject, key:String){
        let dataSave = NSKeyedArchiver.archivedData(withRootObject: object)
        UserDefaults.standard.set(dataSave, forKey: key)
        UserDefaults.standard.synchronize()
    }
    
    static func getDataFromNsDefault(_ key:String) -> Dictionary<String, AnyObject> {
        let dataSave = UserDefaults.standard.object(forKey: key) as! Data
        let dict = NSKeyedUnarchiver.unarchiveObject(with: dataSave) as! Dictionary<String,AnyObject>
        return dict
    }
}
