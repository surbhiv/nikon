//
//  Constant.swift
//  MyRecipeApp
//
//  Created by Surbhi on 24/10/16.
//  Copyright © 2016 Neuronimbus. All rights reserved.
//

import Foundation
import UIKit

struct KEYS {

}


struct ScreenSize {
    static let SCREEN               = UIScreen.main.bounds
    static let SCREEN_WIDTH         = UIScreen.main.bounds.size.width
    static let SCREEN_HEIGHT        = UIScreen.main.bounds.size.height
    static let SCREEN_MAX_LENGTH    = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    static let SCREEN_MIN_LENGTH    = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
}

struct DeviceType {
    static let IS_IPHONE_4_OR_LESS  = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH < 568.0
    static let IS_IPHONE_5          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
    static let IS_IPHONE_6          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
    static let IS_IPHONE_6P         = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
    static let IS_IPAD              = UIDevice.current.userInterfaceIdiom == .pad && ScreenSize.SCREEN_MAX_LENGTH == 1024.0
}

struct iOSVersion {
    static let SYS_VERSION_FLOAT = (UIDevice.current.systemVersion as NSString).floatValue
    static let iOS7 = (iOSVersion.SYS_VERSION_FLOAT < 8.0 && iOSVersion.SYS_VERSION_FLOAT >= 7.0)
    static let iOS8 = (iOSVersion.SYS_VERSION_FLOAT >= 8.0 && iOSVersion.SYS_VERSION_FLOAT < 9.0)
    static let iOS9 = (iOSVersion.SYS_VERSION_FLOAT >= 9.0 && iOSVersion.SYS_VERSION_FLOAT < 10.0)
    static let iOS10 = iOSVersion.SYS_VERSION_FLOAT >= 10.0

}


struct Constants {
    
    static let appDelegate = UIApplication.shared.delegate as! AppDelegate
    static let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main",bundle: nil)
    static let mainStoryboard2: UIStoryboard = UIStoryboard(name: "Main2",bundle: nil)

	 static let BASEURL =  "http://api.nikonschool.in/api/"//STAGING
    
	
	
  //  static let BASEURL = "https://webapi.nikonschool.in/api/"   //LIVe
    static let PRODUCTCATALOUGEBASEURL = "http://api.nikon-asia.com/nind-api/rest/"
    static let IAMNIKONBASEURL = "https://www.iamnikon.in/"
    static let STATE = "status"
    static let MESS =  "message"
    static let FAIL = "failure"
    static let SUCC = "success"
    static let PROFILE = "USER_PROFILE"
    static let USERID = "USERID"
    static let USERHAVEPASSWORD = "USERHAVEPASSWORD"
    //USER DETAIL KEY
    static let USERDETAIL = "USERDETAIL"
    static let USERNAME = "USERNAME"
    static let USEREMAIL = "USEREMAIL"
    static let USERDOB = "USERDOB"
    static let USERDEVICETOKEN = "deviceToken"
    
    //payment key and redirectURL
    //STAGING
//    static  let strPaymentWorkingKey =   "4BD9A059BB89F43C975EC239260FFE74"//"D014EFC977EC0B1DDAECE3DFB71A1FF6"
//    static  let strPaymentMerchantId =   "309"
//    static  let strPaymentAccessCode =   "AVGL00DL82CE50LGEC"//"9YODNX3KLVTGE0OC"
//    static  let strRedirectUrl = "http://staging.nikonschool.in/ResponseGet.aspx"
//    static  let strCancelUrl = "http://staging.nikonschool.in/ResponseGet.aspx"
//    static  let strCurrencyType = "INR";
    
    //LIVE 
    static  let strPaymentWorkingKey =   "D014EFC977EC0B1DDAECE3DFB71A1FF6"//"D014EFC977EC0B1DDAECE3DFB71A1FF6"
    static  let strPaymentMerchantId =   "309"
    static  let strPaymentAccessCode =   "9YODNX3KLVTGE0OC"//"9YODNX3KLVTGE0OC"
    static  let strRedirectUrl = "http://www.nikonschool.in/ResponseGet"
    static  let strCancelUrl = "http://www.nikonschool.in/ResponseGet"
    static  let strCurrencyType = "INR";


}


struct Messages {
    static let internetError = "Please check your internet connection."
    
}

struct Colors {
   
    static let appYello = UIColor.init(red: 253.0/255.0, green: 226.0/255.0, blue: 40.0/255.0, alpha: 1.0)
    static let appPink = UIColor.init(red: 158.0/255.0, green: 14.0/255.0, blue: 64.0/255.0, alpha: 1.0)
    static let textPlaceHolderGray = UIColor.init(red: 154.0/255.0, green: 153.0/255.0, blue: 151.0/255.0, alpha: 1.0)
    static let textYello = UIColor.init(red: 240.0/255.0, green: 210.0/255.0, blue: 37.0/255.0, alpha: 1.0)
    static let indicatorYello = UIColor.init(red: 252.0/255.0, green: 221.0/255.0, blue: 45.0/255.0, alpha: 1.0)
    static let lineYello = UIColor.init(red: 239.0/255.0, green: 209.0/255.0, blue: 34.0/255.0, alpha: 1.0)
    static let termsGray = UIColor.init(red: 154.0/255.0, green: 153.0/255.0, blue: 151.0/255.0, alpha: 1.0)
    static let GrayBG = UIColor.init(red: 154.0/255.0, green: 153.0/255.0, blue: 151.0/255.0, alpha: 1.0)
    static let YelloBG = UIColor.init(red: 154.0/255.0, green: 153.0/255.0, blue: 151.0/255.0, alpha: 1.0)

}


struct Fonts {
    
    static let regFONTname = "OpenSans"
    static let lightFONTname = "OpenSans-Light"
    static let boldFONTname = "OpenSans-Bold"
    static let semiboldFONTname = "OpenSans-Semibold"
    static let extraboldFONTname = "OpenSans-ExtraBold"

    
}
