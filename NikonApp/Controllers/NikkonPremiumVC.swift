//
//  NikkonPremiumVC.swift
//  NikonApp
//
//  Created by Arnab Maity on 10/02/20.
//  Copyright © 2020 Neuronimbus. All rights reserved.
//

import UIKit

class NikkonPremiumVC: UIViewController {

	@IBOutlet weak var nikonPremiumWebView: UIWebView!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		nikonPremiumWebView.delegate = self
		let url = URL(string: "https://www.capturewithnikon.in/nikon-premium-members/")
		let request = URLRequest(url: url!)
		nikonPremiumWebView.loadRequest(request)
	}
}

extension NikkonPremiumVC: UIWebViewDelegate {
	func webViewDidFinishLoad(_ webView: UIWebView) {
		print("Finished loading")
	}
}
