//
//  AddMoreCameraVC.swift
//  NikonApp
//
//  Created by Hitesh Dhawan on 21/04/17.
//  Copyright © 2017 Neuronimbus. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func <= <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l <= r
  default:
    return !(rhs < lhs)
  }
}

protocol sendUpdateOfCameraDelegate {
    func sendUpdate()
}
class AddMoreCameraVC: UIViewController,UITextFieldDelegate,UIPickerViewDelegate,UIPickerViewDataSource {

    
    @IBOutlet weak var selectLenseTextField: UITextField!
    @IBOutlet weak var serialNumberTextField: UITextField!
    
    var sendDelegate : sendUpdateOfCameraDelegate?
    var cameraListArray = [AnyObject]()
    var cameraName = String()
     var picker = UIPickerView()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.picker = UIPickerView.init()
        self.picker.dataSource = self
        self.picker.delegate = self
        self.selectLenseTextField.inputView = self.picker
    }

    @IBAction func okButtonClick(_ sender: AnyObject)
    {
        if selectLenseTextField.text == "" || selectLenseTextField.text == "SELECT CAMERA" || cameraName == ""{
            let alert = UIAlertController.init(title: "Nikon", message: "Please select camera", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                self.dismiss(animated: true, completion: nil)
            }))
            self.present(alert, animated: true, completion: nil)
            
        }
        else if serialNumberTextField.text == ""
        {
            let alert = UIAlertController.init(title: "Nikon", message: "Please enter serial number", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                self.dismiss(animated: true, completion: nil)
            }))
            self.present(alert, animated: true, completion: nil)
            
        }
        else if serialNumberTextField.text?.characters.count <= 5
        {
            let alert = UIAlertController.init(title: "Nikon", message: "Please enter  6 digit serial number", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                self.dismiss(animated: true, completion: nil)
            }))
            self.present(alert, animated: true, completion: nil)
            
        }
        else
        {
            self.addCameraDetail()
        }

        

    }
    
    @IBAction func cancelBtnClick(_ sender: AnyObject)
    {
        self.view .removeFromSuperview()
        self .removeFromParentViewController()
    }
    
    func addCameraDetail()  {
        if Reachability.isConnectedToNetwork()
        {
             var param = "camera_no=\(cameraName)&Sr_no=\(serialNumberTextField.text!)&IPAddress=\(AppDelegate.getUniqueDeviceIdentifierAsString())&UserId=\(UserDefaults.standard.integer(forKey: Constants.USERID))"
                
            
            var method = "AddMoreCamra?"
            method = method.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            param = param.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            Constants.appDelegate.startIndicator()
            Server.postRequestWithURL(Constants.BASEURL+method, paramString: param, completionHandler: { (response) in
                
                if response[Constants.STATE] != nil && response[Constants.STATE] as! String == Constants.SUCC
                {
                    DispatchQueue.main.async
                    {
                        Constants.appDelegate.stopIndicator()
                        
                        var mess = String()
                        if response[Constants.MESS] != nil{
                            mess = response[Constants.MESS] as! String
                        }
                        else{
                            mess = response["Message"] as! String
                        }
                        
                        let alert = UIAlertController.init(title: "Nikon", message: mess, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                            
                            DispatchQueue.main.async
                            {
                                self.view .removeFromSuperview()
                                self .removeFromParentViewController()
                                self.sendDelegate?.sendUpdate()
                            }
                            self.dismiss(animated: true, completion: nil)
                        }))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
                else
                {
                    DispatchQueue.main.async
                    {
                        Constants.appDelegate.stopIndicator()
                        
                        var mess = String()
                        if response[Constants.MESS] != nil{
                            mess = response[Constants.MESS] as! String
                        }
                        else{
                            mess = response["Message"] as! String
                        }
                        
                        let alert = UIAlertController.init(title: "Nikon", message: mess, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                            self.dismiss(animated: true, completion: nil)
                        }))
                        self.present(alert, animated: true, completion: nil)
                        
                    }
                }
                
                
            })
        }
        else
        {
            Constants.appDelegate.stopIndicator()
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
        
        
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return cameraListArray.count
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let value = cameraListArray [row]["CaeraName"] as! String
        return String.init(format: value , locale: nil)
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        let value = cameraListArray [row]["CaeraName"] as! String
        //        if value == "SELECT CAMERA"
        //        {
        //
        //        }
        //        else
        //        {
        //
        //        }
        self.cameraName = value
        self.selectLenseTextField.text = value
    }

    
    //MARK:- TEXTFIELD DELGATE
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == serialNumberTextField
        {
            if (range.location > 9)
            {
                return false
            }
            return true
        }
        return true
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
