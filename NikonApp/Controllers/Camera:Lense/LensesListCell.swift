//
//  LensesListCell.swift
//  NikonApp
//
//  Created by Hitesh Dhawan on 28/03/17.
//  Copyright © 2017 Neuronimbus. All rights reserved.
//

import UIKit

class LensesListCell: UITableViewCell {

    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var lenseNameLbl: UILabel!
    @IBOutlet weak var serialNoLbl: UILabel!
    @IBOutlet weak var viewWarrentyCard: UIButton!
    @IBOutlet weak var statusLbl: UILabel!
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var editbtnLine: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        shadowView.layer.shadowColor = UIColor.black.cgColor
        shadowView.layer.shadowOpacity = 1
        shadowView.layer.shadowOffset = CGSize.zero
        shadowView.layer.shadowRadius = 3

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
