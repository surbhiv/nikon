//
//  EditLenseVC.swift
//  NikonApp
//
//  Created by Hitesh Dhawan on 29/03/17.
//  Copyright © 2017 Neuronimbus. All rights reserved.
//

import UIKit

class EditLenseVC: UIViewController,UIPickerViewDelegate,UIPickerViewDataSource,UITextFieldDelegate,UIImagePickerControllerDelegate,UIPopoverControllerDelegate,UINavigationControllerDelegate {

    //editLense
    @IBOutlet weak var editNikkorUserBtn: UIButton!
    @IBOutlet weak var editOneNikkorUserBtn: UIButton!
    @IBOutlet weak var editRemovePhotoBtn: UIButton!
    @IBOutlet weak var okBtn: UIButton!
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var editSelectLenseTextfield: UITextField!
    @IBOutlet weak var editSerialNoTextfield: UITextField!
    @IBOutlet weak var editWarrantyCardTextField: UITextField!
    @IBOutlet weak var editLenseView: UIView!
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var viewheightconstraint: NSLayoutConstraint!
    
    var lensData = Dictionary<String,AnyObject>()
    var nikkorUserLenseArray = [AnyObject]()
    var oneNikkorUserLenseArray = [AnyObject]()
    var picker = UIPickerView()
    var nikkorUserBool = Bool()
    var haveImage = Bool()
    var imageDataIMG = Data()
    var imageDataToSend = String()
    var userLensId = Int()
    var userLenseDetailId = Int()
    var userLenseSerialNo = String()

    var imagePicker:UIImagePickerController?=UIImagePickerController()


    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        imagePicker?.delegate = self
        self.picker = UIPickerView.init()
        self.picker.dataSource = self
        self.picker.delegate = self
        if lensData["LenseType"]as! String == "1 NIKKOR"
        {
            editOneNikkorUserBtn.isSelected = true
            editNikkorUserBtn.isSelected = false
            nikkorUserBool = false
        }
        if lensData["LenseType"]as! String == "NIKKOR"
        {
            editOneNikkorUserBtn.isSelected = false
            editNikkorUserBtn.isSelected = true
            nikkorUserBool = true
        }
        userLensId = lensData["LenseID"] as! Int
        userLenseDetailId = lensData["LenseDetailID"] as! Int
        userLenseSerialNo = lensData["Lense_SerialNo"] as! String
        editWarrantyCardTextField.text = lensData["LenseWarantyCard"]as? String
        editSelectLenseTextfield.text = lensData["LenseName"]as? String
        editSerialNoTextfield.text = lensData["Lense_SerialNo"]as? String
        editSelectLenseTextfield.inputView = self.picker

    }

    //MARK:- IMAGEPICKERDELEGATE
    func openGallary()
    {
        imagePicker!.allowsEditing = true
        imagePicker!.sourceType = UIImagePickerControllerSourceType.photoLibrary
        present(imagePicker!, animated: true, completion: nil)
    }
    
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)){
            imagePicker!.allowsEditing = true
            imagePicker!.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker!.cameraCaptureMode = .photo
            present(imagePicker!, animated: true, completion: nil)
        }else{
            let alert = UIAlertController(title: "Camera Not Found", message: "This device has no Camera", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style:.default, handler: nil)
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
        }
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {

        let chosenImage = info[UIImagePickerControllerEditedImage] as! UIImage
        haveImage = true
        editWarrantyCardTextField.text = String(describing: info[UIImagePickerControllerReferenceURL] as? URL)
        imageDataIMG = UIImagePNGRepresentation(chosenImage)! as Data
        let imageSize: Int = imageDataIMG.count
        let size = imageSize/1024

        if size > 2048
        {
            DispatchQueue.main.async
            {
                self.perform(#selector(self.imageAlert), with: nil, afterDelay: 0.5)
            }
        }
        else
        {
            
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    func imageAlert()
    {
        let alert = UIAlertController.init(title: "Nikon", message: "Please select image of size less than 2 MB", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
            self.dismiss(animated: true, completion: nil)
        }))
        self.present(alert, animated: true, completion: nil)
        
        
    }
    
    //MARK:- picker delegate
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if nikkorUserBool == true
        {
            
            return self.nikkorUserLenseArray.count
        }
        else
        {
            return self.oneNikkorUserLenseArray.count
        }
        
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        var value = String()
        
        if nikkorUserBool == true
        {
            value = nikkorUserLenseArray [row]["LenseName"] as! String
        }
        else
        {
            value = oneNikkorUserLenseArray [row]["LenseName"] as! String
        }
        return String.init(format: value , locale: nil)
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        var value = Int()
        var textValue = String()
        if nikkorUserBool == true
        {
            value = nikkorUserLenseArray [row]["LenseId"] as! Int
            textValue = nikkorUserLenseArray [row]["LenseName"] as! String
        }
        else
        {
            value = oneNikkorUserLenseArray [row]["LenseId"] as! Int
            textValue = oneNikkorUserLenseArray [row]["LenseName"] as! String
        }
        userLensId = value
        editSelectLenseTextfield.text = textValue
        
    }
    
    @IBAction func nikkorUserBtnClick(_ sender: AnyObject) {
        nikkorUserBool = true
        editNikkorUserBtn.isSelected = true
        editOneNikkorUserBtn.isSelected = false
    }
    @IBAction func oneNikkorUserBtn(_ sender: AnyObject) {
        nikkorUserBool = false
        editNikkorUserBtn.isSelected = false
        editOneNikkorUserBtn.isSelected = true
    }

    @IBAction func removePhotoBtnClick(_ sender: AnyObject) {
        if (editWarrantyCardTextField.text != "")
        {
            editWarrantyCardTextField.text = ""
            haveImage = false
            
        }
    }
    @IBAction func cancelBtnClick(_ sender: AnyObject) {
        self.view .removeFromSuperview()
        self .removeFromParentViewController()
        
    }
    @IBAction func oKBtnClick(_ sender: AnyObject) {
        
        if editSelectLenseTextfield.text == "" || editSelectLenseTextfield.text == "Select Lens"{
            let alert = UIAlertController.init(title: "Nikon", message: "Please select lens", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                self.dismiss(animated: true, completion: nil)
            }))
            self.present(alert, animated: true, completion: nil)
            
        }
        else if editSerialNoTextfield.text == ""
        {
            let alert = UIAlertController.init(title: "Nikon", message: "Please enter serial number", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                self.dismiss(animated: true, completion: nil)
            }))
            self.present(alert, animated: true, completion: nil)
            
        }
        else
        {
            if haveImage == true
            {
                if userLenseSerialNo == editSerialNoTextfield.text
                {
                    self.editcameraLenseWithImage(userLensId, LenseDetailID: userLenseDetailId, Lense_SerialNo: userLenseSerialNo)
                }
                else
                {
                    self.editcameraLenseWithImage(userLensId, LenseDetailID: userLenseDetailId, Lense_SerialNo: editSerialNoTextfield.text!)
                }
            }
            else
            {
                if userLenseSerialNo == editSerialNoTextfield.text
                {
                    self.editcameraLense(userLensId, LenseDetailID: userLenseDetailId, Lense_SerialNo: userLenseSerialNo)
                }
                else
                {
                    self.editcameraLense(userLensId, LenseDetailID: userLenseDetailId, Lense_SerialNo: editSerialNoTextfield.text!)
                }
            
            }
        }
    }
    @IBAction func browseBtnClick(_ sender: AnyObject)
    {
        let optionMenu = UIAlertController(title: nil, message: "Choose Option", preferredStyle: .actionSheet)
        
        // 2
        let cameraAction = UIAlertAction(title: "Open Camera", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.openCamera()
            
        })
        let galleryAction = UIAlertAction(title: "Open Gallery", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.openGallary()
            
        })
        
        //
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            
        })
        
        
        // 4
        optionMenu.addAction(cameraAction)
        optionMenu.addAction(galleryAction)
        optionMenu.addAction(cancelAction)
        
        // 5
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    // MARK: TEXTFIELD DELEGATE
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField .resignFirstResponder()
        //self.Searchtext = self.searchTextfield.text!
        
    }
    //MARK:- Post api
    func editcameraLenseWithImage(_ LenseID : Int, LenseDetailID : Int, Lense_SerialNo : String)
    {
        if Reachability.isConnectedToNetwork()
        {
            //            imageDataToSend = imageDataIMG.base64EncodedStringWithOptions(NSDataBase64EncodingOptions.Encoding76CharacterLineLength)
            imageDataToSend = imageDataIMG.base64EncodedString(options: NSData.Base64EncodingOptions.lineLength64Characters)
            
            var param  = "UserId=\(UserDefaults.standard.integer(forKey: Constants.USERID))&LenseID=\(LenseID)&LenseDetailID=\(LenseDetailID)&Lense_SerialNo=\(Lense_SerialNo)"
            param = param.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            
            let image = "&LenseWarantyCard=\(imageDataToSend)"
            let method = "userlenses"
            
            Constants.appDelegate.startIndicator()
            Server.UploadPicture(Constants.BASEURL+method, paramString: param,ImageString: image, completionHandler: { (response) in
                
                if response[Constants.STATE] != nil && response[Constants.STATE] as! String == Constants.SUCC
                {
                    DispatchQueue.main.async
                    {
                        Constants.appDelegate.stopIndicator()
                        var mess = String()
                        if response[Constants.MESS] != nil{
                            mess = response[Constants.MESS] as! String
                        }
                        else{
                            mess = response["Message"] as! String
                        }

                        let alert = UIAlertController.init(title: "Nikon", message: mess, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                            self.view .removeFromSuperview()
                            self .removeFromParentViewController()
                            self.dismiss(animated: true, completion: nil)
                        }))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
                else
                {
                    DispatchQueue.main.async
                    {
                        Constants.appDelegate.stopIndicator()
                        
                        var mess = String()
                        if response[Constants.MESS] != nil{
                            mess = response[Constants.MESS] as! String
                        }
                        else{
                            mess = response["Message"] as! String
                        }
                        
                        let alert = UIAlertController.init(title: "Nikon", message: mess, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                            self.dismiss(animated: true, completion: nil)
                        }))
                        self.present(alert, animated: true, completion: nil)
                        
                    }
                }
                
                
            })
        }
        else
        {
            Constants.appDelegate.stopIndicator()
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
        
    }
    func editcameraLense(_ LenseID : Int, LenseDetailID : Int, Lense_SerialNo : String)
    {
        if Reachability.isConnectedToNetwork()
        {
            //            imageDataToSend = imageDataIMG.base64EncodedStringWithOptions(NSDataBase64EncodingOptions.Encoding76CharacterLineLength)
            
            var param  = "UserId=\(UserDefaults.standard.integer(forKey: Constants.USERID))&LenseID=\(LenseID)&LenseDetailID=\(LenseDetailID)&Lense_SerialNo=\(Lense_SerialNo)"
            param = param.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            
            let method = "userlenses"
            
            Constants.appDelegate.startIndicator()
            Server.postRequestWithURL(Constants.BASEURL+method, paramString: param, completionHandler: { (response) in
                
                if response[Constants.STATE] != nil && response[Constants.STATE] as! String == Constants.SUCC
                {
                    DispatchQueue.main.async
                    {
                        Constants.appDelegate.stopIndicator()
                        var mess = String()
                        if response[Constants.MESS] != nil{
                            mess = response[Constants.MESS] as! String
                        }
                        else{
                            mess = response["Message"] as! String
                        }

                        let alert = UIAlertController.init(title: "Nikon", message: mess, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                            self.view .removeFromSuperview()
                            self .removeFromParentViewController()
                            self.dismiss(animated: true, completion: nil)
                        }))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
                else
                {
                    DispatchQueue.main.async
                    {
                        Constants.appDelegate.stopIndicator()
                        
                        var mess = String()
                        if response[Constants.MESS] != nil{
                            mess = response[Constants.MESS] as! String
                        }
                        else{
                            mess = response["Message"] as! String
                        }
                        
                        let alert = UIAlertController.init(title: "Nikon", message: mess, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                            self.dismiss(animated: true, completion: nil)
                        }))
                        self.present(alert, animated: true, completion: nil)
                        
                    }
                }
                
                
            })
        }
        else
        {
            Constants.appDelegate.stopIndicator()
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
        
    }



    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
