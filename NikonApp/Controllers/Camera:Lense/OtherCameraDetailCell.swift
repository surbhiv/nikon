//
//  OtherCameraDetailCell.swift
//  NikonApp
//
//  Created by Hitesh Dhawan on 20/03/17.
//  Copyright © 2017 Neuronimbus. All rights reserved.
//

import UIKit

class OtherCameraDetailCell: UITableViewCell {

    @IBOutlet weak var lblCameraName: UILabel!
    @IBOutlet weak var lblSerialNo: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
