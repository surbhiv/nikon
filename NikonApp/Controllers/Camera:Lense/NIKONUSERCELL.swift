//
//  NIKONUSERCELL.swift
//  NikonApp
//
//  Created by Hitesh Dhawan on 17/03/17.
//  Copyright © 2017 Neuronimbus. All rights reserved.
//

import UIKit

class NIKONUSERCELL: UITableViewCell {

    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var yesBtn: UIButton!
    @IBOutlet weak var noBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
