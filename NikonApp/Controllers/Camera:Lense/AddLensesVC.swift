//
//  AddLensesVC.swift
//  NikonApp
//
//  Created by Hitesh Dhawan on 17/03/17.
//  Copyright © 2017 Neuronimbus. All rights reserved.
//

import UIKit

class AddLensesVC: UIViewController,UITableViewDelegate,UITableViewDataSource,UIPickerViewDelegate,UIPickerViewDataSource,UITextFieldDelegate,UIImagePickerControllerDelegate,UIPopoverControllerDelegate,UINavigationControllerDelegate{

    @IBOutlet weak var addLenseTable: UITableView!
    @IBOutlet weak var nikkorUserBtn: UIButton!
    @IBOutlet weak var oneNikkorUserBtn: UIButton!
    @IBOutlet weak var removePhotoBtn: UIButton!
    @IBOutlet weak var viewWarrantyCardBtn: UIButton!
    @IBOutlet weak var addNewBtn: UIButton!
    
    
    @IBOutlet weak var selectLenseTextfield: UITextField!
    @IBOutlet weak var serialNoTextfield: UITextField!
    @IBOutlet weak var warrantyCardTextField: UITextField!
    
        
    
    // VARIABLE
    var editLenceVC = EditLenseVC()
    var lenseListArray = [AnyObject]()
    var userLenseListArray = [AnyObject]()
    var nikkorUserLenseArray = [AnyObject]()
    var oneNikkorUserLenseArray = [AnyObject]()
    var nikkorUserBool = Bool()
    var imageDataIMG = Data()
    var imageDataToSend = String()
    var imagePicker:UIImagePickerController?=UIImagePickerController()
    var picker = UIPickerView()
    var userLensId = Int()
    var userPickedImage = UIImage()
    var viewForWarrenty = UIView()
    
    var tempView = UIView()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imagePicker?.delegate = self
        self.picker = UIPickerView.init()
        self.picker.dataSource = self
        self.picker.delegate = self
        
        addLenseTable.layer.shadowColor = UIColor.black.cgColor
        addLenseTable.layer.shadowOpacity = 1
        addLenseTable.layer.shadowOffset = CGSize.zero
        addLenseTable.layer.shadowRadius = 3
        addLenseTable.layer.masksToBounds = false
        
        selectLenseTextfield.inputView = self.picker
        nikkorUserBool = true
        nikkorUserBtn.isSelected = true
        self.getlenseList()
        self.getUserlenseList()
        // Do any additional setup after loading the view.
    }

    //MARK:- API
    //MARK:- GET CAMERA LIST
    func getlenseList()  {
        if Reachability.isConnectedToNetwork()
        {
            
            
            var method = "userlenses"
            method = method.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            
            Constants.appDelegate.startIndicator()
            Server.getRequestWithURL(Constants.BASEURL+method, completionHandler: { (response) in
                
                
                if response[Constants.STATE] != nil && response[Constants.STATE] as! String == Constants.SUCC
                {
                    //let responseDict = response as! NSDictionary
                    DispatchQueue.main.async
                    {

                        Constants.appDelegate.stopIndicator()
                        self.lenseListArray = response["Lenselist"]as! NSArray as [AnyObject]
                        
                        let nikkorPredicate = NSPredicate(format: "SELF['LenseType']==%@", "NIKKOR")
                        self.nikkorUserLenseArray = (self.lenseListArray as NSArray).filtered(using: nikkorPredicate) as [AnyObject]
                        
                        let oneNikkorPredicate = NSPredicate(format: "SELF['LenseType']==%@", "1 NIKKOR")
                        self.oneNikkorUserLenseArray = (self.lenseListArray as NSArray).filtered(using: oneNikkorPredicate) as [AnyObject]
                        var  dict = Dictionary<String,AnyObject>()
                        dict = ["LenseId":0,"LenseName":"Select Lens","LenseType":"data"] as [String : AnyObject]
                        self.oneNikkorUserLenseArray.insert(dict as AnyObject, at: 0)
                        self.nikkorUserLenseArray.insert(dict as AnyObject, at: 0)
                        

//
                    }
                }
                else
                {
                    DispatchQueue.main.async
                    {
                        Constants.appDelegate.stopIndicator()
                        
                    }
                }
                
                
                
                
            })
        }
        else
        {
            Constants.appDelegate.stopIndicator()
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
        
        
    }
    
    func getUserlenseList()  {
        if Reachability.isConnectedToNetwork()
        {
            
            
            var method = "userlenses/GetUserLense?UserId=\(UserDefaults.standard.integer(forKey: Constants.USERID))"
            method = method.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            
            Constants.appDelegate.startIndicator()
            Server.getRequestWithURL(Constants.BASEURL+method, completionHandler: { (response) in
                
                
                if response[Constants.STATE] != nil && response[Constants.STATE] as! String == Constants.SUCC
                {
                    //let responseDict = response as! NSDictionary
                    DispatchQueue.main.async
                    {
                        Constants.appDelegate.stopIndicator()
                        self.userLenseListArray = response["LenseDetaillist"]as! NSArray as [AnyObject]
                        self.addLenseTable.reloadData()
                    }
                }
                else
                {
                    DispatchQueue.main.async
                    {
                        Constants.appDelegate.stopIndicator()
                        
                    }
                }
                
                
                
                
            })
        }
        else
        {
            Constants.appDelegate.stopIndicator()
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
        
        
    }
    
    func postcameraLenseImage(_ LenseID : Int, LenseDetailID : Int, Lense_SerialNo : String)
    {
        if Reachability.isConnectedToNetwork()
        {
            //            imageDataToSend = imageDataIMG.base64EncodedStringWithOptions(NSDataBase64EncodingOptions.Encoding76CharacterLineLength)
            imageDataToSend = imageDataIMG.base64EncodedString(options: NSData.Base64EncodingOptions.lineLength64Characters)
            
            var param  = "UserId=\(UserDefaults.standard.integer(forKey: Constants.USERID))&LenseID=\(LenseID)&LenseDetailID=\(LenseDetailID)&Lense_SerialNo=\(Lense_SerialNo)"
            param = param.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            
            let image = "&LenseWarantyCard=\(imageDataToSend)"
            let method = "userlenses"
            
            Constants.appDelegate.startIndicator()
            Server.UploadPicture(Constants.BASEURL+method, paramString: param,ImageString: image, completionHandler: { (response) in
                
                if response[Constants.STATE] != nil && response[Constants.STATE] as! String == Constants.SUCC
                {
                    DispatchQueue.main.async
                    {
                        Constants.appDelegate.stopIndicator()
                        var mess = String()
                        if response[Constants.MESS] != nil{
                        mess = response[Constants.MESS] as! String
                        }
                            
                        else{
                            mess = response["Message"] as! String
                        }
                        
                        
                        let alert = UIAlertController.init(title: "Nikon", message: mess, preferredStyle: UIAlertControllerStyle.alert)
                                                alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
//                                                    
                                                    DispatchQueue.main.async
                                                    {
                                                        self.clearAllTextFieldData()
                                                        self.getUserlenseList()
                                                    }
                                                    
                                                    self.dismiss(animated: true, completion: nil)
                                                }))
                                                self.present(alert, animated: true, completion: nil)
                        
                        
                        
                    }
                }
                else
                {
                    DispatchQueue.main.async
                    {
                        Constants.appDelegate.stopIndicator()
                        
                        var mess = String()
                        if response[Constants.MESS] != nil{
                            mess = response[Constants.MESS] as! String
                        }
                        else{
                            mess = response["Message"] as! String
                        }
                        
                        let alert = UIAlertController.init(title: "Nikon", message: mess, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                            self.dismiss(animated: true, completion: nil)
                        }))
                        self.present(alert, animated: true, completion: nil)
                        
                    }
                }
                
                
            })
        }
        else
        {
            Constants.appDelegate.stopIndicator()
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
        
    }
    
    //MARK:- IMAGEPICKERDELEGATE
    func openGallary()
    {
        imagePicker!.allowsEditing = true
        imagePicker!.sourceType = UIImagePickerControllerSourceType.photoLibrary
        present(imagePicker!, animated: true, completion: nil)
    }
    
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)){
            imagePicker!.allowsEditing = true
            imagePicker!.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker!.cameraCaptureMode = .photo
            present(imagePicker!, animated: true, completion: nil)
        }else{
            let alert = UIAlertController(title: "Camera Not Found", message: "This device has no Camera", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style:.default, handler: nil)
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
        }
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {

        let chosenImage = info[UIImagePickerControllerEditedImage] as! UIImage
        userPickedImage = chosenImage
        warrantyCardTextField.text = String(describing: info[UIImagePickerControllerReferenceURL] as? URL)

        imageDataIMG = UIImagePNGRepresentation(chosenImage)! as Data
        let imageSize: Int = imageDataIMG.count
        let size = imageSize/1024
        
       // print(size)
        if size > 2048
        {
            DispatchQueue.main.async
            {
                self.perform(#selector(self.imageAlert), with: nil, afterDelay: 0.5)
            }
        }
        else
        {
            
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    func imageAlert()
    {
        let alert = UIAlertController.init(title: "Nikon", message: "Please select image of size less than 2 MB", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
            self.dismiss(animated: true, completion: nil)
        }))
        self.present(alert, animated: true, completion: nil)
        
        
    }


    //MARK:- Tableview
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userLenseListArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LensesListCell", for: indexPath)as! LensesListCell
        cell.lenseNameLbl.text = userLenseListArray[indexPath.row]["LenseName"]as? String
        cell.serialNoLbl.text = userLenseListArray[indexPath.row]["Lense_SerialNo"]as? String
        let status = userLenseListArray[indexPath.row]["Isapproved"]!!
        cell.editBtn.tag = indexPath.row
        
        if "\(status)" == "1"
        {
            cell.statusLbl.text = "APPROVED"
            cell.statusLbl.textColor = UIColor.init(red: 31.0/255.0, green: 159.0/255.0, blue: 17.0/255.0, alpha: 1)
            cell.editBtn.isHidden = true
            cell.editbtnLine.isHidden = true
        }
        else
        {
            cell.statusLbl.text = "PENDING"
            cell.editBtn.isHidden = false
            cell.editbtnLine.isHidden = false

            cell.statusLbl.textColor = UIColor.init(red: 225.0/255.0, green: 39.0/255.0, blue: 35.0/255.0, alpha: 1)
           
        }
        cell.viewWarrentyCard.tag = indexPath.row
        cell.viewWarrentyCard.addTarget(self, action: #selector(viewWarrentyCardCellBtnClick), for: .touchUpInside)
        
        cell.editBtn.addTarget(self, action: #selector(editLensBtnClick), for: .touchUpInside)
        cell.selectionStyle = .none
        return cell
    }
    
    
    //MARK:- picker delegate
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if nikkorUserBool == true
        {
            
            return self.nikkorUserLenseArray.count
        }
        else
        {
            return self.oneNikkorUserLenseArray.count
        }
        
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        var value = String()
        
        if nikkorUserBool == true
        {
            value = nikkorUserLenseArray [row]["LenseName"] as! String
        }
        else
        {
            value = oneNikkorUserLenseArray [row]["LenseName"] as! String
        }
        return String.init(format: value , locale: nil)
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        var value = Int()
        var textValue = String()
        if nikkorUserBool == true
        {
            value = nikkorUserLenseArray [row]["LenseId"] as! Int
            textValue = nikkorUserLenseArray [row]["LenseName"] as! String
        }
        else
        {
            value = oneNikkorUserLenseArray [row]["LenseId"] as! Int
            textValue = oneNikkorUserLenseArray [row]["LenseName"] as! String
        }
        userLensId = value
        selectLenseTextfield.text = textValue
        
    }
    
    // MARK: TEXTFIELD DELEGATE
    func textFieldDidEndEditing(_ textField: UITextField) {
                textField .resignFirstResponder()
        //self.Searchtext = self.searchTextfield.text!
        
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func nikkorUserBtnClick(_ sender: AnyObject) {
        nikkorUserBool = true
        nikkorUserBtn.isSelected = true
        oneNikkorUserBtn.isSelected = false
    }
    @IBAction func oneNikkorUserBtn(_ sender: AnyObject) {
        nikkorUserBool = false
        nikkorUserBtn.isSelected = false
        oneNikkorUserBtn.isSelected = true
    }
    func clearAllTextFieldData()  {
        selectLenseTextfield.text = ""
        serialNoTextfield.text = ""
        warrantyCardTextField.text = ""
    }
    @IBAction func addNewBtnClick(_ sender: AnyObject)
    {
        
        if selectLenseTextfield.text == ""  || selectLenseTextfield.text == "Select Lens" {
            let alert = UIAlertController.init(title: "Nikon", message: "Please select lens", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                self.dismiss(animated: true, completion: nil)
            }))
            self.present(alert, animated: true, completion: nil)

        }
        else if serialNoTextfield.text == ""
        {
            let alert = UIAlertController.init(title: "Nikon", message: "Please enter serial number", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                self.dismiss(animated: true, completion: nil)
            }))
            self.present(alert, animated: true, completion: nil)

        }
        else if warrantyCardTextField.text == ""
        {
            let alert = UIAlertController.init(title: "Nikon", message: "Please upload photo", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                self.dismiss(animated: true, completion: nil)
            }))
            self.present(alert, animated: true, completion: nil)
            
        }
        else
        {
        
            self.postcameraLenseImage(userLensId, LenseDetailID: 0, Lense_SerialNo: serialNoTextfield.text!)
        }
        
    }
    @IBAction func viewWarrantyCardBtnClick(_ sender: AnyObject)
    {
    
       
        viewForWarrenty = UIView.init(frame: CGRect(x: 0, y: 64, width: self.view.frame.size.width, height: self.view.frame.size.height+64))
        
        
        
        //bg
        let bgImage = UIImageView.init(frame: CGRect(x: 0,y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height+64))
        bgImage.backgroundColor = UIColor.black
        bgImage.alpha = 0.7
        viewForWarrenty.addSubview(bgImage)
        
        let image = UIImageView.init(frame: CGRect(x: 0,y: self.view.frame.size.height/3, width: self.view.frame.size.width, height: self.view.frame.size.height/3))
        image.contentMode = .scaleAspectFit
        image.backgroundColor = UIColor.clear
        image.alpha = 1
        image.image = userPickedImage
        image.contentMode = .scaleAspectFit
        image.backgroundColor = UIColor.clear
        image.alpha = 1

        viewForWarrenty.backgroundColor = UIColor.clear
        
        viewForWarrenty.addSubview(image)
        
        let crossBtn = UIButton.init(frame: CGRect(x: self.view.frame.size.width-35, y: 20, width: 30, height: 30))
        crossBtn.setTitle("x", for: UIControlState())
        crossBtn.setTitleColor(UIColor.white, for: UIControlState())
        crossBtn.titleLabel?.font = UIFont.init(name: "OpenSans", size: 25)
        crossBtn.addTarget(self, action: #selector(crossBtnClick), for: .touchUpInside)
         viewForWarrenty.addSubview(crossBtn)
        
        Constants.appDelegate.window?.addSubview(viewForWarrenty)
    }
    @IBAction func removePhotoBtnClick(_ sender: AnyObject) {
        if (warrantyCardTextField.text != "")
        {
            warrantyCardTextField.text = ""
            
            
        }
    }
    @IBAction func browseBtnClick(_ sender: AnyObject)
    {
        let optionMenu = UIAlertController(title: nil, message: "Choose Option", preferredStyle: .actionSheet)
        
        // 2
        let cameraAction = UIAlertAction(title: "Open Camera", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.openCamera()
            
        })
        let galleryAction = UIAlertAction(title: "Open Gallery", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.openGallary()
            
        })
        
        //
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            
        })
        
        
        // 4
        optionMenu.addAction(cameraAction)
        optionMenu.addAction(galleryAction)
        optionMenu.addAction(cancelAction)
        
        // 5
        self.present(optionMenu, animated: true, completion: nil)
    }
    func crossBtnClick()  {
        viewForWarrenty.removeFromSuperview()
    }
    
    func editLensBtnClick(_ sender : UIButton)  {
//        editLenseView.hidden = false
//        
//        tempView  = UIView.init(frame: CGRectMake(0,  0, self.view.frame.size.width, self.view.frame.size.height))
//        tempView.addSubview(editLenseView)
//        Constants.appDelegate.window?.addSubview(tempView)
        
        let topview = self.parent
        let tag = sender.tag

        var sendDict = Dictionary<String,AnyObject>()
        sendDict = self.userLenseListArray[tag] as! Dictionary
        
//       self.view.bringSubviewToFront(editLenseView)
        editLenceVC  = Constants.mainStoryboard.instantiateViewController(withIdentifier: "EditLenseVC")as! EditLenseVC
        editLenceVC.nikkorUserLenseArray = self.nikkorUserLenseArray
        editLenceVC.oneNikkorUserLenseArray = self.oneNikkorUserLenseArray
        //editLenceVC.userLensId = sendDict["LenseId"] as! Int
        editLenceVC.lensData = sendDict
        
//        topview.addSubview(destVC.view)
        topview!.addChildViewController(editLenceVC)
        editLenceVC.didMove(toParentViewController: topview)
        topview?.view.addSubview(editLenceVC.view)
        
    }
    
    
    func viewWarrentyCardCellBtnClick(_ sender : UIButton)
    {
         let tag = sender.tag
        
        
        viewForWarrenty = UIView.init(frame: CGRect(x: 0, y: 64, width: self.view.frame.size.width, height: self.view.frame.size.height+64))
        
        
        
        //bg
        let bgImage = UIImageView.init(frame: CGRect(x: 0,y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height+64))
        bgImage.backgroundColor = UIColor.black
        bgImage.alpha = 0.7
        viewForWarrenty.addSubview(bgImage)
        
        let image = UIImageView.init(frame: CGRect(x: 0,y: self.view.frame.size.height/3, width: self.view.frame.size.width, height: self.view.frame.size.height/3))
        image.contentMode = .scaleAspectFit
        image.backgroundColor = UIColor.clear
        image.alpha = 1
        image.sd_setImage(with: URL.init(string: userLenseListArray[tag]["LenseWarantyCard"]as! String))
        image.setIndicatorStyle(.whiteLarge)
        image.setShowActivityIndicator(true)
        image.contentMode = .scaleAspectFit
        image.backgroundColor = UIColor.clear
        image.alpha = 1
        
        viewForWarrenty.backgroundColor = UIColor.clear
        
        viewForWarrenty.addSubview(image)
        
        let crossBtn = UIButton.init(frame: CGRect(x: self.view.frame.size.width-35, y: 20, width: 30, height: 30))
        crossBtn.setTitle("x", for: UIControlState())
        crossBtn.setTitleColor(UIColor.white, for: UIControlState())
        crossBtn.titleLabel?.font = UIFont.init(name: "OpenSans", size: 25)
        crossBtn.addTarget(self, action: #selector(crossBtnClick), for: .touchUpInside)
        viewForWarrenty.addSubview(crossBtn)
        
        Constants.appDelegate.window?.addSubview(viewForWarrenty)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
