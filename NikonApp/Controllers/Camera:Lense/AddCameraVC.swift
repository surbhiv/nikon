//
//  AddCameraVC.swift
//  NikonApp
//
//  Created by Hitesh Dhawan on 17/03/17.
//  Copyright © 2017 Neuronimbus. All rights reserved.
//

import UIKit

class AddCameraVC: UIViewController,UITableViewDelegate,UITableViewDataSource,UIPickerViewDelegate,UIPickerViewDataSource,UITextFieldDelegate,sendUpdateOfCameraDelegate {

    @IBOutlet weak var addCameraTable: UITableView!
    
    
    // used to store value
    @IBOutlet weak var selectCameraText: UITextField!
    @IBOutlet weak var serialNoText: UITextField!
    @IBOutlet weak var dateOfPurchaseText: UITextField!
    @IBOutlet weak var purchaseFromText: UITextField!
    @IBOutlet weak var cameraModelText: UITextField!
    
    var cameraListArray = [AnyObject]()
    var otherCameraListArray = [AnyObject]()
    var responseDict = Dictionary<String, AnyObject>()
    var marketedNikonUser = Bool ()
    @IBOutlet weak var addCameraBtn: UIButton!
    @IBOutlet weak var addMoreCameraBtn: UIButton!
    var nikonUser = Bool ()
    
    var titleBtnStr = String()
    var dateOfPurchase = String()
    var serialNumberStr = String()
    var cameraText = String()
    var purchaseFrom = String()
    var otherCamera = String()
    
    var picker = UIPickerView()
    var datePicker = UIDatePicker()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.serialNumberStr = ""
        var  dict = Dictionary<String,AnyObject>()
        dict = ["CaeraName":"SELECT CAMERA" as AnyObject]
        self.cameraListArray.append(dict as AnyObject)
        
        //self.selectCameraText = UITextField()
        self.otherCamera = ""
        
        self.picker = UIPickerView.init()
        self.picker.dataSource = self
        self.picker.delegate = self
        
        addCameraTable.layer.shadowColor = UIColor.black.cgColor
        addCameraTable.layer.shadowOpacity = 1
        addCameraTable.layer.shadowOffset = CGSize.zero
        addCameraTable.layer.shadowRadius = 3
        addCameraTable.layer.masksToBounds = false
        
        datePicker.datePickerMode = .date
        datePicker.maximumDate = Date.init()
        self.getUserCameraList()
        self.getCameraList()
        


        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
    }

    
    //MARK:- API
  //MARK:- GET CAMERA LIST
    func getCameraList()  {
        if Reachability.isConnectedToNetwork()
        {
            
            
            var method = "AddCamra/Getcamra"
            method = method.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            
            Constants.appDelegate.startIndicator()
            Server.getRequestWithURL(Constants.BASEURL+method, completionHandler: { (response) in
                
                
                if response[Constants.STATE] != nil && response[Constants.STATE] as! String == Constants.SUCC
                {
                    //let responseDict = response as! NSDictionary
                    DispatchQueue.main.async
                    {
                        Constants.appDelegate.stopIndicator()
                        let tempArray = response["CamraList"]as! NSArray as [AnyObject]
                        for dict in tempArray
                        {
                            self.cameraListArray.append(dict)
                        }
                    }
                }
                else
                {
                    DispatchQueue.main.async
                    {
                        Constants.appDelegate.stopIndicator()
                        
                    }
                }
                
                
                
                
            })
        }
        else
        {
            Constants.appDelegate.stopIndicator()
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
        
        
    }
    //MARK:- GET USER CAMERA DETAIL AND LIST
    func getUserCameraList()  {
        if Reachability.isConnectedToNetwork()
        {
            
            
            var method = "AddCamra/GetUsercamra?Userid=\(String(UserDefaults.standard.integer(forKey: Constants.USERID)))"
            method = method.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            
            Constants.appDelegate.startIndicator()
            Server.getRequestWithURL(Constants.BASEURL+method, completionHandler: { (response) in
                
                
                if response[Constants.STATE] != nil && response[Constants.STATE] as! String == Constants.SUCC
                {
                    DispatchQueue.main.async
                    {
                        Constants.appDelegate.stopIndicator()
                       self.responseDict = response
                        
                        
                        if self.responseDict["marketed_user"] as! String == "Yes"
                        {
                            self.marketedNikonUser = true
                        }
                        if self.responseDict["nk_user"] as! String == "Yes"
                        {
                            self.nikonUser = true
                        }
                        
                        
//                        if (self.responseDict["marketed_user"] as! String == "No") && (self.responseDict["nk_user"] as! String == "No") && (response["OtherCamera"]as! String == ""){
//                            
//                        }
//                        else if (self.responseDict["marketed_user"] as! String == "YES") && (self.responseDict["nk_user"] as! String == "YES") && (response["OtherCamera"]as! String == ""){
//                            
//                        }
//                        else if (self.responseDict["marketed_user"] as! String == "YES") && (self.responseDict["nk_user"] as! String == "NO") && (response["OtherCamera"]as! String == ""){
//                            
//                        }
//                        else if (self.responseDict["marketed_user"] as! String == "NO") && (self.responseDict["nk_user"] as! String == "YES") && (response["OtherCamera"]as! String == ""){
//                            
//                        }
                        
                        if (self.responseDict["marketed_user"] as! String == "No") && (self.responseDict["nk_user"] as! String == "No") && (response["OtherCamera"]as! String == "")
                        {
                            //self.addCameraBtn.setTitle("ADD", forState: .Normal)
                            self.titleBtnStr = "ADD"
                            
                        }
                        else
                        {
                            self.titleBtnStr = "UPDATE"
                           // self.addCameraBtn.setTitle("UPDATE", forState: .Normal)

                        }
                        
                        if (self.responseDict["marketed_user"] as! String == "Yes") && (self.responseDict["nk_user"] as! String == "Yes")
                        {
                            self.addMoreCameraBtn.isHidden = false
                        }
                        else
                        {
                            self.addMoreCameraBtn.isHidden = true
                            
                        }
                        
                        self.cameraText = response["camera_no"]as! String
                        self.serialNumberStr = response["sr_no"]as! String
                        if(response["DOP"]as! String != "")
                        {
                            
                            let dateFormatter = DateFormatter()
                            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
                            let newdate = dateFormatter.date(from: response["DOP"]as! String)
                            dateFormatter.dateFormat = " dd MMM, yyyy"
                            //let dateString = dateFormatter.stringFromDate(newdate!)
                            dateFormatter.dateFormat = "MM/dd/yyyy"
                            let SendingdateString = dateFormatter.string(from: newdate!)
                            self.dateOfPurchase = SendingdateString
                          
                        //self.dateOfPurchase = Helper.getStringFromDate(response["DOP"]as! String)
                        }
                        self.otherCameraListArray = response["OthercamraList"]as! NSArray as [AnyObject]
                         self.otherCamera = response["OtherCamera"]as! String

                        self.purchaseFrom = response["StorePurchasefrom"]as! String
                        
                        self.addCameraTable.reloadData()
                    }
                }
                else
                {
                    DispatchQueue.main.async
                    {
                        Constants.appDelegate.stopIndicator()
                        
                    }
                }
                
                
                
                
            })
        }
        else
        {
            Constants.appDelegate.stopIndicator()
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
        
        
    }
    
    //MARK:- VALIDATE SERIAL NUMBER OF CAMERA
    func validateSerialNo()  {
        if Reachability.isConnectedToNetwork()
        {
            
            
            var method = "AddCamra/Validatecamra?camra_name=\(self.cameraModelText.text)&sr_no=\(self.serialNumberStr)"
            method = method.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            
            Constants.appDelegate.startIndicator()
            Server.getRequestWithURL(Constants.BASEURL+method, completionHandler: { (response) in
                
                
                if response[Constants.STATE] != nil && response[Constants.STATE] as! String == Constants.SUCC
                {
                    DispatchQueue.main.async
                    {
                        Constants.appDelegate.stopIndicator()
                        
                        var mess = String()
                        if response[Constants.MESS] != nil{
                            mess = response[Constants.MESS] as! String
                        }
                        else{
                            mess = response["Message"] as! String
                        }
                        
                        let alert = UIAlertController.init(title: "Nikon", message: mess, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                            self.dismiss(animated: true, completion: nil)
                        }))
                        self.present(alert, animated: true, completion: nil)
                        

                        
                    }
                }
                else
                {
                    DispatchQueue.main.async
                    {
                        Constants.appDelegate.stopIndicator()
                        
                        var mess = String()
                        if response[Constants.MESS] != nil{
                            mess = response[Constants.MESS] as! String
                        }
                        else{
                            mess = response["Message"] as! String
                        }
                        
                        let alert = UIAlertController.init(title: "Nikon", message: mess, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                            self.dismiss(animated: true, completion: nil)
                        }))
                        self.present(alert, animated: true, completion: nil)
                        

                        
                    }
                }
                
                
                
                
            })
        }
        else
        {
            Constants.appDelegate.stopIndicator()
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
        
        
    }
    //MARK:- ADD CAMERA DETAIL
    func addCameraDetail()  {
        if Reachability.isConnectedToNetwork()
        {
            var nikonUserStr = String()
            var marketedUserStr = String()
            var param = String()
            if nikonUser == true
            {
                nikonUserStr = "Yes"
                
            }
            else
            {
                nikonUserStr = "No"
            }
            if marketedNikonUser == true && nikonUser == true
            {
                marketedUserStr = "Yes"
                param = "nk_user=\(nikonUserStr)&marketed_user=\(marketedUserStr)&camera_no=\(self.cameraText)&sr_no=\(self.serialNumberStr)&OtherCamera=&Userid=\(UserDefaults.standard.integer(forKey: Constants.USERID))&StorePurchasefrom=\(self.purchaseFrom)&DOP=\(dateOfPurchase)"
                
            }
            else
            {
                marketedUserStr = "No"
                param = "nk_user=\(nikonUserStr)&marketed_user=\(marketedUserStr)&camera_no=&sr_no=&OtherCamera=\(self.cameraModelText.text!)&Userid=\(UserDefaults.standard.integer(forKey: Constants.USERID))&StorePurchasefrom=&DOP="
            }
            
            var method = "AddCamra?"
            method = method.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            param = param.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            Constants.appDelegate.startIndicator()
            Server.postRequestWithURL(Constants.BASEURL+method, paramString: param, completionHandler: { (response) in
                
                if response[Constants.STATE] != nil && response[Constants.STATE] as! String == Constants.SUCC
                {
                    DispatchQueue.main.async
                    {
                        Constants.appDelegate.stopIndicator()
                        
                        var mess = String()
                        if response[Constants.MESS] != nil{
                            mess = response[Constants.MESS] as! String
                        }
                        else{
                            mess = response["Message"] as! String
                        }
                        
                        let alert = UIAlertController.init(title: "Nikon", message: mess, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                            
                            DispatchQueue.main.async
                            {
                                self.getUserCameraList()
                            }
                            self.dismiss(animated: true, completion: nil)
                        }))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
                else
                {
                    DispatchQueue.main.async
                    {
                        Constants.appDelegate.stopIndicator()
                        
                        var mess = String()
                        if response[Constants.MESS] != nil{
                            mess = response[Constants.MESS] as! String
                        }
                        else{
                            mess = response["Message"] as! String
                        }
                        
                        let alert = UIAlertController.init(title: "Nikon", message: mess, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                            self.dismiss(animated: true, completion: nil)
                        }))
                        self.present(alert, animated: true, completion: nil)
                        
                    }
                }
                
                
            })
        }
        else
        {
            Constants.appDelegate.stopIndicator()
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
        
        
    }

    //MARK:- DATE PICKER DATE CHANGED
    func datePickerValueChanged(_ sender:UIDatePicker) {
        
        let dateFormatter = DateFormatter()
        // dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        //let date = dateFormatter.dateFromString(String(sender.date))!
        dateFormatter.dateFormat = " dd MMM, yyyy"
        //let dateString = dateFormatter.stringFromDate(sender.date)
        
        //        dateFormatter.dateStyle = NSDateFormatterStyle.MediumStyle
        //        dateFormatter.timeStyle = NSDateFormatterStyle.NoStyle
        //       dobTextField.text = dateFormatter.stringFromDate(sender.date)
        dateFormatter.dateFormat = "MM/dd/yyyy"
        let SendingdateString = dateFormatter.string(from: sender.date)
        dateOfPurchase = SendingdateString
        dateOfPurchaseText.text = SendingdateString
        
    }
    
    @IBAction func addCameraBtnClicked(_ sender: AnyObject)
    {
        if nikonUser == false
        {
            if cameraModelText.text == ""
            {
                let alert = UIAlertController.init(title: "Nikon", message: "Please select camera model", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                    self.dismiss(animated: true, completion: nil)
                }))
                self.present(alert, animated: true, completion: nil)

            }
            else
            {
              self.addCameraDetail()
            }
        }
        else
        {
            if marketedNikonUser == false
            {
                if cameraModelText.text == ""
                {
                    let alert = UIAlertController.init(title: "Nikon", message: "Please select camera model", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                        self.dismiss(animated: true, completion: nil)
                    }))
                    self.present(alert, animated: true, completion: nil)

                }
                else
                {
                    self.addCameraDetail()
                }
            }
            else
            {
                if selectCameraText.text == "SELECT CAMERA" || selectCameraText.text == ""
                {
                    let alert = UIAlertController.init(title: "Nikon", message: "Please select camera", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                        self.dismiss(animated: true, completion: nil)
                    }))
                    self.present(alert, animated: true, completion: nil)

                }
                else if serialNoText.text == ""
                {
                    let alert = UIAlertController.init(title: "Nikon", message: "Please enter serial number", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                        self.dismiss(animated: true, completion: nil)
                    }))
                    self.present(alert, animated: true, completion: nil)

                }
                else if self.purchaseFromText.text == ""
                {
                    let alert = UIAlertController.init(title: "Nikon", message: "Please enter purchase from", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                        self.dismiss(animated: true, completion: nil)
                    }))
                    self.present(alert, animated: true, completion: nil)
                }
                else
                {
                    self.addCameraDetail()
                }
            }
        }
        
    }
    
    @IBAction func addMoreCameraBtnClicked(_ sender: AnyObject)
    {
        
        let topview = self.parent
       
        let addmoreCameraVC  = Constants.mainStoryboard.instantiateViewController(withIdentifier: "AddMoreCameraVC")as! AddMoreCameraVC
        addmoreCameraVC.cameraListArray = self.cameraListArray
       addmoreCameraVC.sendDelegate = self
        
        //        topview.addSubview(destVC.view)
        topview!.addChildViewController(addmoreCameraVC)
        addmoreCameraVC.didMove(toParentViewController: topview)
        topview?.view.addSubview(addmoreCameraVC.view)
//
        
    }
    //MARK:- add more camera delegate
    func sendUpdate()
    {
        self.getUserCameraList()
    }
    
    //MARK:- TABLEVIEW
    func numberOfSections(in tableView: UITableView) -> Int
    {
        if nikonUser == true
        {
            if self.otherCameraListArray.count == 0
            {
            return 4
            }
            else
            {
                return 5
            }
        }
        else
        {
            if self.otherCameraListArray.count == 0
            {
                return 3
            }
            else
            {
                return 4
            }
        }
       
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if section == 3
        {
            if nikonUser == false
            {
            return 1+self.otherCameraListArray.count
            }
            else
            {
                return 1
            }
        }
        if section == 4
        {
            return 1+self.otherCameraListArray.count
 
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "NIKONUSERCELL", for: indexPath)as! NIKONUSERCELL
            cell.questionLabel.text = "ARE YOU A NIKON D-SLR/NIKON 1 CAMERA USER?"
            if nikonUser == true
            {
                cell.yesBtn.isSelected = true
                cell.noBtn.isSelected = false
            }
            else
            {
                cell.yesBtn.isSelected = false
                cell.noBtn.isSelected = true
            }
            cell.yesBtn.addTarget(self, action: #selector(nikonUserYesClicked), for: .touchUpInside)
            cell.noBtn.addTarget(self, action: #selector(nikonUserNoClicked), for: .touchUpInside)
            
            tableView.estimatedRowHeight = 50
            tableView.rowHeight = UITableViewAutomaticDimension
            cell.selectionStyle = .none
            return cell
        }
        else if indexPath.section == 1
        {
            if nikonUser == true
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "NIKONUSERCELL", for: indexPath)as! NIKONUSERCELL
                cell.questionLabel.text = "I HAVE BOUGHT THE NIKON D-SLR/NIKON 1 CAMERA MARKETED BY NIKON INDIA."
                if marketedNikonUser == true
                {
                    cell.yesBtn.isSelected = true
                    cell.noBtn.isSelected = false
                }
                else
                {
                    cell.yesBtn.isSelected = false
                    cell.noBtn.isSelected = true
                }
                cell.yesBtn.addTarget(self, action: #selector(marketedNikonUserYesClicked), for: .touchUpInside)
                cell.noBtn.addTarget(self, action: #selector(marketedNikonUserNoClicked), for: .touchUpInside)

                
                tableView.estimatedRowHeight = 50
                tableView.rowHeight = UITableViewAutomaticDimension
                cell.selectionStyle = .none
                
                return cell
            }
            else
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "CAMERAMODELCELL", for: indexPath)as! CAMERAMODELCELL
                
                self.cameraModelText = cell.cameraModelTextField
                //self.cameraModelText.delegate = self
                self.cameraModelText.text = self.otherCamera
                tableView.estimatedRowHeight = 50
                tableView.rowHeight = UITableViewAutomaticDimension
                cell.selectionStyle = .none
                return cell
            }

        }
        else if indexPath.section == 2
        {
            if nikonUser == true
            {
                if marketedNikonUser == true
                {
                let cell = tableView.dequeueReusableCell(withIdentifier: "NIKONUSERCAMERADETAILCELL", for: indexPath)as! NIKONUSERCAMERADETAILCELL
                
                self.selectCameraText = cell.selectCameraTextField
                self.selectCameraText.inputView = self.picker
                self.serialNoText = cell.serialNoTextField
                self.dateOfPurchaseText = cell.dateOfPurchaseTextField
                self.purchaseFromText = cell.purchaseFromTextField
                
                self.selectCameraText.delegate = self
                self.serialNoText.delegate = self
                self.dateOfPurchaseText.delegate = self
                //self.purchaseFromText.delegate = self
                
                self.selectCameraText.text = self.cameraText
                self.serialNoText.text = self.serialNumberStr
                self.purchaseFromText.text = self.purchaseFrom
                self.dateOfPurchaseText.text = self.dateOfPurchase
                
                self.dateOfPurchaseText.inputView = datePicker
                datePicker.addTarget(self, action: #selector(datePickerValueChanged), for: UIControlEvents.valueChanged)
                
                tableView.estimatedRowHeight = 140
                tableView.rowHeight = UITableViewAutomaticDimension
                cell.selectionStyle = .none
                return cell
                }
                else
                {
                let cell = tableView.dequeueReusableCell(withIdentifier: "CAMERAMODELCELL", for: indexPath)as! CAMERAMODELCELL
                
                self.cameraModelText = cell.cameraModelTextField
                //self.cameraModelText.delegate = self
                self.cameraModelText.text = self.otherCamera
                
                tableView.estimatedRowHeight = 50
                tableView.rowHeight = UITableViewAutomaticDimension
                cell.selectionStyle = .none
                return cell
                }
            }
            else
            {
                 let cell = tableView.dequeueReusableCell(withIdentifier: "AddButtonCell", for: indexPath)as!AddButtonCell
                cell.addBtn.setTitle(self.titleBtnStr, for: UIControlState())
                return cell
            }
        }
        else if indexPath.section == 3
        {
            if nikonUser == false
            {
                if indexPath.row == 0
                {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "headerCellForOtherCamera", for: indexPath)
                    return cell
                    
                }
                else
                {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "OtherCameraDetailCell", for: indexPath)as! OtherCameraDetailCell
                    cell.lblCameraName.text = self.otherCameraListArray[indexPath.row-1]["camera_no"]as? String
                    cell.lblSerialNo.text = self.otherCameraListArray[indexPath.row-1]["Sr_no"]as? String
                    tableView.estimatedRowHeight = 40
                    tableView.rowHeight = UITableViewAutomaticDimension
                    cell.selectionStyle = .none
                    return cell
                    
                }
 
            }
            else
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "AddButtonCell", for: indexPath)as!AddButtonCell
                cell.addBtn.setTitle(self.titleBtnStr, for: UIControlState())
                return cell
            }
        }
        else
        {
            if indexPath.row == 0
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "headerCellForOtherCamera", for: indexPath)
                return cell
                
            }
            else
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "OtherCameraDetailCell", for: indexPath)as! OtherCameraDetailCell
                cell.lblCameraName.text = self.otherCameraListArray[indexPath.row-1]["camera_no"]as? String
                cell.lblSerialNo.text = self.otherCameraListArray[indexPath.row-1]["Sr_no"]as? String
                tableView.estimatedRowHeight = 40
                tableView.rowHeight = UITableViewAutomaticDimension
                cell.selectionStyle = .none
                return cell
                
            }
        }
    }
    //MARK:- picker delegate
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return cameraListArray.count
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let value = cameraListArray [row]["CaeraName"] as! String
        return String.init(format: value , locale: nil)
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        let value = cameraListArray [row]["CaeraName"] as! String
//        if value == "SELECT CAMERA"
//        {
//           
//        }
//        else
//        {
//        
//        }
        self.cameraText = value
        self.selectCameraText.text = value
    }
    
    // MARK: TEXTFIELD DELEGATE
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == self.purchaseFromText
        {
            self.purchaseFrom = textField.text!
        }
        textField .resignFirstResponder()
        //self.Searchtext = self.searchTextfield.text!
        
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == serialNoText
        {
//
            
            let  char = string.cString(using: String.Encoding.utf8)!
            let isBackSpace = strcmp(char, "\\b")
            self.serialNumberStr = "\(serialNumberStr)\(string)"
            if (isBackSpace == -92) {

                let newString = String(self.serialNumberStr.characters.dropLast())
                self.serialNumberStr = newString
            }
                
            if string == " "
            {
                return false
            }
            else
            {
               
                if range.location > 5
                {
                    
                    self.validateSerialNo()
                
                }
                
                return true
            }
        }
        return true
    }

    //MARK:- ACTION
    
    func nikonUserYesClicked(_ sender: UIButton)
    {
        nikonUser = true
        addCameraTable.reloadData()
        
    }
    func nikonUserNoClicked(_ sender: UIButton)
    {
        nikonUser = false
        addCameraTable.reloadData()
    }
    func marketedNikonUserYesClicked(_ sender: UIButton)
    {
        marketedNikonUser = true
        addCameraTable.reloadData()
        
    }
    func marketedNikonUserNoClicked(_ sender: UIButton)
    {
        marketedNikonUser = false
        addCameraTable.reloadData()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
