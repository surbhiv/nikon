//
//  NIKONUSERCAMERADETAILCELL.swift
//  NikonApp
//
//  Created by Hitesh Dhawan on 17/03/17.
//  Copyright © 2017 Neuronimbus. All rights reserved.
//

import UIKit

class NIKONUSERCAMERADETAILCELL: UITableViewCell {

    @IBOutlet weak var selectCameraTextField: UITextField!
    @IBOutlet weak var serialNoTextField: UITextField!
    @IBOutlet weak var dateOfPurchaseTextField: UITextField!
    @IBOutlet weak var purchaseFromTextField: UITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
