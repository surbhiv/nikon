//
//  Dashboard.swift
//  NikonApp
//
//  Created by Surbhi on 16/12/16.
//  Copyright © 2016 Neuronimbus. All rights reserved.
//

import UIKit

class Dashboard: UIViewController ,UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{

    @IBOutlet weak var menuColl: UICollectionView!
    @IBOutlet weak var collectionheight: NSLayoutConstraint!
	
	@IBOutlet weak var bottomCollection: UICollectionView!
	

    var serviceButton: UIButton!
    var sideMenuObj = MenuViewController()

	let x:CGFloat = 2
	
	var bottomBarArray = ["Offers","Service & Support","Store Locator","Loyalty","Gallery"]
	
	var bottomArrayImag:[UIImage] = [UIImage(named: "Offers")!,UIImage(named: "Service & Support")!,UIImage(named: "Store Locator")!,UIImage(named: "Loyalty")!,UIImage(named: "Gallery")!]
	
    override func viewDidLoad() {
        super.viewDidLoad()
		
        sideMenuObj = sideMenuController()!.sideMenu!.menuViewController as! MenuViewController

        self.navigationController?.isNavigationBarHidden = false
        self.setNavigationBar()
		self.bottomCollection.reloadData()
        if (UserDefaults.standard.integer(forKey: Constants.USERID) != 0) {
        self.getUserProfile()
        }
        
        DispatchQueue.main.async
        {
            self.hideSideMenuView()
            //self.view.bringSubviewToFront(self.menuColl)
            self.menuColl.reloadData()
			
        }
    }

    func OpenSideMenu(_ sender: AnyObject){
        
        self.view.endEditing(true)
        toggleSideMenuView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //MARK:- Side Menu
    func setNavigationBar(){
        let navigationViewFrame = self.navigationController?.navigationBar.frame
        let navigationView = UIView.init(frame: navigationViewFrame!)
        navigationView.backgroundColor = UIColor.white
        let menuButton = UIButton.init(type: .custom)
        menuButton.frame = CGRect(x: ScreenSize.SCREEN_WIDTH - 45, y: 12.5, width: 25, height: 25)
        menuButton.setImage(UIImage.init(named: "sideMenu_Icon"), for: UIControlState())
        menuButton.addTarget(self, action: #selector(OpenSideMenu), for: .touchUpInside)
        navigationView.addSubview(menuButton)

        let logoImage = UIImageView.init(frame: CGRect(x: 0, y: 6, width: 33, height: 33))
        logoImage.image = UIImage.init(named: "nikon_login_logo")
        navigationView.addSubview(logoImage)
        
//        let anniversaryImage = UIImageView.init(frame: CGRectMake(45, 5, 48, 40))
//        anniversaryImage.image = UIImage.init(named: "anniversaryImage")
//        navigationView.addSubview(anniversaryImage)

        self.navigationItem.titleView = navigationView
    }

    //MARK:- COllection View
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
		
		if collectionView == bottomCollection{
			
			return CGSize(width: ((collectionView.bounds.size.width)-(x*4))/5, height: 70)

		}
        let heigh = ((collectionView.bounds.size.width - 8)/3)
        let collHeight = (heigh * 2) + 50
        collectionheight.constant = collHeight
        return CGSize(width: (collectionView.bounds.size.width - 8)/3, height: heigh)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		
		if collectionView == menuColl{
			return 6
		}
		else{
			 return bottomBarArray.count
		}
		
    }
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
		return x
	}
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		if collectionView == menuColl{
			let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DashboardCell", for: indexPath) as! DashboardCell
			
			let typeImage = cell.contentView.viewWithTag(1011) as! UIImageView
			let nameLabel = cell.contentView.viewWithTag(1012) as! UILabel
			nameLabel.numberOfLines = 0
			typeImage.image = nil
			typeImage.contentMode = UIViewContentMode.scaleToFill
			
			if indexPath.item == 0 {
				nameLabel.text = "PRODUCT \nCATALOGUE"
			     cell.nameLabelHeight.constant = 33
				typeImage.image = UIImage.init(named: "productCatalogue_icon")
			}
			else if indexPath.item == 1 {
				nameLabel.text = "WORKSHOP"
				typeImage.backgroundColor = UIColor.clear
				  cell.nameLabelHeight.constant = 33
				typeImage.image = UIImage.init(named: "workshop_icon")
			}
			else if indexPath.item == 2 {
				nameLabel.text = "CONTEST"
				  cell.nameLabelHeight.constant = 33
				typeImage.image = UIImage.init(named: "contest_Icon")
			}
			else if indexPath.item == 3 {
				nameLabel.text = ""
				  cell.nameLabelHeight.constant = 0
				typeImage.image = UIImage.init(named: "Capture-with-Nikon")
			}
			else if indexPath.item == 4 {
				nameLabel.text = ""
				  cell.nameLabelHeight.constant = 0
				typeImage.image = UIImage.init(named: "WFA")
			}
			else {
				nameLabel.text = ""
				  cell.nameLabelHeight.constant = 0
				typeImage.image = UIImage.init(named: "Nikon logo")
			}
			return cell
		}
		else {
			let cell1 = collectionView.dequeueReusableCell(withReuseIdentifier: "BottomBarCell", for: indexPath) as! BottomBarCell
			cell1.bottomLbl.text = bottomBarArray[indexPath.item]
			cell1.bottomImg.image = bottomArrayImag[indexPath.item]
			return cell1
		}
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
		if collectionView == menuColl{
			if indexPath.item == 0 {
				
				let destViewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "ProductCatalougeVC")
				self.navigationController?.pushViewController(destViewController, animated: true)
				
			}
			else if indexPath.row == 1{
				
				let destViewController = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "WorkshopVC")
				sideMenuObj.lastSelectedMenuItem = sideMenuObj.selectedMenuItem
				sideMenuObj.selectedMenuItem = 4
				self.navigationController?.pushViewController(destViewController, animated: true)
			}
				
//			else if indexPath.row == 2{
//				let vc = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "ContestChallengesListVC") as! ContestChallengesListVC
//				self.navigationController?.pushViewController(vc, animated: true)
////				let destViewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "LoyaltyVC")
////				sideMenuObj.lastSelectedMenuItem = sideMenuObj.selectedMenuItem
////				sideMenuObj.selectedMenuItem = 5
////				self.navigationController?.pushViewController(destViewController, animated: true)
//			}
				
				
			else if indexPath.row == 2{
				let destViewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "NewContestWebVC") as! NewContestWebVC
				destViewController.title = "Contest"
				//				sideMenuObj.lastSelectedMenuItem = sideMenuObj.selectedMenuItem
				//				sideMenuObj.selectedMenuItem = 6
				
				self.navigationController?.pushViewController(destViewController, animated: true)
			}
				
			else if indexPath.row == 3{
				let destViewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "CaptureWithNikonVC")
				destViewController.title = "Capture with Nikon"
//				sideMenuObj.lastSelectedMenuItem = sideMenuObj.selectedMenuItem
//				sideMenuObj.selectedMenuItem = 6
				
				self.navigationController?.pushViewController(destViewController, animated: true)
			}
			else if indexPath.row == 4{
				
				let vc = Constants.mainStoryboard.instantiateViewController(withIdentifier: "WeddingAwardVC") as! WeddingAwardVC
				vc.title = "Wedding Film Awards"
				self.navigationController?.pushViewController(vc, animated: true)
				//            let destViewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "WebVC") as! WebVC
				//            destViewController.isPush = true
				//            destViewController.title = "CONTEST"
				//            destViewController.loadingURLSTR = "https://www.capturewithnikon.in"
				//            self.navigationController?.pushViewController(destViewController, animated: true)
			}
			else if indexPath.row == 5{
				let destViewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "NikkonPremiumVC") as! NikkonPremiumVC
				destViewController.title = "Nikon Premium Member"
				self.navigationController?.pushViewController(destViewController, animated: true)
			}
		}
		else{
			if indexPath.item == 0{
				
				let destViewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "WebVC") as! WebVC
				destViewController.isPush = true
				destViewController.title = "OFFERS"
				//        destViewController.loadingURLSTR = "https://www.nikonoffers.com"
				destViewController.loadingURLSTR = "http://www.nikon.co.in/en_IN/about/events_and_promotions"
				self.navigationController?.pushViewController(destViewController, animated: true)
			}
			else if indexPath.row == 1{
				let destViewController = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "ServiceAndSupportScrollVC") as! ServiceAndSupportScrollVC
				destViewController.selectedIndex = 2
				self.navigationController?.pushViewController(destViewController, animated: true)
			}
			else if indexPath.row == 2{
				let destViewController = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "StoreLocatorVC")
				self.navigationController?.pushViewController(destViewController, animated: true)
			}
			else if indexPath.row == 3{
				let destViewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "LoyaltyVC")
				sideMenuObj.lastSelectedMenuItem = sideMenuObj.selectedMenuItem
				sideMenuObj.selectedMenuItem = 5
				self.navigationController?.pushViewController(destViewController, animated: true)
			}
			else if indexPath.row == 4{
				let destViewController = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "GalleryMainVC")
				sideMenuObj.lastSelectedMenuItem = sideMenuObj.selectedMenuItem
				sideMenuObj.selectedMenuItem = 6
				
				self.navigationController?.pushViewController(destViewController, animated: true)
			}
		}
		
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if kind == UICollectionElementKindSectionFooter{
            let vw = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionFooter, withReuseIdentifier: "DashboardFooter", for: indexPath)
            return vw
        }
        else{
            let vw = UICollectionReusableView.init(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
            return  vw
        }
    }
    
    //MARK:- BUTTON ACTION
    
    @IBAction func ServiceButtonClicked(_ sender: AnyObject)
    {
        let destViewController = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "ServiceAndSupportScrollVC") as! ServiceAndSupportScrollVC
        destViewController.selectedIndex = 2
        self.navigationController?.pushViewController(destViewController, animated: true)
        
        
        
    }
    
    @IBAction func OfferButtonClicked(_ sender: AnyObject)
    {
//        let destViewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "PDFOpenVC")as! PDFOpenVC
//        destViewController.pdfURLStr = "http://www.nikon.co.in/en_IN/about/events_and_promotions"
//        destViewController.titleStr = "Events & Promotions"
//        destViewController.isFromMenu = false
//
//        self.navigationController?.pushViewController(destViewController, animated: true)
        
        let destViewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "WebVC") as! WebVC
        destViewController.isPush = true
        destViewController.title = "OFFERS"
//        destViewController.loadingURLSTR = "https://www.nikonoffers.com"
        destViewController.loadingURLSTR = "http://www.nikon.co.in/en_IN/about/events_and_promotions"
        self.navigationController?.pushViewController(destViewController, animated: true)
        
    }
    
    
    //MARK:- GET USER PROFILE
    func getUserProfile(){
        if Reachability.isConnectedToNetwork()
        {
            let method = "getuserprofile?emp_id=\(UserDefaults.standard.integer(forKey: Constants.USERID))"
            Constants.appDelegate.startIndicator()
            Server.getRequestWithURL(Constants.BASEURL+method, completionHandler: { (response) in
            
                if response[Constants.STATE] != nil && response[Constants.STATE] as! String == Constants.SUCC {

                    let dict = NSMutableDictionary()
                    Constants.appDelegate.stopIndicator()
                    for item in response{
                        dict.setValue(item.1, forKey: item.0)
                    }
                    Helper.saveDataInNsDefault(dict, key: Constants.PROFILE)
                    
                }
                else {
                    Constants.appDelegate.stopIndicator()
                    var mess = String()
                    if response[Constants.MESS] != nil{
                    mess = response[Constants.MESS] as! String
                    }
                    else{
                     mess = response["Message"] as! String
                }
                    DispatchQueue.main.async
                    {
                    let alert = UIAlertController.init(title: "Nikon", message: mess, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                        self.dismiss(animated: true, completion: nil)
                    }))
                    self.present(alert, animated: true, completion: nil)
                    }
                }
            })
        }
        else
        {
        Constants.appDelegate.stopIndicator()
        CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
    }
}
