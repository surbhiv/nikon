//
//  MyWorkShopVC.swift
//  NikonApp
//
//  Created by Hitesh Dhawan on 30/03/17.
//  Copyright © 2017 Neuronimbus. All rights reserved.
//

import UIKit

class MyWorkShopVC: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var myWorkshopTable: UITableView!
    var myWorkShopDataArray = [AnyObject]()
    var infoLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationController?.isNavigationBarHidden = false
        self.setNavigationBar()
         addInfoLabel()
        self.getMyWorkShopData()
    }
    
    func addInfoLabel() {
        
        //        let attributedString = NSMutableAttributedString(string: "NOTIFICATION NOT AVAILABLE!")
        //        attributedString.addAttributes([NSFontAttributeName: UIFont.systemFontOfSize(24.0)], range: NSMakeRange(0, 27))
        
        infoLabel = UILabel()
        //        infoLabel.attributedText = attributedString
        infoLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
        infoLabel.numberOfLines = 4
        infoLabel.textColor = UIColor.black
        infoLabel.font = UIFont(name: "OpenSans", size: 18)
        infoLabel.text = "No workshop found"
        infoLabel.textAlignment = NSTextAlignment.center
        infoLabel.frame = CGRect(x: (self.view.frame.size.width/2) - 100, y: (self.view.frame.size.height/2) - 150, width: 200, height: 100)
        //        tblNotification.addSubview(infoLabel)
        self.view.addSubview(infoLabel)
    }

    func OpenSideMenu(_ sender: AnyObject){
        
        self.view.endEditing(true)
        toggleSideMenuView()
    }
    func PopView(_ sender: AnyObject){
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func setNavigationBar(){
        self.navigationItem.hidesBackButton = true
        let navigationViewFrame = ScreenSize.SCREEN//self.navigationController?.navigationBar.frame
        let navigationView = UIView.init(frame: CGRect(x: 0, y: 0, width: navigationViewFrame.size.width, height: 44))
        navigationView.backgroundColor = UIColor.clear
        let menuButton = UIButton.init(type: .custom)
        
        menuButton.frame = CGRect(x: ScreenSize.SCREEN_WIDTH - 45, y: 7, width: 25, height: 25)
        menuButton.setImage(UIImage.init(named: "sideMenu_Icon"), for: UIControlState())
        menuButton.addTarget(self, action: #selector(OpenSideMenu), for: .touchUpInside)
        navigationView.addSubview(menuButton)
        //        self.navigationController?.navigationBar.tintColor = Colors.appPink
        
        let titleLabel = UILabel.init(frame:CGRect(x: 60,y: 0,width: ( (navigationViewFrame.size.width) - 110),height: 40))
        titleLabel.textColor = UIColor.black
        titleLabel.text = "MY WORKSHOP"
        titleLabel.font = UIFont.init(name:"OpenSans-SemiBold", size: 15)// UIFont(name:"OpenSans-SemiBold", size: 15)!
        titleLabel.textAlignment = .center
        navigationView.addSubview(titleLabel)
        
        let logoImage = UIImageView.init(frame: CGRect(x: 0, y: 6, width: 33, height: 33))
        logoImage.image = UIImage.init(named: "nikon_login_logo")
        navigationView.addSubview(logoImage)
        
        self.navigationItem.titleView = navigationView
    }

    
    func getMyWorkShopData() {
        if Reachability.isConnectedToNetwork()
        {
            let method = "UserWorkshop?empid=\(UserDefaults.standard.integer(forKey: Constants.USERID))"
            Constants.appDelegate.startIndicator()
            Server.getRequestWithURL(Constants.BASEURL+method, completionHandler: { (response) in
                
                if response[Constants.STATE] != nil && response[Constants.STATE] as! String == Constants.SUCC {
                
                DispatchQueue.main.async
                {
                    Constants.appDelegate.stopIndicator()
                    
                    self.infoLabel.isHidden = true
                    self.myWorkshopTable.isHidden = false
                    self.myWorkShopDataArray = response["UserList"]as! NSArray as [AnyObject]
                    self.myWorkshopTable.reloadData()
                }
                
                }
                else
                {
                    Constants.appDelegate.stopIndicator()
                    self.infoLabel.isHidden = false
                    self.myWorkshopTable.isHidden = true
                }
            })
        }
        else
        {
            Constants.appDelegate.stopIndicator()
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
    }

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myWorkShopDataArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyWorkShopCell", for: indexPath)as! MyWorkShopCell
        cell.workShopNameLbl.text = myWorkShopDataArray[indexPath.row]["workshopName"]as? String
        cell.workShopCityLbl.text = myWorkShopDataArray[indexPath.row]["workshocity"]as? String
        cell.workShopDateLbl.text = Helper.getDateFromString(myWorkShopDataArray[indexPath.row]["workshopDate"]as! String)
        cell.selectionStyle  = .none
               return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let destVC = Constants.mainStoryboard.instantiateViewController(withIdentifier: "PDFOpenVC")as! PDFOpenVC
        //todo
        //Staging "http://staging.nikonschool.in/InvoiceDownload.aspx?"
        // LIVE "https://www.nikonschool.in/Invoice_Download?"
        destVC.pdfURLStr = "https://www.nikonschool.in/Invoice_Download?orderno=\(myWorkShopDataArray[indexPath.row]["orderid"]as! String)"// "https://www.nikonschool.in/Invoice_Download?orderno=\(myWorkShopDataArray[indexPath.row]["orderid"]as! String)"
        self.navigationController?.pushViewController(destVC, animated: true)
            }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
