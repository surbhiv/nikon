//
//  WorkshopSubViewVC.swift
//  NikonApp
//
//  Created by Surbhi on 13/01/17.
//  Copyright © 2017 Neuronimbus. All rights reserved.
//

import UIKit

class WorkshopSubViewVC: UIViewController {

    //MARK:- IBOUTLETS
    
    @IBOutlet weak var backgroundImage: UIImageView!
    
    @IBOutlet weak var viewImageButton: UIButton!
   
    
    @IBOutlet var viewWorkShopButton: UIButton!
    //MARK:- VARIABLE Declarations
    var headingText = String()
    var descriptionText = String()
    var imageString = String()
    var indexOfViewWorkShopButton = NSInteger()
    var workShopTypeArray = [AnyObject]()

    
    
    //MARK:- START
    override func viewDidLoad() {
        super.viewDidLoad()
       
        self.backgroundImage.sd_setImage(with: URL.init(string: imageString), placeholderImage: UIImage.init(named: "product_placeholder.jpg"))
        self.backgroundImage.alignmentRectInsets.top
        
        self.backgroundImage.setIndicatorStyle(.gray)
        self.backgroundImage.setShowActivityIndicator(true)

        viewWorkShopButton.tag = indexOfViewWorkShopButton
        viewWorkShopButton .addTarget(self, action: #selector(viewWorkShopButtonClick), for: .touchUpInside)
        
        viewImageButton.tag = indexOfViewWorkShopButton
        viewImageButton .addTarget(self, action: #selector(viewImageButtonButtonClick), for: .touchUpInside)
    }

    //MARK:-BUTTON ACTION
    func viewWorkShopButtonClick(_ sender: UIButton){

        
        let workShopTypeArray = UserDefaults.standard .value(forKey: "workShopType") as! Array<AnyObject>

        let index = sender.tag

        let desination = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "WorkshopLevelVC") as! WorkshopLevelVC
        desination.selectedLevelIndex = index
        let levelId = workShopTypeArray [index]["Level_Id"] as! Int
        desination.levelId = String (levelId)
        desination.levelName = String ( workShopTypeArray [index]["Level_name"] as! String).uppercased()
        let image = workShopTypeArray [index]["level_image_large"] as! String
        let updateURL = image.replacingOccurrences(of: " ", with: "%20")
        desination.imageString = updateURL
        
        self.navigationController?.pushViewController(desination, animated: true)
    }
    
    
    func viewImageButtonButtonClick(_ sender: UIButton){
        
        let workShopTypeArray = UserDefaults.standard .value(forKey: "workShopType") as! Array<AnyObject>
        let index = sender.tag
        
        let desination = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "GalleryWorkShopSubVC") as! GalleryWorkShopSubVC
        let levelId = workShopTypeArray [index]["Level_Id"] as! Int
        desination.levelId = levelId
        desination.namLabeltext = String ( workShopTypeArray [index]["Level_name"] as! String).uppercased()
        let image = workShopTypeArray [index]["level_image_large"] as! String
        let updateURL = image.replacingOccurrences(of: " ", with: "%20")
        desination.imageString = updateURL

       // desination.selectedSection = 0
        self.navigationController?.pushViewController(desination, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
