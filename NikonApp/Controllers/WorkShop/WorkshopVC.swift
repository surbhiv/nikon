//
//  WorkshopVC.swift
//  NikonApp
//
//  Created by Surbhi on 20/12/16.
//  Copyright © 2016 Neuronimbus. All rights reserved.
//

import UIKit

class WorkshopVC: UIViewController, UIScrollViewDelegate {

    //MARK:- IBOUTLETS
    @IBOutlet weak var headerScroll: UIScrollView!
    @IBOutlet weak var detailScroll: UIScrollView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var widthConstraint: NSLayoutConstraint!
    @IBOutlet weak var pageControl: UIPageControl!
    
    
    //MARK:- VARIABLE Declarations
    fileprivate var indicatorLabel : UILabel!
    fileprivate var ControllerArray = [AnyObject]()
//    private var headerArray = ["BASICS OF D-SLR","PHOTO WALK","ADVANCED OUTDOOR","ADVANCED CLASSROOM","PHOTO SEMINAR","WEDDING WORKSHOP"]
    fileprivate var headerArray = [AnyObject]()
    fileprivate var workShopDataArray = [AnyObject]()
    var isFromMenu = Bool()
    var selectedIndex : Int = 0
    
    var subview1 = WorkshopSubViewVC()
    var subview2 = WorkshopSubViewVC()
    var subview3 = WorkshopSubViewVC()
    var subview4 = WorkshopSubViewVC()
    var subview5 = WorkshopSubViewVC()
    var subview6 = WorkshopSubViewVC()
    
    //MARK:- START
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        self.setNavigationBar()
        DispatchQueue.main.async
        {
            //print(self.isFromMenu)
            self.getdata()
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- NAVIGATION
    func OpenSideMenu(_ sender: AnyObject){
        
        self.view.endEditing(true)
        toggleSideMenuView()
    }
    func PopView(_ sender: AnyObject)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    func setNavigationBar(){
        self.navigationItem.hidesBackButton = true
        let navigationViewFrame = ScreenSize.SCREEN//self.navigationController?.navigationBar.frame
        let navigationView = UIView.init(frame: CGRect(x: 0, y: 0, width: navigationViewFrame.size.width, height: 44))
        navigationView.backgroundColor = UIColor.clear
        let menuButton = UIButton.init(type: .custom)
        
        menuButton.frame = CGRect(x: ScreenSize.SCREEN_WIDTH - 45, y: 7, width: 25, height: 25)
        menuButton.setImage(UIImage.init(named: "sideMenu_Icon"), for: UIControlState())
        menuButton.addTarget(self, action: #selector(OpenSideMenu), for: .touchUpInside)
        navigationView.addSubview(menuButton)
//        self.navigationController?.navigationBar.tintColor = Colors.appPink
        
        let titleLabel = UILabel.init(frame:CGRect(x: 60,y: 0,width: ( (navigationViewFrame.size.width) - 110),height: 40))
        titleLabel.textColor = UIColor.black
        titleLabel.text = "WORKSHOP"
        titleLabel.font = UIFont(name:"OpenSans-SemiBold", size: 15)!
        titleLabel.textAlignment = .center
        navigationView.addSubview(titleLabel)
        
//        let logoImage = UIImageView.init(frame: CGRectMake(25, 6, 33, 33))
//        logoImage.image = UIImage.init(named: "nikon_login_logo")
//        navigationView.addSubview(logoImage)
        
        if isFromMenu == false
        {
        let backButton = UIButton.init(type: .custom)
        backButton.frame = CGRect(x: 0, y: 9, width: 20, height: 20)
        backButton.backgroundColor = UIColor.white
        backButton.setImage(UIImage.init(named: "back"), for: UIControlState())
        backButton.addTarget(self, action: #selector(PopView), for: .touchUpInside)
        navigationView.addSubview(backButton)
            
            let logoImage = UIImageView.init(frame: CGRect(x: 25, y: 6, width: 33, height: 33))
            logoImage.image = UIImage.init(named: "nikon_login_logo")
            navigationView.addSubview(logoImage)
            
            
        }
        else
        {
            let logoImage = UIImageView.init(frame: CGRect(x: 0, y: 6, width: 33, height: 33))
            logoImage.image = UIImage.init(named: "nikon_login_logo")
            navigationView.addSubview(logoImage)
        }

        
        
        self.navigationItem.titleView = navigationView
        
    }
    
    
    
    //MARK:- WORKSHOP GET DATA
    
    func getdata(){
        if Reachability.isConnectedToNetwork()
        {
            let method = "Workshop/GetWorkshopLevel"
            Constants.appDelegate.startIndicator()
            
            Server.getRequestWithURL(Constants.BASEURL+method, completionHandler: { (response) in
                
                if response[Constants.STATE] != nil && response[Constants.STATE] as! String == Constants.SUCC {
                    DispatchQueue.main.async
                    {
                        Constants.appDelegate.stopIndicator()
                        self.workShopDataArray = response["GetList"] as! NSMutableArray as [AnyObject]
                        for arrayIndex in 0 ..< self.workShopDataArray.count {
                            let levelName = self.workShopDataArray[arrayIndex]["Level_name"]as! String
                            self.headerArray.insert(levelName.uppercased() as AnyObject, at: arrayIndex)

                        }
                        self.setHeaderScroll()
                        self.setUpScrollView()
                        
                    }
                }
                else {
                    Constants.appDelegate.stopIndicator()
                    var mess = String()
                    if response[Constants.MESS] != nil{
                        mess = response[Constants.MESS] as! String
                    }
                    else{
                        mess = response["Message"] as! String
                    }
                    DispatchQueue.main.async
                    {

                    let alert = UIAlertController.init(title: "Nikon", message: mess, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                        self.dismiss(animated: true, completion: nil)
                    }))
                    self.present(alert, animated: true, completion: nil)
                    }
                }
            })
        }
        else
        {
            Constants.appDelegate.stopIndicator()
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
    }


    //MARK:- SCROLLVIEW SetUp
    func setHeaderScroll() {
        var scrollContentCount = 0;
        var frame : CGRect = CGRect(x: 0, y: 0, width: 0, height: 0)
        var labelx : CGFloat = 0.0
        let width  : CGFloat = ScreenSize.SCREEN_WIDTH / 2.0//ScreenSize.SCREEN_WIDTH / 3.0
        let firstLabelWidth : CGFloat = ScreenSize.SCREEN_WIDTH / 2.0//ScreenSize.SCREEN_WIDTH / 3.0
        
        indicatorLabel = UILabel.init(frame: CGRect(x: labelx, y: 43, width: firstLabelWidth, height: 2))
        indicatorLabel.backgroundColor = Colors.appYello
        indicatorLabel.text = "";
        headerScroll.addSubview(indicatorLabel)
        self.headerScroll.isScrollEnabled=true
        self.headerScroll.delegate = self
        
        for arrayIndex in 0 ..< workShopDataArray.count {
            frame.origin.x = labelx
            // set scorllview properties
            frame.origin.y = 0;
            frame.size.width=width;
            frame.size.height=50;
            
            
            let accountLabelButton = UIButton.init(type: UIButtonType.custom)
            accountLabelButton.addTarget(self, action:#selector(HeaderlabelSelected), for: UIControlEvents.touchUpInside)
            
            accountLabelButton.frame = frame;
            accountLabelButton.tag = arrayIndex;
            accountLabelButton.setAttributedTitle(NSAttributedString(string: headerArray[arrayIndex] as! String,  attributes: [NSFontAttributeName : UIFont.init(name: "OpenSans-Semibold", size: 15)!, NSForegroundColorAttributeName : UIColor.white]), for: UIControlState())
            
            if DeviceType.IS_IPHONE_5 || DeviceType.IS_IPHONE_4_OR_LESS {
                accountLabelButton.setAttributedTitle(NSAttributedString(string: headerArray[arrayIndex] as! String,  attributes: [NSForegroundColorAttributeName : UIColor.white,NSFontAttributeName : UIFont.init(name: "OpenSans-Semibold", size: 13.5)!]), for: UIControlState())
            }
            accountLabelButton.titleLabel?.textAlignment = NSTextAlignment.center
            
            // changes done by jitender

            accountLabelButton.titleLabel!.lineBreakMode = .byWordWrapping
            accountLabelButton.titleLabel!.numberOfLines = 1
            accountLabelButton.titleLabel!.textAlignment = .center
            
            headerScroll.addSubview(accountLabelButton)
            
            if arrayIndex == 0 {
                accountLabelButton.alpha=1.0;
            }
            else {
                accountLabelButton.alpha=0.7;
            }
            
            labelx=labelx+width;
            scrollContentCount = scrollContentCount + 1;
        }
        
        // set scroll content size
        
        DispatchQueue.main.async(execute: {
            self.headerScroll.contentSize = CGSize(width: width * CGFloat(self.workShopDataArray.count), height: 40)
            
            self.indicatorLabel.frame =  CGRect(x: (ScreenSize.SCREEN_WIDTH / 2.0) * CGFloat(self.selectedIndex) ,y: 43, width: ScreenSize.SCREEN_WIDTH / 2.0, height: 2)
            self.detailScroll.scrollRectToVisible(CGRect(x: ScreenSize.SCREEN_WIDTH * CGFloat(self.selectedIndex), y: 0, width: ScreenSize.SCREEN_WIDTH, height: self.detailScroll.frame.size.height), animated: false)

        })
        automaticallyAdjustsScrollViewInsets = false;
        detailScroll.delegate=self;
    }

    
    func HeaderlabelSelected(_ sender: UIButton) {
        detailScroll.tag = sender.tag
        
        pageControl.currentPage = sender.tag
        let xAxis : CGFloat = ScreenSize.SCREEN_WIDTH * CGFloat(sender.tag)
        detailScroll.setContentOffset(CGPoint(x: xAxis, y: 0), animated: true)
        
        let buttonArray = NSMutableArray()
        selectedIndex = sender.tag
        
        
        for view in headerScroll.subviews {
            if view.isKind(of: UIButton.self) {
                let labelButton = view as! UIButton
                buttonArray.add(labelButton)
                
                
                if labelButton.tag == sender.tag {
                    
                    labelButton.alpha = 1.0
                    
                }
                else {
                    labelButton.alpha = 0.7
                }
            }
        }
        
        let labelButton = buttonArray.object(at: sender.tag) as! UIButton
        var frame : CGRect = (labelButton).frame
        frame.origin.y = 48
        frame.size.height = 3
        
        UIView.animate(withDuration: 0.2, animations: {
            if sender.tag == 0 {
                self.indicatorLabel.frame =  CGRect(x: 0,y: 43, width: ScreenSize.SCREEN_WIDTH / 2.0, height: 2)
            }
            else {
                self.indicatorLabel.frame =  CGRect(x: (ScreenSize.SCREEN_WIDTH / 2.0) * CGFloat(sender.tag) ,y: 43, width: ScreenSize.SCREEN_WIDTH / 2.0, height: 2)
            }
            
            self.headerScroll.scrollRectToVisible(self.indicatorLabel.frame, animated: false)
            if (sender.tag == 0)
            {
            self.headerScroll.setContentOffset(CGPoint(x: self.indicatorLabel.frame.origin.x , y: 0), animated: true)
            }
            else{
                self.headerScroll.setContentOffset(CGPoint(x: self.indicatorLabel.frame.origin.x-ScreenSize.SCREEN_WIDTH / 2.0 , y: 0), animated: true)

            }
            }, completion: nil)
    }
    
    //DETAIL SCROLL
    //,,,,
    func setUpScrollView() {
        self.detailScroll.isScrollEnabled = true

         for arrayIndex in 0 ..< self.workShopDataArray.count {
            
            subview1 = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "WorkshopSubViewVC") as! WorkshopSubViewVC
            subview1.headingText = headerArray[arrayIndex] as! String
            subview1.descriptionText = workShopDataArray[arrayIndex]["Level_desc"] as! String
            subview1.imageString = workShopDataArray[arrayIndex]["level_image_large"] as! String
            
            subview1.indexOfViewWorkShopButton = arrayIndex
            ControllerArray.append(subview1)
        }
        //subview1.workShopTypeArray = headerArray
        DispatchQueue.main.async(execute: {
        UserDefaults.standard.set(self.workShopDataArray, forKey: "workShopType")
        UserDefaults.standard.synchronize()
            })
        setViewControllers(ControllerArray as NSArray, animated: false)
    }
    
    
    //  Add and remove view controllers
    func setViewControllers(_ viewControllers : NSArray, animated : Bool) {
        if self.childViewControllers.count > 0 {
            for vC in self.childViewControllers {
                vC.willMove(toParentViewController: nil)
                vC.removeFromParentViewController()
            }
        }
        
        for vC in viewControllers {
            self.addChildViewController(vC as! UIViewController)
            (vC as AnyObject).didMove(toParentViewController: self)
        }
        
        //TODO animations
        if ((detailScroll) != nil) {
            reloadPages()
        }
    }
    
    func reloadPages() {
        var scrollContentCount = 0;
        
        for arrayIndex in 0 ..< headerArray.count {
            // set scorllview properties
            var frame : CGRect = CGRect(x: 0, y: 0, width: 0, height: 0)
            frame.origin.x = ScreenSize.SCREEN_WIDTH * CGFloat(arrayIndex)
            frame.origin.y = 0;
            frame.size = ScreenSize.SCREEN.size
            
            detailScroll.isPagingEnabled = true
            let contentWidth : CGFloat = ScreenSize.SCREEN_WIDTH * CGFloat(scrollContentCount+1)
            widthConstraint.constant = contentWidth;
            
            let vC = self.childViewControllers[arrayIndex]
            addView(vC.view, contentView: contentView, frame: frame)
            //contentView.backgroundColor = UIColor.redColor()
            scrollContentCount = scrollContentCount + 1;
        }
        
        DispatchQueue.main.async(execute: {
            self.detailScroll.contentSize = CGSize(width: self.widthConstraint.constant, height: self.detailScroll.frame.size.height)
        })
        detailScroll.delegate = self
    }
    
    // adding views to scrollview
    func addView(_ view : UIView, contentView : UIView, frame : CGRect) {
        view.frame = frame
        contentView.addSubview(view)
        view.translatesAutoresizingMaskIntoConstraints = false
        
        DispatchQueue.main.async(execute: {
            contentView.addConstraint(NSLayoutConstraint.init(item: view, attribute: NSLayoutAttribute.leading, relatedBy: NSLayoutRelation.equal, toItem: contentView, attribute:  NSLayoutAttribute.leading, multiplier: 1.0, constant: frame.origin.x))
            contentView.addConstraint(NSLayoutConstraint.init(item: view, attribute: NSLayoutAttribute.width, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute:  NSLayoutAttribute.notAnAttribute, multiplier: 1.0, constant: frame.size.width))
            contentView.addConstraint(NSLayoutConstraint.init(item: view, attribute: NSLayoutAttribute.top, relatedBy: NSLayoutRelation.equal, toItem: contentView, attribute:  NSLayoutAttribute.top, multiplier: 1.0, constant:0))
            contentView.addConstraint(NSLayoutConstraint.init(item: view, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute:  NSLayoutAttribute.notAnAttribute, multiplier: 1.0, constant: frame.size.height - 155))
        })
    }
    
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        if scrollView == self.detailScroll {
            
            let pageWidth = ScreenSize.SCREEN_WIDTH
            let page = (Int)(floor((self.detailScroll.contentOffset.x - pageWidth / 3.0) / pageWidth) + 1);
            
            let but = UIButton.init()
            but.tag = page
            pageControl.currentPage = page

            self.HeaderlabelSelected(but)
        }
    }

    
}
