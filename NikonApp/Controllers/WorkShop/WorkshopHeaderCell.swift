//
//  WorkshopHeaderCell.swift
//  NikonApp
//
//  Created by Hitesh Dhawan on 22/03/17.
//  Copyright © 2017 Neuronimbus. All rights reserved.
//

import UIKit

class WorkshopHeaderCell: UITableViewCell {

    @IBOutlet weak var stateText: UITextField!
    @IBOutlet weak var stateLabel: UILabel!
    @IBOutlet weak var stateLine: UILabel!
    @IBOutlet weak var stateLineHeight: NSLayoutConstraint!
    @IBOutlet weak var descriptionHeight: NSLayoutConstraint!
    
    @IBOutlet weak var cityText: UITextField!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var cityLine: UILabel!
    @IBOutlet weak var cityLineHeight: NSLayoutConstraint!

    @IBOutlet weak var monthText: UITextField!
    @IBOutlet weak var monthLabel: UILabel!
    @IBOutlet weak var monthLine: UILabel!
    @IBOutlet weak var monthLineHeight: NSLayoutConstraint!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
