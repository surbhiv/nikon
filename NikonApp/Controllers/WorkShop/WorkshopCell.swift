//
//  WorkshopCell.swift
//  NikonApp
//
//  Created by Hitesh Dhawan on 06/02/17.
//  Copyright © 2017 Neuronimbus. All rights reserved.
//

import UIKit

class WorkshopCell: UITableViewCell {
    
    @IBOutlet weak var statusLbl: UILabel!
    @IBOutlet weak var stateLbl: UILabel!
    @IBOutlet weak var cityLbl: UILabel!
    @IBOutlet weak var workshopType: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var courseDetailTextView: UITextView!
    @IBOutlet weak var moreBtn: UIButton!
    @IBOutlet weak var viewLessBtn: UIButton!
    @IBOutlet weak var enrollBtn: UIButton!
    @IBOutlet weak var heightConstraintForTextView: NSLayoutConstraint!
    @IBOutlet weak var containerViewHeight: NSLayoutConstraint!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
