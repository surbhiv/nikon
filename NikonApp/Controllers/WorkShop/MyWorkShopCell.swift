//
//  MyWorkShopCell.swift
//  NikonApp
//
//  Created by Hitesh Dhawan on 30/03/17.
//  Copyright © 2017 Neuronimbus. All rights reserved.
//

import UIKit

class MyWorkShopCell: UITableViewCell {

    @IBOutlet weak var workShopNameLbl: UILabel!
    @IBOutlet weak var workShopCityLbl: UILabel!
    @IBOutlet weak var workShopDateLbl: UILabel!
    @IBOutlet weak var downloadBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
