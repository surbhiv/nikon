//
//  WorkshopLevelVC.swift
//  NikonApp
//
//  Created by Hitesh Dhawan on 06/02/17.
//  Copyright © 2017 Neuronimbus. All rights reserved.
//

import UIKit

class WorkshopLevelVC: UIViewController,UITableViewDataSource,UITableViewDelegate,UIPickerViewDelegate,UIPickerViewDataSource,UITextFieldDelegate {

    //MARK:- OUTLET
    @IBOutlet weak var levelTableView: UITableView!
    @IBOutlet weak var levelNameLabel: UILabel!
    @IBOutlet weak var workShopDescription: UILabel!
    @IBOutlet weak var headerViewTable: UIView!
    @IBOutlet weak var levelTypeText: UITextField!
    @IBOutlet weak var stateText: UITextField!
    @IBOutlet weak var cityText: UITextField!
    @IBOutlet weak var monthText: UITextField!

    @IBOutlet weak var stateLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var monthLabel: UILabel!
    
    @IBOutlet weak var stateLine: UILabel!
    @IBOutlet weak var cityLine: UILabel!
    @IBOutlet weak var monthLine: UILabel!
    
    @IBOutlet weak var stateLineHeight: NSLayoutConstraint!
    @IBOutlet weak var cityLineHeight: NSLayoutConstraint!
    @IBOutlet weak var monthLineHeight: NSLayoutConstraint!
    
    //MARK:- VARIABLE DEclaration
     var dataArray = [AnyObject]()
     var stateArray = [AnyObject]()
     var cityArray = [AnyObject]()
     var  monthArray = ["January","February","March","April","May","June","July","August","September","October","November","December"]
    
     var workShopLevelArray = [AnyObject]()
     var selectedLevelIndex = NSInteger()
     var imageString = String()
     var levelId = String()
     var stateId = String()
     var cityId = String()
     var monthId = String()
     var levelName = String()
     var picker = UIPickerView()
    
    fileprivate var expandedSections = NSMutableIndexSet()
    
    //MARK: - Start
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationController?.isNavigationBarHidden = false
        self.setNavigationBar()
        
        //define picker
        self.picker = UIPickerView.init()
        self.picker.dataSource = self
        self.picker.delegate = self
        
       
        
        
        workShopLevelArray = UserDefaults.standard .value(forKey: "workShopType") as! Array<AnyObject>
        
 //["\(levelName)","SELECT STATE","SELECT CITY","SELECT MONTH"]
        stateId = "0"
        cityId = "0"
        monthId = "0"
        self.getWorkShopData(levelId, stateId: "0", cityId: "0", month: "0")
        self.getStateName()
    }
    
    
    //MARK:- NAVIGATION
    func OpenSideMenu(_ sender: AnyObject){
        
        self.view.endEditing(true)
        toggleSideMenuView()
    }
    func PopView(_ sender: AnyObject){
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func setNavigationBar(){
        self.navigationItem.hidesBackButton = true
        let navigationViewFrame = ScreenSize.SCREEN//self.navigationController?.navigationBar.frame
        let navigationView = UIView.init(frame: CGRect(x: 0, y: 0, width: navigationViewFrame.size.width, height: 44))
        navigationView.backgroundColor = UIColor.clear
        let menuButton = UIButton.init(type: .custom)
        
        menuButton.frame = CGRect(x: ScreenSize.SCREEN_WIDTH - 45, y: 7, width: 25, height: 25)
        menuButton.setImage(UIImage.init(named: "sideMenu_Icon"), for: UIControlState())
        menuButton.addTarget(self, action: #selector(OpenSideMenu), for: .touchUpInside)
        navigationView.addSubview(menuButton)
//        self.navigationController?.navigationBar.tintColor = Colors.appPink
        
        let titleLabel = UILabel.init(frame:CGRect(x: 60,y: 0,width: ( (navigationViewFrame.size.width) - 110),height: 40))
        titleLabel.textColor = UIColor.black
        titleLabel.text = "WORKSHOP"
        titleLabel.font = UIFont.init(name:"OpenSans-SemiBold", size: 15)// UIFont(name:"OpenSans-SemiBold", size: 15)!
        titleLabel.textAlignment = .center
        navigationView.addSubview(titleLabel)
        
        let logoImage = UIImageView.init(frame: CGRect(x: 25, y: 6, width: 33, height: 33))
        logoImage.image = UIImage.init(named: "nikon_login_logo")
        navigationView.addSubview(logoImage)
        
                let backButton = UIButton.init(type: .custom)
                backButton.frame = CGRect(x: 0, y: 9, width: 20, height: 20)
                backButton.backgroundColor = UIColor.white
                backButton.setImage(UIImage.init(named: "back"), for: UIControlState())
                backButton.addTarget(self, action: #selector(PopView), for: .touchUpInside)
                navigationView.addSubview(backButton)
        
        
        self.navigationItem.titleView = navigationView
        
    }
    

    //MARK:- GET WORKSHOP DATA
    func getWorkShopData(_ levelId : String , stateId : String , cityId : String , month : String) {
        if Reachability.isConnectedToNetwork()
        {
            let method = "Workshop/GetWorkshop?Level_ID=\(levelId)&StateId=\(stateId)&cityId=\(cityId)&month=\(month)"

            Constants.appDelegate.startIndicator()
            Server.getRequestWithURL(Constants.BASEURL+method, completionHandler: { (response) in
                
                if response[Constants.STATE] != nil && response[Constants.STATE] as! String == Constants.SUCC {
                    
                    self.dataArray = response["workshopList"] as! Array<AnyObject>
                    DispatchQueue.main.async
                    {
                        Constants.appDelegate.stopIndicator()
                        self.levelTableView .reloadData()
                                               
                    }
                }
                else {
                    Constants.appDelegate.stopIndicator()
                    DispatchQueue.main.async
                    {
                        if self.dataArray.count > 0{
                        self.dataArray.removeAll()
                        }
                        self.levelTableView .reloadData()
                        
                    
                    var mess = String()
                    if response[Constants.MESS] != nil{
                        mess = response[Constants.MESS] as! String
                    }
                    else{
                        mess = response["Message"] as! String
                    }
                        DispatchQueue.main.async
                        {
                    let alert = UIAlertController.init(title: "Nikon", message: mess, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                        self.dismiss(animated: true, completion: nil)
                    }))
                    self.present(alert, animated: true, completion: nil)
                        }
                    }
                }
            })
        }
        else
        {
            Constants.appDelegate.stopIndicator()
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }

        
    }

    //MARK:- GETSTATE NAME
    func getStateName(){
        if Reachability.isConnectedToNetwork()
        {
           
       
               let method = "GetStateCity"
         
            
            Constants.appDelegate.startIndicator()
            Server.getRequestWithURL(Constants.BASEURL+method, completionHandler: { (response) in
                
                if response[Constants.STATE] != nil && response[Constants.STATE] as! String == Constants.SUCC {
                    
                    self.stateArray = response["statelist"] as! Array<AnyObject>
                    DispatchQueue.main.async
                    {
                        Constants.appDelegate.stopIndicator()
                        //self.getCityName("52")
                        
                    }
                }
                else {
                    Constants.appDelegate.stopIndicator()
                   
                    var mess = String()
                    if response[Constants.MESS] != nil{
                        mess = response[Constants.MESS] as! String
                    }
                    else{
                        mess = response["Message"] as! String
                    }
                    DispatchQueue.main.async
                    {
                    let alert = UIAlertController.init(title: "Nikon", message: mess, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                        self.dismiss(animated: true, completion: nil)
                    }))
                    self.present(alert, animated: true, completion: nil)
                    }
                }
            })
        }
        else
        {
            Constants.appDelegate.stopIndicator()
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
    }
    
    //MARK:- GETCITY NAME
    func getCityName(_ state_id:NSString){
        if Reachability.isConnectedToNetwork()
        {
            let method = "GetStateCity?state_id=\(state_id)"
            
            Constants.appDelegate.startIndicator()
            Server.getRequestWithURL(Constants.BASEURL+method, completionHandler: { (response) in
                
                if response[Constants.STATE] != nil && response[Constants.STATE] as! String == Constants.SUCC {
                    
                    self.cityArray = response["Citylist"] as! Array<AnyObject>
                    DispatchQueue.main.async
                    {
                        Constants.appDelegate.stopIndicator()
                    }
                }
                else {
                    Constants.appDelegate.stopIndicator()
                    
                    var mess = String()
                    if response[Constants.MESS] != nil {
                        mess = response[Constants.MESS] as! String
                    }
                    else {
                        mess = response["Message"] as! String
                    }
                    DispatchQueue.main.async
                    {
                    let alert = UIAlertController.init(title: "Nikon", message: mess, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                        self.dismiss(animated: true, completion: nil)
                    }))
                    self.present(alert, animated: true, completion: nil)
                    }
                }
            })
        }
        else
        {
            Constants.appDelegate.stopIndicator()
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
    }


    //MARK:- tableview delegate
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
         if indexPath.section == 0 {
            
//            tableView.estimatedRowHeight = 425
//            tableView.rowHeight = UITableViewAutomaticDimension
//            let width = ScreenSize.SCREEN_WIDTH - 20
//            let height = (width * 3.0)/5.0
//            
//            return (255 + height)
            let fon = UIFont.init(name: "OpenSans", size: 15.0)
            let height = Helper.getHeightForText((workShopLevelArray [selectedLevelIndex]["Level_desc"] as? String)!, fon: fon!)+20
            return (320 + height)

        }
         else{
            if expandedSections.contains(indexPath.section){
                let hgt = Helper.getHeightForText(dataArray [indexPath.section - 1]["Course_detail"] as! String, fon: UIFont.init(name: "OpenSans", size: 12.0)!)
                return (190 + hgt)
            }
            else{
                return 210
            }
        }
    }
    
    
  
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.dataArray.count + 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1;
    }
    
    //MARK:- tableview Datasource
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "WorkshopHeaderCell", for: indexPath) as! WorkshopHeaderCell
            levelNameLabel = cell.contentView.viewWithTag(90011) as! UILabel
            levelNameLabel.text = workShopLevelArray [selectedLevelIndex]["Level_name"] as? String
            
            workShopDescription = cell.contentView.viewWithTag(90012) as! UILabel
            workShopDescription.text = workShopLevelArray [selectedLevelIndex]["Level_desc"] as? String
            
            levelTypeText = cell.contentView.viewWithTag(90013) as! UITextField
            stateText = cell.contentView.viewWithTag(90014) as! UITextField
            cityText = cell.contentView.viewWithTag(90015) as! UITextField
            monthText = cell.contentView.viewWithTag(90016) as! UITextField
            
            
            let fon = UIFont.init(name: "OpenSans", size: 15.0)
            cell.descriptionHeight.constant = Helper.getHeightForText((workShopLevelArray [selectedLevelIndex]["Level_desc"] as? String)!, fon: fon!)+20
            // for animation
            
            stateLine = cell.stateLine
            stateLabel = cell.stateLabel
            stateLineHeight = cell.stateLineHeight
            
            cityLine = cell.cityLine
            cityLabel = cell.cityLabel
            cityLineHeight = cell.cityLineHeight
            
            monthLine = cell.monthLine
            monthLabel = cell.monthLabel
            monthLineHeight = cell.monthLineHeight
            
            
            
            //set picker to textfield
            self.levelTypeText.inputView = self.picker
            self.stateText.inputView = self.picker
            self.cityText.inputView = self.picker
            self.monthText.inputView = self.picker
            
            self.levelTypeText.delegate = self
            self.stateText.delegate = self
            self.cityText.delegate = self
            self.monthText.delegate = self
            
            levelTypeText.text = workShopLevelArray [selectedLevelIndex]["Level_name"] as? String
            
            cell.selectionStyle = .none
            return cell
        }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "WorkshopCell", for: indexPath) as! WorkshopCell
            cell.statusLbl.text =  dataArray [indexPath.section - 1]["Workshop_Status"] as? String
            cell.workshopType.text = dataArray [indexPath.section - 1]["Level_name"] as? String
            cell.stateLbl.text = dataArray [indexPath.section - 1]["StateName"] as? String
            cell.dateLbl.text = Helper.getStringFromDate(dataArray [indexPath.section - 1]["WorkshopDate"] as! String)
            cell.cityLbl.text = dataArray [indexPath.section - 1]["WorkshopPlace"] as? String
            

           // cell.courseDetailTextView.setValue(dataArray [indexPath.section - 1]["Course_detail"] as! String, forKey: "contentToHTMLString")
            let courseDetailStr = dataArray [indexPath.section - 1]["Course_detail"] as! String
            do {
                let str = try NSAttributedString(data: courseDetailStr.data(using: String.Encoding.unicode, allowLossyConversion: true)!, options: [NSDocumentTypeDocumentAttribute : NSHTMLTextDocumentType], documentAttributes: nil)
                cell.courseDetailTextView.attributedText = str
            } catch {
                print(error)
            }

            let fon = UIFont.init(name: "OpenSans", size: 12.0)
            
            if expandedSections.contains(indexPath.section){
                cell.heightConstraintForTextView.constant = Helper.getHeightForTextView(dataArray [indexPath.section - 1]["Course_detail"] as! String ,label: cell.courseDetailTextView, fon: fon!)+30
                cell.containerViewHeight.constant = 116 + cell.heightConstraintForTextView.constant
                cell.moreBtn.isHidden = true
                cell.viewLessBtn.isHidden = false
            }
            else{
                cell.heightConstraintForTextView.constant = Helper.getHeightForTextView_WithLine(dataArray[indexPath.section - 1]["Course_detail"] as! String, label: cell.courseDetailTextView, fon: fon!)+30
                cell.containerViewHeight.constant = 190//184
                
                cell.moreBtn.isHidden = false
                cell.viewLessBtn.isHidden = true
            }
            
            cell.moreBtn.tag = indexPath.section
            cell.moreBtn .addTarget(self, action: #selector(moreButtonClicked), for: .touchUpInside)
            
            cell.viewLessBtn.tag = indexPath.section
            cell.viewLessBtn .addTarget(self, action: #selector(moreButtonClicked), for: .touchUpInside)
            
            
            cell.enrollBtn.tag = indexPath.section
            if  (cell.statusLbl.text == "Open" && dataArray[indexPath.section - 1]["AvailableSeat"] as! Int >= 1)
            {
                cell.statusLbl.textColor = UIColor.init(red: 31.0/255.0, green: 159.0/255.0, blue: 17.0/255.0, alpha: 1)
                cell.enrollBtn.backgroundColor = Colors.appYello
                cell.enrollBtn.isUserInteractionEnabled = true
            }
            else
            {
                cell.statusLbl.text = "Close"
                cell.statusLbl.textColor = UIColor.red
                cell.enrollBtn.backgroundColor = UIColor.lightGray
                cell.enrollBtn.isUserInteractionEnabled = false
            }
            cell.enrollBtn .addTarget(self, action: #selector(enrollBtnClicked), for: .touchUpInside)
            

            cell.selectionStyle = .none
            return cell
        }
    }
    
    
    //MARK:-BUTTON ACTION
     func enrollBtnClicked(_ sender: UIButton){
        if (UserDefaults.standard.integer(forKey: Constants.USERID) != 0) {
        
         let index =  sender.tag
        
        let desination = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "WorkShopDetailVC") as! WorkShopDetailVC
    
        let workShopId = dataArray [index-1]["Workshop_Id"] as! Int
        if (dataArray [index - 1]["Workshop_Status"] as? String == "Open")
        {
            desination.statusBool = true
        }
        else
        {
            desination.statusBool = false
        }
        desination.workShopId = String(workShopId)
        self.navigationController?.pushViewController(desination, animated: true)
        }
        else
        {
        
            let alert = UIAlertController.init(title: "Nikon", message: "Please login to continue.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
               
                DispatchQueue.main.async
                {
                
                    
                    let destinationController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "LoginVC")as! LoginVC
                    destinationController.isWantTodismissVC = true
                    self.present(destinationController, animated: true, completion: nil);

                }
            }))
            alert.addAction(UIAlertAction.init(title: "Cancel", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                 self.dismiss(animated: true, completion: nil)
            }))
            self.present(alert, animated: true, completion: nil)

        }
        

    }
    
    func moreButtonClicked(_ sender: UIButton){
        let index =  sender.tag

        let currentlyExpanded : Bool = expandedSections.contains(index)
        
        if currentlyExpanded == true {
            expandedSections.remove(index)
            UIView.animate(withDuration: 1, animations: {
                DispatchQueue.main.async
                {
                    self.levelTableView.reloadSections(IndexSet.init(integer: index), with: UITableViewRowAnimation.none)
                    self.levelTableView.scrollToRow(at: IndexPath.init(row: 0, section: index), at: .none, animated: false)
                }
            })
        }
        else {
            var otherindex = Int()
            
            if expandedSections.count > 0 {
                otherindex = expandedSections.firstIndex
                expandedSections.remove(otherindex)
                UIView.animate(withDuration: 0.0, animations: {
                    DispatchQueue.main.async
                    {
                        self.levelTableView.reloadSections(IndexSet.init(integer: otherindex), with: UITableViewRowAnimation.none)
                    }

                })
            }
            expandedSections.add(index)
            
            UIView.animate(withDuration: 1, animations: {
                DispatchQueue.main.async
                {
                    self.levelTableView.reloadSections(IndexSet.init(integer: index), with: UITableViewRowAnimation.none)
                    self.levelTableView.scrollToRow(at: IndexPath.init(row: 0, section: index), at: .none, animated: false)
                }
            })
        }
    }

    //MARK: - PICKEVIEW for date
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if self.levelTypeText.isFirstResponder {
            return workShopLevelArray.count
        }
        if self.stateText.isFirstResponder {
            return stateArray.count
        }
        if self.cityText.isFirstResponder{
            return cityArray.count
        }
        if self.monthText.isFirstResponder {
            return monthArray.count
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if self.levelTypeText.isFirstResponder {
            return String.init(format: workShopLevelArray [row]["Level_name"] as! String , locale: nil).uppercased()
        }
        if self.stateText.isFirstResponder{
            return String.init(format: stateArray [row]["State_Name"] as! String, locale: nil)
        }
        if self.cityText.isFirstResponder {
            return String.init(format: cityArray [row]["CityName"] as! String, locale: nil)
        }
        if self.monthText.isFirstResponder{
            return String.init(format: monthArray [row] , locale: nil)
        }
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if self.levelTypeText.isFirstResponder {
        
            self.levelTypeText.text = String.init(format: workShopLevelArray [row]["Level_name"] as! String, locale: nil)
            selectedLevelIndex = row
            let levelid = workShopLevelArray [row]["Level_Id"] as! Int
            levelId = String(levelid)
            
            self.levelNameLabel.text = String.init(format: workShopLevelArray [row]["Level_name"] as! String, locale: nil)
            self.workShopDescription.text = workShopLevelArray [row]["Level_desc"]as? String
            
        }
        if self.stateText.isFirstResponder{
        
            self.stateText.text = String.init(format: stateArray [row]["State_Name"] as! String, locale: nil)
             let stateIdInt = stateArray [row]["state_id"] as! Int
             stateId = String(stateIdInt)
            self.cityText.text = ""
            cityId = "0"
            monthId = "0"
            
            
            
        }
        if self.cityText.isFirstResponder {
            let cityIdInt = cityArray [row]["state_id"] as! Int
            cityId = String(cityIdInt)
            self.cityText.text =  String.init(format: cityArray [row]["CityName"] as! String, locale: nil)
             monthId = "0"
            self.monthText.text = ""
            
            
            

        }
        if self.monthText.isFirstResponder {
            let monthIdInt = row+1
            monthId = String(monthIdInt)
            self.monthText.text =  String.init(format: monthArray [row], locale: nil)
            
        }
        
    }
    //MARK:- FUNCTION TO ACTIVATE OR DEACTIVATE
    func ACTIVATETextField(_ textf : UITextField,textLabel : UILabel , line: UILabel, height : NSLayoutConstraint)
    {
        UIView.animate(withDuration: 1.0, delay: 0, options: UIViewAnimationOptions.transitionFlipFromBottom, animations: {
            
            textLabel.isHidden = false
            textf.attributedPlaceholder = NSAttributedString(string: "")
            Helper.activatetextFieldForWorkShop(line,height: height,textlabel: textLabel)
            
            },
                                   completion: { (true) in
                                    textLabel.isHidden = false
        })
    }
    
    
    func DEACTIVATETextField(_ textf : UITextField,textLabel : UILabel , line: UILabel, height : NSLayoutConstraint)
    {
        if textf.text == ""
        {
            UIView.animate(withDuration: 1.0, animations: {
                textLabel.isHidden = true
                
                Helper.deActivatetextFieldForWorkShop(line,height: height,textlabel: textLabel)
                
                var mess = textLabel.text!
                
//                if textf == self.dayText {
//                    mess = "DD"
//                }
//                else if textf == self.monthText {
//                    mess = "MM"
//                }
//                else if textf == self.yearText {
//                    mess = "YYYY"
//                }
                
                textf.attributedPlaceholder = NSAttributedString(string:mess ,  attributes: [NSForegroundColorAttributeName : Colors.textPlaceHolderGray, NSFontAttributeName : UIFont(name: "OpenSans", size: 14)!])
                
                }, completion: nil)
        }
        else
        {
            Helper.deActivatetextField(line,height: height,textlabel: textLabel)
        }
        
//        else if textf == dayText {
//            monthText.userInteractionEnabled = true
//        }
//        else if textf == monthText{
//            yearText.userInteractionEnabled = true
//        }
    }

 // MARK: TEXTFIELD DELEGATE
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == stateText {
            ACTIVATETextField(stateText, textLabel: stateLabel, line: stateLine, height: stateLineHeight)
        }
        else if textField == cityText {
            ACTIVATETextField(cityText, textLabel: cityLabel, line: cityLine, height: cityLineHeight)
        }
        else if textField == monthText {
            ACTIVATETextField(monthText, textLabel: monthLabel, line: monthLine, height: monthLineHeight)
        }
        
    }
    
     func textFieldDidEndEditing(_ textField: UITextField) {
    
         if textField == stateText
         {
            DEACTIVATETextField(stateText, textLabel: stateLabel, line: stateLine, height: stateLineHeight)
            DispatchQueue.main.async{
                self.getCityName(String(self.stateId) as! NSString)
                self.getWorkShopData(self.levelId, stateId: self.stateId, cityId: self.cityId, month: self.monthId)
            }
         }
        else if textField == cityText
        {
            DEACTIVATETextField(cityText, textLabel: cityLabel, line: cityLine, height: cityLineHeight)
            DispatchQueue.main.async{
                
                self.getWorkShopData(self.levelId, stateId: self.stateId, cityId: self.cityId, month: self.monthId)
            }
        }
        if textField == monthText
        {
            DEACTIVATETextField(monthText, textLabel: monthLabel, line: monthLine, height: monthLineHeight)
            DispatchQueue.main.async{
                
                self.getWorkShopData(self.levelId, stateId: self.stateId, cityId: self.cityId, month: self.monthId)
            }
        }
        else
         {
            DispatchQueue.main.async{
               
                self.getWorkShopData(self.levelId, stateId: self.stateId, cityId: self.cityId, month: self.monthId)
            }
           
         }
        textField .resignFirstResponder()
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
