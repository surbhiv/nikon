//
//  WorkShopDetailVC.swift
//  NikonApp
//
//  Created by Hitesh Dhawan on 10/02/17.
//  Copyright © 2017 Neuronimbus. All rights reserved.
//

import UIKit

class WorkShopDetailVC: UIViewController {

    
    //MARK:- IBOUTLET
    
    @IBOutlet weak var workShopType: UILabel!
    @IBOutlet weak var workShopDetailText: UITextView!
    @IBOutlet weak var workShopNumberOfDay: UILabel!
    @IBOutlet weak var workShopStatus: UILabel!
    @IBOutlet weak var workShopDaysLeft: UILabel!
    @IBOutlet weak var workShopDate: UILabel!
    @IBOutlet weak var workShopTiming: UILabel!
    @IBOutlet weak var workShopLocation: UILabel!
    @IBOutlet weak var workShopVenueAddress: UILabel!
    @IBOutlet weak var nikonUserPrice: UILabel!
    @IBOutlet weak var otherUserPrice: UILabel!
    @IBOutlet weak var workShopReuirementText: UITextView!
    
    @IBOutlet weak var heightConstraintForVenueAddress: NSLayoutConstraint!
    @IBOutlet weak var heightConstraintForWorkShopDetail: NSLayoutConstraint!
    @IBOutlet weak var heightForWorkShopRequirement: NSLayoutConstraint!
        @IBOutlet weak var heightConstraintForScrollView: NSLayoutConstraint!
    //MARK:- VARIABLE DECLRATION
    var statusBool = Bool()
    var workShopDetailDict = Dictionary<String,AnyObject>()//[String ]()
    var workShopId = String()
    var userId = String()
	

	
    //MARK:- START
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationController?.isNavigationBarHidden = false
        self.setNavigationBar()
        userId = String(UserDefaults.standard.object(forKey: Constants.USERID) as! Int)
        self.getWorkShopDetail(workShopId, userId: userId)

    }

    //MARK:- NAVIGATION
    func OpenSideMenu(_ sender: AnyObject){
        
        self.view.endEditing(true)
        toggleSideMenuView()
    }
    func PopView(_ sender: AnyObject){
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func setNavigationBar(){
        self.navigationItem.hidesBackButton = true
        let navigationViewFrame = ScreenSize.SCREEN//self.navigationController?.navigationBar.frame
        let navigationView = UIView.init(frame: CGRect(x: 0, y: 0, width: navigationViewFrame.size.width, height: 44))
        navigationView.backgroundColor = UIColor.clear
        let menuButton = UIButton.init(type: .custom)
        
        menuButton.frame = CGRect(x: ScreenSize.SCREEN_WIDTH - 45, y: 7, width: 25, height: 25)
        menuButton.setImage(UIImage.init(named: "sideMenu_Icon"), for: UIControlState())
        menuButton.addTarget(self, action: #selector(OpenSideMenu), for: .touchUpInside)
        navigationView.addSubview(menuButton)
//        self.navigationController?.navigationBar.tintColor = Colors.appPink
        
        let titleLabel = UILabel.init(frame:CGRect(x: 60,y: 0,width: ( (navigationViewFrame.size.width) - 110),height: 40))
        titleLabel.textColor = UIColor.black
        titleLabel.text = "WORKSHOP"
        titleLabel.font = UIFont.init(name:"OpenSans-SemiBold", size: 15)// UIFont(name:"OpenSans-SemiBold", size: 15)!
        titleLabel.textAlignment = .center
        navigationView.addSubview(titleLabel)
        
        let logoImage = UIImageView.init(frame: CGRect(x: 25, y: 6, width: 33, height: 33))
        logoImage.image = UIImage.init(named: "nikon_login_logo")
        navigationView.addSubview(logoImage)
        
        let backButton = UIButton.init(type: .custom)
        backButton.frame = CGRect(x: 0, y: 9, width: 20, height: 20)
        backButton.backgroundColor = UIColor.white
        backButton.setImage(UIImage.init(named: "back"), for: UIControlState())
        backButton.addTarget(self, action: #selector(PopView), for: .touchUpInside)
        navigationView.addSubview(backButton)
        
        
        self.navigationItem.titleView = navigationView
        
    }
    
   
    //MARK:- GET WORKSHOP DATA
    func getWorkShopDetail(_ workShopId : String , userId : String ) {
        if Reachability.isConnectedToNetwork()
        {
            let method = "Workshop/GetWorkshopDetails?WorkshopID=\(workShopId)&UserId=\(userId)"
            
            Constants.appDelegate.startIndicator()
            Server.getRequestWithURL(Constants.BASEURL+method, completionHandler: { (response) in
                
                
               // if response[Constants.STATE] != nil && response[Constants.STATE] as! String == Constants.SUCC {
                   // print(response)
                    self.workShopDetailDict = response
              
                    DispatchQueue.main.async
                    {
                        Constants.appDelegate.stopIndicator()
                      
                        let fon = UIFont.init(name: "OpenSans", size: 12.0)
                        self.heightConstraintForWorkShopDetail.constant = Helper.getHeightForTextView(self.workShopDetailDict["Course_detail"] as! String, label: self.workShopDetailText, fon: fon!)
                        self.heightForWorkShopRequirement.constant = Helper.getHeightForTextView(self.workShopDetailDict["Course_Req"] as! String, label: self.workShopReuirementText, fon: fon!)+5
                        self.heightConstraintForVenueAddress.constant = (Helper.getHeightForTextView(self.workShopDetailDict["Venue"] as! String, label: self.workShopDetailText, fon: fon!)+30)*2
                        
                        self.heightConstraintForScrollView.constant = 650+self.heightConstraintForVenueAddress.constant+self.heightForWorkShopRequirement.constant+self.heightConstraintForWorkShopDetail.constant
                        
                       // self.workShopDetailText.setValue(self.workShopDetailDict["Course_detail"] as! String, forKey: "contentToHTMLString")
                        let courseDetailStr = self.workShopDetailDict["Course_detail"] as! String
                        let courseRequirement = self.workShopDetailDict["Course_Req"] as! String
                        do {
                            let str = try NSAttributedString(data: courseDetailStr.data(using: String.Encoding.unicode, allowLossyConversion: true)!, options: [NSDocumentTypeDocumentAttribute : NSHTMLTextDocumentType], documentAttributes: nil)
                            self.workShopDetailText.attributedText = str
                            
                            let str1 = try NSAttributedString(data: courseRequirement.data(using: String.Encoding.unicode, allowLossyConversion: true)!, options: [NSDocumentTypeDocumentAttribute : NSHTMLTextDocumentType], documentAttributes: nil)
                            self.workShopReuirementText.attributedText = str1
                        } catch {
                            print(error)
                        }

                        
                        self.workShopNumberOfDay.text = self.workShopDetailDict["Course_duration"] as? String
                       // let status = self.workShopDetailDict["Status"] as! Bool
                        self.workShopStatus.text = (self.statusBool ? "OPEN" : "CLOSE")
                        self.workShopType.text = self.workShopDetailDict["Level_name"]!.uppercased as String
                        self.workShopDaysLeft.text = String(self.workShopDetailDict["AvailableSeat"] as! Int)
                        self.workShopDate.text = Helper.getStringFromDate(self.workShopDetailDict["WorkshopDate"] as! String)
                        self.workShopTiming.text = "\(self.workShopDetailDict["Course_start_time"] as! String)"+" TO "+"\(self.workShopDetailDict["Course_end_time"] as! String)"
                        self.workShopLocation.text = "\(self.workShopDetailDict["WorkshopPlace"] as! String)"+","+"\(self.workShopDetailDict["StateName"] as! String)"
                        self.workShopVenueAddress.text =  self.workShopDetailDict["Venue"] as? String
                        self.nikonUserPrice.text =  self.workShopDetailDict["Nikon_Usr_fee"] as? String
                        self.otherUserPrice.text =  self.workShopDetailDict["Nikon_NonUsr_fee"] as? String
						
                        //self.workShopReuirementText.setValue(self.workShopDetailDict["Course_Req"] as! String, forKey: "contentToHTMLString")

                    }
            })
        }
        else
        {
            Constants.appDelegate.stopIndicator()
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
        
        
    }
    
    
   
    @IBAction func enrollButtonClick(_ sender: AnyObject) {
        
        if workShopDetailDict["Nk_user"]as! String == "No"
        {
            let alert = UIAlertController.init(title: "Nikon", message: "If you add your camera details, you become a Nikon user and earn discounts to be redeemed later when joining workshops. Do you want to add camera details", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction.init(title: "YES", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
            let desination = Constants.mainStoryboard.instantiateViewController(withIdentifier: "CameraLenseMainScroll") as! CameraLenseMainScroll
                self.navigationController?.pushViewController(desination, animated: true)
                           }))
            alert.addAction(UIAlertAction.init(title: "NO", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                
                let desination = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "WorkShopPayNowVC") as! WorkShopPayNowVC
                desination.workShopDetailDict = self.workShopDetailDict
                self.navigationController?.pushViewController(desination, animated: true)
                
            }))
            self.present(alert, animated: true, completion: nil)

        }
		 
       else
        {
            let desination = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "WorkShopPayNowVC") as! WorkShopPayNowVC
            desination.workShopDetailDict = self.workShopDetailDict
            self.navigationController?.pushViewController(desination, animated: true)

        }
        
        

        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
