//
//  WorkShopPayNowVC.swift
//  NikonApp
//
//  Created by Hitesh Dhawan on 13/02/17.
//  Copyright © 2017 Neuronimbus. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}


extension Date {
    var age: Int {
        return (Calendar.current as NSCalendar).components(.year, from: self, to: Date(), options: []).year!
    }
}

class WorkShopPayNowVC: UIViewController, UITextViewDelegate {

    
    //MARK:- IBOUTLET
    
    @IBOutlet weak var workShopType: UILabel!
    @IBOutlet weak var workShopDate: UILabel!
    @IBOutlet weak var workShopFees: UILabel!
    @IBOutlet weak var workShopLocation: UILabel!
    @IBOutlet weak var workShopVenueAddress: UILabel!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userDOB: UILabel!
    @IBOutlet weak var reedemLoyalityPoint: UILabel!
    @IBOutlet weak var totalINRValue: UILabel!
    @IBOutlet weak var redeemWorkButton: UIButton!
	
	@IBOutlet weak var payNowBtn: UIButton!
	
	@IBOutlet weak var submitBtn: UIButton!
	
	@IBOutlet weak var termsLabel: UITextView!
    
    @IBOutlet weak var heightConstraintForVenueAddress: NSLayoutConstraint!
 
    @IBOutlet weak var checkBoxButton: UIButton!
    //MARK:- VARIABLE DECLRATION
    var workShopDetailDict = Dictionary<String,AnyObject>()
    var userDetailDict = Dictionary<String,AnyObject>()
    
    var userId = String()
    var userAddress = String()
    var userAge = String()
    var userCountory = String()
    var userEmail = String()
    var userFname = String()
    var userLname = String()
    var userGender = String()
    var userPhone = String()
    var userPincode = String()
    var userDob = String()
    var courseFee = String()
    var nikonUser = String()
    var otherUser = String()
    var cameraNum = String()
    var serialNum = String()
	
	var payNowIsHidden = false
    var redeemNowIsHidden = false
	var submitIsHidden = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationController?.isNavigationBarHidden = false
        self.setNavigationBar()
//        userId = String(NSUserDefaults .standardUserDefaults().objectForKey(Constants.USERID) as! Int)
//        self.getUserDetail(userId)
//        self.setData()
        let str = "I certify that I have read and agree to the Terms and Privacy Policy."
        
        let attributedString = NSMutableAttributedString(string: str)
        attributedString.addAttribute(NSLinkAttributeName, value: "www.google.com", range: NSRange(location: 44, length: 5))
        attributedString.addAttribute(NSLinkAttributeName, value: "www.apple.com", range: NSRange(location: 54, length: 15))
        attributedString.addAttribute(NSForegroundColorAttributeName, value: UIColor.black, range: NSRange(location: 0, length: 69) )
        attributedString.addAttribute(NSFontAttributeName, value: UIFont.italicSystemFont(ofSize: 13.0), range: NSRange(location: 0, length: 69) )
        
        self.termsLabel.attributedText = attributedString
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        userId = String(UserDefaults.standard.object(forKey: Constants.USERID) as! Int)
        self.getUserDetail(userId as NSString)
        self.setData()
		
		if workShopDetailDict["Nikon_NonUsr_fee"] as? String != "0"{
		   self.payNowBtn.isHidden = false
			self.redeemWorkButton.isHidden = false
			self.submitBtn.isHidden = true
		}
		else{
			self.submitBtn.isHidden = false
			self.payNowBtn.isHidden = true
			self.redeemWorkButton.isHidden = true
		}
    }
	
	
    //MARK:- NAVIGATION
    func OpenSideMenu(_ sender: AnyObject){
        
        self.view.endEditing(true)
        toggleSideMenuView()
    }
    func PopView(_ sender: AnyObject){
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func setNavigationBar(){
        self.navigationItem.hidesBackButton = true
        let navigationViewFrame = ScreenSize.SCREEN//self.navigationController?.navigationBar.frame
        let navigationView = UIView.init(frame: CGRect(x: 0, y: 0, width: navigationViewFrame.size.width, height: 44))
        navigationView.backgroundColor = UIColor.clear
        let menuButton = UIButton.init(type: .custom)
        
        menuButton.frame = CGRect(x: ScreenSize.SCREEN_WIDTH - 45, y: 7, width: 25, height: 25)
        menuButton.setImage(UIImage.init(named: "sideMenu_Icon"), for: UIControlState())
        menuButton.addTarget(self, action: #selector(OpenSideMenu), for: .touchUpInside)
        navigationView.addSubview(menuButton)
//        self.navigationController?.navigationBar.tintColor = Colors.appPink
        
        let titleLabel = UILabel.init(frame:CGRect(x: 60,y: 0,width: ( (navigationViewFrame.size.width) - 110),height: 40))
        titleLabel.textColor = UIColor.black
        titleLabel.text = "WORKSHOP"
        titleLabel.font = UIFont.init(name:"OpenSans-SemiBold", size: 15)// UIFont(name:"OpenSans-SemiBold", size: 15)!
        titleLabel.textAlignment = .center
        navigationView.addSubview(titleLabel)
        
        let logoImage = UIImageView.init(frame: CGRect(x: 25, y: 6, width: 33, height: 33))
        logoImage.image = UIImage.init(named: "nikon_login_logo")
        navigationView.addSubview(logoImage)
        
        let backButton = UIButton.init(type: .custom)
        backButton.frame = CGRect(x: 0, y: 9, width: 20, height: 20)
        backButton.backgroundColor = UIColor.white
        backButton.setImage(UIImage.init(named: "back"), for: UIControlState())
        backButton.addTarget(self, action: #selector(PopView), for: .touchUpInside)
        navigationView.addSubview(backButton)
        
        
        self.navigationItem.titleView = navigationView
        
    }

    func setData (){
        
        let fon = UIFont.init(name: "OpenSans", size: 12.0)
        //print(self.workShopDetailDict["Venue"] as! String)
       // print(fon)
        self.heightConstraintForVenueAddress.constant = (Helper.getHeightForText(self.workShopDetailDict["Venue"] as! String, fon: fon!)+20)*2
        
        self.workShopType.text = self.workShopDetailDict["Level_name"]!.uppercased as String
        self.workShopDate.text = Helper.getStringFromDate(self.workShopDetailDict["WorkshopDate"] as! String)
        self.workShopLocation.text = "\(self.workShopDetailDict["WorkshopPlace"] as! String)"+","+"\(self.workShopDetailDict["StateName"] as! String)"
        self.workShopVenueAddress.text =  self.workShopDetailDict["Venue"] as? String
       // print(NSUserDefaults .standardUserDefaults().objectForKey(Constants.USERID))
        if self.workShopDetailDict["Nk_user"]as! String == "Yes"
        {
            courseFee = self.workShopDetailDict["Nikon_Usr_fee"] as! String
            self.nikonUser = "Yes"
            self.otherUser = "No"
        }
        else
        {
            courseFee = self.workShopDetailDict["Nikon_NonUsr_fee"] as! String
            self.nikonUser = "No"
            self.otherUser = "Yes"


            
        }
        self.workShopFees.text = courseFee
        
        self.reedemLoyalityPoint.text = String (self.workShopDetailDict["RedeemPoint"] as! Int)
        self.totalINRValue.text =  String (self.workShopDetailDict["RedeemPointINR"] as! Int)
        self.userName.text = UserDefaults.standard .object(forKey: Constants.USERNAME) as? String
        self.userDOB.text = UserDefaults.standard .object(forKey: Constants.USERDOB) as? String
//        let redeemPointINRValue = self.workShopDetailDict["RedeemPointINR"] as! Int
//        if (redeemPointINRValue > Int(courseFee))
//        {
//            self.redeemWorkButton.userInteractionEnabled = true
//        }
        self.redeemWorkButton.isUserInteractionEnabled = true
      
    }

    //MARK:- GET USER DETAIL API
    func getUserDetail(_ userId : NSString)  {
        if Reachability.isConnectedToNetwork()
        {
            let method = "getuserprofile?emp_id=\(userId)"
            
            Constants.appDelegate.startIndicator()
            Server.getRequestWithURL(Constants.BASEURL+method, completionHandler: { (response) in
                
                
                // if response[Constants.STATE] != nil && response[Constants.STATE] as! String == Constants.SUCC {
           //  print(response)
                let responseDict = response // as NSDictionary
                
                DispatchQueue.main.async
                {
                    Constants.appDelegate.stopIndicator()
                    self.userDetailDict = responseDict
                    
                    self.userFname   = responseDict["User_Fname"] as! String
                    self.userLname   = responseDict["User_Lname"] as! String
                    self.userEmail   = responseDict["User_Email"] as! String
                    self.userPhone   = responseDict["User_Phone"] as! String
                    self.userGender  = responseDict["User_Gender"] as! String
                    self.userPincode = responseDict["User_Pincode"] as! String
                    
                    
                    let dict = NSMutableDictionary()
                    for item in response
                    {
                        var value = item.1
					
						if (value.isEmpty) != nil{
							value = "" as AnyObject
						}
//						if (value.isEmpty || value == nil){
//							value = "" as AnyObject
//						}
						
                        dict.setValue(value, forKey: item.0)
                    }
                    
                    self.serialNum = dict["Sr_No"] as! String
                    self.cameraNum = dict["Camera_No"] as! String
                    self.userDob     = dict["User_Dob"] as? String ?? ""
                    self.userAddress = "\(responseDict["User_Address"] as! String) \(responseDict["State_name"] as! String)"
                    
                    
                }
                
                
            })
        }
        else
        {
            Constants.appDelegate.stopIndicator()
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func checkBoxButtonPressed(_ sender: AnyObject) {
       if(checkBoxButton.isSelected == true)
       {
        checkBoxButton.isSelected = false
        }
        else
       {
        checkBoxButton.isSelected = true
        }
    }
	
	@IBAction func submitBtnAction(_ sender: UIButton) {
		if(checkBoxButton.isSelected == true)
		{
			if userFname == "" || userPincode == "" || userEmail == "" || userPhone == "" || userDob == ""
			{
				let alertController = UIAlertController.init(title: "Nikon", message: "Please update profile information", preferredStyle: UIAlertControllerStyle.alert)
				let  yesButton = UIAlertAction.init(title: "OK", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
					
					let destViewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "EditProfileVC")as! EditProfileVC
					destViewController.userProfileDataDict = self.userDetailDict
					
					self.navigationController?.pushViewController(destViewController, animated: true)
					//
				})
				
				alertController.addAction(yesButton)
				
				self.present(alertController, animated: true, completion: {
				})
				
			}
			else
			{
				
				
//				let detailVC = Constants.mainStoryboard2.instantiateViewController(withIdentifier:"PaymentWebVC" ) as! PaymentWebVC
//
//				self.navigationController?.pushViewController(detailVC, animated: true)
				self.postWorkshopPayments()
			}
		}
		else
		{
			CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "Nikon", "message": "Please select terms & conditions."] )
			
		}

	}
	
	
	
	
	
	
    @IBAction func enrollNowButtonPressed(_ sender: AnyObject) {
        
        if(checkBoxButton.isSelected == true)
        {
            if userFname == "" || userPincode == "" || userEmail == "" || userPhone == "" || userDob == ""
            {
                let alertController = UIAlertController.init(title: "Nikon", message: "Please update profile information", preferredStyle: UIAlertControllerStyle.alert)
                let  yesButton = UIAlertAction.init(title: "OK", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                    
                    let destViewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "EditProfileVC")as! EditProfileVC
                    destViewController.userProfileDataDict = self.userDetailDict
                    
                    self.navigationController?.pushViewController(destViewController, animated: true)
                    //
                })
                
                alertController.addAction(yesButton)
                
                self.present(alertController, animated: true, completion: {
                })

            }
            else
            {
                self.postWorkshopPayment()
            }
        }
        else
        {
              CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "Nikon", "message": "Please select terms & conditions."] )
            
        }
    }

    @IBAction func redeemWorkButtonPressed(_ sender: AnyObject)
    {
        if(checkBoxButton.isSelected == true)
        {
            let redeemPointINRValue = self.workShopDetailDict["RedeemPointINR"] as! Int
            if (redeemPointINRValue < Int(courseFee))
            {
                CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "Nikon", "message": "Insufficient points. You don’t have the sufficient points in your account to redeem this workshop."] )
            }
            else
            {
                self.redeemWorkshopByPoint()
            }

            
        }
        else
        {
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "Nikon", "message": "Please select terms & conditions."] )
            
        }
        
    }
    //MARK:- API
    func postWorkshopPayment()  {
        
         let method = "Workshop_Payment"
        
        let workShopId = String(self.workShopDetailDict["Workshop_Id"] as! Int)
        let courseId   = String(self.workShopDetailDict["Course_id"] as! Int)
        let nikonUser = self.workShopDetailDict["Nk_user"]as! String
        let workShopVenue = self.workShopDetailDict["Venue"] as! String
        let workShopDate = self.workShopDetailDict["WorkshopDate"] as! String
        let courseName =  self.workShopDetailDict["Level_name"]as! String
       
        
        
//        let dateFormatter = NSDateFormatter()
//        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
//        let date = dateFormatter.dateFromString(self.userDob)
        
        print(userDetailDict["User_Age"]as! String)
        


       
        var param = "UserId=\(userId)&Workshop_Id=\(workShopId)&Course_id=\(courseId)&Camera_No=\(self.cameraNum)&Nk_User=\(nikonUser)&Sr_No=\(self.serialNum)&User_Address=\(self.userAddress)&User_Age=\(userDetailDict["User_Age"]as! String)&User_Country=India&User_Email=\(self.userEmail)&User_Fname=\(self.userFname)&User_Gender=\(self.userGender)&User_Lname=\(self.userLname)&User_Phone=\(self.userPhone)&User_Pincode=\(self.userPincode)&Workshop_Venue=\(workShopVenue)&Workshop_Date=\(workShopDate)&User_Dob=\(self.userDob)&Course_Fee=\(courseFee)&Course_Name=\(courseName)&NikonmarketedUser=\(otherUser)"
        param = param.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!

        
        if Reachability.isConnectedToNetwork() {
            Constants.appDelegate.startIndicator()
            
            Server.postRequestWithURL(Constants.BASEURL+method, paramString: param) { (response) in
                
                if response[Constants.STATE] != nil && response[Constants.STATE] as! String == Constants.SUCC {
                    
                    DispatchQueue.main.async
                    {
                        
                        Constants.appDelegate.stopIndicator()
                        print(response)
						
                        let orderId = response["order_id"]as! String
					
                        let destVC = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "PaymentVC")as! PaymentVC
                        destVC.orderId = orderId
                        destVC.amount = self.courseFee
                        destVC.userDetailDict = self.userDetailDict
                        self.navigationController?.pushViewController(destVC, animated: true)

                    }
				}
					
			
                else{
                    Constants.appDelegate.stopIndicator()
                    var mess = String()
                    if response[Constants.MESS] != nil{
                        mess = response[Constants.MESS] as! String
                    }
                    else{
                        mess = response["Message"] as! String
                    }
                    CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "Nikon", "message": mess] )
                }
            }
        }
			
        else {
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
      
    }
	
	func postWorkshopPayments()  {
		
		let method = "Workshop_Payment"
		
		let workShopId = String(self.workShopDetailDict["Workshop_Id"] as! Int)
		let courseId   = String(self.workShopDetailDict["Course_id"] as! Int)
		let nikonUser = self.workShopDetailDict["Nk_user"]as! String
		let workShopVenue = self.workShopDetailDict["Venue"] as! String
		let workShopDate = self.workShopDetailDict["WorkshopDate"] as! String
		let courseName =  self.workShopDetailDict["Level_name"]as! String
		
		
		
		//        let dateFormatter = NSDateFormatter()
		//        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
		//        let date = dateFormatter.dateFromString(self.userDob)
		
		print(userDetailDict["User_Age"]as! String)
		
		
		
		
		var param = "UserId=\(userId)&Workshop_Id=\(workShopId)&Course_id=\(courseId)&Camera_No=\(self.cameraNum)&Nk_User=\(nikonUser)&Sr_No=\(self.serialNum)&User_Address=\(self.userAddress)&User_Age=\(userDetailDict["User_Age"]as! String)&User_Country=India&User_Email=\(self.userEmail)&User_Fname=\(self.userFname)&User_Gender=\(self.userGender)&User_Lname=\(self.userLname)&User_Phone=\(self.userPhone)&User_Pincode=\(self.userPincode)&Workshop_Venue=\(workShopVenue)&Workshop_Date=\(workShopDate)&User_Dob=\(self.userDob)&Course_Fee=\(courseFee)&Course_Name=\(courseName)&NikonmarketedUser=\(otherUser)"
		param = param.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
		
		
		if Reachability.isConnectedToNetwork() {
			Constants.appDelegate.startIndicator()
			
			Server.postRequestWithURL(Constants.BASEURL+method, paramString: param) { (response) in
				
				if response[Constants.STATE] != nil && response[Constants.STATE] as! String == Constants.SUCC {
					
					DispatchQueue.main.async
						{
							
							Constants.appDelegate.stopIndicator()
							print(response)
							
							let orderId = response["order_id"]as! String
							
							let destVC = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "PaymentWebVC")as! PaymentWebVC
							destVC.orderId = orderId
							//destVC.amount = self.courseFee
							destVC.userDetailDict = self.userDetailDict
							self.navigationController?.pushViewController(destVC, animated: true)
							
					}
				}
					
					
				else{
					Constants.appDelegate.stopIndicator()
					var mess = String()
					if response[Constants.MESS] != nil{
						mess = response[Constants.MESS] as! String
					}
					else{
						mess = response["Message"] as! String
					}
					CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "Nikon", "message": mess] )
				}
			}
		}
			
		else {
			CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
		}
		
	}
    
    func redeemWorkshopByPoint()  {
        
        let method = "Workshop_Redeem"
        
        let workShopId = String(self.workShopDetailDict["Workshop_Id"] as! Int)
        let courseId   = String(self.workShopDetailDict["Course_id"] as! Int)
        let nikonUser = self.workShopDetailDict["Nk_user"]as! String
        let workShopVenue = self.workShopDetailDict["Venue"] as! String
        let workShopDate = self.workShopDetailDict["WorkshopDate"] as! String
        let courseName =  self.workShopDetailDict["Level_name"]as! String
        
        
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        let date = dateFormatter.date(from: self.userDob)
        
       // print(date!.age)
        
        
        
        
        //changes told by Arvind Sir
        var param = "UserId=\(userId)&Workshop_Id=\(workShopId)&Course_id=\(courseId)&Camera_No=\(self.cameraNum)&Nk_User=\(nikonUser)&Sr_No=\(self.serialNum)&User_Address=\(self.userAddress)&User_Age=\(userDetailDict["User_Age"]as! String)&User_Country=India&User_Email=\(self.userEmail)&User_Fname=\(self.userFname)&User_Gender=\(self.userGender)&User_Lname=\(self.userLname)&User_Phone=\(self.userPhone)&User_Pincode=\(self.userPincode)&Workshop_Venue=\(workShopVenue)&Workshop_Date=\(workShopDate)&User_Dob=\(self.userDob)&Course_Fee=\(courseFee)&Course_Name=\(courseName)&NikonmarketedUser=\(otherUser)&ReddemPoint=\(self.workShopDetailDict["RedeemPoint"] as! Int)"
        
        param = param.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
//        let param = "UserId=\(userId)&Workshop_Id=\(workShopId)&Course_id=\(courseId)&Camera_No=&Nk_User=\(nikonUser)&Sr_No=&User_Address=\(self.userAddress)&User_Age=\(25)&User_Country=India&User_Email=\(self.userEmail)&User_Fname=\(self.userFname)&User_Gender=\(self.userGender)&User_Lname=\(self.userLname)&User_Phone=\(self.userPhone)&User_Pincode=\(self.userPincode)&Workshop_Venue=\(workShopVenue)&Workshop_Date=\(workShopDate)&User_Dob=\(self.userDob)&Course_Fee=\(courseFee)&Course_Name=\(courseName)&NikonmarketedUser=\(otherUser)&ReddemPoint=\(Int(courseFee)!)"
        
        if Reachability.isConnectedToNetwork() {
            Constants.appDelegate.startIndicator()
            
            Server.postRequestWithURL(Constants.BASEURL+method, paramString: param) { (response) in
                Constants.appDelegate.stopIndicator()
                if response[Constants.STATE] != nil && response[Constants.STATE] as! String == Constants.SUCC {
                    
                    DispatchQueue.main.async
                    {
                        
                        
                        print(response)
						
                        let orderId = response["order_id"]as! String
                        var mess = String()
                        if response[Constants.MESS] != nil{
                            mess = response[Constants.MESS] as! String
                        }
                        else{
                            mess = response["Message"] as! String
                        }
                        DispatchQueue.main.async
                        {
                            
                            
                            let alert = UIAlertController.init(title: "Nikon", message: mess, preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                                let destViewController = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "WorkshopVC")
                                self.navigationController?.pushViewController(destViewController, animated: true)

                               
                            }))
                            self.present(alert, animated: true, completion: nil)

                            
                        }

                       
                        
                    }
                    
                }
                else
                {
                    Constants.appDelegate.stopIndicator()
                    var mess = String()
                    if response[Constants.MESS] != nil{
                        mess = response[Constants.MESS] as! String
                    }
                    else{
                        mess = response["Message"] as! String
                    }
                    DispatchQueue.main.async
                    {

                    CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "Nikon", "message": mess] )
                        
                    }
                }
            }
        }
        else {
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
        
    }
 //MARK:- Textview delegate
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool {
        if URL == Foundation.URL.init(string: "www.google.com") {
            TermsPressed()
            return true
            //enrollment_Terms_condition
        }
        else if URL == Foundation.URL.init(string: "www.apple.com") {
            PrivacyPressed()
            return true
        }
        
        return false
    }
    
    func TermsPressed(){
        
        let vc = Constants.mainStoryboard.instantiateViewController(withIdentifier: "WebVC") as! WebVC
        vc.title = "TERMS & CONDITIONS"
        vc.isPush = true
        vc.isTermsType = "WORKSHOP"
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func PrivacyPressed(){
        
        let destViewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "WebVC") as! WebVC
        destViewController.isPush = true
        destViewController.title = "PRIVACY POLICY"
        self.navigationController?.pushViewController(destViewController, animated: true)
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
/*
let orderIDs = response["order_id"] as! String
let destVC = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "PaymentWebVC")as! PaymentWebVC
destVC.orderId = orderIDs
self.navigationController?.pushViewController(destVC, animated: true)
*/
