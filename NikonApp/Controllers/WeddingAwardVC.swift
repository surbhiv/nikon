//
//  WeddingAwardVC.swift
//  NikonApp
//
//  Created by Arnab Maity on 10/02/20.
//  Copyright © 2020 Neuronimbus. All rights reserved.
//

import UIKit

class WeddingAwardVC: UIViewController {

	@IBOutlet weak var weddingWebView: UIWebView!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		weddingWebView.delegate = self
		let url = URL(string: "http://capturewithnikon.in/wfa/")
		let request = URLRequest(url: url!)
		weddingWebView.loadRequest(request)
	}
}

extension WeddingAwardVC: UIWebViewDelegate {
	func webViewDidFinishLoad(_ webView: UIWebView) {
		print("Finished loading")
	}
}
