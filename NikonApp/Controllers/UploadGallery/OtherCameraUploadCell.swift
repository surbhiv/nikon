//
//  OtherCameraUploadCell.swift
//  NikonApp
//
//  Created by Hitesh Dhawan on 21/06/17.
//  Copyright © 2017 Neuronimbus. All rights reserved.
//

import UIKit

class OtherCameraUploadCell: UITableViewCell {

    @IBOutlet weak var yesBtn: UIButton!
    @IBOutlet weak var noBtn: UIButton!
    
    @IBOutlet weak var selectOtherCameraTxt: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
