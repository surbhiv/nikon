//
//  ExposureUploadCell.swift
//  NikonApp
//
//  Created by Hitesh Dhawan on 21/06/17.
//  Copyright © 2017 Neuronimbus. All rights reserved.
//

import UIKit

class ExposureUploadCell: UITableViewCell {

    @IBOutlet weak var selectApertureTxt: UITextField!
    @IBOutlet weak var selectShutterSpeedTxt: UITextField!
    @IBOutlet weak var selectISOTxt: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
