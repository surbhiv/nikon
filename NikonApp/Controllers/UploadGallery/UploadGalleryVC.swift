//
//  UploadGalleryVC.swift
//  NikonApp
//
//  Created by Hitesh Dhawan on 21/06/17.
//  Copyright © 2017 Neuronimbus. All rights reserved.
//

import UIKit
//import ANProgressStepper
import CMSteppedProgressBar

enum textfieldName : Int {
    case selectCameraTag = 881
    case selectLenseTag = 882
    case selectOtherCameraTag = 883
    case selectApertureTag = 884
    case selectShutterSpeedTag = 885
    case selectISOTag = 886
    case selectAlbumTag = 887
    
}
class UploadGalleryVC: UIViewController,UITableViewDataSource,UITableViewDelegate,UIPickerViewDelegate,UIPickerViewDataSource,UITextFieldDelegate,UIImagePickerControllerDelegate,UIPopoverControllerDelegate,UINavigationControllerDelegate,UITextViewDelegate,CMSteppedProgressBarDelegate {

    @IBOutlet weak var uploadTable: UITableView!
    @IBOutlet weak var userUploadImageCollection : UICollectionView!
    
    @IBOutlet weak var albumTxt: UITextField!
    
    @IBOutlet weak var uploadPhotoLbl: UILabel!
    @IBOutlet weak var uploadPhotoTxt: UITextField!
    
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var descriptionTextView: UITextView!
    
    @IBOutlet weak var addMoreImageLbl: UILabel!
    
    //@IBOutlet weak var stepper: ANProgressStepper!
    @IBOutlet weak var stepper: CMSteppedProgressBar!
    
    
    //used to store textfield value
    
    @IBOutlet weak var selectCameraTxt: UITextField!
    @IBOutlet weak var selectLenseTxt: UITextField!
    @IBOutlet weak var selectApertureTxt: UITextField!
    @IBOutlet weak var selectShutterSpeedTxt: UITextField!
    @IBOutlet weak var selectISOTxt: UITextField!
    @IBOutlet weak var selectOtherCameraTxt: UITextField!
    
    //dorp down array
    var albumArray = [AnyObject]()
    var cameraListArray = [AnyObject]()
    var lensListArray = [AnyObject]()
    var exposureListArray = [AnyObject]()
    var ISOListArray = [AnyObject]()
    var shutterSpeedListArray = [AnyObject]()
    var otherCameraListArray = [AnyObject]()
    
    //user uploadImage in a week
    var userUploadedImageInWeekDataArray = [AnyObject]()
    
    // bool to check image click by DSLR camera or not
    var isShootByNikonCamera = Bool()
    
    //picker
     var picker = UIPickerView()
    
    // check which picker view will be open
    var selectedTextfieldValue = Int()
    
    //parameter to be send
    var albumID = String()
    var lenseID = String()
    
    //Image
    var imageDataIMG = Data()
    var imageDataToSend = String()
    var imagePicker:UIImagePickerController?=UIImagePickerController()
    var isPickedImage = Bool()
    
    //userUploadedImages count
    var userUploadedGalleryCount = String()
    var userUploadedGalleryCountIntegerValue = NSInteger()

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        isShootByNikonCamera = true
        userUploadedGalleryCountIntegerValue = 0
        self.stepper.currentStep = 3
        self.navigationController?.isNavigationBarHidden = false
        self.setNavigationBar()
        
        // picker initialization
        self.picker = UIPickerView.init()
        self.picker.dataSource = self
        self.picker.delegate = self
        
        imagePicker?.delegate = self
        
        self.setUpForProgressbar()
        
        self.albumTxt.inputView = picker
        
//        dispatch_async(dispatch_get_main_queue())
//        {
//            self.performSelector(#selector(self.reloadTableData), withObject: nil, afterDelay: 0.5)
//        }
        
        self.getData()
    }
    
    func setUpForProgressbar()  {
        self.stepper.animDuration = 0.3
        self.stepper.barColor = UIColor.lightGray
        self.stepper.tintColor = Colors.appYello
        self.stepper.numberOfSteps = 5
        self.stepper.animOption = .curveEaseIn
        self.stepper.delegate = self
        self.stepper.currentStep = 1
    }
	
    func steppedBar(_ steppedBar: CMSteppedProgressBar!, didSelect index: UInt) {
        print(index)
    }
    //MARK:- NAVIGATION
    func OpenSideMenu(_ sender: AnyObject){
        
        self.view.endEditing(true)
        toggleSideMenuView()
    }
    func PopView(_ sender: AnyObject){
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func setNavigationBar(){
        self.navigationItem.hidesBackButton = true
        let navigationViewFrame = ScreenSize.SCREEN//self.navigationController?.navigationBar.frame
        let navigationView = UIView.init(frame: CGRect(x: 0, y: 0, width: navigationViewFrame.size.width, height: 44))
        navigationView.backgroundColor = UIColor.clear
        let menuButton = UIButton.init(type: .custom)
        
        menuButton.frame = CGRect(x: ScreenSize.SCREEN_WIDTH - 45, y: 7, width: 25, height: 25)
        menuButton.setImage(UIImage.init(named: "sideMenu_Icon"), for: UIControlState())
        menuButton.addTarget(self, action: #selector(OpenSideMenu), for: .touchUpInside)
        navigationView.addSubview(menuButton)
        //        self.navigationController?.navigationBar.tintColor = Colors.appPink
        
        let titleLabel = UILabel.init(frame:CGRect(x: 60,y: 0,width: ( (navigationViewFrame.size.width) - 110),height: 40))
        titleLabel.textColor = UIColor.black
        titleLabel.text = "ADD GALLERY"
        titleLabel.font = UIFont.init(name:"OpenSans-SemiBold", size: 15)// UIFont(name:"OpenSans-SemiBold", size: 15)!
        titleLabel.textAlignment = .center
        navigationView.addSubview(titleLabel)
        
        
        
        let backButton = UIButton.init(type: .custom)
        backButton.frame = CGRect(x: 0, y: 9, width: 20, height: 20)
        backButton.backgroundColor = UIColor.white
        backButton.setImage(UIImage.init(named: "back"), for: UIControlState())
        backButton.addTarget(self, action: #selector(PopView), for: .touchUpInside)
        navigationView.addSubview(backButton)
            
        let logoImage = UIImageView.init(frame: CGRect(x: 25, y: 6, width: 33, height: 33))
        logoImage.image = UIImage.init(named: "nikon_login_logo")
        navigationView.addSubview(logoImage)
            
        self.navigationItem.titleView = navigationView
    }
    
    //MARK:- GETDATA
    func getData(){
        if Reachability.isConnectedToNetwork()
        {
            let method = "AddGalleryData?userid=\(UserDefaults.standard.integer(forKey: Constants.USERID))"
            Constants.appDelegate.startIndicator()
            Server.getRequestWithURL(Constants.BASEURL+method, completionHandler: { (response) in
                
                if response[Constants.STATE] != nil && response[Constants.STATE] as! String == Constants.SUCC {
                    
                    DispatchQueue.main.async
                    {
                        Constants.appDelegate.stopIndicator()

                        if response["UserGalleryDataCount"] != nil
                        {
                            self.userUploadedGalleryCount = "\(response["UserGalleryDataCount"]!)"
                            self.userUploadedGalleryCountIntegerValue = response["UserGalleryDataCount"]as! NSInteger
                        }
                        if response["CameraList"] != nil
                        {
                            self.cameraListArray = response["CameraList"]as! NSArray as [AnyObject]
                            let dict = ["CameraName":"Select Camera"]
                            self.cameraListArray.insert(dict as AnyObject, at: 0)
                        }
                        if response["LensList"] != nil
                        {
                            self.lensListArray = response["LensList"]as! NSArray as [AnyObject]
                            let dict = ["LenseId":"0" ,  "LenseName" : "Select Lenses" ,  "LenseType" : "Select"]
                            self.lensListArray.insert(dict as AnyObject, at: 0)
                        }
                        if response["ExposureList"] != nil
                        {
                            self.exposureListArray = response["ExposureList"]as! NSArray as [AnyObject]
                            let dict = ["ExposureId":"0" ,  "ExposureName" : "Select Aperture"]
                            self.exposureListArray.insert(dict as AnyObject, at: 0)
                        }
                        if response["ISOList"] != nil
                        {
                            self.ISOListArray = response["ISOList"]as! NSArray as [AnyObject]
                            let dict = ["IsoID":"0" ,  "IsoName" : "Select ISO"]
                            self.ISOListArray.insert(dict as AnyObject, at: 0)

                        }
                        if response["ShotOnList"] != nil
                        {
                            self.shutterSpeedListArray = response["ShotOnList"]as! NSArray as [AnyObject]
                            let dict = ["ShotOnName":"Select Shutter Speed" ,  "ShotonID" : "0"]
                            self.shutterSpeedListArray.insert(dict as AnyObject, at: 0)
                        }
                        if response["AlbumList"] != nil
                        {
                            self.albumArray = response["AlbumList"]as! NSArray as [AnyObject]
                            let dict = ["GalleryImage":"http://nikonschool.in/UploadImage/UserGallery/Still Life Photography5918.png", "GalleryMasterID" : "0", "GalleryName" : "Select Album", "GalleryType" : "0"]
                            self.albumArray.insert(dict as AnyObject, at: 0)
                        }
                        if response["OtherCameraList"] != nil
                        {
                            self.otherCameraListArray = response["OtherCameraList"]as! NSArray as [AnyObject]
                            let dict = ["CameraName":"Other Camera"]
                            self.otherCameraListArray.insert(dict as AnyObject, at: 0)
                        }
                        if response["UserGallerDataList"] != nil
                        {
                            self.userUploadedImageInWeekDataArray = response["UserGallerDataList"]as! NSArray as [AnyObject]
                            
                        }
                        DispatchQueue.main.async
                        {
                            
                            self.addMoreImageLbl.text = "You can add \(4-self.userUploadedGalleryCountIntegerValue) more images in this week"
                            if self.userUploadedGalleryCountIntegerValue == 1
                            {
                                self.stepper.currentStep = 1
                                
                            }
                            else if self.userUploadedGalleryCountIntegerValue == 2
                            {
                                self.stepper.currentStep = 2
                            }
                            else if self.userUploadedGalleryCountIntegerValue == 3
                            {
                                self.stepper.currentStep = 3
                            }
                            else if self.userUploadedGalleryCountIntegerValue == 4
                            {
                               self.stepper.currentStep = 4
                            }
                            else if self.userUploadedGalleryCountIntegerValue == 0
                            {
                                self.stepper.currentStep = 0
                            }
                            
                            self.uploadTable.reloadData()
                            self.userUploadImageCollection.reloadData()
                        }
                        
                        
                    }
                }
                else {
                   
                    //                    var mess = String()
                    //                    if response[Constants.MESS] != nil{
                    //                        mess = response[Constants.MESS] as! String
                    //                    }
                    //                    else{
                    //                        mess = response["Message"] as! String
                    //                    }
                    //
                    //                    dispatch_async(dispatch_get_main_queue())
                    //                    {
                    //                    let alert = UIAlertController.init(title: "Nikon", message: mess, preferredStyle: UIAlertControllerStyle.Alert)
                    //                    alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.Default, handler: { (action:UIAlertAction) in
                    //                        self.dismissViewControllerAnimated(true, completion: nil)
                    //                    }))
                    //                    self.presentViewController(alert, animated: true, completion: nil)
                    //                    }
                    
                    
                    
                }
            })
        }
        else
        {
            Constants.appDelegate.stopIndicator()
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
    }
    
    //MARK:- Delete api
   
    func deleteImageAPI(_ userGalleryId : String){
        if Reachability.isConnectedToNetwork()
        {
            let method = "DeleteUserGallery?UserGalleryID=\(userGalleryId)&UserID=\(UserDefaults.standard.integer(forKey: Constants.USERID))"
            Constants.appDelegate.startIndicator()
            Server.getRequestWithURL(Constants.BASEURL+method, completionHandler: { (response) in
                
                if response[Constants.STATE] != nil && response[Constants.STATE] as! String == Constants.SUCC {
                    
                
                    DispatchQueue.main.async
                    {
                        Constants.appDelegate.stopIndicator()
                        
                        var mess = String()
                        if response[Constants.MESS] != nil{
                            mess = response[Constants.MESS] as! String
                        }
                        else{
                            mess = response["Message"] as! String
                        }
                        
                        let alert = UIAlertController.init(title: "Nikon", message: mess, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                            //
                            DispatchQueue.main.async
                            {
                                
                                self.getData()
                            }
                            
                            self.dismiss(animated: true, completion: nil)
                        }))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
                else
                {
                    DispatchQueue.main.async
                    {
                        Constants.appDelegate.stopIndicator()
                        
                        var mess = String()
                        if response[Constants.MESS] != nil{
                            mess = response[Constants.MESS] as! String
                        }
                        else{
                            mess = response["Message"] as! String
                        }
                        
                        let alert = UIAlertController.init(title: "Nikon", message: mess, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                            self.dismiss(animated: true, completion: nil)
                        }))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
            })
        }
        else
        {
            Constants.appDelegate.stopIndicator()
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
    }

    //MARK:- POST API
    func postMethodToUploadUserGalleryImage()  {
        if Reachability.isConnectedToNetwork()
        {
            imageDataToSend = imageDataIMG.base64EncodedString(options: NSData.Base64EncodingOptions.lineLength64Characters)
            var param = String()
            if isShootByNikonCamera == true
            {
                param  = "emp_id=\(UserDefaults.standard.integer(forKey: Constants.USERID))&Description=\(descriptionTextView.text!)&ShotonByNikonorNot=Yes&CameraName=\(selectCameraTxt.text!)&LenseId=\(lenseID)&Exposure=\(selectApertureTxt.text!)&Shoton=\(selectShutterSpeedTxt.text!)&Iso=\(selectISOTxt.text!)&GalleryMasterID=\(albumID)"
                
            }
            else
            {
                param  = "emp_id=\(UserDefaults.standard.integer(forKey: Constants.USERID))&Description=\(descriptionTextView.text)&ShotonByNikonorNot=No&CameraName=\(selectOtherCameraTxt.text!)&LenseId=\(0)&Exposure=&Shoton=&Iso=&GalleryMasterID=\(albumID)"
                
            }
            print(param)
            param = param.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            print(param)
            let image = "&gallery_image=\(imageDataToSend)"
            let method = "UploadUserGallery"
            
            Constants.appDelegate.startIndicator()
            Server.UploadPicture(Constants.BASEURL+method, paramString: param,ImageString: image, completionHandler: { (response) in
                
                if response[Constants.STATE] != nil && response[Constants.STATE] as! String == Constants.SUCC
                {
                    DispatchQueue.main.async
                    {
                        Constants.appDelegate.stopIndicator()

                        var mess = String()
                        if response[Constants.MESS] != nil{
                            mess = response[Constants.MESS] as! String
                        }
                        else{
                            mess = response["Message"] as! String
                        }
                        
                        let alert = UIAlertController.init(title: "Nikon", message: mess, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                            //
                            DispatchQueue.main.async
                            {
                                self.clearAllTextfieldData()
                                self.getData()
                            }
                            
                            self.dismiss(animated: true, completion: nil)
                        }))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
                else
                {
                    DispatchQueue.main.async
                    {
                        Constants.appDelegate.stopIndicator()
                        
                        var mess = String()
                        if response[Constants.MESS] != nil{
                            mess = response[Constants.MESS] as! String
                        }
                        else{
                            mess = response["Message"] as! String
                        }
                        
                        let alert = UIAlertController.init(title: "Nikon", message: mess, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                            self.dismiss(animated: true, completion: nil)
                        }))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
            })
        }
        else
        {
            Constants.appDelegate.stopIndicator()
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }

    }
    
    
    //MARK:- Clear All data
    
    func clearAllTextfieldData()  {
        albumTxt.text = ""
        uploadPhotoTxt.text = ""
        descriptionTextView.text = ""
        if isShootByNikonCamera == true
        {
            selectCameraTxt.text = ""
            selectLenseTxt.text = ""
            selectApertureTxt.text = ""
            selectShutterSpeedTxt.text = ""
            selectISOTxt.text = ""
        }
        else
        {
            selectOtherCameraTxt.text = ""
        }
        albumID = "0"
        lenseID = "0"
    }
    //MARK:- resign all textfield
    func resignAllTextField()  {
        albumTxt.resignFirstResponder()
        uploadPhotoTxt.resignFirstResponder()
        descriptionTextView.resignFirstResponder()
        if isShootByNikonCamera == true
        {
            selectCameraTxt.resignFirstResponder()
            selectLenseTxt.resignFirstResponder()
            selectApertureTxt.resignFirstResponder()
            selectShutterSpeedTxt.resignFirstResponder()
            selectISOTxt.resignFirstResponder()
        }
        else
        {
            selectOtherCameraTxt.resignFirstResponder()
        }

    }

    //MARK:- IMAGEPICKERDELEGATE
    func openGallary()
    {
        imagePicker!.allowsEditing = false
        imagePicker!.sourceType = UIImagePickerControllerSourceType.photoLibrary
        present(imagePicker!, animated: true, completion: nil)
    }
    
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)){
            imagePicker!.allowsEditing = true
            imagePicker!.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker!.cameraCaptureMode = .photo
            present(imagePicker!, animated: true, completion: nil)
        }else{
            let alert = UIAlertController(title: "Camera Not Found", message: "This device has no Camera", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style:.default, handler: nil)
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
        }
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
       
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        //userPickedImage = chosenImage
        uploadPhotoTxt.text = String(describing: info[UIImagePickerControllerReferenceURL] as! URL)
        imageDataIMG = UIImagePNGRepresentation(chosenImage)! as Data
        let imageSize: Int = imageDataIMG.count
        let size = imageSize/1024
        // print(size)
        if size > 2048
        {
            isPickedImage = false
            DispatchQueue.main.async
            {
                self.perform(#selector(self.imageAlert), with: nil, afterDelay: 0.5)
            }
        }
        else
        {
            isPickedImage = true
            
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    func imageAlert()
    {
        let alert = UIAlertController.init(title: "Nikon", message: "Please select image of size less than 2 MB", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
            self.dismiss(animated: true, completion: nil)
        }))
        self.present(alert, animated: true, completion: nil)
        
        
    }


    //MARK:- Reload Table data
    
    func reloadTableData()  {
        self.uploadTable.reloadData()
    }
    func reloadCollectionData()  {
        self.userUploadImageCollection.reloadData()
    }

    
    //MARK:- Tableview
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isShootByNikonCamera == true
        {
            return 2
        }
        else
        {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        tableView.estimatedRowHeight = 260
        tableView.rowHeight = UITableViewAutomaticDimension
        
        if isShootByNikonCamera == true
        {
            if indexPath.row == 0
            {
                let nikonCameraCell = tableView.dequeueReusableCell(withIdentifier: "DSLRCameraUploadCell")as! DSLRCameraUploadCell
            
                self.selectCameraTxt = nikonCameraCell.selectCameraTxt
                self.selectLenseTxt = nikonCameraCell.selectLenseTxt
                
                self.selectCameraTxt.delegate = self
                self.selectLenseTxt.delegate = self
                
                self.selectCameraTxt.inputView = picker
                self.selectLenseTxt.inputView = picker
                
                nikonCameraCell.yesBtn.addTarget(self, action: #selector(yesBtnClicked), for: .touchUpInside)
                nikonCameraCell.noBtn.addTarget(self, action: #selector(noBtnClicked), for: .touchUpInside)
                
                nikonCameraCell.selectionStyle = .none
                return nikonCameraCell
            }
            else
            {
                let exposureCell = tableView.dequeueReusableCell(withIdentifier: "ExposureUploadCell")as! ExposureUploadCell
                
                self.selectApertureTxt = exposureCell.selectApertureTxt
                self.selectShutterSpeedTxt = exposureCell.selectShutterSpeedTxt
                self.selectISOTxt = exposureCell.selectISOTxt
                
                self.selectApertureTxt.delegate = self
                self.selectShutterSpeedTxt.delegate = self
                self.selectISOTxt.delegate = self
                
                self.selectApertureTxt.inputView = picker
                self.selectShutterSpeedTxt.inputView = picker
                self.selectISOTxt.inputView = picker
                
                exposureCell.selectionStyle = .none
                return exposureCell
                
            }
        }
        else
        {
            let otherCameraCell = tableView.dequeueReusableCell(withIdentifier: "OtherCameraUploadCell")as! OtherCameraUploadCell
            
            self.selectOtherCameraTxt = otherCameraCell.selectOtherCameraTxt
            self.selectOtherCameraTxt.delegate = self
            self.selectOtherCameraTxt.inputView = picker
            
            otherCameraCell.yesBtn.addTarget(self, action: #selector(yesBtnClicked), for: .touchUpInside)
            otherCameraCell.noBtn.addTarget(self, action: #selector(noBtnClicked), for: .touchUpInside)
            
            otherCameraCell.selectionStyle = .none
            return otherCameraCell
        }
        
        
    }
    
    //MARK:- COllection View
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func numberOfSectionsInCollectionView(_ collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAtIndexPath indexPath: IndexPath) -> UICollectionViewCell {
      
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "UserGalleryUploadedCollectionCell", for: indexPath)as! UserGalleryUploadedCollectionCell
        
        if  indexPath.row <= (self.userUploadedImageInWeekDataArray.count - 1)
        {
            var newURL = self.userUploadedImageInWeekDataArray[indexPath.row]["Gallery_Thumb_Img"] as! String
            newURL = newURL.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            cell.uploadImageView.sd_setImage(with: URL.init(string: newURL), placeholderImage: UIImage.init(named: "product_placeholder.jpg"))
            cell.uploadImageView.setShowActivityIndicator(true)
            cell.uploadImageView.setIndicatorStyle(.gray)
            let statusStr = self.userUploadedImageInWeekDataArray[indexPath.row]["ApprovedorRejectStatus"] as! String
            cell.statusLbl.text = statusStr.capitalized
            cell.addImageView.isHidden = true
            cell.deleteBtn.tag = indexPath.row
            cell.deleteBtn.addTarget(self, action: #selector(deleteImageBtnClicked), for: .touchUpInside )
        }
        else
        {
            cell.addImageView.isHidden = false
        }
            return cell
       
    }

    
    //MARK:- picker delegate
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
       
        if selectedTextfieldValue == textfieldName.selectAlbumTag.rawValue
        {
            return albumArray.count
        }
        else if selectedTextfieldValue == textfieldName.selectCameraTag.rawValue
        {
            return cameraListArray.count
        }
        else if selectedTextfieldValue == textfieldName.selectLenseTag.rawValue
        {
            return lensListArray.count
        }
        else if selectedTextfieldValue == textfieldName.selectOtherCameraTag.rawValue
        {
            return otherCameraListArray.count
        }
        else if selectedTextfieldValue == textfieldName.selectApertureTag.rawValue
        {
            return exposureListArray.count
        }
        else if selectedTextfieldValue == textfieldName.selectShutterSpeedTag.rawValue
        {
            return shutterSpeedListArray.count
        }
        else if selectedTextfieldValue == textfieldName.selectISOTag.rawValue
        {
            return ISOListArray.count
        }
        else
        {
            return 0
        }
        
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        
        if selectedTextfieldValue == textfieldName.selectAlbumTag.rawValue
        {
            let value = albumArray [row]["GalleryName"] as! String
            return String.init(format: value , locale: nil)
        }
        else if selectedTextfieldValue == textfieldName.selectCameraTag.rawValue
        {
            let value = cameraListArray [row]["CameraName"] as! String
            return String.init(format: value , locale: nil)

        }
        else if selectedTextfieldValue == textfieldName.selectLenseTag.rawValue
        {
            let value = lensListArray [row]["LenseName"] as! String
            return String.init(format: value , locale: nil)
 
        }
        else if selectedTextfieldValue == textfieldName.selectOtherCameraTag.rawValue
        {
            let value = otherCameraListArray [row]["CameraName"] as! String
            return String.init(format: value , locale: nil)
            
        }
        else if selectedTextfieldValue == textfieldName.selectApertureTag.rawValue
        {
            let value = exposureListArray [row]["ExposureName"] as! String
            return String.init(format: value , locale: nil)

        }
        else if selectedTextfieldValue == textfieldName.selectShutterSpeedTag.rawValue
        {
            let value = shutterSpeedListArray [row]["ShotOnName"] as! String
            return String.init(format: value , locale: nil)
        }
        else if selectedTextfieldValue == textfieldName.selectISOTag.rawValue
        {
            let value = ISOListArray [row]["IsoName"] as! String
            return String.init(format: value , locale: nil)
        }
        else
        {
            return ""
        }

        
        
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if selectedTextfieldValue == textfieldName.selectAlbumTag.rawValue
        {
            let value = albumArray [row]["GalleryName"] as! String
            self.albumTxt.text = value
            
            albumID = "\(albumArray [row]["GalleryMasterID"]!!)"
        }
        else if selectedTextfieldValue == textfieldName.selectCameraTag.rawValue
        {
            let value = cameraListArray [row]["CameraName"] as! String
            self.selectCameraTxt.text = value
            
        }
        else if selectedTextfieldValue == textfieldName.selectLenseTag.rawValue
        {
            let value = lensListArray [row]["LenseName"] as! String
            self.selectLenseTxt.text = value
            
            lenseID = "\(lensListArray [row]["LenseId"]!!)"
            
        }
        else if selectedTextfieldValue == textfieldName.selectOtherCameraTag.rawValue
        {
            let value = otherCameraListArray [row]["CameraName"] as! String
            self.selectOtherCameraTxt.text = value

            
        }
        else if selectedTextfieldValue == textfieldName.selectApertureTag.rawValue
        {
            let value = exposureListArray [row]["ExposureName"] as! String
            self.selectApertureTxt.text = value

            
        }
        else if selectedTextfieldValue == textfieldName.selectShutterSpeedTag.rawValue
        {
            let value = shutterSpeedListArray [row]["ShotOnName"] as! String
            self.selectShutterSpeedTxt.text = value

        }
        else if selectedTextfieldValue == textfieldName.selectISOTag.rawValue
        {
            let value = ISOListArray [row]["IsoName"] as! String
            self.selectISOTxt.text = value

        }
        

       
    }
    
    //MARK:- TextVIew Delegate
    func textViewDidBeginEditing(_ textView: UITextView) {
        textView.text = ""
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == ""
        {
            textView.text = "Description"
        }
    }
    
    //MARK:- textfield delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.selectedTextfieldValue = textField.tag
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        textField.resignFirstResponder()
        DispatchQueue.main.async
        {
            self.picker.reloadAllComponents()
            self.picker.selectRow(0, inComponent: 0, animated: true)
        }
        
    }

    
    //MARK:- Action
    
    func deleteImageBtnClicked(_ sender : UIButton)  {
        print(sender.tag)
        
        let userGalleryID = "\(userUploadedImageInWeekDataArray[sender.tag]["UserGalleryID"]!!)"
        let alert = UIAlertController.init(title: "Nikon", message: "Are you sure want to delete?", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
            //
            DispatchQueue.main.async
            {
                self.deleteImageAPI(userGalleryID)
            }
            
            
            self.dismiss(animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction.init(title: "Cancel", style: UIAlertActionStyle.cancel, handler: { (action:UIAlertAction) in
            //
            
            
            self.dismiss(animated: true, completion: nil)
        }))
        self.present(alert, animated: true, completion: nil)
        

        
    }
    func yesBtnClicked()  {
        isShootByNikonCamera = true
        DispatchQueue.main.async
        {
            self.reloadTableData()
        }
    }
    
    func noBtnClicked()  {
        
        isShootByNikonCamera = false
        DispatchQueue.main.async
        {
            self.reloadTableData()
        }
        
    }
    
    @IBAction func browseBtnClick(_ sender: AnyObject)
    {
        self.resignAllTextField()
        let optionMenu = UIAlertController(title: nil, message: "Choose Option", preferredStyle: .actionSheet)
        
        // 2
//        let cameraAction = UIAlertAction(title: "Open Camera", style: .Default, handler: {
//            (alert: UIAlertAction!) -> Void in
//            self.openCamera()
//            
//        })
        let galleryAction = UIAlertAction(title: "Open Gallery", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.openGallary()
            
        })
        
        //
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            
        })
        
        
        // 4
       // optionMenu.addAction(cameraAction)
        optionMenu.addAction(galleryAction)
        optionMenu.addAction(cancelAction)
        
        // 5
        self.present(optionMenu, animated: true, completion: nil)
    }

    @IBAction func submitBtnClicked(_ sender: AnyObject) {
        
        self.resignAllTextField()
        
        if userUploadedGalleryCountIntegerValue == 4
        {
            let alert = UIAlertController.init(title: "Nikon", message: "You can't upload more than four images in a week.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                self.dismiss(animated: true, completion: nil)
            }))
            self.present(alert, animated: true, completion: nil)
            
            return

        }
        
        if albumTxt.text == ""  || albumTxt.text == "Select Album" || albumID == "0" {
            let alert = UIAlertController.init(title: "Nikon", message: "Please select album", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                self.dismiss(animated: true, completion: nil)
            }))
            self.present(alert, animated: true, completion: nil)
            
        }
        else if isPickedImage == false
        {
            let alert = UIAlertController.init(title: "Nikon", message: "Please select image", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                self.dismiss(animated: true, completion: nil)
            }))
            self.present(alert, animated: true, completion: nil)
        }
        else if descriptionTextView.text == "" || descriptionTextView.text == "Description*"
        {
            let alert = UIAlertController.init(title: "Nikon", message: "Please enter description", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                self.dismiss(animated: true, completion: nil)
            }))
            self.present(alert, animated: true, completion: nil)
        }
        else if isShootByNikonCamera == true
        {
            if selectCameraTxt.text == ""  || selectCameraTxt.text == "Select Camera" {
                let alert = UIAlertController.init(title: "Nikon", message: "Please select camera", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                    self.dismiss(animated: true, completion: nil)
                }))
                self.present(alert, animated: true, completion: nil)
                
            }
            else if selectLenseTxt.text == ""  || selectLenseTxt.text == "Select Lenses" || lenseID == "0" {
                let alert = UIAlertController.init(title: "Nikon", message: "Please select lense", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                    self.dismiss(animated: true, completion: nil)
                }))
                self.present(alert, animated: true, completion: nil)
                
            }
            else if selectApertureTxt.text == ""  || selectApertureTxt.text == "Select Aperture" {
                let alert = UIAlertController.init(title: "Nikon", message: "Please select aperture", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                    self.dismiss(animated: true, completion: nil)
                }))
                self.present(alert, animated: true, completion: nil)
                
            }
            else if selectShutterSpeedTxt.text == ""  || selectShutterSpeedTxt.text == "Select Shutter Speed" {
                let alert = UIAlertController.init(title: "Nikon", message: "Please select shutter speed", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                    self.dismiss(animated: true, completion: nil)
                }))
                self.present(alert, animated: true, completion: nil)
                
            }
            else if selectISOTxt.text == ""  || selectISOTxt.text == "Select ISO" {
                let alert = UIAlertController.init(title: "Nikon", message: "Please select ISO", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                    self.dismiss(animated: true, completion: nil)
                }))
                self.present(alert, animated: true, completion: nil)
                
            }
            else
            {
                self.postMethodToUploadUserGalleryImage()
            }

        }
        else
        {
            if selectOtherCameraTxt.text == ""  || selectOtherCameraTxt.text == "Other Camera" {
                let alert = UIAlertController.init(title: "Nikon", message: "Please select other camera", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                    self.dismiss(animated: true, completion: nil)
                }))
                self.present(alert, animated: true, completion: nil)
                
            }
            else
            {
                self.postMethodToUploadUserGalleryImage()
                
            }

        }

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
