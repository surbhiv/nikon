//
//  UserGalleryUploadedCollectionCell.swift
//  NikonApp
//
//  Created by Hitesh Dhawan on 22/06/17.
//  Copyright © 2017 Neuronimbus. All rights reserved.
//

import UIKit

class UserGalleryUploadedCollectionCell: UICollectionViewCell {
    @IBOutlet weak var uploadImageView: UIImageView!
    @IBOutlet weak var statusLbl: UILabel!
    @IBOutlet weak var deleteBtn: UIButton!
    @IBOutlet weak var addImageView: UIView!
    
}
