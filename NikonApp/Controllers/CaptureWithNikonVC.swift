//
//  CaptureWithNikonVC.swift
//  NikonApp
//
//  Created by Arnab Maity on 10/02/20.
//  Copyright © 2020 Neuronimbus. All rights reserved.
//

import UIKit

class CaptureWithNikonVC: UIViewController {

	@IBOutlet weak var captureWithNikonWebView: UIWebView!
	
	
	override func viewDidLoad() {
        super.viewDidLoad()
		captureWithNikonWebView.delegate = self
		let url = URL(string: "https://www.capturewithnikon.in/")
		let request = URLRequest(url: url!)
		captureWithNikonWebView.loadRequest(request)
	}
}

extension CaptureWithNikonVC: UIWebViewDelegate {
	func webViewDidFinishLoad(_ webView: UIWebView) {
		print("Finished loading")
	}
}
