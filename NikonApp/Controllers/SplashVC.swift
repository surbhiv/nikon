//
//  SplashVC.swift
//  NikonApp
//
//  Created by Surbhi on 22/03/17.
//  Copyright © 2017 Neuronimbus. All rights reserved.
//

import UIKit

class SplashVC: UIViewController{
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if (UserDefaults.standard.integer(forKey: Constants.USERID) != 0) {
            goToDashboard()
        }
        else
        {
            goToLogin()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
	
	
	
	
	
    //change on 5 April
    func goToDashboard(){
        let vc = Constants.mainStoryboard.instantiateViewController(withIdentifier: "Dashboard")
        sideMenuController()?.setContentViewController(vc)
    }
    
//    func goToDashboard(){
//        let home = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("Dashboard")
//        let destinationController = ENSideMenuNavigationController.init(menuViewController: MenuViewController(), contentViewController: home)//Constants.mainStoryboard.instantiateViewControllerWithIdentifier("SideMenuNavigation") as! ENSideMenuNavigationController
//        
//        Constants.appDelegate.window?.makeKeyAndVisible()
//        Constants.appDelegate.window?.rootViewController = destinationController
//        sideMenuController()?.setContentViewController(home)
//    }

//    func goToLogin(){
//        
//        let vc = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("LoginVC")
//        let naviController = UINavigationController.init(rootViewController: vc)
//        Constants.appDelegate.window?.makeKeyAndVisible()
//        Constants.appDelegate.window?.rootViewController = naviController
//
//    }
    func goToLogin(){
        let vc = Constants.mainStoryboard.instantiateViewController(withIdentifier: "LoginVC")
        self.navigationController!.pushViewController(vc, animated: false)
    }

}

extension UIViewController {
	func hideKeyboardWhenTappedAround() {
		let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
		tap.cancelsTouchesInView = false
		view.addGestureRecognizer(tap)
	}
	
	@objc func dismissKeyboard() {
		view.endEditing(true)
	}
}
