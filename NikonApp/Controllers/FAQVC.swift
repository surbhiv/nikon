//
//  FAQVC.swift
//  NikonApp
//
//  Created by Hitesh Dhawan on 14/03/17.
//  Copyright © 2017 Neuronimbus. All rights reserved.
//

import UIKit

class FAQVC: UIViewController,UITableViewDelegate,UITableViewDataSource,UIPickerViewDelegate,UIPickerViewDataSource,UITextFieldDelegate {

    @IBOutlet weak var faqTable: UITableView!
    @IBOutlet weak var queryTextfield: UITextField!
    @IBOutlet weak var searchTextfield: UITextField!
    
    
    var faqArray = [AnyObject]()
    var queryTypeArray = [AnyObject]()
    var Searchtext = String()
    var QueryType = String()
    var picker = UIPickerView()
    var isFromMenu = Bool()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationController?.isNavigationBarHidden = false
        self.setNavigationBar()
        var  dict = Dictionary<String,AnyObject>()
        dict = ["ContactUsQueryType":"Select query type" as AnyObject,"ContactUsQueryTypeID":0 as AnyObject]
        self.queryTypeArray.append(dict as AnyObject)
        
        self.picker = UIPickerView.init()
        self.picker.dataSource = self
        self.picker.delegate = self
        
        self.Searchtext = ""
        self.queryTextfield.inputView = picker
        self.QueryType = ""
        
        self.getQueryTypeData()
        self.getFAQData()
        
        
    }
    
    //MARK:- NAVIGATION
    func OpenSideMenu(_ sender: AnyObject){
        
        self.view.endEditing(true)
        toggleSideMenuView()
    }
    func PopView(_ sender: AnyObject){
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func setNavigationBar(){
        self.navigationItem.hidesBackButton = true
        let navigationViewFrame = ScreenSize.SCREEN//self.navigationController?.navigationBar.frame
        let navigationView = UIView.init(frame: CGRect(x: 0, y: 0, width: navigationViewFrame.size.width, height: 44))
        navigationView.backgroundColor = UIColor.clear
        let menuButton = UIButton.init(type: .custom)
        
        menuButton.frame = CGRect(x: ScreenSize.SCREEN_WIDTH - 45, y: 7, width: 25, height: 25)
        menuButton.setImage(UIImage.init(named: "sideMenu_Icon"), for: UIControlState())
        menuButton.addTarget(self, action: #selector(OpenSideMenu), for: .touchUpInside)
        navigationView.addSubview(menuButton)
//        self.navigationController?.navigationBar.tintColor = Colors.appPink
        
        let titleLabel = UILabel.init(frame:CGRect(x: 60,y: 0,width: ( (navigationViewFrame.size.width) - 110),height: 40))
        titleLabel.textColor = UIColor.black
        titleLabel.text = "FAQ's"
        titleLabel.font = UIFont.init(name:"OpenSans-SemiBold", size: 15)// UIFont(name:"OpenSans-SemiBold", size: 15)!
        titleLabel.textAlignment = .center
        navigationView.addSubview(titleLabel)
        
//        let logoImage = UIImageView.init(frame: CGRectMake(25, 6, 33, 33))
//        logoImage.image = UIImage.init(named: "nikon_login_logo")
//        navigationView.addSubview(logoImage)
        
        if isFromMenu == false
        {
            let backButton = UIButton.init(type: .custom)
            backButton.frame = CGRect(x: 0, y: 9, width: 20, height: 20)
            backButton.backgroundColor = UIColor.white
            backButton.setImage(UIImage.init(named: "back"), for: UIControlState())
            backButton.addTarget(self, action: #selector(PopView), for: .touchUpInside)
            navigationView.addSubview(backButton)
            
            let logoImage = UIImageView.init(frame: CGRect(x: 25, y: 6, width: 33, height: 33))
            logoImage.image = UIImage.init(named: "nikon_login_logo")
            navigationView.addSubview(logoImage)
            
            
        }
        else
        {
            let logoImage = UIImageView.init(frame: CGRect(x: 0, y: 6, width: 33, height: 33))
            logoImage.image = UIImage.init(named: "nikon_login_logo")
            navigationView.addSubview(logoImage)
        }

        //}

        
        self.navigationItem.titleView = navigationView
        
    }


    @IBAction func GoButtonClicked(_ sender: AnyObject)
    {
        self.getFAQData()

    }
    //MARK:- API
    func getQueryTypeData()  {
        if Reachability.isConnectedToNetwork()
        {
          
            
            var method = "Faq"
            method = method.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            
            Constants.appDelegate.startIndicator()
            Server.getRequestWithURL(Constants.BASEURL+method, completionHandler: { (response) in
                
                
                if response[Constants.STATE] != nil && response[Constants.STATE] as! String == Constants.SUCC
                {
                    DispatchQueue.main.async
                    {
                        Constants.appDelegate.stopIndicator()
                        let tempArray = response["GetFaqType"]as! NSArray as [AnyObject]
                        for dict in tempArray
                        {
                             self.queryTypeArray.append(dict)
                            
                        }
                    }
                }
                else
                {
                    DispatchQueue.main.async
                    {
                        Constants.appDelegate.stopIndicator()
                    }
                }
            })
        }
        else
        {
            Constants.appDelegate.stopIndicator()
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
    }
    
    
    func getFAQData()  {
        if Reachability.isConnectedToNetwork()
        {
            
            
            var method = "Faq?Searchtext=\(self.Searchtext)&QueryType=\(self.QueryType)"
            method = method.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            
            Constants.appDelegate.startIndicator()
            Server.getRequestWithURL(Constants.BASEURL+method, completionHandler: { (response) in
                
                
                if response[Constants.STATE] != nil && response[Constants.STATE] as! String == Constants.SUCC
                {
                    print(response)
                    //let responseDict = response as! NSDictionary
                    
                    DispatchQueue.main.async
                    {
                        Constants.appDelegate.stopIndicator()
                        self.faqArray = response["Faqlist"]as! NSArray as [AnyObject]
                        
                        self.faqTable.reloadData()
                    }
                }
                else
                {
                    DispatchQueue.main.async
                    {
                        Constants.appDelegate.stopIndicator()
                        if self.faqArray.count > 0
                        {
                            self.faqArray.removeAll()
                        }
                        self.faqTable.reloadData()
                        
                    }
                }
                
                
            })
        }
        else
        {
            Constants.appDelegate.stopIndicator()
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
        
        
    }


    
    
    //MARK:- TABLEVIEW
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return faqArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FAQCell", for: indexPath)as! FAQCell
        let questionStr = self.faqArray[indexPath.row]["Question"] as? String
        let answerStr = self.faqArray[indexPath.row]["Answer"] as? String
        
        do
        {
            let atributedQuestionStr = try NSAttributedString.init(data: questionStr!.data(using: String.Encoding.unicode)!, options: [ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType ], documentAttributes: nil)
//            print(atributedStr)
            cell.questionLbl.attributedText = atributedQuestionStr
                    }
        catch
        {
            print("error occure")
            
        }
        
        do
        {
            let atributedAnswerStr = try NSAttributedString.init(data: answerStr!.data(using: String.Encoding.unicode)!, options: [ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType ], documentAttributes: nil)
            //            print(atributedStr)
            cell.answerLbl.attributedText = atributedAnswerStr
        }
        catch
        {
            print("error occure")
            
        }

        tableView.estimatedRowHeight = 45
        tableView.rowHeight = UITableViewAutomaticDimension

        cell.selectionStyle = .none
        return cell
    }
    
    
    //MARK:- picker delegate
    
    //MARK: - PICKEVIEW for date
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
    
        return queryTypeArray.count
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
           let value = queryTypeArray [row]["ContactUsQueryType"] as! String
            return String.init(format: value , locale: nil)
       
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        let value = queryTypeArray [row]["ContactUsQueryTypeID"] as! Int
        let value1 = queryTypeArray [row]["ContactUsQueryType"] as! String
        if value == 0
        {
            QueryType = ""
        }
        else
        {
            QueryType = String(value)
        }
        self.queryTextfield.text = value1
    }
    
    // MARK: TEXTFIELD DELEGATE
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        textField .resignFirstResponder()
        self.Searchtext = self.searchTextfield.text!
        
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
