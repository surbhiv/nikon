//
//  ProfileViewController.swift
//  NikonApp
//
//  Created by Hitesh Dhawan on 28/02/17.
//  Copyright © 2017 Neuronimbus. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController,updateProtocol {

    @IBOutlet weak var profileScrollView: UIScrollView!
    @IBOutlet weak var contentScrollView: UIView!
    
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var pointLbl: UILabel!
 
    @IBOutlet weak var firstNameLbl: UILabel!
    @IBOutlet weak var emailLbl: UILabel!
    @IBOutlet weak var cityLbl: UILabel!
    @IBOutlet weak var genderLbl: UILabel!
    @IBOutlet weak var dobLbl: UILabel!
    @IBOutlet weak var titleValueLbl: UILabel!
    @IBOutlet weak var phoneNumberLbl: UILabel!
    @IBOutlet weak var lastNameLbl: UILabel!
    @IBOutlet weak var stateNameLbl: UILabel!
    @IBOutlet weak var pincodeLbl: UILabel!
    @IBOutlet weak var addressLbl: UILabel!
   
    @IBOutlet weak var totalHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var heightConstraintForScrollView: NSLayoutConstraint!
    
    var userProfileDataDict = Dictionary<String,AnyObject>()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationController?.isNavigationBarHidden = false
        self.setNavigationBar()
        self.userImageView.layer.cornerRadius = 40
        self.userImageView.clipsToBounds = true
        self.userImageView.layer.borderWidth = 2
        self.userImageView.layer.borderColor = UIColor.black.cgColor
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
        if (UserDefaults.standard.integer(forKey: Constants.USERID) != 0) {
            self.getUserDetail(String(UserDefaults.standard.integer(forKey: Constants.USERID)) as NSString)
            
        }
    }
   //MARK:- DELEGATE TO UPDATE SIDE MENU
    func updateProfileMenu()
   
   {
    let sideMenuObj = sideMenuController()?.sideMenu?.menuViewController as! MenuViewController
    sideMenuObj.getUserDetail(String(UserDefaults.standard.integer(forKey: Constants.USERID)) as NSString)
    
    }

    
    //MARK:- NAVIGATION
    func OpenSideMenu(_ sender: AnyObject){
        
        self.view.endEditing(true)
        toggleSideMenuView()
       
    }
    func PopView(_ sender: AnyObject){
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func setNavigationBar(){
        self.navigationItem.hidesBackButton = true
        let navigationViewFrame = ScreenSize.SCREEN//self.navigationController?.navigationBar.frame
        let navigationView = UIView.init(frame: CGRect(x: 0, y: 0, width: navigationViewFrame.size.width, height: 44))
        navigationView.backgroundColor = UIColor.clear
        let menuButton = UIButton.init(type: .custom)
        
        menuButton.frame = CGRect(x: ScreenSize.SCREEN_WIDTH - 45, y: 7, width: 25, height: 25)
        menuButton.setImage(UIImage.init(named: "sideMenu_Icon"), for: UIControlState())
        menuButton.addTarget(self, action: #selector(OpenSideMenu), for: .touchUpInside)
        navigationView.addSubview(menuButton)
//        self.navigationController?.navigationBar.tintColor = Colors.appPink
        
        let titleLabel = UILabel.init(frame:CGRect(x: 60,y: 0,width: ( (navigationViewFrame.size.width) - 110),height: 40))
        titleLabel.textColor = UIColor.black
        titleLabel.text = "MY PROFILE"
        titleLabel.font = UIFont(name:"OpenSans-SemiBold", size: 15)!
        titleLabel.textAlignment = .center
        navigationView.addSubview(titleLabel)
        
        let logoImage = UIImageView.init(frame: CGRect(x: 0, y: 6, width: 33, height: 33))
        logoImage.image = UIImage.init(named: "nikon_login_logo")
        navigationView.addSubview(logoImage)

        self.navigationItem.titleView = navigationView
        
    }

    
    //MARK:- GET USER DETAIL API
    func getUserDetail(_ userId : NSString)  {
        if Reachability.isConnectedToNetwork()
        {
            let method = "getuserprofile?emp_id=\(userId)"
            
            Constants.appDelegate.startIndicator()
            Server.getRequestWithURL(Constants.BASEURL+method, completionHandler: { (response) in
                
                
                // if response[Constants.STATE] != nil && response[Constants.STATE] as! String == Constants.SUCC {

                let responseDict = response // as NSDictionary
                
                DispatchQueue.main.async
                {
                    Constants.appDelegate.stopIndicator()
                    let userName = "\(responseDict["User_Title"] as! String) \(responseDict["User_Fname"] as! String) \(responseDict["User_Lname"] as! String)"
                    self.userProfileDataDict = responseDict
                    let titleVale = responseDict["User_Title"] as! String
                    let firstName = responseDict["User_Fname"] as! String
                    let lastName = responseDict["User_Lname"] as! String
                    let email = responseDict["User_Email"] as! String
                    let mobileNumber = responseDict["User_Phone"] as! String
                    let cityName = responseDict["City_name"] as! String
                    let stateName = responseDict["State_name"] as! String
                    let gender = responseDict["User_Gender"] as! String
                    let postalCode = responseDict["User_Pincode"] as! String
                    let dob = responseDict["User_Dob"] as! String
                    let address = "\(responseDict["User_Address"] as! String) \(responseDict["State_name"] as! String)"
                    let profileimage = responseDict["ProfileImage"] as! String
                    let totalPoint = String(responseDict["TotalPoint"] as! Int)

                    
                   
                    DispatchQueue.main.async
                    {
                        self.titleValueLbl.text = titleVale
                        self.firstNameLbl.text = firstName
                        self.lastNameLbl.text = lastName
                        self.emailLbl.text = email
                        self.phoneNumberLbl.text = mobileNumber
                        self.cityLbl.text = cityName
                        self.stateNameLbl.text = stateName
                        self.genderLbl.text = gender
                        self.pincodeLbl.text = postalCode
                        self.dobLbl.text = Helper.getDOBStringFromDate(dob)//dob
                        self.addressLbl.text = address
                        self.userImageView.sd_setImage(with: URL.init(string: profileimage), placeholderImage: UIImage.init(named: "profilePlaceHolder"))
                        self.pointLbl.text = "You have \(totalPoint) Points"
                        self.userNameLbl.text = userName
                        
                        //self.getContestData(email)
                    }
                    
                    
                }
                
                
            })
        }
        else
        {
            Constants.appDelegate.stopIndicator()
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
        
        
    }
    /*
    //MARK:- GETCONTEST DATA
    func getContestData(emailId : NSString){
        if Reachability.isConnectedToNetwork()
        {
            let method = "personal_info.php?useremail=\(emailId)"
            Constants.appDelegate.startIndicator()
            Server.getRequestWithURL(Constants.IAMNIKONBASEURL+method, completionHandler: { (response) in
                
                Constants.appDelegate.stopIndicator()
                if response[Constants.STATE] != nil && response[Constants.STATE] as! String == Constants.SUCC
                {
                    dispatch_async(dispatch_get_main_queue())
                    {
                    self.levelLbl.text = response["level"]as? String
                    self.photographGenreLbl.text = response["user_genre"]as? String
                    self.cameraLbl.text = response["user_camara"]as? String
                    let fon = UIFont.init(name: "OpenSans", size: 13.0)

                    self.heightContraintForOtherInformationView.constant = 170+(Helper.getHeightForText((response["user_genre"]as? String)!, fon: fon!))/2+(Helper.getHeightForText((response["user_camara"]as? String)!, fon: fon!))/2
                        self.totalHeightConstraint.constant = 480 + self.heightContraintForOtherInformationView.constant
                    self.heightConstraintForGenre.constant = (Helper.getHeightForText((response["user_genre"]as? String)!, fon: fon!))/2+80
                        
                    self.heightConstraintForScrollView.constant = self.totalHeightConstraint.constant+270
                    }
                }
            })
        }
        else
        {
            Constants.appDelegate.stopIndicator()
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
    }
   */
    @IBAction func editProfileButtonClick(_ sender: AnyObject) {
        let destViewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "EditProfileVC")as! EditProfileVC
        destViewController.userProfileDataDict = self.userProfileDataDict
        destViewController.updateDelegate = self
        self.navigationController?.pushViewController(destViewController, animated: true)

    }
    @IBAction func knowMoreButtonClick(_ sender: AnyObject) {
        
        let dest = Constants.mainStoryboard.instantiateViewController(withIdentifier: "ProfileKnowMoreVC")
        self.navigationController?.pushViewController(dest, animated: true)
    }
    @IBAction func redeemWorkShopButtonClick(_ sender: AnyObject) {
        let destViewController = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "WorkshopVC")
        self.navigationController?.pushViewController(destViewController, animated: true)
    }
    @IBAction func redeemGoodiesButtonClick(_ sender: AnyObject) {
        let destViewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "LoyaltyScrollVC") as! LoyaltyScrollVC
        destViewController.selectedSection = 2
        self.navigationController?.pushViewController(destViewController, animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
