//
//  ChangePasswordVC.swift
//  NikonApp
//
//  Created by Hitesh Dhawan on 03/04/17.
//  Copyright © 2017 Neuronimbus. All rights reserved.
//

import UIKit
import GGLSignIn
import GoogleSignIn
import Google
import Firebase
import FirebaseCore
import FBSDKCoreKit
import FBSDKLoginKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func <= <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l <= r
  default:
    return !(rhs < lhs)
  }
}



class ChangePasswordVC: UIViewController {

    @IBOutlet weak var oldPassTextField: UITextField!
    @IBOutlet weak var newPassTextField: UITextField!
    @IBOutlet weak var cofirmPassTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func okBtnClicked(_ sender: AnyObject)
    {
        if oldPassTextField.text == ""
        {
            let alert = UIAlertController.init(title: "Nikon", message: "Please enter old password", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                self.dismiss(animated: true, completion: nil)
            }))
            self.present(alert, animated: true, completion: nil)
            
        }
        else if oldPassTextField.text?.characters.count <= 5
                {
                    let alert = UIAlertController.init(title: "Nikon", message: "Please enter minimum 6 characters for old password", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                        self.dismiss(animated: true, completion: nil)
                    }))
                    self.present(alert, animated: true, completion: nil)
                    
                }

//        else if !Helper.isValidPassword(oldPassTextField.text!)
//        {
//            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "Nikon", "message": "Please enter old password with atleast 1 alphabet, 1 number and length should be minimum 6"] )
//            //oldPassTextField.becomeFirstResponder()
//        }
        else if newPassTextField.text == ""
        {
            let alert = UIAlertController.init(title: "Nikon", message: "Please enter new password", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                self.dismiss(animated: true, completion: nil)
            }))
            self.present(alert, animated: true, completion: nil)
            
        }
        else if !Helper.isValidPassword(newPassTextField.text!)
        {
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "Nikon", "message": "Please enter new password with atleast 1 alphabet, 1 number and length should be minimum 6"] )
            //oldPassTextField.becomeFirstResponder()
        }

//        else if newPassTextField.text?.characters.count <= 5
//        {
//            let alert = UIAlertController.init(title: "Nikon", message: "Please enter minimum 6 characters", preferredStyle: UIAlertControllerStyle.Alert)
//            alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.Default, handler: { (action:UIAlertAction) in
//                self.dismissViewControllerAnimated(true, completion: nil)
//            }))
//            self.presentViewController(alert, animated: true, completion: nil)
//            
//        }
        else if newPassTextField.text != cofirmPassTextField.text
        {
            let alert = UIAlertController.init(title: "Nikon", message: "Confirm password does not match with new password.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                self.dismiss(animated: true, completion: nil)
            }))
            self.present(alert, animated: true, completion: nil)
            
        }
        else
        {
            self.changePassWordAPI()
        }
    }
   
    @IBAction func cancelBtnClicked(_ sender: AnyObject)
    {
        self.view .removeFromSuperview()
        self .removeFromParentViewController()

    }
    func changePassWordAPI()  {
        if Reachability.isConnectedToNetwork()
        {
            
            let param = "Oldpassword=\(oldPassTextField.text!)&Newpassword=\(newPassTextField.text!)&emp_id=\(UserDefaults.standard.integer(forKey: Constants.USERID))"
            
            var method = "ChangePassword?"
            method = method.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            
            Constants.appDelegate.startIndicator()
            Server.postRequestWithURL(Constants.BASEURL+method, paramString: param, completionHandler: { (response) in
                
                if response[Constants.STATE] != nil && response[Constants.STATE] as! String == Constants.SUCC
                {
                    DispatchQueue.main.async
                    {
                        Constants.appDelegate.stopIndicator()
                        
                        var mess = String()
                        if response[Constants.MESS] != nil{
                            mess = response[Constants.MESS] as! String
                        }
                        else{
                            mess = response["Message"] as! String
                        }
                        
                        let alert = UIAlertController.init(title: "Nikon", message: mess, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                            self.dismiss(animated: true, completion: nil)
                            self.view .removeFromSuperview()
                            self .removeFromParentViewController()
                            self.logOut()

                        }))
                        self.present(alert, animated: true, completion: nil)
                        
                        
                        
                    }
                }
                else
                {
                    DispatchQueue.main.async
                    {
                        Constants.appDelegate.stopIndicator()
                        
                        var mess = String()
                        if response[Constants.MESS] != nil{
                            mess = response[Constants.MESS] as! String
                        }
                        else{
                            mess = response["Message"] as! String
                        }
                        
                        let alert = UIAlertController.init(title: "Nikon", message: mess, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                            self.dismiss(animated: true, completion: nil)
                        }))
                        self.present(alert, animated: true, completion: nil)
                        
                    }
                }
                
                
            })
        }
        else
        {
            Constants.appDelegate.stopIndicator()
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
        
        
    }
    func logOut()  {
        GIDSignIn.sharedInstance().signOut()
        let loginManager: FBSDKLoginManager = FBSDKLoginManager()
        loginManager.logOut()
        
        UserDefaults.standard.removeObject(forKey: Constants.USERHAVEPASSWORD)
        UserDefaults.standard.removeObject(forKey: Constants.USERID)
        UserDefaults.standard.synchronize()
        let destinationController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "SideMenuNavigation")
        //self.presentViewController(destinationController, animated: true, completion: nil);
        Constants.appDelegate.window?.rootViewController = destinationController
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
