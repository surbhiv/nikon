//
//  ProfileKnowMoreVC.swift
//  NikonApp
//
//  Created by Hitesh Dhawan on 08/03/17.
//  Copyright © 2017 Neuronimbus. All rights reserved.
//

import UIKit

class ProfileKnowMoreVC: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var headingDescriptionLabel: UILabel!
    
    @IBOutlet weak var pointTable: UITableView!
    
    var pointData = [AnyObject]()
    var totalPointStr = String()
    var redeemPointStr = String()
    var remainingPointStr = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationController?.isNavigationBarHidden = false
        self.setNavigationBar()
        if (UserDefaults.standard.integer(forKey: Constants.USERID) != 0) {
            self.getUserKnowMoreData(String(UserDefaults.standard.integer(forKey: Constants.USERID)) as NSString)
            
        }
    }
    
    
    //MARK:- NAVIGATION
    func OpenSideMenu(_ sender: AnyObject){
        
        self.view.endEditing(true)
        toggleSideMenuView()
    }
    func PopView(_ sender: AnyObject){
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func setNavigationBar(){
        self.navigationItem.hidesBackButton = true
        let navigationViewFrame = ScreenSize.SCREEN//self.navigationController?.navigationBar.frame
        let navigationView = UIView.init(frame: CGRect(x: 0, y: 0, width: navigationViewFrame.size.width, height: 44))
        navigationView.backgroundColor = UIColor.clear
        let menuButton = UIButton.init(type: .custom)
        
        menuButton.frame = CGRect(x: ScreenSize.SCREEN_WIDTH - 45, y: 7, width: 25, height: 25)
        menuButton.setImage(UIImage.init(named: "sideMenu_Icon"), for: UIControlState())
        menuButton.addTarget(self, action: #selector(OpenSideMenu), for: .touchUpInside)
        navigationView.addSubview(menuButton)
//        self.navigationController?.navigationBar.tintColor = Colors.appPink
        
        let titleLabel = UILabel.init(frame:CGRect(x: 60,y: 0,width: ( (navigationViewFrame.size.width) - 110),height: 40))
        titleLabel.textColor = UIColor.black
        titleLabel.text = "POINTS DETAILS"
        titleLabel.font = UIFont(name:"OpenSans-SemiBold", size: 15)!
        titleLabel.textAlignment = .center
        navigationView.addSubview(titleLabel)
        
        let logoImage = UIImageView.init(frame: CGRect(x: 25, y: 6, width: 33, height: 33))
        logoImage.image = UIImage.init(named: "nikon_login_logo")
        navigationView.addSubview(logoImage)
        
                let backButton = UIButton.init(type: .custom)
                backButton.frame = CGRect(x: 0, y: 9, width: 20, height: 20)
                backButton.backgroundColor = UIColor.white
                backButton.setImage(UIImage.init(named: "back"), for: UIControlState())
                backButton.addTarget(self, action: #selector(PopView), for: .touchUpInside)
                navigationView.addSubview(backButton)
        
        
        self.navigationItem.titleView = navigationView
        
    }
    
    
    //MARK:- GET USER DETAIL API
    func getUserKnowMoreData(_ userId : NSString)  {
        if Reachability.isConnectedToNetwork()
        {
            let method = "LoyalityPointSummary?UserId=\(userId)"
            
            Constants.appDelegate.startIndicator()
            Server.getRequestWithURL(Constants.BASEURL+method, completionHandler: { (response) in
                
                
                // if response[Constants.STATE] != nil && response[Constants.STATE] as! String == Constants.SUCC {

                DispatchQueue.main.async
                {
                    Constants.appDelegate.stopIndicator()
                    
                    DispatchQueue.main.async
                    {
                       self.pointData = response["Pointdetails"] as! NSArray as [AnyObject]
                        self.totalPointStr = String(response["TotalPoint"] as! Int)
                        self.redeemPointStr = String(response["RedeemedPoints"] as! Int)
                        self.remainingPointStr = String(response["RemainingPoints"] as! Int)
                        self.headingLabel.text = (response["shortdesc"] as! String)
                        self.headingDescriptionLabel.text = (response["desc"] as! String)
                        
                        self.pointTable.reloadData()
                       
                        
                    }
                    
                    
                }
                
                
            })
        }
        else
        {
            Constants.appDelegate.stopIndicator()
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
        
        
    }

    
 

    //MARK:- Tableview
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 2
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let titleView = UIView.init(frame: CGRect(x: 0 , y: 0 , width: self.view.frame.size.width ,height: 35))
        let titleLabel = UILabel.init(frame: CGRect(x: 8 , y: 2 , width: self.view.frame.size.width-40 ,height: 25))
        titleLabel.font = UIFont.init(name: "OpenSans-Semibold", size: 14)
        titleLabel.backgroundColor = UIColor.white
        titleView.backgroundColor = UIColor.white
        titleLabel.textColor = Colors.appYello
        if section == 0
        {
           titleLabel.text = "Earn Points Details"
        }
        else
        {
            titleLabel.text = "User Points Details"
        }
        titleView.addSubview(titleLabel)
        return titleView
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "knowMoreCell", for: indexPath)as! knowMoreCell
        
        if indexPath.section == 0
        {
        cell.keyLabel.text = self.pointData[indexPath.row]["Activity"] as? String
        cell.valueLabel.text = String(self.pointData[indexPath.row]["Point"] as! Int)
            if indexPath.row%2 == 0
            {
             cell.backgroundColor = UIColor.init(red: 227.0/255.0, green: 227.0/255.0, blue: 227.0/255.0, alpha: 1)
            }
            else
            {
                cell.backgroundColor = UIColor.init(red: 248.0/255.0, green: 248.0/255.0, blue: 248.0/255.0, alpha: 1)
                
            }
        }
        else
        {
            if indexPath.row == 0
            {
                cell.keyLabel.text = "Total Points:"
                cell.valueLabel.text = self.totalPointStr
                cell.backgroundColor = UIColor.init(red: 227.0/255.0, green: 227.0/255.0, blue: 227.0/255.0, alpha: 1)
            }
            if indexPath.row == 1
            {
                cell.keyLabel.text = "Redeemed Points:"
                cell.valueLabel.text = self.redeemPointStr
                cell.backgroundColor = UIColor.init(red: 248.0/255.0, green: 248.0/255.0, blue: 248.0/255.0, alpha: 1)
            }
            if indexPath.row == 2
            {
                cell.keyLabel.text = "Remaining Points:"
                cell.valueLabel.text = self.remainingPointStr
                cell.backgroundColor = UIColor.init(red: 227.0/255.0, green: 227.0/255.0, blue: 227.0/255.0, alpha: 1)
            }
            
            
        }
        self.pointTable.estimatedRowHeight = 35
        self.pointTable.rowHeight = UITableViewAutomaticDimension
        return cell
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0
        {
            return self.pointData.count
        }
        else
        {
            return 3
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
