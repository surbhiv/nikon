//
//  knowMoreCell.swift
//  NikonApp
//
//  Created by Hitesh Dhawan on 08/03/17.
//  Copyright © 2017 Neuronimbus. All rights reserved.
//

import UIKit

class knowMoreCell: UITableViewCell {

    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var keyLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
