//
//  EditProfileVC.swift
//  NikonApp
//
//  Created by Hitesh Dhawan on 15/03/17.
//  Copyright © 2017 Neuronimbus. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

extension UIImage {
    var uncompressedPNGData: Data?      { return UIImagePNGRepresentation(self)        }
    var highestQualityJPEGNSData: Data? { return UIImageJPEGRepresentation(self, 1.0)  }
    var highQualityJPEGNSData: Data?    { return UIImageJPEGRepresentation(self, 0.75) }
    var mediumQualityJPEGNSData: Data?  { return UIImageJPEGRepresentation(self, 0.5)  }
    var lowQualityJPEGNSData: Data?     { return UIImageJPEGRepresentation(self, 0.25) }
    var lowestQualityJPEGNSData:Data?   { return UIImageJPEGRepresentation(self, 0.0)  }
}
protocol updateProtocol {
    func updateProfileMenu()
}
class EditProfileVC: UIViewController,UIPickerViewDelegate,UIPickerViewDataSource,UITextFieldDelegate,UIImagePickerControllerDelegate,UIPopoverControllerDelegate,UINavigationControllerDelegate {

    @IBOutlet weak var viewForChangePassBtn: UIView!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var phoneNumberTextField: UITextField!
    @IBOutlet weak var stateNameTextField: UITextField!
    @IBOutlet weak var cityTextField: UITextField!
    
    @IBOutlet weak var pincodeTextField: UITextField!
    @IBOutlet weak var dobTextField: UITextField!
    @IBOutlet weak var addressTextField: UITextField!
    
    @IBOutlet weak var maleBtn: UIButton!
    @IBOutlet weak var femaleBtn: UIButton!
    @IBOutlet weak var uploadBtn: UIButton!
    @IBOutlet weak var uploadBtn1: UIButton!
    @IBOutlet weak var changePassBtn: UIButton!
    var updateDelegate : updateProtocol?

    
    var imageDataIMG = Data()
    var userProfileDataDict = Dictionary<String,AnyObject>()
    var imagePicker:UIImagePickerController?=UIImagePickerController()
    
    var stateArray = [AnyObject]()
    var cityArray = [AnyObject]()
    var titleArray = ["Select Title","Mr.","Mrs.","Ms."]
    
    var stateId = String()
    var cityId = String()
    var dobStr = String()
    var imageDataToSend = String()
    
    var picker = UIPickerView()
    var datePicker = UIDatePicker()
    
    //Varible to store Value for update
    
    //MARK:- START
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationController?.isNavigationBarHidden = false
        self.setNavigationBar()
        self.userImageView.layer.borderWidth = 2
        self.userImageView.layer.borderColor = UIColor.black.cgColor
        userImageView.layer.cornerRadius = 40
        userImageView.layer.masksToBounds = true
        //        userImageView.layer.borderWidth = 1
        //        userImageView.layer.borderColor = UIColor.whiteColor().CGColor
        
        self.uploadBtn.layer.borderColor = UIColor.white.cgColor
        self.uploadBtn.layer.borderWidth = 1

        
         imagePicker?.delegate = self
        //define picker
        self.picker = UIPickerView.init()
        self.picker.dataSource = self
        self.picker.delegate = self
        
        datePicker.datePickerMode = .date
        datePicker.maximumDate = Date.init()
        self.dobTextField.inputView = datePicker
        datePicker.addTarget(self, action: #selector(datePickerValueChanged), for: UIControlEvents.valueChanged)
        
//        let attributedStr = NSAttributedString(string: "UPLOAD\nPHOTO",  attributes: [NSFontAttributeName : UIFont.init(name: "Helvetica-Bold", size: 13.0)!, NSForegroundColorAttributeName : UIColor.whiteColor()])
//        uploadBtn.setAttributedTitle(attributedStr, forState: .Normal)
        uploadBtn.layer.borderWidth = 1
        uploadBtn.layer.borderColor = UIColor.white.cgColor
        uploadBtn1.layer.borderWidth = 1
        uploadBtn1.layer.borderColor = UIColor.white.cgColor
        changePassBtn.layer.borderWidth = 1
        changePassBtn.layer.borderColor = UIColor.white.cgColor
        
        self.stateNameTextField.inputView = self.picker
        self.cityTextField.inputView = self.picker
        self.titleTextField.inputView = self.picker
        
        self.stateId = String(userProfileDataDict["State_id"] as! Int)
        self.cityId = String(userProfileDataDict["City_id"] as! Int)
        
        var  dict = Dictionary<String,AnyObject>()
        dict = ["State_Name":"Select State" as AnyObject,"state_id":0 as AnyObject]
        self.stateArray.append(dict as AnyObject)
        
        var  dict1 = Dictionary<String,AnyObject>()
        dict1 = ["CityName":"Select City" as AnyObject,"City_id":0 as AnyObject,"state_id":0 as AnyObject]
        self.cityArray.append(dict1 as AnyObject)


        //if (( userProfileDataDict["FacebookId"]?.isKindOfClass(NSNull ) ) == true  && ( userProfileDataDict["GmailId"]?.isKindOfClass(NSNull ) ) == true)
        if (UserDefaults.standard.integer(forKey: Constants.USERHAVEPASSWORD) != 0)
        {
            viewForChangePassBtn.isHidden = false
            uploadBtn.isHidden = true
        }
        else
        {
            viewForChangePassBtn.isHidden = true
            uploadBtn.isHidden = false
        }
        self.getStateName()
        self.getCityName(String(userProfileDataDict["State_id"] as! Int) as NSString)
        self.setData()
        

    }
    func datePickerValueChanged(_ sender:UIDatePicker) {
        
        let dateFormatter = DateFormatter()
       // dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        //let date = dateFormatter.dateFromString(String(sender.date))!
//        dateFormatter.dateFormat = " dd MMM, yyyy"
        dateFormatter.dateFormat = " dd/MM/yyyy"
        let dateString = dateFormatter.string(from: sender.date)
        
//        dateFormatter.dateStyle = NSDateFormatterStyle.MediumStyle
//        dateFormatter.timeStyle = NSDateFormatterStyle.NoStyle
//       dobTextField.text = dateFormatter.stringFromDate(sender.date)
        dateFormatter.dateFormat = "MM/dd/yyyy"
        let SendingdateString = dateFormatter.string(from: sender.date)
        dobStr = SendingdateString
        dobTextField.text = dateString
        
    }
    
    //MARK:- NAVIGATION
    func OpenSideMenu(_ sender: AnyObject){
        
        self.view.endEditing(true)
        toggleSideMenuView()
    }
    func PopView(_ sender: AnyObject){
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func setNavigationBar(){
        self.navigationItem.hidesBackButton = true
        let navigationViewFrame = ScreenSize.SCREEN//self.navigationController?.navigationBar.frame
        let navigationView = UIView.init(frame: CGRect(x: 0, y: 0, width: navigationViewFrame.size.width, height: 44))
        navigationView.backgroundColor = UIColor.clear
        let menuButton = UIButton.init(type: .custom)
        
        menuButton.frame = CGRect(x: ScreenSize.SCREEN_WIDTH - 45, y: 7, width: 25, height: 25)
        menuButton.setImage(UIImage.init(named: "sideMenu_Icon"), for: UIControlState())
        menuButton.addTarget(self, action: #selector(OpenSideMenu), for: .touchUpInside)
        navigationView.addSubview(menuButton)
//        self.navigationController?.navigationBar.tintColor = Colors.appPink
        
        let titleLabel = UILabel.init(frame:CGRect(x: 60,y: 0,width: ( (navigationViewFrame.size.width) - 110),height: 40))
        titleLabel.textColor = UIColor.black
        titleLabel.text = "EDIT PROFILE"
        titleLabel.font = UIFont.init(name:"OpenSans-SemiBold", size: 15)// UIFont(name:"OpenSans-SemiBold", size: 15)!
        titleLabel.textAlignment = .center
        navigationView.addSubview(titleLabel)
        
        let logoImage = UIImageView.init(frame: CGRect(x: 25, y: 6, width: 33, height: 33))
        logoImage.image = UIImage.init(named: "nikon_login_logo")
        navigationView.addSubview(logoImage)
        
        let backButton = UIButton.init(type: .custom)
        backButton.frame = CGRect(x: 0, y: 9, width: 20, height: 20)
        backButton.backgroundColor = UIColor.white
        backButton.setImage(UIImage.init(named: "back"), for: UIControlState())
        backButton.addTarget(self, action: #selector(PopView), for: .touchUpInside)
        navigationView.addSubview(backButton)
        
        
        self.navigationItem.titleView = navigationView
        
    }

    
    //MARK:- SET USER PROFILE DATA
    func setData()
    {
        titleTextField.text = userProfileDataDict["User_Title"]as? String
        firstNameTextField.text = userProfileDataDict["User_Fname"]as? String
        lastNameTextField.text = userProfileDataDict["User_Lname"]as? String
        emailTextField.text = userProfileDataDict["User_Email"]as? String
        phoneNumberTextField.text = userProfileDataDict["User_Phone"]as? String
        stateNameTextField.text = userProfileDataDict["State_name"]as? String
        cityTextField.text = userProfileDataDict["City_name"]as? String
        pincodeTextField.text = userProfileDataDict["User_Pincode"]as? String
        dobStr = userProfileDataDict["User_Dob"]as? String ?? ""
        dobTextField.text = Helper.getDOBStringFromDate(userProfileDataDict["User_Dob"]as? String ?? "")
        addressTextField.text = "\(userProfileDataDict["User_Address"]as! String)"
        if userProfileDataDict["User_Gender"]as! String == "Male"
        {
            maleBtn.isSelected = true
            femaleBtn.isSelected = false
        }
        else if userProfileDataDict["User_Gender"]as! String == "Female"
        {
            maleBtn.isSelected = false
            femaleBtn.isSelected = true
        }
        else
        {
            maleBtn.isSelected = false
            femaleBtn.isSelected = false
        }
        self.userImageView.sd_setImage(with: URL.init(string: userProfileDataDict["ProfileImage"]as! String), placeholderImage: UIImage.init(named: "profilePlaceHolder"))
        self.userImageView.setShowActivityIndicator(true)
        
    }

    //MARK:- STATE NAME API
    func getStateName(){
        if Reachability.isConnectedToNetwork()
        {
            
            
            let method = "GetStateCity"
            
            
            Constants.appDelegate.startIndicator()
            Server.getRequestWithURL(Constants.BASEURL+method, completionHandler: { (response) in
                
                if response[Constants.STATE] != nil && response[Constants.STATE] as! String == Constants.SUCC {
                    
                    DispatchQueue.main.async
                    {
                        Constants.appDelegate.stopIndicator()
                        let tempArray = response["statelist"] as! Array<AnyObject>
                        for dict in tempArray
                        {
                            self.stateArray.append(dict)
                            
                        }
                    }
                }
                else
                {
                    Constants.appDelegate.stopIndicator()
                    
//                    var mess = String()
//                    if response[Constants.MESS] != nil{
//                        mess = response[Constants.MESS] as! String
//                    }
//                    else{
//                        mess = response["Message"] as! String
//                    }
//                    dispatch_async(dispatch_get_main_queue())
//                    {
//                    let alert = UIAlertController.init(title: "Nikon", message: mess, preferredStyle: UIAlertControllerStyle.Alert)
//                    alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.Default, handler: { (action:UIAlertAction) in
//                        self.dismissViewControllerAnimated(true, completion: nil)
//                    }))
//                    self.presentViewController(alert, animated: true, completion: nil)
//                    }
                }
            })
        }
        else
        {
            Constants.appDelegate.stopIndicator()
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
    }
    
    //MARK:- GETCITY NAME
    func getCityName(_ state_id:NSString){
        if Reachability.isConnectedToNetwork()
        {
            let method = "GetStateCity?state_id=\(state_id)"
            
            Constants.appDelegate.startIndicator()
            Server.getRequestWithURL(Constants.BASEURL+method, completionHandler: { (response) in
                
                if response[Constants.STATE] != nil && response[Constants.STATE] as! String == Constants.SUCC {
                    
                    DispatchQueue.main.async
                    {
                        Constants.appDelegate.stopIndicator()
                        
                        let tempArray = response["Citylist"] as! Array<AnyObject>
                        for dict in tempArray
                        {
                            self.cityArray.append(dict)
                            
                        }
                        

                    }
                }
                else {
                    Constants.appDelegate.stopIndicator()
                    
//                    var mess = String()
//                    if response[Constants.MESS] != nil {
//                        mess = response[Constants.MESS] as! String
//                    }
//                    else {
//                        mess = response["Message"] as! String
//                    }
//                    dispatch_async(dispatch_get_main_queue())
//                    {
//                    let alert = UIAlertController.init(title: "Nikon", message: mess, preferredStyle: UIAlertControllerStyle.Alert)
//                    alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.Default, handler: { (action:UIAlertAction) in
//                        self.dismissViewControllerAnimated(true, completion: nil)
//                    }))
//                    self.presentViewController(alert, animated: true, completion: nil)
//                    }
                }
            })
        }
        else
        {
            Constants.appDelegate.stopIndicator()
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
    }
    //MARK:- UPDATE USER PROFILE DATA
    func updateUserProfileDetail()
    {
        if Reachability.isConnectedToNetwork()
        {
            var gender = String()
            if maleBtn.isSelected == true
            {
                gender = "Male"
            }
            else
            {
                gender = "Female"
            }
            var param = "emp_id=\(UserDefaults.standard.integer(forKey: Constants.USERID))&User_Title=\(titleTextField.text!)&User_Fname=\(firstNameTextField.text!)&User_Lname=\(lastNameTextField.text!)&User_Gender=\(gender)&User_Address=\(addressTextField.text!)&User_Country=India&User_Pincode=\(pincodeTextField.text!)&User_Phone=\(phoneNumberTextField.text!)&State_id=\(stateId)&State_name=\(stateNameTextField.text!)&City_id=\(cityId)&City_name=\(cityTextField.text!)&User_Dob=\(dobStr)"
            param = param.replacingOccurrences(of: " ", with: "%20")
          //  param = param.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!
            var method = "EditUser?"
            method = method.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            
            Constants.appDelegate.startIndicator()
            Server.postRequestWithURL(Constants.BASEURL+method, paramString: param, completionHandler: { (response) in
                
                if response[Constants.STATE] != nil && response[Constants.STATE] as! String == Constants.SUCC
                {
                    DispatchQueue.main.async
                    {
                        Constants.appDelegate.stopIndicator()
                        
//                        var mess = String()
//                        if response[Constants.MESS] != nil{
//                            mess = response[Constants.MESS] as! String
//                        }
//                        else{
//                            mess = response["Message"] as! String
//                        }
                        
                        let alert = UIAlertController.init(title: "Nikon", message: "User profile updated successfully.", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                            self.updateDelegate?.updateProfileMenu()
                            self.navigationController?.popViewController(animated: true)
                            self.dismiss(animated: true, completion: nil)
                        }))
                        self.present(alert, animated: true, completion: nil)
                        
                        
                        
                    }
                }
                else
                {
                    DispatchQueue.main.async
                    {
                        Constants.appDelegate.stopIndicator()
                        
                        var mess = String()
                        if response[Constants.MESS] != nil{
                            mess = response[Constants.MESS] as! String
                        }
                        else{
                            mess = response["Message"] as! String
                        }
                        
                        let alert = UIAlertController.init(title: "Nikon", message: mess, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                            self.dismiss(animated: true, completion: nil)
                        }))
                        self.present(alert, animated: true, completion: nil)
                        
                    }
                }
                
                
            })
        }
        else
        {
            Constants.appDelegate.stopIndicator()
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }

    }
    
    //MARK:- UPDATE USER PROFILE IMAGE
    
    func updateUserProfileImage()
    {
        if Reachability.isConnectedToNetwork()
        {
            
            imageDataToSend = imageDataIMG.base64EncodedString(options: NSData.Base64EncodingOptions.lineLength64Characters)
            
            let param = "emp_id=\(UserDefaults.standard.integer(forKey: Constants.USERID))"
            let image = "&ProfileImage=\(imageDataToSend)"
            //param = param.stringByReplacingOccurrencesOfString(" ", withString: "")
            //  param = param.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!
            let method = "UpdateUserImage?"
            
            Constants.appDelegate.startIndicator()
            Server.UploadPicture(Constants.BASEURL+method, paramString: param,ImageString: image, completionHandler: { (response) in
                
                Constants.appDelegate.stopIndicator()

                if response[Constants.STATE] != nil && response[Constants.STATE] as! String == Constants.SUCC
                {
                    DispatchQueue.main.async
                    {
                        Constants.appDelegate.stopIndicator()
                        
//                        var mess = String()
//                        if response[Constants.MESS] != nil{
//                            mess = response[Constants.MESS] as! String
//                        }
//                        else{
//                            mess = response["Message"] as! String
//                        }
                        
                        let alert = UIAlertController.init(title: "Nikon", message: "User profile updated successfully.", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                            self.updateDelegate?.updateProfileMenu()
                            self.navigationController?.popViewController(animated: true)
                            self.dismiss(animated: true, completion: nil)
                        }))
                        self.present(alert, animated: true, completion: nil)
                        
                        
                        
                    }
                }
                else
                {
                    DispatchQueue.main.async
                    {
                        Constants.appDelegate.stopIndicator()
                        
                        var mess = String()
                        if response[Constants.MESS] != nil{
                            mess = response[Constants.MESS] as! String
                        }
                        else{
                            mess = response["Message"] as! String
                        }
                        
                        let alert = UIAlertController.init(title: "Nikon", message: mess, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                            self.dismiss(animated: true, completion: nil)
                        }))
                        self.present(alert, animated: true, completion: nil)
                        
                    }
                }
                
                
            })
        }
        else
        {
            Constants.appDelegate.stopIndicator()
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
        
    }
    
    //MARK: - PICKEVIEW for date
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if self.stateNameTextField.isFirstResponder {
            return stateArray.count
        }
        if self.cityTextField.isFirstResponder{
            return cityArray.count
        }
        if self.titleTextField.isFirstResponder{
            return titleArray.count
        }
        
        
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if self.stateNameTextField.isFirstResponder{
            return String.init(format: stateArray [row]["State_Name"] as! String, locale: nil)
        }
        if self.cityTextField.isFirstResponder {
            return String.init(format: cityArray [row]["CityName"] as! String, locale: nil)
        }
        if self.titleTextField.isFirstResponder {
            return String.init(format: titleArray [row] , locale: nil)
        }
        
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if self.stateNameTextField.isFirstResponder
        {
            self.stateNameTextField.text = String.init(format: stateArray [row]["State_Name"] as! String, locale: nil)
            let stateIdInt = stateArray [row]["state_id"] as! Int
            stateId = String(stateIdInt)
            self.cityTextField.text = "Select City"
            cityId = "0"
        }
        if self.cityTextField.isFirstResponder
        {
            let cityIdInt = cityArray [row]["City_id"] as! Int
            cityId = String(cityIdInt)
            self.cityTextField.text =  String.init(format: cityArray [row]["CityName"] as! String, locale: nil)
        }
        if self.titleTextField.isFirstResponder
        {
            self.titleTextField.text =  String.init(format: titleArray [row] , locale: nil)
        }
        
        
    }
    // MARK: TEXTFIELD DELEGATE
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        
        textField .resignFirstResponder()
        if textField == self.stateNameTextField
        {
            if cityArray.count > 0
            {
                cityArray.removeAll()
                var  dict1 = Dictionary<String,AnyObject>()
                dict1 = ["CityName":"Select City" as AnyObject,"City_id":0 as AnyObject,"state_id":0 as AnyObject]
                self.cityArray.append(dict1 as AnyObject)
                
            }
            DispatchQueue.main.async
            {
            self.getCityName(self.stateId as NSString)
            if self.cityArray.count > 1
            {
                let cityIdInt = self.cityArray [1]["City_id"] as! Int
                self.cityId = String(cityIdInt)
                self.cityTextField.text = self.cityArray [1]["CityName"] as? String
            }
            }
        }
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == phoneNumberTextField {
            if (range.location > 9)
            {
                return false
            }
        }
        if textField == pincodeTextField {
            if (range.location > 5)
            {
                return false
            }
        }
        return true
    }
//MARK:- ACTION
    @IBAction func uploadPhotoBtnClick(_ sender: AnyObject)
    {
        let optionMenu = UIAlertController(title: nil, message: "Choose Option", preferredStyle: .actionSheet)
        
        // 2
        let cameraAction = UIAlertAction(title: "Open Camera", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.openCamera()
            
        })
        let galleryAction = UIAlertAction(title: "Open Gallery", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.openGallary()
            
        })
        
        //
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            
        })
        
        
        // 4
        optionMenu.addAction(cameraAction)
        optionMenu.addAction(galleryAction)
        optionMenu.addAction(cancelAction)
        
        // 5
        self.present(optionMenu, animated: true, completion: nil)
    }
    
   
    //MARK:- IMAGEPICKERDELEGATE
    func openGallary()
    {
        imagePicker!.allowsEditing = true
        imagePicker!.sourceType = UIImagePickerControllerSourceType.photoLibrary
        present(imagePicker!, animated: true, completion: nil)
    }
    
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)){
            imagePicker!.allowsEditing = true
            imagePicker!.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker!.cameraCaptureMode = .photo
            present(imagePicker!, animated: true, completion: nil)
        }else{
            let alert = UIAlertController(title: "Camera Not Found", message: "This device has no Camera", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style:.default, handler: nil)
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
        }
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {

        if let pickedImage = info[UIImagePickerControllerEditedImage] as? UIImage {
            
            self.userImageView.contentMode = .scaleAspectFit
            
           // let resizedImage = Helper.resizeImage(pickedImage, newWidth: 400.0)
            imageDataIMG = UIImagePNGRepresentation(pickedImage)! as Data
           
            let imageSize: Int = imageDataIMG.count
            let size = imageSize/1024

            if size > 2048
            {
                DispatchQueue.main.async
                {
                    self.perform(#selector(self.imageAlert), with: nil, afterDelay: 0.5)
                }
                dismiss(animated: true, completion: nil)

                return
            }
            else
            {
                self.userImageView.image = pickedImage

                dismiss(animated: true, completion: nil)
                self.perform(#selector(self.updateUserProfileImage), with: nil, afterDelay: 1.0)
                
            }

//            self.updateUserProfileImage()
        }
        
        
        
    }
    func imageAlert()
    {
        let alert = UIAlertController.init(title: "Nikon", message: "Please select image of size less than 2 MB", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
            self.dismiss(animated: true, completion: nil)
        }))
        self.present(alert, animated: true, completion: nil)
        
        
    }

    //MARK:- ACTION
    @IBAction func maleoBtnClick(_ sender: AnyObject)
    {
        maleBtn.isSelected = true
        femaleBtn.isSelected = false
    }

    @IBAction func femaleoBtnClick(_ sender: AnyObject)
    {
        maleBtn.isSelected = false
        femaleBtn.isSelected = true
    }
    
    @IBAction func submitBtnClick(_ sender: AnyObject)
    {
        if self.titleTextField.text == "Select Title" || self.titleTextField.text == ""
        {
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "Nikon", "message": "Please select title"] )
        }
        else if self.firstNameTextField.text == ""
        {
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "Nikon", "message": "Please enter first name"] )
        }
        else if self.lastNameTextField.text == ""
        {
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "Nikon", "message": "Please enter last name"] )
        }
        else if self.phoneNumberTextField.text == ""
        {
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "Nikon", "message": "Please enter phone number"] )
        }
        else if self.phoneNumberTextField.text?.characters.count < 10
        {
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "Nikon", "message": "Please enter 10 digit phone number"] )
        }
        else if self.stateNameTextField.text == "Select State" || self.stateNameTextField.text == ""
        {
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "Nikon", "message": "Please select state name"] )
        }
        else if self.cityTextField.text == "Select City" || self.cityTextField.text == ""
        {
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "Nikon", "message": "Please select city name"] )
        }
        else if self.pincodeTextField.text == ""
        {
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "Nikon", "message": "Please enter postal code"] )
        }
        else if self.pincodeTextField.text?.characters.count < 6
        {
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "Nikon", "message": "Please enter 6 digit postal code"] )
        }
        else if self.dobTextField.text == ""
        {
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "Nikon", "message": "Please enter date of birth"] )
        }
        else if self.addressTextField.text == ""
        {
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "Nikon", "message": "Please enter address"] )
        }
        else
        {
           self.updateUserProfileDetail()
        }
        
    }
    @IBAction func changePasswordBtnClick(_ sender: AnyObject) {
        let destVC = Constants.mainStoryboard.instantiateViewController(withIdentifier: "ChangePasswordVC")as! ChangePasswordVC
        
        let topview = self.parent
        //        topview.addSubview(destVC.view)
        topview!.addChildViewController(destVC)
        destVC.didMove(toParentViewController: topview)
        topview?.view.addSubview(destVC.view)

        
        //self.navigationController?.pushViewController(destVC, animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
