//
//  PaymentVC.m
//  DoctorLogin
//
//  Created by Hitesh Dhawan on 17/08/16.
//  Copyright © 2016 Surbhi Varma. All rights reserved.
//

#import "PaymentVC.h"
#import "CCTool.h"
#import "MyAccountVC.h"


@interface PaymentVC () <UIWebViewDelegate>
{
    BOOL paymentISsucess;
}
@property (weak, nonatomic) IBOutlet UIBarButtonItem *leftbarButtonItem;

@end

@implementation PaymentVC

- (void)viewDidLoad {
    
    [super viewDidLoad];

    paymentISsucess = NO;
    NSLog(@"Order ID = %@",_orderId);
    _merchantId = @"67400";
    _accessCode = @"AVEU00DC36AS88UESA";
    _currency = @"INR";
    _redirectUrl = @"https://www.logintohealth.com/demo/doctors/app_payment_success";
    _cancelUrl = @"https://www.logintohealth.com/demo/doctors/app_payment_success";
    _rsaKeyUrl = @"https://www.logintohealth.com/demo/home/GetRSA";

    
    _webView.delegate = self;
    _webView.scalesPageToFit = YES;
    
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeGradient];
    
    [self loadWebview];
    
    
    self.navigationItem.hidesBackButton = YES;
    
    if (_isComingFromOTP) {
        _topView.hidden = NO;
    }
    else
    {
        _topView.hidden = YES;
        UIBarButtonItem *rightBtn = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"left_arrow.png"] style:UIBarButtonItemStylePlain target:self action:@selector(BackButtonPressed:)];
        self.navigationItem.leftBarButtonItem=rightBtn;
        self.navigationItem.leftBarButtonItem.tintColor = [UIColor darkGrayColor];
    }
}


-(void)loadWebview{
    
    NSError *error = nil;
    id response;
    
    NSData *data = [NSURLConnection sendSynchronousRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.logintohealth.com/demo/home/GetRSA?order_id=%@",_orderId]]] returningResponse:&response error:&error];
    NSString *rsaKey = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
    rsaKey = [rsaKey stringByTrimmingCharactersInSet:[NSCharacterSet newlineCharacterSet]];
    rsaKey = [NSString stringWithFormat:@"-----BEGIN PUBLIC KEY-----\n%@\n-----END PUBLIC KEY-----\n",rsaKey];
    NSLog(@"%@",rsaKey);
    
    //Encrypting Card Details
    NSString *myRequestString = [NSString stringWithFormat:@"amount=%@&currency=%@",_amount,_currency];
    CCTool *ccTool = [[CCTool alloc] init];
    NSString *encVal = [ccTool encryptRSA:myRequestString key:rsaKey];
    encVal = [encVal stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet alphanumericCharacterSet]];
    //(NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL, (CFStringRef)encVal, NULL, (CFStringRef)@"!*'();:@&=+$,/?%#[]", kCFStringEncodingUTF8 ));
    
    //Preparing for a webview call
    NSString *urlAsString = [NSString stringWithFormat:@"https://test.ccavenue.com/transaction/initTrans"];
    NSString *encryptedStr = [NSString stringWithFormat:@"merchant_id=%@&order_id=%@&redirect_url=%@&cancel_url=%@&enc_val=%@&access_code=%@&orderNo=%@&amount=%@currency=%@&language=EN&billing_tel=%@&billing_email=%@&billing_city=%@&billing_state=%@&billing_zip=%@&billing_name=%@&billing_country=India&billing_address=%@&merchant_param1=%@",_merchantId,_orderId,_redirectUrl,_cancelUrl,encVal,_accessCode,_orderNum,_amount,_currency,_billing_tel,_billing_email,_billing_city,_billing_state,_billing_zip,_merchant_Name,_billing_address,[PersistencyManager getSubscriptionId]];
    //

    NSData *myRequestData = [NSData dataWithBytes: [encryptedStr UTF8String] length: [encryptedStr length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: urlAsString]];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    [request setValue:urlAsString forHTTPHeaderField:@"Referer"];
    [request setHTTPMethod: @"POST"];
    [request setHTTPBody: myRequestData];
    [_webView loadRequest:request];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView{
    
    [SVProgressHUD dismiss];

    NSString *string = webView.request.URL.absoluteString;
    NSLog(@"**** \n\n STRING -- %@ \n\n ****",string);
    
    if ([string rangeOfString:@"/app_payment_success"].location != NSNotFound || [string rangeOfString:@"requestType=PAYMENT"].location != NSNotFound) {
        
        paymentISsucess = YES;
    }
}

-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    NSLog(@"Error == %@",error);
}

- (IBAction)BackButtonPressed:(id)sender {
    
    if (paymentISsucess == YES)
    {
        if ([[[[[[UIApplication sharedApplication] keyWindow] rootViewController] childViewControllers] firstObject] isKindOfClass:[MyAccountVC class]])
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"SubscriptionUpdated" object:nil];
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
        else
        {
            AppDelegate *appDelegate = APP_DELEGATE;
            [PersistencyManager loginWithDoctorId:_doctorId];
            [appDelegate showDashboard];
        }
    }
    else{
        if (_isComingFromOTP)
            [self dismissViewControllerAnimated:YES completion:nil];
        else
            [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
}


@end
