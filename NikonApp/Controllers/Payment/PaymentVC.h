//
//  PaymentVC.h
//  DoctorLogin
//
//  Created by Hitesh Dhawan on 17/08/16.
//  Copyright © 2016 Surbhi Varma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PaymentVC : UIViewController

@property (weak, nonatomic) IBOutlet UIWebView *webView;

@property (nonatomic) NSString *accessCode;
@property (nonatomic) NSString *merchantId;
@property (nonatomic) NSString *currency;
@property (nonatomic) NSString *amount;
@property (nonatomic) NSString *orderId;
@property (nonatomic) NSString *orderNum;

@property (nonatomic) NSString *redirectUrl;
@property (nonatomic) NSString *cancelUrl;
@property (nonatomic) NSString *rsaKeyUrl;

@property (nonatomic) NSString *merchant_Name;
@property (nonatomic) NSString *billing_address;
@property (nonatomic) NSString *billing_city;
@property (nonatomic) NSString *billing_state;
@property (nonatomic) NSString *billing_zip;
@property (nonatomic) NSString *billing_tel;
@property (nonatomic) NSString *billing_email;
@property (nonatomic) NSString *doctorId;

@property BOOL isComingFromOTP;
@property (weak, nonatomic) IBOutlet UIView *topView;
@end
