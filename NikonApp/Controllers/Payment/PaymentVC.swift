//
//  PaymentVC.swift
//  NikonApp
//
//  Created by Hitesh Dhawan on 09/03/17.
//  Copyright © 2017 Neuronimbus. All rights reserved.
//

import UIKit

class PaymentVC: UIViewController,UIWebViewDelegate {

    @IBOutlet weak var paymentWebView: UIWebView!
    var backButton = UIButton()
    var orderId = String()
    var amount =  String()
    var userId = String()
    var userAddress = String()
    var userAge = String()
    var userCountory = String()
    var userEmail = String()
    var userFname = String()
    var userLname = String()
    var userGender = String()
    var userPhone = String()
    var userPincode = String()
    var userDob = String()
    var userStateName = String()
    var usercityName = String()
    var userSNo = String()
    var userCameraNo = String()

    var userDetailDict = Dictionary<String,AnyObject>()
    override func viewDidLoad() {
        super.viewDidLoad()

        
         //self.navigationController?.navigationBarHidden = true
        // Do any additional setup after loading the view.
        self.navigationController?.isNavigationBarHidden = false
        self.setNavigationBar()

        
        self.userFname   = userDetailDict["User_Fname"] as! String
        self.userLname   = userDetailDict["User_Lname"] as! String
        self.userEmail   = userDetailDict["User_Email"] as! String
        self.userPhone   = userDetailDict["User_Phone"] as! String
        self.userGender  = userDetailDict["User_Gender"] as! String
        self.userPincode = userDetailDict["User_Pincode"] as! String
        self.userDob     = userDetailDict["User_Dob"] as! String
        self.userAddress = "\(userDetailDict["User_Address"] as! String) \(userDetailDict["State_name"] as! String)"
        self.userStateName = userDetailDict["State_name"] as! String
        self.usercityName = userDetailDict["City_name"] as! String
        self.userSNo = userDetailDict["Sr_No"] as! String
        self.userCameraNo = userDetailDict["Camera_No"] as! String



        self.postOrderId()
    }
    override func viewWillAppear(_ animated: Bool) {
        sideMenuController()?.sideMenu?.allowPanGesture = false
        sideMenuController()?.sideMenu?.allowLeftSwipe = false
        sideMenuController()?.sideMenu?.allowRightSwipe = false
        
    }
    override func viewDidDisappear(_ animated: Bool) {
        sideMenuController()?.sideMenu?.allowPanGesture = true
        sideMenuController()?.sideMenu?.allowLeftSwipe = true
        sideMenuController()?.sideMenu?.allowRightSwipe = true
    }
    
    //MARK:- NAVIGATION
//    func OpenSideMenu(sender: AnyObject){
//        
//        self.view.endEditing(true)
//        toggleSideMenuView()
//    }
    func PopView(_ sender: AnyObject)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    func setNavigationBar(){
        self.navigationItem.hidesBackButton = true
        let navigationViewFrame = ScreenSize.SCREEN//self.navigationController?.navigationBar.frame
        let navigationView = UIView.init(frame: CGRect(x: 0, y: 0, width: navigationViewFrame.size.width, height: 44))
        navigationView.backgroundColor = UIColor.clear
//        let menuButton = UIButton.init(type: .Custom)
//        
//        menuButton.frame = CGRectMake(ScreenSize.SCREEN_WIDTH - 45, 7, 25, 25)
//        menuButton.setImage(UIImage.init(named: "sideMenu_Icon"), forState: .Normal)
//        menuButton.addTarget(self, action: #selector(OpenSideMenu), forControlEvents: .TouchUpInside)
//        navigationView.addSubview(menuButton)
        //        self.navigationController?.navigationBar.tintColor = Colors.appPink
        
        let titleLabel = UILabel.init(frame:CGRect(x: 60,y: 0,width: ( (navigationViewFrame.size.width) - 110),height: 40))
        titleLabel.textColor = UIColor.black
        titleLabel.text = "PAYMENT"
        titleLabel.font = UIFont(name:"OpenSans-SemiBold", size: 15)!
        titleLabel.textAlignment = .center
        navigationView.addSubview(titleLabel)
        
        let logoImage = UIImageView.init(frame: CGRect(x: 25, y: 6, width: 33, height: 33))
        logoImage.image = UIImage.init(named: "nikon_login_logo")
        navigationView.addSubview(logoImage)
        
             backButton = UIButton.init(type: .custom)
            backButton.frame = CGRect(x: 0, y: 9, width: 20, height: 20)
            backButton.backgroundColor = UIColor.white
            backButton.setImage(UIImage.init(named: "back"), for: UIControlState())
            backButton.addTarget(self, action: #selector(PopView), for: .touchUpInside)
            navigationView.addSubview(backButton)
    
        
        self.navigationItem.titleView = navigationView
        
    }

    func postOrderId()  {
       
            let method = "GetRsaData"
            let param = "Merchant_Id=\(Constants.strPaymentMerchantId)&access_code=\(Constants.strPaymentAccessCode)&WorkingKey=\(Constants.strPaymentWorkingKey)&order_id=\(orderId)"
            
            if Reachability.isConnectedToNetwork() {
                Constants.appDelegate.startIndicator()
                
                Server.postRequestWithURL(Constants.BASEURL+method, paramString: param) { (response) in
                    
                    if response[Constants.STATE] != nil && response[Constants.STATE] as! String == Constants.SUCC {
                        
                        DispatchQueue.main.async
                        {

                        Constants.appDelegate.stopIndicator()
                        
//                       print(response["Rsafeed"] as! String)
                            
                            var rsaKey = response["Rsafeed"] as! String
                            
                            if rsaKey.contains("error")
                            {
                                self.backButton.isHidden = false
                                let alert = UIAlertController.init(title: "Nikon", message: rsaKey, preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                                    //
                                    DispatchQueue.main.async
                                    {
                                        
                                        self.navigationController?.popViewController(animated: true)
                                    }
                                    
                                    self.dismiss(animated: true, completion: nil)
                                }))
                                self.present(alert, animated: true, completion: nil)
                               return
                            }
                            self.backButton.isHidden = true

                            

//                        var rsaKey = String(response["Rsafeed"] as! String , NSASCIIStringEncoding)
                        rsaKey = rsaKey.trimmingCharacters(in: CharacterSet.newlines)
                        //rsaKey = rsaKey.trimmingCharacters(in: CharacterSet.newlines)
                            print(rsaKey)
                        
                        rsaKey = "-----BEGIN PUBLIC KEY-----\n\(rsaKey)\n-----END PUBLIC KEY-----\n"
                        print("\(rsaKey)")
                        let myRequestString = "amount=\(self.amount)&currency=\(Constants.strCurrencyType)"
                        let ccTool = CCTool.init()
                            
                        var encodeValue = String()
                            
                        encodeValue = ccTool.encryptRSA(myRequestString, key: rsaKey)
                        encodeValue = encodeValue.addingPercentEncoding(withAllowedCharacters: CharacterSet.alphanumerics)!
							
                            // LIVE URL
                            let urlString = "https://secure.ccavenue.com/transaction/initTrans"

                        let encrtptedStr = "merchant_id=\(Constants.strPaymentMerchantId)&order_id=\(self.orderId)&redirect_url=\(Constants.strRedirectUrl)&cancel_url=\(Constants.strCancelUrl)&enc_val=\(encodeValue)&access_code=\(Constants.strPaymentAccessCode)&amount=\(self.amount)currency\(Constants.strCurrencyType)&language=EN&billing_tel=\(self.userPhone)&billing_email=\(self.userEmail)&billing_city=\(self.usercityName)&billing_zip=\(self.userPincode)&billing_name=\(self.userFname)&billing_country=India&billing_address=\(self.userAddress)&delivery_tel=\(self.userPhone)&delivery_city=\(self.usercityName)&delivery_zip=\(self.userPincode)&delivery_name=\(self.userFname)&delivery_country=India&delivery_address=\(self.userAddress)&billing_state=\(self.userStateName)&delivery_state=\(self.userStateName)&merchant_param1=\(String(UserDefaults.standard.integer(forKey: Constants.USERID)))"
                        
                            
                        let urlRequest = NSMutableURLRequest(url: URL(string: urlString)!)
                        
                        let headers = [
                            "content-type": "application/x-www-form-urlencoded",
                            "cache-control": "no-cache",
                            ]
                        //        urlRequest.setValue( "application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
                        urlRequest.allHTTPHeaderFields = headers
                        urlRequest.httpMethod = "POST"
                        
                        let postData = NSData.init(data: encrtptedStr.data(using: String.Encoding.utf8)!) as Data
                        
                        urlRequest.httpBody = postData//paramString.dataUsingEncoding(NSUTF8StringEncoding)
                        self.paymentWebView.loadRequest(urlRequest as URLRequest)
                        }
                    
                    }
                    else {
                        Constants.appDelegate.stopIndicator()
                        var mess = String()
                        if response[Constants.MESS] != nil{
                            mess = response[Constants.MESS] as! String
                        }
                        else{
                            mess = response["Message"] as! String
                        }
                        CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "Nikon", "message": mess] )
                    }
                }
            }
            else {
                CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
            }
            
        }
    func webViewDidStartLoad(_ webView: UIWebView) {
        Constants.appDelegate.startIndicator()
    }
	
    func webViewDidFinishLoad(_ webView: UIWebView) {
        Constants.appDelegate.stopIndicator()
        let yourTargetUrl = webView.request?.url
        let successURLStr = "\(yourTargetUrl!)"
        if successURLStr.contains("https://webapi.nikonschool.in/payment/gotodashboard")
        {
        
           let  destViewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "Dashboard")
            
            sideMenuController()?.setContentViewController(destViewController)

        }
        print(successURLStr)
        
    }
    
  


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
	

}
