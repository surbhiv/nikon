//
//  AskQuestionVC.swift
//  NikonApp
//
//  Created by Hitesh Dhawan on 15/03/17.
//  Copyright © 2017 Neuronimbus. All rights reserved.
//

import UIKit

class AskQuestionVC: UIViewController,UITableViewDelegate,UITableViewDataSource,UIPickerViewDelegate,UIPickerViewDataSource,UITextFieldDelegate {

    @IBOutlet weak var questionTable: UITableView!
    @IBOutlet weak var searchTextfield: UITextField!
    
    @IBOutlet weak var postQuestionView: UIView!
    @IBOutlet weak var myQuestionView: UIView!
    @IBOutlet weak var myQuestionTable: UITableView!
    
    @IBOutlet weak var postQuestionTextfield: UITextField!
    @IBOutlet weak var selectTagTextfield: UITextField!
    @IBOutlet weak var questionSegmentControl: UISegmentedControl!
    
    var questionArray = [AnyObject]()
    var myQuestionArray = [AnyObject]()
    var tagTypeArray = [AnyObject]()
    var searchText = String()
    var postQuestionText = String()
    var tagValue = String()
    var isFromMenu = Bool()
    var selectedSegment = NSInteger()
    var picker = UIPickerView()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationController?.isNavigationBarHidden = false
        self.setNavigationBar()
        
        self.picker = UIPickerView.init()
        self.picker.dataSource = self
        self.picker.delegate = self

        self.selectTagTextfield.inputView = picker
        
        var  dict = Dictionary<String,AnyObject>()
        dict = ["Tagname":"Select Tag" as AnyObject,"TagId":0 as AnyObject]
        self.tagTypeArray.append(dict as AnyObject)
        
        self.searchText = ""
        self.tagValue = ""
        
        self.getTagTypeData()
        self.getQuestionData()

    }

    
    //MARK:- NAVIGATION
    func OpenSideMenu(_ sender: AnyObject){
        
        self.view.endEditing(true)
        toggleSideMenuView()
    }
    func PopView(_ sender: AnyObject){
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func setNavigationBar(){
        self.navigationItem.hidesBackButton = true
        let navigationViewFrame = ScreenSize.SCREEN//self.navigationController?.navigationBar.frame
        let navigationView = UIView.init(frame: CGRect(x: 0, y: 0, width: navigationViewFrame.size.width, height: 44))
        navigationView.backgroundColor = UIColor.clear
        let menuButton = UIButton.init(type: .custom)
        
        menuButton.frame = CGRect(x: ScreenSize.SCREEN_WIDTH - 45, y: 7, width: 25, height: 25)
        menuButton.setImage(UIImage.init(named: "sideMenu_Icon"), for: UIControlState())
        menuButton.addTarget(self, action: #selector(OpenSideMenu), for: .touchUpInside)
        navigationView.addSubview(menuButton)
        
        let titleLabel = UILabel.init(frame:CGRect(x: 60,y: 0,width: ( (navigationViewFrame.size.width) - 110),height: 40))
        titleLabel.textColor = UIColor.black
        titleLabel.text = "Ask A Question"
        titleLabel.font = UIFont.init(name:"OpenSans-SemiBold", size: 15)// UIFont(name:"OpenSans-SemiBold", size: 15)!
        titleLabel.textAlignment = .center
        navigationView.addSubview(titleLabel)
        
        let logoImage = UIImageView.init(frame: CGRect(x: 0, y: 6, width: 33, height: 33))
        logoImage.image = UIImage.init(named: "nikon_login_logo")
        navigationView.addSubview(logoImage)
        
        if isFromMenu == false
        {
            let backButton = UIButton.init(type: .custom)
            backButton.frame = CGRect(x: 0, y: 9, width: 20, height: 20)
            backButton.backgroundColor = UIColor.white
            backButton.setImage(UIImage.init(named: "back"), for: UIControlState())
            backButton.addTarget(self, action: #selector(PopView), for: .touchUpInside)
            navigationView.addSubview(backButton)
        }

        self.navigationItem.titleView = navigationView
        
    }

    //MARK:- API
    func getTagTypeData()  {
        if Reachability.isConnectedToNetwork()
        {
            
            
            var method = "AskaQuestion"
            method = method.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            
            Constants.appDelegate.startIndicator()
            Server.getRequestWithURL(Constants.BASEURL+method, completionHandler: { (response) in
                
                
                if response[Constants.STATE] != nil && response[Constants.STATE] as! String == Constants.SUCC
                {
                    DispatchQueue.main.async
                    {
                        Constants.appDelegate.stopIndicator()
                        let tempArray = response["TagList"]as! NSArray as [AnyObject]
                        for dict in tempArray
                        {
                            self.tagTypeArray.append(dict)
                            
                        }
                        
                        
                        
                    }
                }
                else
                {
                    DispatchQueue.main.async
                    {
                        Constants.appDelegate.stopIndicator()
                        
                    }
                }
               

                
                
            })
        }
        else
        {
            Constants.appDelegate.stopIndicator()
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
        
        
    }
    
    
    func getQuestionData()  {
        if Reachability.isConnectedToNetwork()
        {
            
            "&Userid=\(UserDefaults.standard.integer(forKey: Constants.USERID))"
            var method = "AskaQuestion?searchtext=\(self.searchText)"
            method = method.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            
            Constants.appDelegate.startIndicator()
            Server.getRequestWithURL(Constants.BASEURL+method, completionHandler: { (response) in
                
                
                if response[Constants.STATE] != nil && response[Constants.STATE] as! String == Constants.SUCC
                {
                    DispatchQueue.main.async
                    {
                        Constants.appDelegate.stopIndicator()
                        self.questionArray = response["AskQuestion"]as! NSArray as [AnyObject]
                        
                        self.questionTable.reloadData()
                    }
                }
                else
                {
                    DispatchQueue.main.async
                    {
                        Constants.appDelegate.stopIndicator()
                        if self.questionArray.count > 0
                        {
                            self.questionArray.removeAll()
                        }
                        self.questionTable.reloadData()
                        
                    }
                }
                
                
            })
        }
        else
        {
            Constants.appDelegate.stopIndicator()
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
        
        
    }
    
    func postQuestionData()  {
        if Reachability.isConnectedToNetwork()
        {
            
            let param = "question=\(self.postQuestionText)&Userid=\(UserDefaults.standard.integer(forKey: Constants.USERID))&TagId=\(self.tagValue)"
            
            var method = "AskaQuestion?"
            method = method.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            
            Constants.appDelegate.startIndicator()
            Server.postRequestWithURL(Constants.BASEURL+method, paramString: param, completionHandler: { (response) in
            
                if response[Constants.STATE] != nil && response[Constants.STATE] as! String == Constants.SUCC
                {
                    DispatchQueue.main.async
                    {
                        Constants.appDelegate.stopIndicator()
                    
                        var mess = String()
                        if response[Constants.MESS] != nil{
                            mess = response[Constants.MESS] as! String
                        }
                        else{
                            mess = response["Message"] as! String
                        }
                        
                        let alert = UIAlertController.init(title: "Nikon", message: mess, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                            self.dismiss(animated: true, completion: nil)
                        }))
                        self.present(alert, animated: true, completion: nil)


                        
                    }
                }
                else
                {
                    DispatchQueue.main.async
                    {
                        Constants.appDelegate.stopIndicator()
                       
                        var mess = String()
                        if response[Constants.MESS] != nil{
                            mess = response[Constants.MESS] as! String
                        }
                        else{
                            mess = response["Message"] as! String
                        }
                        
                        let alert = UIAlertController.init(title: "Nikon", message: mess, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                            self.dismiss(animated: true, completion: nil)
                        }))
                        self.present(alert, animated: true, completion: nil)

                    }
                }
                
                
            })
        }
        else
        {
            Constants.appDelegate.stopIndicator()
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
        
        
    }
    func getMyQuestionData()  {
        if Reachability.isConnectedToNetwork()
        {
            
            
            var method = "AskaQuestion?searchtext=&Userid=\(UserDefaults.standard.integer(forKey: Constants.USERID))"
            method = method.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            
            Constants.appDelegate.startIndicator()
            Server.getRequestWithURL(Constants.BASEURL+method, completionHandler: { (response) in
                
                
                if response[Constants.STATE] != nil && response[Constants.STATE] as! String == Constants.SUCC
                {
                    DispatchQueue.main.async
                    {
                        Constants.appDelegate.stopIndicator()
                        
                        self.myQuestionArray = response["AskQuestion"]as! NSArray as [AnyObject]
                        
                        self.myQuestionTable.reloadData()
                    }
                }
                else
                {
                    DispatchQueue.main.async
                    {
                        Constants.appDelegate.stopIndicator()
                        
                        if self.myQuestionArray.count > 0
                        {
                            self.myQuestionArray.removeAll()
                        }
                        self.myQuestionTable.reloadData()
                        
                        var mess = String()
                        if response[Constants.MESS] != nil{
                            mess = response[Constants.MESS] as! String
                        }
                        else{
                            mess = response["Message"] as! String
                        }
                        
                        let alert = UIAlertController.init(title: "Nikon", message: mess, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                            self.dismiss(animated: true, completion: nil)
                        }))
                        self.present(alert, animated: true, completion: nil)

                        
                        
                        
                    }
                }
                
                
            })
        }
        else
        {
            Constants.appDelegate.stopIndicator()
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
        
        
    }


    
    //MARK:- TABLEVIEW
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.myQuestionTable
        {
            return myQuestionArray.count
        }
        else{
        return questionArray.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == questionTable
        {
        let cell = tableView.dequeueReusableCell(withIdentifier: "QuestionCell", for: indexPath)as! QuestionCell
            let questionStr = self.questionArray[indexPath.row]["question"] as? String
            let answerStr = self.questionArray[indexPath.row]["answer"] as? String
            do
            {
                let atributedStr = try NSAttributedString.init(data: questionStr!.data(using: String.Encoding.unicode)!, options: [ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType ], documentAttributes: nil)
                cell.questionLbl.attributedText = atributedStr
            }
            catch
            {
                print("error occure")
                
            }
            
            do
            {
                let atributedStr1 = try NSAttributedString.init(data: answerStr!.data(using: String.Encoding.unicode)!, options: [ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType ], documentAttributes: nil)
                cell.answerLbl.attributedText = atributedStr1
            }
            catch
            {
                print("error occure")
                
            }

//        cell.questionLbl.text = self.questionArray[indexPath.row]["question"] as? String
//        cell.answerLbl.text = self.questionArray[indexPath.row]["answer"] as? String
        cell.userNameLbl.text = "-\(self.questionArray[indexPath.row]["UserName"] as! String)-"
        tableView.estimatedRowHeight = 65
        tableView.rowHeight = UITableViewAutomaticDimension
        
        cell.selectionStyle = .none
        return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "QuestionCell", for: indexPath)as! QuestionCell
            
            let questionStr = self.myQuestionArray[indexPath.row]["question"] as? String
            let answerStr = self.myQuestionArray[indexPath.row]["answer"] as? String
            do
            {
                let atributedStr = try NSAttributedString.init(data: questionStr!.data(using: String.Encoding.unicode)!, options: [ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType ], documentAttributes: nil)
                cell.questionLbl.attributedText = atributedStr
            }
            catch
            {
                print("error occure")
                
            }
            
            do
            {
                let atributedStr1 = try NSAttributedString.init(data: answerStr!.data(using: String.Encoding.unicode)!, options: [ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType ], documentAttributes: nil)
                cell.answerLbl.attributedText = atributedStr1
            }
            catch
            {
                print("error occure")
                
            }

//            cell.questionLbl.text = self.myQuestionArray[indexPath.row]["question"] as? String
//            cell.answerLbl.text = self.myQuestionArray[indexPath.row]["answer"] as? String
            cell.userNameLbl.text = "-\(self.myQuestionArray[indexPath.row]["UserName"] as! String)-"
            tableView.estimatedRowHeight = 65
            tableView.rowHeight = UITableViewAutomaticDimension
            
            cell.selectionStyle = .none
            return cell

            
        }
    }
    
    
    //MARK:- picker delegate
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return tagTypeArray.count
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let value = tagTypeArray [row]["Tagname"] as! String
        return String.init(format: value , locale: nil)
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
//        let value = tagTypeArray [row]["TagId"] as! Int
        let value = tagTypeArray [row]["TagId"] as! NSNumber

        let value1 = tagTypeArray [row]["Tagname"] as! String
        if value == 0
        {
            tagValue = ""
        }
        else
        {
            tagValue = "\(value)"
        }
        self.selectTagTextfield.text = value1
    }
    
    // MARK: TEXTFIELD DELEGATE
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == self.searchTextfield
        {
            self.searchText = self.searchTextfield.text!
        }
        if textField == postQuestionTextfield
        {
            self.postQuestionText = self.postQuestionTextfield.text!
        }
        textField .resignFirstResponder()
        //self.Searchtext = self.searchTextfield.text!
        
    }
    
    //MARK:- Button Action
    
    @IBAction func searchHelpButtonClicked(_ sender: AnyObject)
    {
        self.searchTextfield.resignFirstResponder()
        self.getQuestionData()
        
    }

    @IBAction func submitQuestionButtonClicked(_ sender: AnyObject)
    {
        self.postQuestionTextfield.resignFirstResponder()
        
        if (UserDefaults.standard.integer(forKey: Constants.USERID) != 0)
        {
            if tagValue == "0" || tagValue == ""
            {
                let alert = UIAlertController.init(title: "Nikon", message: "Please select tag value", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                    self.dismiss(animated: true, completion: nil)
                }))
                self.present(alert, animated: true, completion: nil)
                
                
            }
            else if self.postQuestionText == ""
            {
                let alert = UIAlertController.init(title: "Nikon", message: "Please ask a question", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                    self.dismiss(animated: true, completion: nil)
                }))
                self.present(alert, animated: true, completion: nil)
                
            }
            else
            {
                
                self.postQuestionData()
            }

        }
        else
        {
            self.loginAlert()
        }

        
    }
    @IBAction func questionSegmentValueChanged(_ sender: AnyObject)
    {
        if self.questionSegmentControl.selectedSegmentIndex == 0
        {
            selectedSegment = 0
            self.postQuestionView.isHidden = true
            self.myQuestionView.isHidden = true
            self.questionTable.isHidden = false
            self.searchText = ""
            self.searchTextfield.text = ""
            self.getQuestionData()

            
        }
        else if self.questionSegmentControl.selectedSegmentIndex == 1
        {
            selectedSegment = 1
            self.postQuestionView.isHidden = false
            self.myQuestionView.isHidden = true
            self.questionTable.isHidden = true
        }
        else
        {
            if (UserDefaults.standard.integer(forKey: Constants.USERID) != 0)
            {
                self.getMyQuestionData()
                self.postQuestionView.isHidden = true
                self.myQuestionView.isHidden = false
                self.questionTable.isHidden = true
                
            }
            else
            {
                self.questionSegmentControl.selectedSegmentIndex = selectedSegment
                self.loginAlert()
            }
            
        }
        
    }
    //MARK:- LoginAlert
    

    func loginAlert()  {
        let alert = UIAlertController.init(title: "Nikon", message: "Please login to continue.", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
            
            DispatchQueue.main.async
            {
                let destinationController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "LoginVC")as! LoginVC
                destinationController.isWantTodismissVC = true
                self.present(destinationController, animated: true, completion: nil);            }
        }))
        alert.addAction(UIAlertAction.init(title: "Cancel", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
            self.dismiss(animated: true, completion: nil)
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
