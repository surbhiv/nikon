//
//  WebVC.swift
//  NikonApp
//
//  Created by Surbhi on 20/12/16.
//  Copyright © 2016 Neuronimbus. All rights reserved.
//

import UIKit

class WebVC: UIViewController, UIWebViewDelegate {

    var loadingURLSTR = String()
    var titleString = String()
    var idStr = String()
    var isPush = Bool()
    var isTermsType = String()
    var isFromMenu = Bool()
    @IBOutlet weak var shadowView: UIView!
    
    @IBOutlet weak var myWebView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        self.setNavigationBar()
        
            self.shadowView.layer.shadowColor = UIColor.gray.cgColor
        self.shadowView.layer.shadowOpacity = 2
        self.shadowView.layer.shadowOffset = CGSize.zero
        self.shadowView.layer.shadowRadius = 5
        self.shadowView.clipsToBounds = false
        //self.shadowView.layer.borderWidth = 0.4
        

        
        if title == "ABOUT US"{
            idStr = "aboutus"
        }
        else if title == "CONTACT US"{
            idStr = "contactus"
        }
        else if title == "TERMS & CONDITIONS"{
            
            if isTermsType == "LOGIN" {
                idStr = "Login_Terms_condition"
            }
            else{
                idStr =  "Loyalty_Terms_condition" //"term_condition"
            }
            
            
            
        }
        else if title == "PRIVACY POLICY"{
            idStr = "privacypolicy"
        }
        
        self.loadDescription()
        
    

    }
    
    func loadDescription() // recipes/featured/user_id/
    {
        if Reachability.isConnectedToNetwork()
        {
            
            if (title?.contains("CONTEST"))! || (title?.contains("OFFER"))!{
                self.myWebView.loadRequest(URLRequest.init(url: URL.init(string: self.loadingURLSTR)!))
                return
            }
            
            let method = "PageData?Pagetype="
            
            Server.getRequestWithURL(Constants.BASEURL+method+idStr, completionHandler: { (response) in
                
                if response[Constants.STATE] != nil && response[Constants.STATE] as! String == Constants.SUCC {
                    let dict = response 
                    self.loadingURLSTR = dict["Pagedata"] as! String
                    DispatchQueue.main.async
                    {
                        Constants.appDelegate.startIndicator()
                        self.myWebView.loadHTMLString(self.loadingURLSTR, baseURL: Bundle.main.bundleURL)
                    }
                }
                else {
                    var mess = String()
                    if response[Constants.MESS] != nil{
                        mess = response[Constants.MESS] as! String
                    }
                    else{
                        mess = response["Message"] as! String
                    }
                    DispatchQueue.main.async
                    {
                    let alert = UIAlertController.init(title: "Nikon", message: mess, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                        self.dismiss(animated: true, completion: nil)
                    }))
                    self.present(alert, animated: true, completion: nil)
                    }
                }
            })
        }
        else
        {
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func OpenSideMenu(_ sender: AnyObject){
        
        self.view.endEditing(true)
        toggleSideMenuView()
    }
    func PopView(_ sender: AnyObject){
        
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- Side Menu
    func setNavigationBar(){
        
            self.navigationItem.hidesBackButton = true
            let navigationViewFrame = self.navigationController?.navigationBar.frame
            let navigationView = UIView.init(frame: navigationViewFrame!)
            navigationView.backgroundColor = UIColor.white
        
        
            if isPush == true{
                let backButton = UIButton.init(type: .custom)
                backButton.frame = CGRect(x: 0, y: 9, width: 20, height: 20)
                backButton.backgroundColor = UIColor.white
                backButton.setImage(UIImage.init(named: "back"), for: UIControlState())
                backButton.addTarget(self, action: #selector(PopView), for: .touchUpInside)
                navigationView.addSubview(backButton)
                
                let logoImage = UIImageView.init(frame: CGRect(x: 25, y: 6, width: 33, height: 33))
                logoImage.image = UIImage.init(named: "nikon_login_logo")
                navigationView.addSubview(logoImage)
            }
            else{
                let logoImage = UIImageView.init(frame: CGRect(x: 0, y: 6, width: 33, height: 33))
                logoImage.image = UIImage.init(named: "nikon_login_logo")
                navigationView.addSubview(logoImage)
                
                let menuButton = UIButton.init(type: .custom)
                menuButton.frame = CGRect(x: ScreenSize.SCREEN_WIDTH - 45, y: 7, width: 25, height: 25)
                menuButton.setImage(UIImage.init(named: "sideMenu_Icon"), for: UIControlState())
                menuButton.addTarget(self, action: #selector(OpenSideMenu), for: .touchUpInside)
                navigationView.addSubview(menuButton)
            }
       
        
        let titleLabel = UILabel.init(frame:CGRect(x: ( (navigationViewFrame?.size.width)! - 220)/2,y: 0,width: 220,height: 44))
        titleLabel.textColor = UIColor.black
        titleLabel.text = title
        titleLabel.font = UIFont(name:"OpenSans-SemiBold", size: 15)!
        titleLabel.textAlignment = .center
        navigationView.addSubview(titleLabel)

        
            self.navigationItem.titleView = navigationView
    }
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        if request.url?.scheme == "mailto" || request.url?.scheme == "tel" {
            
           // print(request.URL)
            let url = request.url!
            UIApplication.shared.openURL(url)
        }
        
        return true
    }
    func webViewDidStartLoad(_ webView: UIWebView) {
        Constants.appDelegate.startIndicator()
       
        
    }
    func webViewDidFinishLoad(_ webView: UIWebView)
    {
         Constants.appDelegate.stopIndicator()
    }
    
    



}
