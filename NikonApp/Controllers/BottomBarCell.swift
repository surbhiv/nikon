//
//  BottomBarCell.swift
//  NikonApp
//
//  Created by Arnab Maity on 07/02/20.
//  Copyright © 2020 Neuronimbus. All rights reserved.
//

import UIKit

class BottomBarCell: UICollectionViewCell {
    
	@IBOutlet weak var bottomImg: UIImageView!
	
	@IBOutlet weak var bottomLbl: UILabel!
	
	
}
