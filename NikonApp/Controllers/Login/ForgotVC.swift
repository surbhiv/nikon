//
//  ForgotVC.swift
//  NikonApp
//
//  Created by Surbhi on 19/12/16.
//  Copyright © 2016 Neuronimbus. All rights reserved.
//

import UIKit

class ForgotVC: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var okButton: UIButton!
    @IBOutlet weak var CancelButton: UIButton!
    @IBOutlet weak var forgotView: UIView!

    
    override func viewDidLoad() {
        super.viewDidLoad()

        forgotView.layer.cornerRadius = 5.0
        forgotView.clipsToBounds = true
        emailTextField.becomeFirstResponder()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func cancelPressed(_ sender: AnyObject) {
        emailTextField.resignFirstResponder()
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func OkPressed(_ sender: AnyObject) {
        
        emailTextField.resignFirstResponder()
        let method = "ForgotPassword"
        let param = "Email=\(emailTextField.text!)"
        
        if emailTextField.text != "" && Helper.isValidEmail(emailTextField.text!) == true {
            if Reachability.isConnectedToNetwork() {
                Constants.appDelegate.startIndicator()
                Server.postRequestWithURL(Constants.BASEURL+method, paramString: param) { (response) in
                    Constants.appDelegate.stopIndicator()
                    if response[Constants.STATE] != nil && response[Constants.STATE] as! String == Constants.SUCC {
                        
                        DispatchQueue.main.async(execute: {
                            let alert = UIAlertController.init(title: "Nikon", message: response["message"]! as? String, preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                                self.dismiss(animated: true, completion: nil)
                            }))
                            self.present(alert, animated: true, completion: nil)
                        })
                    }
                    else {
                        var mess = String()
                        if response[Constants.MESS] != nil{
                            mess = response[Constants.MESS] as! String
                        }
                        else{
                            mess = response["Message"] as! String
                        }
                        DispatchQueue.main.async(execute: {
                           CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":mess])
                        })

                    }
                }
            }
            else {
                DispatchQueue.main.async(execute: {
                CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
                })
            }
        }
        else{
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "Nikon", "message":"Please enter a valid email address."])

        }
        
    }
    
    @IBAction func FadeViewPressed(_ sender: AnyObject) {
        emailTextField.resignFirstResponder()
        self.dismiss(animated: true, completion: nil)
    }

}
