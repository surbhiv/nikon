//
//  LoginView.swift
//  NikonApp
//
//  Created by Surbhi on 15/12/16.
//  Copyright © 2016 Neuronimbus. All rights reserved.
//

import UIKit

class LoginView: UIViewController {

    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var forgotPass: UIButton!
    @IBOutlet weak var fbLogin: UIButton!
    @IBOutlet weak var googleLogin: UIButton!
    @IBOutlet weak var termsButton: UIButton!
    @IBOutlet weak var privacyButton: UIButton!

    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var emailtext: UITextField!
    @IBOutlet weak var emailLine: UILabel!
    @IBOutlet weak var emailLineHeight: NSLayoutConstraint!
    
    @IBOutlet weak var passLabel: UILabel!
    @IBOutlet weak var passText: UITextField!
    @IBOutlet weak var passLine: UILabel!
    @IBOutlet weak var passLineHeight: NSLayoutConstraint!
    
    @IBOutlet weak var guestBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        emailLabel.isHidden = true
        passLabel.isHidden = true
        
        DispatchQueue.main.async {
            // Do stuff to UI
            self.emailtext.textColor = UIColor.white
            self.passText.textColor = UIColor.white
            self.emailtext.font = UIFont(name: "OpenSans", size: 14.5)
            self.passText.font = UIFont(name: "OpenSans", size: 14.5)

            self.emailtext.attributedPlaceholder = NSAttributedString(string: "E-MAIL ID",  attributes: [NSForegroundColorAttributeName : Colors.textPlaceHolderGray, NSFontAttributeName : UIFont(name: "OpenSans", size: 14)!])
            self.passText.attributedPlaceholder = NSAttributedString(string: "PASSWORD",  attributes: [NSForegroundColorAttributeName : Colors.textPlaceHolderGray, NSFontAttributeName : UIFont(name: "OpenSans", size: 14)!])
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: TEXTFIELD DELEGATE
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == emailtext {
            ACTIVATETextField(emailtext, textLabel: emailLabel, line: emailLine, height: emailLineHeight)
        }
        else if textField == passText {
            ACTIVATETextField(passText, textLabel: passLabel, line: passLine, height: passLineHeight)
        }
    }
    
    func ACTIVATETextField(_ textf : UITextField,textLabel : UILabel , line: UILabel, height : NSLayoutConstraint)
    {
        UIView.animate(withDuration: 1.0, delay: 0, options: UIViewAnimationOptions.transitionFlipFromBottom, animations: {
            
            textLabel.isHidden = false
            textf.attributedPlaceholder = NSAttributedString(string: "")
            Helper.activatetextField(line,height: height,textlabel: textLabel)
            
            },
                                   completion: { (true) in
                                    textLabel.isHidden = false
        })
    }
    
    
    func DEACTIVATETextField(_ textf : UITextField,textLabel : UILabel , line: UILabel, height : NSLayoutConstraint)
    {
        if textf.text == ""
        {
            UIView.animate(withDuration: 1.0, animations: {
                textLabel.isHidden = true
                
                Helper.deActivatetextField(line,height: height,textlabel: textLabel)
                
                textf.attributedPlaceholder = NSAttributedString(string: textLabel.text! ,  attributes: [NSForegroundColorAttributeName : Colors.textPlaceHolderGray, NSFontAttributeName : UIFont(name: "OpenSans", size: 14)!])
                }, completion: nil)
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == emailtext {
            DEACTIVATETextField(emailtext, textLabel: emailLabel, line: emailLine, height: emailLineHeight)
        }
        else if textField == passText {
            DEACTIVATETextField(passText, textLabel: passLabel, line: passLine, height: passLineHeight)
        }
    }
}
