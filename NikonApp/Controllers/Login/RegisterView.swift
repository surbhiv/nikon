//
//  RegisterView.swift
//  NikonApp
//
//  Created by Surbhi on 15/12/16.
//  Copyright © 2016 Neuronimbus. All rights reserved.
//

import UIKit

class RegisterView: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var fnameLabel: UILabel!
    @IBOutlet weak var fnameText: UITextField!
    @IBOutlet weak var fnameLine: UILabel!
    @IBOutlet weak var fnameLineHeight: NSLayoutConstraint!
    
    @IBOutlet weak var lnameLabel: UILabel!
    @IBOutlet weak var lnameText: UITextField!
    @IBOutlet weak var lnameLine: UILabel!
    @IBOutlet weak var lnameLineHeight: NSLayoutConstraint!
    
    @IBOutlet weak var mobileLabel: UILabel!
    @IBOutlet weak var mobiletext: UITextField!
    @IBOutlet weak var mobileLine: UILabel!
    @IBOutlet weak var mobileLineHeight: NSLayoutConstraint!
    
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var emailtext: UITextField!
    @IBOutlet weak var emailLine: UILabel!
    @IBOutlet weak var emailLineHeight: NSLayoutConstraint!
    
    @IBOutlet weak var passLabel: UILabel!
    @IBOutlet weak var passText: UITextField!
    @IBOutlet weak var passLine: UILabel!
    @IBOutlet weak var passLineHeight: NSLayoutConstraint!

    @IBOutlet weak var confirmPassLabel: UILabel!
    @IBOutlet weak var confirmPassText: UITextField!
    @IBOutlet weak var confirmPassLine: UILabel!
    @IBOutlet weak var confirmPassLineHeight: NSLayoutConstraint!
    
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var dayText: UITextField!
    @IBOutlet weak var dayLine: UILabel!
    @IBOutlet weak var dayLineHeight: NSLayoutConstraint!
    
    @IBOutlet weak var genderlabel: UILabel!
    @IBOutlet weak var maleBtn: UIButton!
    @IBOutlet weak var femaleBtn: UIButton!
    var genderSelected : String = ""
    
    
    @IBOutlet weak var registerButton: UIButton!
    var datePicker = UIDatePicker()
    var currentYear = Int()

    //MARK: -  Start
    override func viewDidLoad() {
        super.viewDidLoad()
		self.hideKeyboardWhenTappedAround()
        emailLabel.isHidden = true
        fnameLabel.isHidden = true
        lnameLabel.isHidden = true
        mobileLabel.isHidden = true
        passLabel.isHidden = true
        confirmPassLabel.isHidden = true
        dayLabel.isHidden = true

        let date = Date()
        let calendar = Calendar.current
        let components = (calendar as NSCalendar).components([.year], from: date)
        currentYear =  components.year!
        
        datePicker.datePickerMode = .date
        datePicker.maximumDate = Date.init()
        self.dayText.inputView = datePicker
        datePicker.addTarget(self, action: #selector(datePickerValueChanged), for: UIControlEvents.valueChanged)

        
        DispatchQueue.main.async {
            // Do stuff to UI
            
            self.emailtext.textColor = UIColor.white
            self.passText.textColor = UIColor.white
            self.fnameText.textColor = UIColor.white
            self.lnameText.textColor = UIColor.white
            self.mobiletext.textColor = UIColor.white
            self.confirmPassText.textColor = UIColor.white
            self.dayText.textColor = UIColor.white

            self.emailtext.font = UIFont(name: "OpenSans", size: 14.5)
            self.passText.font = UIFont(name: "OpenSans", size: 14.5)
            self.fnameText.font = UIFont(name: "OpenSans", size: 14.5)
            self.lnameText.font = UIFont(name: "OpenSans", size: 14.5)
            self.mobiletext.font = UIFont(name: "OpenSans", size: 14.5)
            self.confirmPassText.font = UIFont(name: "OpenSans", size: 14.5)
            self.dayText.font = UIFont(name: "OpenSans", size: 14.5)

            
            self.fnameText.attributedPlaceholder = NSAttributedString(string: "FIRST NAME",  attributes: [NSForegroundColorAttributeName : Colors.textPlaceHolderGray, NSFontAttributeName : UIFont(name: "OpenSans", size: 14)!])
            self.lnameText.attributedPlaceholder = NSAttributedString(string: "LAST NAME",  attributes: [NSForegroundColorAttributeName : Colors.textPlaceHolderGray, NSFontAttributeName : UIFont(name: "OpenSans", size: 14)!])
            self.mobiletext.attributedPlaceholder = NSAttributedString(string: "MOBILE NUMBER (OPTIONAL)",  attributes: [NSForegroundColorAttributeName : Colors.textPlaceHolderGray, NSFontAttributeName : UIFont(name: "OpenSans", size: 14)!])
            self.emailtext.attributedPlaceholder = NSAttributedString(string: "E-MAIL ID",  attributes: [NSForegroundColorAttributeName : Colors.textPlaceHolderGray, NSFontAttributeName : UIFont(name: "OpenSans", size: 14)!])
            self.passText.attributedPlaceholder = NSAttributedString(string: "PASSWORD (atleast 6 characters)",  attributes: [NSForegroundColorAttributeName : Colors.textPlaceHolderGray, NSFontAttributeName : UIFont(name: "OpenSans", size: 14)!])
            self.confirmPassText.attributedPlaceholder = NSAttributedString(string: "CONFIRM PASSWORD (atleast 6 characters)",  attributes: [NSForegroundColorAttributeName : Colors.textPlaceHolderGray, NSFontAttributeName : UIFont(name: "OpenSans", size: 14)!])
            self.dayText.attributedPlaceholder = NSAttributedString(string: "DATE OF BIRTH (OPTIONAL)",  attributes: [NSForegroundColorAttributeName : Colors.textPlaceHolderGray, NSFontAttributeName : UIFont(name: "OpenSans", size: 14)!])

        }
        // Do any additional setup after loading the view.
    }

    func datePickerValueChanged(_ sender:UIDatePicker) {
        
        let dateFormatter = DateFormatter()

        dateFormatter.dateFormat = " dd MMM, yyyy"
        let dateString = dateFormatter.string(from: sender.date)
        
        dateFormatter.dateFormat = "MM/dd/yyyy"
        let SendingdateString = dateFormatter.string(from: sender.date)

        dayText.text = dateString
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: TEXTFIELD DELEGATE
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == fnameText {
            ACTIVATETextField(fnameText, textLabel: fnameLabel, line: fnameLine, height: fnameLineHeight)
        }
        else if textField == lnameText {
            ACTIVATETextField(lnameText, textLabel: lnameLabel, line: lnameLine, height: lnameLineHeight)
        }
        else if textField == mobiletext {
            ACTIVATETextField(mobiletext, textLabel: mobileLabel, line: mobileLine, height: mobileLineHeight)
        }
        else if textField == emailtext {
            ACTIVATETextField(emailtext, textLabel: emailLabel, line: emailLine, height: emailLineHeight)
        }
        else if textField == passText {
            ACTIVATETextField(passText, textLabel: passLabel, line: passLine, height: passLineHeight)
        }
        else if textField == confirmPassText {
            ACTIVATETextField(confirmPassText, textLabel: confirmPassLabel, line: confirmPassLine, height: confirmPassLineHeight)
        }
        else if textField == dayText {

            ACTIVATETextField(dayText, textLabel: dayLabel, line: dayLine, height: dayLineHeight)
        }

    }
    
    func ACTIVATETextField(_ textf : UITextField,textLabel : UILabel , line: UILabel, height : NSLayoutConstraint)
    {
        UIView.animate(withDuration: 1.0, delay: 0, options: UIViewAnimationOptions.transitionFlipFromBottom, animations: {
            
            textLabel.isHidden = false
            textf.attributedPlaceholder = NSAttributedString(string: "")
            Helper.activatetextField(line,height: height,textlabel: textLabel)
            
            },
            completion: { (true) in
                                    textLabel.isHidden = false
        })
    }
    
    
    func DEACTIVATETextField(_ textf : UITextField,textLabel : UILabel , line: UILabel, height : NSLayoutConstraint)
    {
        if textf.text == ""
        {
            UIView.animate(withDuration: 1.0, animations: {
                textLabel.isHidden = true
                
                Helper.deActivatetextField(line,height: height,textlabel: textLabel)
                
                var mess = textLabel.text!
                
                if textf == self.dayText {
                    mess = "DATE OF BIRTH (OPTIONAL)"
                }

                textf.attributedPlaceholder = NSAttributedString(string:mess ,  attributes: [NSForegroundColorAttributeName : Colors.textPlaceHolderGray, NSFontAttributeName : UIFont(name: "OpenSans", size: 14)!])

                }, completion: nil)
        }
        else{
            textLabel.isHidden = false
            Helper.deActivatetextField(line,height: height,textlabel: textLabel)

        }

    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == fnameText {
            DEACTIVATETextField(fnameText, textLabel: fnameLabel, line: fnameLine, height: fnameLineHeight)
        }
        else if textField == lnameText {
            DEACTIVATETextField(lnameText, textLabel: lnameLabel, line: lnameLine, height: lnameLineHeight)
        }
        else if textField == mobiletext {
            DEACTIVATETextField(mobiletext, textLabel: mobileLabel, line: mobileLine, height: mobileLineHeight)
        }
        else if textField == emailtext {
            DEACTIVATETextField(emailtext, textLabel: emailLabel, line: emailLine, height: emailLineHeight)
        }
        else if textField == passText {
            DEACTIVATETextField(passText, textLabel: passLabel, line: passLine, height: passLineHeight)
        }
        else if textField == confirmPassText {
            DEACTIVATETextField(confirmPassText, textLabel: confirmPassLabel, line: confirmPassLine, height: confirmPassLineHeight)
        }
        else if textField == dayText {
            DEACTIVATETextField(dayText, textLabel: dayLabel, line: dayLine, height: dayLineHeight)
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == mobiletext {
            if (range.location > 9)
            {
                return false
            }
        }
        
        return true
    }
    
    //MARK:- MALE BUTTON PRESSED
    @IBAction func MaleButtonPressed(_ sender : AnyObject){
        
        emailtext.resignFirstResponder()
        passText.resignFirstResponder()
        fnameText.resignFirstResponder()
        lnameText.resignFirstResponder()
        confirmPassText.resignFirstResponder()
        mobiletext.resignFirstResponder()
        dayText.resignFirstResponder()
        
        maleBtn.isSelected = true
        femaleBtn.isSelected = false
        genderSelected = "Male"
    }
    
    @IBAction func FeMaleButtonPressed(_ sender : AnyObject){
        emailtext.resignFirstResponder()
        passText.resignFirstResponder()
        fnameText.resignFirstResponder()
        lnameText.resignFirstResponder()
        confirmPassText.resignFirstResponder()
        mobiletext.resignFirstResponder()
        dayText.resignFirstResponder()
        
        maleBtn.isSelected = false
        femaleBtn.isSelected = true
        genderSelected = "Female"
    }

    
}
