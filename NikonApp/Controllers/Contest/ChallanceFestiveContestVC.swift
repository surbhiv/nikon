//
//  ChallanceFestiveContestVC.swift
//  NikonApp
//
//  Created by Surbhi on 08/09/17.
//  Copyright © 2017 Neuronimbus. All rights reserved.
//

import UIKit

class ChallanceFestiveContestVC: UIViewController, UIImagePickerControllerDelegate,UIPopoverControllerDelegate,UINavigationControllerDelegate {


    @IBOutlet weak var festiveBackImage: UIImageView!
    @IBOutlet weak var viewForUploadImage: UIView!
    @IBOutlet weak var userChooseImage: UIImageView!
    @IBOutlet weak var categoryOfImage: UILabel!
    @IBOutlet weak var userImageDescription: UITextView!

    var imageDataIMG = Data()
    var imgURLStr = String()
    var categoryStr = String()
    var userEmail = String()
    var imageDataToSend = String()
    var imageURLStr = String()
    var backimageURLStr = String()

    var picker:UIImagePickerController?=UIImagePickerController()

    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.festiveBackImage.image = UIImage.init(named: "product_placeholder.jpg")
        self.festiveBackImage.imageFromServerURL(self.backimageURLStr)
        
        let profile = Helper.getDataFromNsDefault(Constants.PROFILE)
        userEmail = profile["User_Email"]as! String
        
        picker?.delegate = self
        userImageDescription.layer.borderColor = UIColor.black.cgColor
        userImageDescription.layer.borderWidth = 0.7
        userImageDescription.clipsToBounds = true
        self.navigationController?.isNavigationBarHidden = false
        self.setNavigationBar()

        // Do any additional setup after loading the view.
    }

    //MARK:- NAVIGATION
    func OpenSideMenu(_ sender: AnyObject){
        
        self.view.endEditing(true)
        toggleSideMenuView()
    }
    
    func PopView(_ sender: AnyObject){
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func setNavigationBar(){
        self.navigationItem.hidesBackButton = true
        let navigationViewFrame = ScreenSize.SCREEN//self.navigationController?.navigationBar.frame
        let navigationView = UIView.init(frame: CGRect(x: 0, y: 0, width: navigationViewFrame.size.width, height: 44))
        navigationView.backgroundColor = UIColor.clear
        let menuButton = UIButton.init(type: .custom)
        
        menuButton.frame = CGRect(x: ScreenSize.SCREEN_WIDTH - 45, y: 7, width: 25, height: 25)
        menuButton.setImage(UIImage.init(named: "sideMenu_Icon"), for: UIControlState())
        menuButton.addTarget(self, action: #selector(OpenSideMenu), for: .touchUpInside)
        navigationView.addSubview(menuButton)
        //        self.navigationController?.navigationBar.tintColor = Colors.appPink
        
        let titleLabel = UILabel.init(frame:CGRect(x: 60,y: 0,width: ( (navigationViewFrame.size.width) - 110),height: 40))
        titleLabel.textColor = UIColor.black
        titleLabel.text = "CONTEST"
        titleLabel.font = UIFont.init(name:"OpenSans-SemiBold", size: 15)// UIFont(name:"OpenSans-SemiBold", size: 15)!
        titleLabel.textAlignment = .center
        navigationView.addSubview(titleLabel)
        
        let logoImage = UIImageView.init(frame: CGRect(x: 25, y: 6, width: 33, height: 33))
        logoImage.image = UIImage.init(named: "nikon_login_logo")
        navigationView.addSubview(logoImage)
        
        let backButton = UIButton.init(type: .custom)
        backButton.frame = CGRect(x: 0, y: 9, width: 20, height: 20)
        backButton.backgroundColor = UIColor.white
        backButton.setImage(UIImage.init(named: "back"), for: UIControlState())
        backButton.addTarget(self, action: #selector(PopView), for: .touchUpInside)
        navigationView.addSubview(backButton)
        self.navigationItem.titleView = navigationView
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    //MARK:- Action

    @IBAction func uploadButtonPressed(_ sender: AnyObject) {
        
        let optionMenu = UIAlertController(title: nil, message: "Choose Option", preferredStyle: .actionSheet)
        
        let galleryAction = UIAlertAction(title: "Open Gallery", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.openGallary()
            
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            
        })
        optionMenu.addAction(galleryAction)
        optionMenu.addAction(cancelAction)
        
        self.present(optionMenu, animated: true, completion: nil)
    }

    @IBAction func userImageUploadButtonPressed(_ sender: AnyObject) {
        if userImageDescription.text == ""
        {
            let alert = UIAlertController.init(title: "Nikon", message: "Please enter image description", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                self.dismiss(animated: true, completion: nil)
            }))
            self.present(alert, animated: true, completion: nil)
            
        }
        else
        {
            self.postContestImage()
            
            self.viewForUploadImage.isHidden = true
        }
    }
    
    
    @IBAction func cancelButtonPressed(_ sender: AnyObject) {
        self.viewForUploadImage.isHidden = true
    }

    //MARK:- IMAGEPICKERDELEGATE
    func openGallary()
    {
        picker!.allowsEditing = true
        picker!.sourceType = UIImagePickerControllerSourceType.photoLibrary
        present(picker!, animated: true, completion: nil)
    }
    
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)){
            picker!.allowsEditing = true
            picker!.sourceType = UIImagePickerControllerSourceType.camera
            picker!.cameraCaptureMode = .photo
            present(picker!, animated: true, completion: nil)
        }else{
            let alert = UIAlertController(title: "Camera Not Found", message: "This device has no Camera", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style:.default, handler: nil)
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
        }
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        imageDataIMG = UIImagePNGRepresentation(chosenImage)! as Data
        let imageSize: Int = imageDataIMG.count
        let size = imageSize/1024
        
         print(size)
//        if size > 2048
//        {
//            dispatch_async(dispatch_get_main_queue())
//            {
//                self.performSelector(#selector(self.imageAlert), withObject: nil, afterDelay: 0.5)
//            }
//        }
//        else
//        {
            userChooseImage.image = chosenImage
            self.viewForUploadImage.isHidden = false
            self.categoryOfImage.text = categoryStr
//        }
        
        dismiss(animated: true, completion: nil)
    }
    
    func imageAlert()
    {
        let alert = UIAlertController.init(title: "Nikon", message: "Please select image of size less than 2 MB", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
            self.dismiss(animated: true, completion: nil)
        }))
        self.present(alert, animated: true, completion: nil)
        
        
    }
    
    func sendImageStr(_ str : String)
    {
        // isImageFromMyGallery = true
        
        
        imageDataIMG =  try! Data.init(contentsOf: URL.init(string: str)! )
        self.viewForUploadImage.isHidden = false
        userChooseImage.sd_setImage(with: URL.init(string: str))
        
        userChooseImage.setIndicatorStyle(.gray)
        userChooseImage.setShowActivityIndicator(true)
        self.categoryOfImage.text = categoryStr
    }
    
    func postContestImage()
    {
        if Reachability.isConnectedToNetwork()
        {
            imageDataToSend = imageDataIMG.base64EncodedString(options: NSData.Base64EncodingOptions.lineLength64Characters)
            
            var param  = "email_id=\(userEmail ?? "")&description=\(userImageDescription.text ?? "")&type=\(categoryStr ?? "")&image=\(imageDataToSend ?? "")"
            param = param.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            
            let image = "&image=\(imageDataToSend)"
            //let method = "https://www.iamnikon.in/challange_up_ios.php"
            let method = "challange_up_ios.php"

            Constants.appDelegate.startIndicator()
            Server.UploadPictureForContest(Constants.IAMNIKONBASEURL+method, paramString: param,ImageString: image, completionHandler: { (response) in
                
                if response[Constants.STATE] != nil && response[Constants.STATE] as! String == Constants.SUCC
                {

                    DispatchQueue.main.async
                    {
                        Constants.appDelegate.stopIndicator()
                        
                        var mess = String()
                        if response[Constants.MESS] != nil{
                            mess = response[Constants.MESS] as! String
                        }
                        else{
                            mess = response["Message"] as! String
                        }
                        
                        let alert = UIAlertController.init(title: "Nikon", message: mess, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                            self.navigationController?.popViewController(animated: true)
                            self.dismiss(animated: true, completion: nil)
                        }))
                        self.present(alert, animated: true, completion: nil)
                        
                        
                        
                    }
                }
                else
                {
                    DispatchQueue.main.async
                    {
                        Constants.appDelegate.stopIndicator()
                        
                        var mess = String()
                        if response[Constants.MESS] != nil{
                            mess = response[Constants.MESS] as! String
                        }
                        else if response["Message"] != nil{
                            mess = response["Message"] as! String
                        }
                        else{
                            mess = "No response from Back."
                        }
                        
                        let alert = UIAlertController.init(title: "Nikon", message: mess, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                            self.dismiss(animated: true, completion: nil)
                        }))
                        self.present(alert, animated: true, completion: nil)
                        
                    }
                }
                
                
            })
        }
        else
        {
            Constants.appDelegate.stopIndicator()
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
        
    }
    

}

extension UIImageView {
    public func imageFromServerURL(_ urlString: String) {
        
        URLSession.shared.dataTask(with: URL(string: urlString)!, completionHandler: { (data, response, error) -> Void in
            
            if error != nil {
                print(error)
                return
            }
            DispatchQueue.main.async(execute: { () -> Void in
                let image = UIImage(data: data!)
                self.image = image
            })
            
        }).resume()
    }
}
