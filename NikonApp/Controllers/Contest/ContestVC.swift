//
//  ContestVC.swift
//  NikonApp
//
//  Created by Hitesh Dhawan on 18/02/17.
//  Copyright © 2017 Neuronimbus. All rights reserved.
//

import UIKit

class ContestVC: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate {
    //MARK:- IBOUTLET

    @IBOutlet weak var cameraText: UITextField!
    @IBOutlet weak var genreText: UITextField!
    @IBOutlet weak var oneWordText: UITextField!
    @IBOutlet weak var contenstScrollView: UIScrollView!
    @IBOutlet weak var contestTable: UITableView!
    @IBOutlet weak var viewForTable: UIView!
    
    //MARK:- VARIABLE DECLARATION
    var sendingDict = Dictionary<String,AnyObject>()
    var genreArray = [AnyObject]()
    var cameraArray = [AnyObject]()
    var userOneWordString = String()
    var userEmail = String()
    var userName = String()
    var isFromMenu = Bool()
    
    //genre
    var userSelectedGenreValueString = String()
    var userSelectedGenreIDString = String()
    var userSelectedGenreArray = [AnyObject]()
    var selectedIndexGenreArray = NSMutableIndexSet()

    //camera
    var userSelectedCameraIdString = String()
    var userSelectedCameraValueString = String()
    var userSelectedCameraArray = [AnyObject]()
    var selectedIndexCameraArray = NSMutableIndexSet()

   
    //textfield type
    var selectedTextField = Int()
    
    

    
    //MARK: - Start

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationController?.isNavigationBarHidden = false
        self.setNavigationBar()
        let profile = Helper.getDataFromNsDefault(Constants.PROFILE)
        userEmail = profile["User_Email"]as! String
        userName = profile["User_Fname"]as! String
        self.getContestData()
        selectedTextField = 0
       // self.genreText.inputView = contestTable
    }
    
    //MARK:- NAVIGATION
    func OpenSideMenu(_ sender: AnyObject){
        
        self.view.endEditing(true)
        toggleSideMenuView()
    }
    func PopView(_ sender: AnyObject){
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func setNavigationBar(){
        self.navigationItem.hidesBackButton = true
        let navigationViewFrame = ScreenSize.SCREEN//self.navigationController?.navigationBar.frame
        let navigationView = UIView.init(frame: CGRect(x: 0, y: 0, width: navigationViewFrame.size.width, height: 44))
        navigationView.backgroundColor = UIColor.clear
        let menuButton = UIButton.init(type: .custom)
        
        menuButton.frame = CGRect(x: ScreenSize.SCREEN_WIDTH - 45, y: 7, width: 25, height: 25)
        menuButton.setImage(UIImage.init(named: "sideMenu_Icon"), for: UIControlState())
        menuButton.addTarget(self, action: #selector(OpenSideMenu), for: .touchUpInside)
        navigationView.addSubview(menuButton)
//        self.navigationController?.navigationBar.tintColor = Colors.appPink
        
        let titleLabel = UILabel.init(frame:CGRect(x: 60,y: 0,width: ( (navigationViewFrame.size.width) - 110),height: 40))
        titleLabel.textColor = UIColor.black
        titleLabel.text = "CONTEST"
        titleLabel.font = UIFont.init(name:"OpenSans-SemiBold", size: 15)// UIFont(name:"OpenSans-SemiBold", size: 15)!
        titleLabel.textAlignment = .center
        navigationView.addSubview(titleLabel)
        
//        let logoImage = UIImageView.init(frame: CGRectMake(25, 6, 33, 33))
//        logoImage.image = UIImage.init(named: "nikon_login_logo")
//        navigationView.addSubview(logoImage)
        
        if isFromMenu == false
            {
                let backButton = UIButton.init(type: .custom)
                backButton.frame = CGRect(x: 0, y: 9, width: 20, height: 20)
                backButton.backgroundColor = UIColor.white
                backButton.setImage(UIImage.init(named: "back"), for: UIControlState())
                backButton.addTarget(self, action: #selector(PopView), for: .touchUpInside)
                navigationView.addSubview(backButton)
                
                let logoImage = UIImageView.init(frame: CGRect(x: 25, y: 6, width: 33, height: 33))
                logoImage.image = UIImage.init(named: "nikon_login_logo")
                navigationView.addSubview(logoImage)
                
                
        }
        else
        {
            let logoImage = UIImageView.init(frame: CGRect(x: 0, y: 6, width: 33, height: 33))
            logoImage.image = UIImage.init(named: "nikon_login_logo")
            navigationView.addSubview(logoImage)
        }

        //}
        self.navigationItem.titleView = navigationView
    }
    
    //MARK:- GETCONTEST DATA
    /*
    func getContestData(){
        if Reachability.isConnectedToNetwork()
        {
            let method = "personal_info.php?useremail=\(userEmail)"
            Constants.appDelegate.startIndicator()
            Server.getRequestWithURL_Contest(Constants.IAMNIKONBASEURL+method, completionHandler: { (response) in
                
                Constants.appDelegate.stopIndicator()
                if response[Constants.STATE] != nil && response[Constants.STATE] as! String == Constants.SUCC
                {
                    self.genreArray = response["genre"] as! NSArray as [AnyObject]
                    self.cameraArray = response["camara"] as! NSArray as [AnyObject]
                    self.userOneWordString = response["message"] as! String
                    self.userSelectedCameraIdString = response["user_camara"] as! String
                    self.userSelectedGenreIDString = response["user_genre"] as! String
                    if self.userSelectedGenreIDString != ""
                    {
                        let tempArray = self.userSelectedGenreIDString.componentsSeparatedByString(",") as NSArray
                        for index in 0 ..< tempArray.count
                        {
                            for newIndex in 0 ..< self.genreArray.count
                            {
                                let dict = self.genreArray[newIndex] as! Dictionary<String,AnyObject>
                                let valueStr = dict["value"] as? String
                                let idStr = dict["id"] as? String
                                if idStr! == tempArray[index] as! String
                                {
                                    if self.userSelectedGenreValueString == ""
                                    {
                                        self.userSelectedGenreValueString = valueStr!
                                    }
                                    else
                                    {
                                        self.userSelectedGenreValueString = self.userSelectedGenreValueString + "," + valueStr!
                                    }
                                    self.userSelectedGenreArray.append(dict)
                                    self.selectedIndexGenreArray.addIndex(newIndex)
                                }
                            }
                        }
                    
                    }
                    if self.userSelectedCameraIdString != ""
                    {
                        let tempArray = self.userSelectedCameraIdString.componentsSeparatedByString(",") as NSArray
                        for index in 0 ..< tempArray.count
                        {
                            for newIndex in 0 ..< self.cameraArray.count
                            {
                                let dict = self.cameraArray[newIndex] as! Dictionary<String,AnyObject>
                                let valueStr = dict["value"] as? String
                                let idStr = dict["id"] as? String
                                if idStr! == tempArray[index] as! String
                                {
                                    if self.userSelectedCameraValueString == ""
                                    {
                                        self.userSelectedCameraValueString = valueStr!
                                    }
                                    else
                                    {
                                        self.userSelectedCameraValueString = self.userSelectedCameraValueString + "," + valueStr!
                                    }
                                    self.userSelectedCameraArray.append(dict)
                                    self.selectedIndexCameraArray.addIndex(newIndex)
                                }
                            }
                        }
                        
                    }
                    
                    dispatch_async(dispatch_get_main_queue()){
                        self.oneWordText.text = self.userOneWordString
                        self.cameraText.text = self.userSelectedCameraValueString
                        self.genreText.text = self.userSelectedGenreValueString
                       
                    }
                    
                }
            })
        }
        else
        {
            Constants.appDelegate.stopIndicator()
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
    }
 */
    func getContestData(){
        if Reachability.isConnectedToNetwork()
        {
//            let method = "personal_info.php?useremail=\(userEmail)"
            let method = "NindApi?Method=personal_info&Value=\(userEmail)"

            Constants.appDelegate.startIndicator()
            Server.getRequestWithURL(Constants.BASEURL+method, completionHandler: { (response) in
                
                Constants.appDelegate.stopIndicator()
                if response[Constants.STATE] != nil && response[Constants.STATE] as! String == Constants.SUCC
                {
                    let tempDict = response["MethodResult"]
                    let result = Helper.convertStringToDictionary(tempDict! as! String)
                    
                    self.genreArray = result!["genre"] as! NSArray as [AnyObject]
                    self.cameraArray = result!["camara"] as! NSArray as [AnyObject]
                    self.userOneWordString = result!["message"] as! String
                    self.userSelectedCameraIdString = result!["user_camara"] as! String
                    self.userSelectedGenreIDString = result!["user_genre"] as! String
                    if self.userSelectedGenreIDString != ""
                    {
                        let tempArray = self.userSelectedGenreIDString.components(separatedBy: ",") as NSArray
                        for index in 0 ..< tempArray.count
                        {
                            for newIndex in 0 ..< self.genreArray.count
                            {
                                let dict = self.genreArray[newIndex] as! Dictionary<String,AnyObject>
                                let valueStr = dict["value"] as? String
                                let idStr = dict["id"] as? String
                                if idStr! == tempArray[index] as! String
                                {
                                    if self.userSelectedGenreValueString == ""
                                    {
                                        self.userSelectedGenreValueString = valueStr!
                                    }
                                    else
                                    {
                                        self.userSelectedGenreValueString = self.userSelectedGenreValueString + "," + valueStr!
                                    }
                                    self.userSelectedGenreArray.append(dict as AnyObject)
                                    self.selectedIndexGenreArray.add(newIndex)
                                }
                            }
                        }
                        
                    }
                    if self.userSelectedCameraIdString != ""
                    {
                        let tempArray = self.userSelectedCameraIdString.components(separatedBy: ",") as NSArray
                        for index in 0 ..< tempArray.count
                        {
                            for newIndex in 0 ..< self.cameraArray.count
                            {
                                let dict = self.cameraArray[newIndex] as! Dictionary<String,AnyObject>
                                let valueStr = dict["value"] as? String
                                let idStr = dict["id"] as? String
                                if idStr! == tempArray[index] as! String
                                {
                                    if self.userSelectedCameraValueString == ""
                                    {
                                        self.userSelectedCameraValueString = valueStr!
                                    }
                                    else
                                    {
                                        self.userSelectedCameraValueString = self.userSelectedCameraValueString + "," + valueStr!
                                    }
                                    self.userSelectedCameraArray.append(dict as AnyObject)
                                    self.selectedIndexCameraArray.add(newIndex)
                                }
                            }
                        }
                        
                    }
                    
                    DispatchQueue.main.async{
                        self.oneWordText.text = self.userOneWordString
                        self.cameraText.text = self.userSelectedCameraValueString
                        self.genreText.text = self.userSelectedGenreValueString
                        
                    }
                    
                }
            })
        }
        else
        {
            Constants.appDelegate.stopIndicator()
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
    }

    
    //MARK:- POST CONTEXT DATA
    
    func postContestData()
    {
        if Reachability.isConnectedToNetwork()
        {
            
            var genreString = String()
            for item in userSelectedGenreArray {
                let dict = item
                if genreString == "" {
                    genreString = "\"\(dict["id"] as! String)\""
                }
                else{
                    genreString = "\(genreString)" + "," + "\"\(dict["id"] as! String)\""
                }
            }
            
            var cameraString = String()
            for item in userSelectedCameraArray {
                let dict = item
                if cameraString == "" {
                    cameraString = "\"\(dict["id"] as! String)\""
                }
                else{
                    cameraString = "\(cameraString)" + "," + "\"\(dict["id"] as! String)\""
                }
            }

            let newparam1 = "genre=[\(genreString)]&camera=[\(cameraString)]&email_id=\(userEmail)&user_name=\(userName)&tag=\(self.oneWordText.text!)"
            let method = "NindApi?Method=user_save_info&"
            
            
            Constants.appDelegate.startIndicator()
            Server.getRequestWithURL(Constants.BASEURL+method+newparam1, completionHandler: { (response) in
                
                Constants.appDelegate.stopIndicator()
                if response[Constants.STATE] != nil && response[Constants.STATE] as! String == Constants.SUCC
                {
                    DispatchQueue.main.async
                    {
                        let destinationVC = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "ContestChallengesUploadVC") as! ContestChallengesUploadVC
                        destinationVC.imgURLStr = self.sendingDict["image"]as! String
                        destinationVC.categoryStr = self.sendingDict["category"]as! String
                        destinationVC.userEmail = self.userEmail
                        self.navigationController?.pushViewController(destinationVC, animated: true)
                    }
                    
                }
                else
                {
                    
                }
                
            })
        }
        else
        {
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
            
        }
        //        let vc = Constants.mainStoryboard2.instantiateViewControllerWithIdentifier("ContestChallengesListVC") as! ContestChallengesListVC
        //        self.navigationController?.pushViewController(vc, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK:- TABLEVIEW

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if selectedTextField == 1 {
            return self.genreArray.count
        }
        else
        {
            return self.cameraArray.count
        }
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40;
    }
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ContestCell", for: indexPath) as! CustomContestTableViewCell
        if selectedTextField == 1
        {
            cell.nameLabel?.text = self.genreArray[indexPath.row]["value"] as? String
            if self.selectedIndexGenreArray.contains(indexPath.row)
            {
                cell.checkBoxButton.setImage(UIImage.init(named: "check_Fill"), for: UIControlState())
            }
            else
            {
                cell.checkBoxButton.setImage(UIImage.init(named: "check_Empty"), for: UIControlState())
            }
        }
        else
        {
            cell.nameLabel?.text = self.cameraArray[indexPath.row]["value"] as? String
            if self.selectedIndexCameraArray.contains(indexPath.row)
            {
                cell.checkBoxButton.setImage(UIImage.init(named: "check_Fill"), for: UIControlState())
            }
            else
            {
                cell.checkBoxButton.setImage(UIImage.init(named: "check_Empty"), for: UIControlState())
            }
        }

       
    
    return cell
}
    
     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if selectedTextField == 1
        {
        
            if self.selectedIndexGenreArray.contains(indexPath.row)
            {
                self.selectedIndexGenreArray.remove(indexPath.row)//(self.selectedIndexGenreArray.indexOf(indexPath)!)
            }
            else
            {
                self.selectedIndexGenreArray.add(indexPath.row)
            }
        }
        else
        {
            if self.selectedIndexCameraArray.contains(indexPath.row)
            {
                self.selectedIndexCameraArray.remove(indexPath.row)//(self.selectedIndexGenreArray.indexOf(indexPath)!)
            }
            else
            {
                self.selectedIndexCameraArray.add(indexPath.row)
            }
        }
        tableView.reloadData()

    }
    
    
    //MARK:- TEXTFIELD DELEGATE
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == oneWordText
        {
            if string == " "
            {
                return false
            }
            else
            {
                return true
            }
        }
        return true
    }
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        if textField == oneWordText
        {
            
        }
        else
        {
        if textField == genreText
        {
           self.selectedTextField = 1
        }
        else
        {
            self.selectedTextField = 2
        }
        DispatchQueue.main.async{
            
            self.view .bringSubview(toFront: self.viewForTable)
            self.viewForTable.isHidden = false
            self.contestTable.isHidden = false
            self.contestTable.reloadData()
            textField.resignFirstResponder()
        }
        }
    }
    /*
    func textFieldDidEndEditing(textField: UITextField) {
         if textField == genreText {
        dispatch_async(dispatch_get_main_queue()){
            
            self.contestTable.hidden = true
            self.viewForTable.hidden = true
            for index in self.selectedIndexGenreArray {
                let dict = self.genreArray[index] as! Dictionary<String,AnyObject>
                let valueStr = dict["value"] as? String
                let idStr = dict["id"] as? String
                self.userSelectedGenreArray.append(dict)
                if self.userSelectedGenreIDString == ""{
                    self.userSelectedGenreIDString = idStr!
                }
                else{
                    self.userSelectedGenreIDString = self.userSelectedGenreIDString + "," + idStr!
                }
                
                if self.userSelectedGenreValueString == ""{
                    self.userSelectedGenreValueString = valueStr!
                }
                else{
                    self.userSelectedGenreValueString = self.userSelectedGenreValueString + "," + valueStr!
                }
            }
           
            self.genreText.text = self.userSelectedGenreValueString
        }
        
      }
        else
         {
            selectedTextField = 2
        }
    }
*/
    
    
    //MARK:- ACTION
    
    @IBAction func OkButtonPressed(_ sender: AnyObject) {
        if selectedTextField == 1 {
            self.userSelectedGenreIDString = ""
            self.userSelectedGenreValueString = ""
            for index in self.selectedIndexGenreArray
            {
                let dict = self.genreArray[index] as! Dictionary<String,AnyObject>
                let valueStr = dict["value"] as? String
                let idStr = dict["id"] as? String
                self.userSelectedGenreArray.append(dict as AnyObject)
                if self.userSelectedGenreIDString == ""
                {
                    self.userSelectedGenreIDString = idStr!
                }
                else
                {
                    self.userSelectedGenreIDString = self.userSelectedGenreIDString + "," + idStr!
                }
                
                if self.userSelectedGenreValueString == ""
                {
                    self.userSelectedGenreValueString = valueStr!
                }
                else
                {
                    self.userSelectedGenreValueString = self.userSelectedGenreValueString + "," + valueStr!
                }
            }
            if self.selectedIndexGenreArray.count == 0{
                self.genreText.text = ""
            }
            else
            {
                self.genreText.text = self.userSelectedGenreValueString
            }
            
            self.viewForTable.isHidden = true
            
            
        }
        else
        {
            self.userSelectedCameraIdString = ""
            self.userSelectedCameraValueString = ""
            for index in self.selectedIndexCameraArray
            {
                let dict = self.cameraArray[index] as! Dictionary<String,AnyObject>
                let valueStr = dict["value"] as? String
                let idStr = dict["id"] as? String
                self.userSelectedCameraArray.append(dict as AnyObject)
                if self.userSelectedCameraIdString == ""
                {
                    self.userSelectedCameraIdString = idStr!
                }
                else
                {
                    self.userSelectedCameraIdString = self.userSelectedCameraIdString + "," + idStr!
                }
                
                if self.userSelectedCameraValueString == ""
                {
                    self.userSelectedCameraValueString = valueStr!
                }
                else
                {
                    self.userSelectedCameraValueString = self.userSelectedCameraValueString + "," + valueStr!
                }
            }
            if self.selectedIndexCameraArray.count == 0{
                self.cameraText.text = ""
            }
            else
            {
                self.cameraText.text = self.userSelectedCameraValueString
            }
            
            self.viewForTable.isHidden = true

        }
    }
    
    @IBAction func cancelButtonPressed(_ sender: AnyObject) {
        self.viewForTable.isHidden = true
    }
    @IBAction func takeUpChallengeButtonPressed(_ sender: AnyObject) {
        self.postContestData()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
}
