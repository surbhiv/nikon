//
//  ContestChallengesListVC.swift
//  NikonApp
//
//  Created by Hitesh Dhawan on 20/02/17.
//  Copyright © 2017 Neuronimbus. All rights reserved.
//

import UIKit

class ContestChallengesListVC: UIViewController,UITableViewDelegate,UITableViewDataSource {

    //MARK:- IBOUTLET
    @IBOutlet weak var challengeTable: UITableView!
    
    
    //MARK:- VARIBLE DECLARATION
    var challengeData = [AnyObject]()
    var isFromMenu = Bool()
    
    //MARK:- START
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationController?.isNavigationBarHidden = false
        self.setNavigationBar()
        //self.getChallengeData()
    }
    //MARK:- ACTION
    @IBAction func ChallengeGalleryButtonPressed(_ sender: AnyObject)
    {
		let alert = UIAlertController(title: "Nikon", message: "No Data Available",         preferredStyle: UIAlertControllerStyle.alert)
		
		alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { _ in
			//Cancel Action
		}))
//		alert.addAction(UIAlertAction(title: "Sign out",
//									  style: UIAlertActionStyle.default,
//									  handler: {(_: UIAlertAction!) in
//										//Sign out action
//		}))
		self.present(alert, animated: true, completion: nil)
//        let destinationVC = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "ContestChallengeGalleryCollectionVC") as! ContestChallengeGalleryCollectionVC
//        var sendArray = [AnyObject]()
//        for index in 0 ..< challengeData.count
//        {
//            let tempArr = self.challengeData[index]["challenges"]as! NSArray
//            for newIndex in 0 ..< tempArr.count
//            {
//             sendArray.append(tempArr[newIndex] as AnyObject)
//            }
//        }
//        destinationVC.galleryImageArray = sendArray
//        destinationVC.challengeGalleryFullDataArray = self.challengeData
//        self.navigationController?.pushViewController(destinationVC, animated: true)
		
    }
    //MARK:- open in browser method called
    func challengeKeyToOpenInWebviewClicked(_ sender:UIButton)  {
    
        let destViewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "PDFOpenVC")as! PDFOpenVC
        destViewController.pdfURLStr = self.challengeData[sender.tag]["challenge_url"] as! String
        destViewController.titleStr = self.challengeData[sender.tag]["post_title"] as! String
        destViewController.isFromMenu = false
        
        self.navigationController?.pushViewController(destViewController, animated: true)
    }
    
    
    
    func challengeClicked(_ sender:UIButton)  {

        if (UserDefaults.standard.integer(forKey: Constants.USERID) != 0)
        {
            let destViewController = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "ContestVC")as! ContestVC
            var sendDict = Dictionary<String,AnyObject>()
            sendDict = ["image":"\(Constants.IAMNIKONBASEURL)\(self.challengeData[sender.tag]["post_image"] as! String)" as AnyObject,"category":self.challengeData[sender.tag]["post_title"] as! String as AnyObject]

            destViewController.sendingDict = sendDict
            self.navigationController?.pushViewController(destViewController, animated: true)
        }
        else
        {
            self.loginAlert()
        }
    }
    
    func challengeClicked_Festive(_ sender:UIButton){
        if (UserDefaults.standard.integer(forKey: Constants.USERID) != 0)
        {
            let destViewController = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "ChallanceFestiveContestVC")as! ChallanceFestiveContestVC
            let category  = self.challengeData[sender.tag]["post_title"] as! String
            let backImage = "\(Constants.IAMNIKONBASEURL)\(self.challengeData[sender.tag]["background_image"] as! String)"

            destViewController.categoryStr = category
            destViewController.backimageURLStr = backImage
            
            
            self.navigationController?.pushViewController(destViewController, animated: true)
        }
        else
        {
            self.loginAlert()
        }
    }
    
    //MARK:- NAVIGATION
    func OpenSideMenu(_ sender: AnyObject){
        
        self.view.endEditing(true)
        toggleSideMenuView()
    }
    func PopView(_ sender: AnyObject){
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func setNavigationBar(){
        self.navigationItem.hidesBackButton = true
        let navigationViewFrame = ScreenSize.SCREEN//self.navigationController?.navigationBar.frame
        let navigationView = UIView.init(frame: CGRect(x: 0, y: 0, width: navigationViewFrame.size.width, height: 44))
        navigationView.backgroundColor = UIColor.clear
        let menuButton = UIButton.init(type: .custom)
        
        menuButton.frame = CGRect(x: ScreenSize.SCREEN_WIDTH - 45, y: 7, width: 25, height: 25)
        menuButton.setImage(UIImage.init(named: "sideMenu_Icon"), for: UIControlState())
        menuButton.addTarget(self, action: #selector(OpenSideMenu), for: .touchUpInside)
        navigationView.addSubview(menuButton)
//        self.navigationController?.navigationBar.tintColor = Colors.appPink
        
        let titleLabel = UILabel.init(frame:CGRect(x: 60,y: 0,width: ( (navigationViewFrame.size.width) - 110),height: 40))
        titleLabel.textColor = UIColor.black
        titleLabel.text = "CONTEST"
        titleLabel.font = UIFont.init(name:"OpenSans-SemiBold", size: 15)// UIFont(name:"OpenSans-SemiBold", size: 15)!
        titleLabel.textAlignment = .center
        navigationView.addSubview(titleLabel)
        
        let logoImage = UIImageView.init(frame: CGRect(x: 25, y: 6, width: 33, height: 33))
        logoImage.image = UIImage.init(named: "nikon_login_logo")
        navigationView.addSubview(logoImage)
        
        if isFromMenu == false
        {
            let backButton = UIButton.init(type: .custom)
            backButton.frame = CGRect(x: 0, y: 9, width: 20, height: 20)
            backButton.backgroundColor = UIColor.white
            backButton.setImage(UIImage.init(named: "back"), for: UIControlState())
            backButton.addTarget(self, action: #selector(PopView), for: .touchUpInside)
            navigationView.addSubview(backButton)
        }
        self.navigationItem.titleView = navigationView
    }
    
    //MARK:- GETCONTEST DATA
    /*
    func getChallengeData(){
        if Reachability.isConnectedToNetwork()
        {
            let method = "challenge_info.php"
            Constants.appDelegate.startIndicator()
            Server.getRequestWithURL_Contest(Constants.IAMNIKONBASEURL+method, completionHandler: { (response) in
                
                Constants.appDelegate.stopIndicator()
               // if response[Constants.STATE] != nil && response[Constants.STATE] as! String == Constants.SUCC
               // {
                    self.challengeData =  response["categories"] as! NSArray as [AnyObject]
                      dispatch_async(dispatch_get_main_queue())
                      {
                        self.challengeTable.reloadData()
                      }

               
                //}
            })
        }
        else
        {
            Constants.appDelegate.stopIndicator()
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
    }
    */
    func getChallengeData(){
        if Reachability.isConnectedToNetwork()
        {
//            let method = "challenge_info.php"
            let method = "NindApi?Method=challenge_info&Value="
            Constants.appDelegate.startIndicator()
            Server.getRequestWithURL(Constants.BASEURL+method, completionHandler: { (response) in
                
                Constants.appDelegate.stopIndicator()
                 if response[Constants.STATE] != nil && response[Constants.STATE] as! String == Constants.SUCC
                 {
                    let tempDict = response["MethodResult"]
                    
                    let result = Helper.convertStringToDictionary(tempDict! as! String)
                    self.challengeData =  result!["categories"] as! NSArray as [AnyObject]

                    DispatchQueue.main.async
                    {
                        self.challengeTable.reloadData()
                    }
                
                
                }
                else
                {
                    
                }
            })
        }
        else
        {
            Constants.appDelegate.stopIndicator()
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
    }


    //MARK:- TABLEVIEW
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return challengeData.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 140
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ContestChallengesListCustomCell", for: indexPath) as! ContestChallengesListCustomCell
        
        let imageUrlStr = "\(Constants.IAMNIKONBASEURL)\(self.challengeData[indexPath.row]["post_image"] as! String)"
        print(imageUrlStr)
        
        cell.challengeImage.sd_setImage(with: URL.init(string:imageUrlStr ), placeholderImage: UIImage.init(named: "product_placeholder.jpg"))
        cell.challengeImage.setShowActivityIndicator(true)
        let statusOfChallenge = self.challengeData[indexPath.row]["ping_status"] as! String
//        let challengeKeyNum = self.challengeData[indexPath.row]["challenge_key"] as! NSNumber
        let challengeKey = "\(self.challengeData[indexPath.row]["challenge_key"]!!)"//Int.init(challengeKeyNum)

        cell.challengeButton.tag = indexPath.row
        //todo
        
        if challengeKey == "1"
        {
            cell.challengeButton.addTarget(self, action: #selector(challengeKeyToOpenInWebviewClicked), for: .touchUpInside)
        }
        if challengeKey == "2"
        {
            cell.challengeButton.addTarget(self, action: #selector(challengeClicked_Festive), for: .touchUpInside)
        }
        else
        {
            if  statusOfChallenge == "open"
            {
                cell.challengeButton.addTarget(self, action: #selector(challengeClicked), for: .touchUpInside)
            }
        }
       
        
        
        return cell
        
    }

    //MARK:- LoginAlert
    
    func loginAlert()  {
        let alert = UIAlertController.init(title: "Nikon", message: "Please login to continue.", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
            
            DispatchQueue.main.async
            {
                let destinationController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "LoginVC")as! LoginVC
                destinationController.isWantTodismissVC = true
                self.present(destinationController, animated: true, completion: nil);            }
        }))
        alert.addAction(UIAlertAction.init(title: "Cancel", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
            self.dismiss(animated: true, completion: nil)
        }))
        self.present(alert, animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
