//
//  ContestImageZoomVC.swift
//  NikonApp
//
//  Created by Hitesh Dhawan on 22/02/17.
//  Copyright © 2017 Neuronimbus. All rights reserved.
//

import UIKit

protocol imageZoomDelegate {
    func hideZoomView()
}
class ContestImageZoomVC: UIViewController {

    //MARK:- IBOUTLET
    @IBOutlet weak var viewForImageZoom: UIView!
    @IBOutlet weak var zoomImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!

    @IBOutlet weak var crossButton: UIButton!
    var imageHideDelegate : imageZoomDelegate?
    
    //MARK:- VARIABLE
    var imageStr = String()
    var nameStr  = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        zoomImage.sd_setImage(with: URL.init(string: imageStr), placeholderImage: UIImage.init(named: "product_placeholder.jpg"))
        zoomImage.setShowActivityIndicator(true)
        self.view.bringSubview(toFront: nameLabel)
//        self.view .insertSubview(nameLabel, aboveSubview:zoomImage)
        nameLabel.text = nameStr
        //tapGesture =   UITapGestureRecognizer.init(target: self, action: #selector(hideView))
    }
    @IBAction func crossButtonPressed(_ sender: AnyObject) {
        imageHideDelegate?.hideZoomView()

    }
    @IBAction func hideZoomView(_ sender: AnyObject) {
        imageHideDelegate?.hideZoomView()
    }
    func hideView() {
        imageHideDelegate?.hideZoomView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
