//
//  ContestChallengesListCustomCell.swift
//  NikonApp
//
//  Created by Hitesh Dhawan on 20/02/17.
//  Copyright © 2017 Neuronimbus. All rights reserved.
//

import UIKit

class ContestChallengesListCustomCell: UITableViewCell {

    @IBOutlet weak var challengeImage: UIImageView!
    @IBOutlet weak var challengeButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
