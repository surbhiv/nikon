//
//  ContestChallengeGalleryCollectionVC.swift
//  NikonApp
//
//  Created by Hitesh Dhawan on 21/02/17.
//  Copyright © 2017 Neuronimbus. All rights reserved.
//

import UIKit

class ContestChallengeGalleryCollectionVC: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UIPickerViewDelegate,UIPickerViewDataSource,UITextFieldDelegate,imageZoomDelegate {

    //MARK:- IBOUTLET
    @IBOutlet weak var galleryCollectionView: UICollectionView!
    
    //Mark:- VARIABLE 
    var challengeGalleryFullDataArray  = [AnyObject]()
    var galleryImageArray = [AnyObject]()
    var newCategoryImageArray = [AnyObject]()
    var picker = UIPickerView()
    var categoryText: UITextField!
    var selectedIndex = Int()
    var menuButton = UIButton()
    var backButton = UIButton()
    var childVC = ContestImageZoomVC()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        //define picker
        self.picker = UIPickerView.init()
        self.picker.dataSource = self
        self.picker.delegate = self
        selectedIndex = 0
        self.navigationController?.isNavigationBarHidden = false
        self.setNavigationBar()
    }
    
    
    //MARK:- NAVIGATION
    func OpenSideMenu(_ sender: AnyObject){
        
        self.view.endEditing(true)
        toggleSideMenuView()
    }
    func PopView(_ sender: AnyObject){
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func setNavigationBar(){
        self.navigationItem.hidesBackButton = true
        let navigationViewFrame = ScreenSize.SCREEN//self.navigationController?.navigationBar.frame
        let navigationView = UIView.init(frame: CGRect(x: 0, y: 0, width: navigationViewFrame.size.width, height: 44))
        navigationView.backgroundColor = UIColor.clear
        menuButton = UIButton.init(type: .custom)
        
        menuButton.frame = CGRect(x: ScreenSize.SCREEN_WIDTH - 45, y: 7, width: 25, height: 25)
        menuButton.setImage(UIImage.init(named: "sideMenu_Icon"), for: UIControlState())
        menuButton.addTarget(self, action: #selector(OpenSideMenu), for: .touchUpInside)
        navigationView.addSubview(menuButton)
//        self.navigationController?.navigationBar.tintColor = Colors.appPink
        
        let titleLabel = UILabel.init(frame:CGRect(x: 60,y: 0,width: ( (navigationViewFrame.size.width) - 110),height: 40))
        titleLabel.textColor = UIColor.black
        titleLabel.text = "CONTEST CHALLENGE"
        titleLabel.font = UIFont.init(name:"OpenSans-SemiBold", size: 15)// UIFont(name:"OpenSans-SemiBold", size: 15)!
        titleLabel.textAlignment = .center
        navigationView.addSubview(titleLabel)
        
        let logoImage = UIImageView.init(frame: CGRect(x: 25, y: 6, width: 33, height: 33))
        logoImage.image = UIImage.init(named: "nikon_login_logo")
        navigationView.addSubview(logoImage)
        
        backButton = UIButton.init(type: .custom)
        backButton.frame = CGRect(x: 0, y: 9, width: 20, height: 20)
        backButton.backgroundColor = UIColor.white
        backButton.setImage(UIImage.init(named: "back"), for: UIControlState())
        backButton.addTarget(self, action: #selector(PopView), for: .touchUpInside)
        navigationView.addSubview(backButton)
        self.navigationItem.titleView = navigationView
    }

   

    //MARK:- COllection View
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: (ScreenSize.SCREEN_WIDTH )/2 - 8,height: ((ScreenSize.SCREEN_WIDTH)/2) - 8 )
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.selectedIndex == 0
        {
            return galleryImageArray.count

        }
        else
        {
             newCategoryImageArray = challengeGalleryFullDataArray[selectedIndex-1]["challenges"]as! NSArray as [AnyObject]
            return newCategoryImageArray.count
        }
        
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        
        return CGSize(width: self.view.frame.width,height: 230)  // Header size
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ChallengeGalleryCollectionCell", for: indexPath)
        
        let productImage = cell.contentView .viewWithTag(14301) as! UIImageView
        let productName = cell.contentView .viewWithTag(14302) as! UILabel

        if selectedIndex == 0
        {
            productImage.sd_setImage(with: URL.init(string: self.galleryImageArray[indexPath.row]["image"] as! String), placeholderImage: UIImage.init(named: "product_placeholder.jpg"))
            productName.text = self.galleryImageArray[indexPath.row]["username"] as? String
            
        }
        else
        {
            productImage.sd_setImage(with: URL.init(string: self.newCategoryImageArray[indexPath.row]["image"] as! String), placeholderImage: UIImage.init(named: "product_placeholder.jpg"))
            productName.text = self.newCategoryImageArray[indexPath.row]["username"] as? String
        }
        
        productImage.setShowActivityIndicator(true)
        productImage.setIndicatorStyle(.gray)
        
        
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let destVC = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "ContestLargeImageCollectionVC")as! ContestLargeImageCollectionVC
        if selectedIndex == 0
        {
            destVC.contestImageDataArray = self.galleryImageArray
        }
        else
        {
            destVC.contestImageDataArray = self.newCategoryImageArray

        }
        destVC.selectedIndex = indexPath.item
        self.navigationController?.pushViewController(destVC, animated: true)
    }
    
    func addView(_ view : UIView, contentView : UIView, frame : CGRect) {
        view.frame = frame
        contentView.addSubview(view)
        view.translatesAutoresizingMaskIntoConstraints = false
        
        DispatchQueue.main.async(execute: {
            contentView.addConstraint(NSLayoutConstraint.init(item: view, attribute: NSLayoutAttribute.leading, relatedBy: NSLayoutRelation.equal, toItem: contentView, attribute:  NSLayoutAttribute.leading, multiplier: 1.0, constant: frame.origin.x))
            contentView.addConstraint(NSLayoutConstraint.init(item: view, attribute: NSLayoutAttribute.width, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute:  NSLayoutAttribute.notAnAttribute, multiplier: 1.0, constant: frame.size.width))
            contentView.addConstraint(NSLayoutConstraint.init(item: view, attribute: NSLayoutAttribute.top, relatedBy: NSLayoutRelation.equal, toItem: contentView, attribute:  NSLayoutAttribute.top, multiplier: 1.0, constant:0))
            contentView.addConstraint(NSLayoutConstraint.init(item: view, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute:  NSLayoutAttribute.notAnAttribute, multiplier: 1.0, constant: frame.size.height))
        })
    }

    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        var reusableView = UICollectionReusableView()
        if kind == UICollectionElementKindSectionHeader
        {
            
            let hview = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "ChallengeGalleryReusableView", for: indexPath) as! ChallengeGalleryReusableView
            //hview.backgroundColor = UIColor.redColor()
           // hview.contestGalleryTypeText.text = "fgdgdf"
            self.categoryText = hview.contestGalleryTypeText
            self.categoryText.inputView = self.picker
            self.categoryText.delegate = self
            reusableView = hview
            
            reusableView = hview
        }
        return reusableView
    }

    //MARK: - PICKEVIEW
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return challengeGalleryFullDataArray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
            return String.init(format: self.challengeGalleryFullDataArray[row]["post_title"]as! String , locale: nil)
       
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
      
            
            self.categoryText.text = String.init(format: self.challengeGalleryFullDataArray[row]["post_title"]as! String, locale: nil)
          self.selectedIndex = row+1

        
       
        
    }
    
    //MARK:- TEXTFIELD DELEGATE
    func textFieldDidEndEditing(_ textField: UITextField) {
        if selectedIndex != 0
        {
            Constants.appDelegate.startIndicator()

        self.categoryText.text = String.init(format: self.challengeGalleryFullDataArray[selectedIndex-1]["post_title"]as! String, locale: nil)
        DispatchQueue.main.async
        {
            self.galleryCollectionView.reloadData()
             Constants.appDelegate.stopIndicator()
        }
        }
        
    }
    
    //MARK:- ACTION
    func crossBtnPressed()  {
         backButton.isUserInteractionEnabled = true
        menuButton.isUserInteractionEnabled = true
        self.childVC.view.removeFromSuperview()
    }
    func hideZoomView()  {
         backButton.isUserInteractionEnabled = true
        menuButton.isUserInteractionEnabled = true

        self.childVC.view.removeFromSuperview()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
