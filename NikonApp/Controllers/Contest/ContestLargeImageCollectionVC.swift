//
//  ContestLargeImageCollectionVC.swift
//  NikonApp
//
//  Created by Hitesh Dhawan on 24/03/17.
//  Copyright © 2017 Neuronimbus. All rights reserved.
//

import UIKit

class ContestLargeImageCollectionVC: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout, UIScrollViewDelegate {

    @IBOutlet weak var currenNumberLabel: UILabel!
    @IBOutlet weak var totalNumberLabel: UILabel!
    @IBOutlet weak var contestLargeImageCollectionView: UICollectionView!
    
    var contestImageDataArray = [AnyObject]()
    var selectedIndex = NSInteger()

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationController?.isNavigationBarHidden = false
        self.setNavigationBar()
        currenNumberLabel.text = "\(selectedIndex)"
        totalNumberLabel.text = "\(contestImageDataArray.count)"
        
        contestLargeImageCollectionView.reloadData()
        
        self.perform(#selector(Scrollview), with: nil, afterDelay: 0.01)
    }
    func Scrollview(){
        
        contestLargeImageCollectionView.scrollRectToVisible(CGRect(x: contestLargeImageCollectionView.frame.size.width * CGFloat(selectedIndex), y: 0, width: contestLargeImageCollectionView.frame.size.width, height: contestLargeImageCollectionView.frame.size.height), animated: false)
    }
    
    

    
    //MARK:- NAVIGATION
    func OpenSideMenu(_ sender: AnyObject){
        
        self.view.endEditing(true)
        toggleSideMenuView()
    }
    func PopView(_ sender: AnyObject){
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func setNavigationBar(){
        self.navigationItem.hidesBackButton = true
        let navigationViewFrame = ScreenSize.SCREEN//self.navigationController?.navigationBar.frame
        let navigationView = UIView.init(frame: CGRect(x: 0, y: 0, width: navigationViewFrame.size.width, height: 44))
        navigationView.backgroundColor = UIColor.clear
        let menuButton = UIButton.init(type: .custom)
        
        menuButton.frame = CGRect(x: ScreenSize.SCREEN_WIDTH - 45, y: 7, width: 25, height: 25)
        menuButton.setImage(UIImage.init(named: "sideMenu_Icon"), for: UIControlState())
        menuButton.addTarget(self, action: #selector(OpenSideMenu), for: .touchUpInside)
        navigationView.addSubview(menuButton)
        //        self.navigationController?.navigationBar.tintColor = Colors.appPink
        
        let titleLabel = UILabel.init(frame:CGRect(x: 60,y: 0,width: ( (navigationViewFrame.size.width) - 110),height: 40))
        titleLabel.textColor = UIColor.black
        titleLabel.text = "CONTEST"
        titleLabel.font = UIFont.init(name:"OpenSans-SemiBold", size: 15)// UIFont(name:"OpenSans-SemiBold", size: 15)!
        titleLabel.textAlignment = .center
        navigationView.addSubview(titleLabel)
        
        let logoImage = UIImageView.init(frame: CGRect(x: 25, y: 6, width: 33, height: 33))
        logoImage.image = UIImage.init(named: "nikon_login_logo")
        navigationView.addSubview(logoImage)
        
        let backButton = UIButton.init(type: .custom)
        backButton.frame = CGRect(x: 0, y: 9, width: 20, height: 20)
        backButton.backgroundColor = UIColor.white
        backButton.setImage(UIImage.init(named: "back"), for: UIControlState())
        backButton.addTarget(self, action: #selector(PopView), for: .touchUpInside)
        navigationView.addSubview(backButton)
        self.navigationItem.titleView = navigationView
    }
    
        
    //MARK:- COllection View
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: collectionView.frame.size.width  ,height: collectionView.frame.size.height )
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return contestImageDataArray.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "contestLargeImageCell", for: indexPath)
        let contestImage = cell.contentView .viewWithTag(120067) as! UIImageView
        let name = cell.contentView .viewWithTag(120068) as! UILabel
        
        let dict = self.contestImageDataArray[indexPath.row] as! Dictionary<String,AnyObject>
        let image = dict["image"] as? String
        name.text = dict["username"]as? String
        let updateURL = image?.replacingOccurrences(of: " ", with: "%20")
        contestImage.sd_setImage(with: URL.init(string: updateURL!), placeholderImage: UIImage.init(named: "product_placeholder.jpg"))
        contestImage.setShowActivityIndicator(true)
        contestImage.setIndicatorStyle(.gray)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        
    }
    
    //MARK:- SCROLL VIEW
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageWidth = ScreenSize.SCREEN_WIDTH
        let page = (Int)(floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1);
        currenNumberLabel.text = "\(page)"
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
