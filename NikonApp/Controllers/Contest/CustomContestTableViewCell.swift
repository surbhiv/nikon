//
//  CustomContestTableViewCell.swift
//  NikonApp
//
//  Created by Hitesh Dhawan on 20/02/17.
//  Copyright © 2017 Neuronimbus. All rights reserved.
//

import UIKit

class CustomContestTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var checkBoxButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
