//
//  ContestUserMyGallery.swift
//  NikonApp
//
//  Created by Hitesh Dhawan on 23/03/17.
//  Copyright © 2017 Neuronimbus. All rights reserved.
//

import UIKit
protocol sendDataDelegate {
    func sendImageStr(_ str : String)
}
class ContestUserMyGallery: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate {

    @IBOutlet weak var addGalleryBtn: UIButton!
    @IBOutlet weak var heightConstraintForAddgalleryBtn: NSLayoutConstraint!
    @IBOutlet weak var myGalleryCollectionView: UICollectionView!
    var myGalleryArray = [AnyObject]()
    var  isFromMenu = Bool()
    
    var infoLabel: UILabel!
    
    var sendDictionaryDelegate : sendDataDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        self.setNavigationBar()
        self.getData()
        addInfoLabel()
        // Do any additional setup after loading the view.
    }
    func addInfoLabel() {
        
        //        let attributedString = NSMutableAttributedString(string: "NOTIFICATION NOT AVAILABLE!")
        //        attributedString.addAttributes([NSFontAttributeName: UIFont.systemFontOfSize(24.0)], range: NSMakeRange(0, 27))
        
        infoLabel = UILabel()
        //        infoLabel.attributedText = attributedString
        infoLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
        infoLabel.numberOfLines = 4
        infoLabel.textColor = UIColor.black
        infoLabel.font = UIFont(name: "OpenSans", size: 18)
        infoLabel.text = "No Pic Available."
        infoLabel.textAlignment = NSTextAlignment.center
        infoLabel.frame = CGRect(x: (self.view.frame.size.width/2) - 100, y: (self.view.frame.size.height/2) - 150, width: 200, height: 100)
        //        tblNotification.addSubview(infoLabel)
        self.view.addSubview(infoLabel)
    }
    //MARK:- NAVIGATION
    func OpenSideMenu(_ sender: AnyObject){
        
        self.view.endEditing(true)
        toggleSideMenuView()
    }
    func PopView(_ sender: AnyObject){
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func setNavigationBar(){
        self.navigationItem.hidesBackButton = true
        let navigationViewFrame = ScreenSize.SCREEN//self.navigationController?.navigationBar.frame
        let navigationView = UIView.init(frame: CGRect(x: 0, y: 0, width: navigationViewFrame.size.width, height: 44))
        navigationView.backgroundColor = UIColor.clear
        let menuButton = UIButton.init(type: .custom)
        
        menuButton.frame = CGRect(x: ScreenSize.SCREEN_WIDTH - 45, y: 7, width: 25, height: 25)
        menuButton.setImage(UIImage.init(named: "sideMenu_Icon"), for: UIControlState())
        menuButton.addTarget(self, action: #selector(OpenSideMenu), for: .touchUpInside)
        navigationView.addSubview(menuButton)
//        self.navigationController?.navigationBar.tintColor = Colors.appPink
        
        let titleLabel = UILabel.init(frame:CGRect(x: 60,y: 0,width: ( (navigationViewFrame.size.width) - 110),height: 40))
        titleLabel.textColor = UIColor.black
        titleLabel.text = "MY GALLERY"
        titleLabel.font = UIFont.init(name:"OpenSans-SemiBold", size: 15)// UIFont(name:"OpenSans-SemiBold", size: 15)!
        titleLabel.textAlignment = .center
        navigationView.addSubview(titleLabel)
        
        
        if isFromMenu == false
        {
            let backButton = UIButton.init(type: .custom)
            backButton.frame = CGRect(x: 0, y: 9, width: 20, height: 20)
            backButton.backgroundColor = UIColor.white
            backButton.setImage(UIImage.init(named: "back"), for: UIControlState())
            backButton.addTarget(self, action: #selector(PopView), for: .touchUpInside)
            navigationView.addSubview(backButton)

            let logoImage = UIImageView.init(frame: CGRect(x: 25, y: 6, width: 33, height: 33))
            logoImage.image = UIImage.init(named: "nikon_login_logo")
            navigationView.addSubview(logoImage)
            addGalleryBtn.isHidden = true
            heightConstraintForAddgalleryBtn.constant = 0
            
            
        }
        else
        {
            let logoImage = UIImageView.init(frame: CGRect(x: 0, y: 6, width: 33, height: 33))
            logoImage.image = UIImage.init(named: "nikon_login_logo")
            navigationView.addSubview(logoImage)
            addGalleryBtn.isHidden = false
            heightConstraintForAddgalleryBtn.constant = 40
            
            
        }
        self.navigationItem.titleView = navigationView
    }

    //MARK:- GETDATA
    func getData(){
        if Reachability.isConnectedToNetwork()
        {
            let method = "UserGallery?GalleryMasterId=0&UserId=\(UserDefaults.standard.integer(forKey: Constants.USERID))"
            Constants.appDelegate.startIndicator()
            Server.getRequestWithURL(Constants.BASEURL+method, completionHandler: { (response) in
                
                if response[Constants.STATE] != nil && response[Constants.STATE] as! String == Constants.SUCC {
                    
                    DispatchQueue.main.async
                    {
                        Constants.appDelegate.stopIndicator()
                        self.infoLabel.isHidden = true
                        self.myGalleryCollectionView.isHidden = false
                        self.myGalleryArray = response["galleryList"]as! NSArray as [AnyObject]
                        self.myGalleryCollectionView.reloadData()
                    }
                }
                else {
                    Constants.appDelegate.stopIndicator()
                    self.infoLabel.isHidden = false
                    self.myGalleryCollectionView.isHidden = true
                }
            })
        }
        else
        {
            Constants.appDelegate.stopIndicator()
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
    }
    
    //MARK:- COllection View
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: ((ScreenSize.SCREEN_WIDTH - 20)/2),height: (((ScreenSize.SCREEN_WIDTH - 20)/2)) )
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return myGalleryArray.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MyUserGalleryCell", for: indexPath)
        let productImage = cell.contentView .viewWithTag(150111) as! UIImageView
    
        let dict = self.myGalleryArray[indexPath.row] as! Dictionary<String,AnyObject>
        let image = dict["Gallery_Big_Img"] as? String
        let updateURL = image?.replacingOccurrences(of: " ", with: "%20")
        productImage.sd_setImage(with: URL.init(string: updateURL!), placeholderImage: UIImage.init(named: "product_placeholder.jpg"))
        productImage.setShowActivityIndicator(true)
        productImage.setIndicatorStyle(.gray)
        
        return cell
    }
    /*
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 5
    }
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 5
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return self.myGalleryArray.count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("MyUserGalleryCell", forIndexPath: indexPath) as! OtherGalleryCell
        let dict = self.myGalleryArray[indexPath.section] as! Dictionary<String,AnyObject>
        
        
        
        let image = dict["Gallery_Big_Img"] as? String
        let updateURL = image?.stringByReplacingOccurrencesOfString(" ", withString: "%20")
        
        cell.backImage.image = nil
        cell.backImage.sd_setImageWithURL(NSURL.init(string: updateURL!))
        cell.backImage.setShowActivityIndicatorView(true)
        cell.backImage.setIndicatorStyle(.Gray)
        
        return cell
    }
    */
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let dict = self.myGalleryArray[indexPath.row] as! Dictionary<String,AnyObject>
        let image = dict["Gallery_Big_Img"] as? String
        let updateURL = image?.replacingOccurrences(of: " ", with: "%20")

        sendDictionaryDelegate?.sendImageStr(updateURL!)
        self.navigationController?.popViewController(animated: true)
        
    }
    

    @IBAction func addGalleryBtnClicked(_ sender: AnyObject) {
        
        let destVC = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "UploadGalleryVC")as! UploadGalleryVC
        self.navigationController?.pushViewController(destVC, animated: false)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
