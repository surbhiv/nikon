//
//  LeaderBoardVC.swift
//  NikonApp
//
//  Created by Surbhi on 21/12/16.
//  Copyright © 2016 Neuronimbus. All rights reserved.
//

import UIKit

class LeaderBoardVC: UIViewController,UITableViewDataSource, UITableViewDelegate {

    
    @IBOutlet weak var leadTable: UITableView!
    var leaderArray = [AnyObject]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getdata()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getdata(){
        if Reachability.isConnectedToNetwork()
        {
            let method = "LeaderBoard"
            Constants.appDelegate.startIndicator()
            Server.getRequestWithURL(Constants.BASEURL+method, completionHandler: { (response) in
                
                if response[Constants.STATE] != nil && response[Constants.STATE] as! String == Constants.SUCC {
                    self.leaderArray = response["RewardList"] as! Array<AnyObject>
                    DispatchQueue.main.async
                    {
                        Constants.appDelegate.stopIndicator()
                        self.leadTable.isHidden = false
                        self.leadTable.reloadData()
                    }
                }
                else {
                    self.leadTable.isHidden = true
                    Constants.appDelegate.stopIndicator()
                    var mess = String()
                    if response[Constants.MESS] != nil{
                        mess = response[Constants.MESS] as! String
                    }
                    else{
                        mess = response["Message"] as! String
                    }
                    DispatchQueue.main.async
                    {
                    let alert = UIAlertController.init(title: "Nikon", message: mess, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                        self.dismiss(animated: true, completion: nil)
                    }))
                    self.present(alert, animated: true, completion: nil)
                    }
                }
            })
        }
        else
        {
            Constants.appDelegate.stopIndicator()
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
    }

    
    //MARK:- TABLEVIEW
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if  section == 0  {
            return 1
        }
        return leaderArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if  indexPath.section == 0 {
            var cell = tableView.dequeueReusableCell(withIdentifier: "LeaderBoardHeaderCell")
            if cell == nil {
                cell = UITableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "LeaderBoardHeaderCell")
            }
            
            let detailLabel = cell?.contentView.viewWithTag(1301) as! UILabel
            detailLabel.text = "More and more participation in the activities will fetch you more and more points. Now, get to know who all are leading the loyalty program by earning the maximum number of loyalty points and reaching the highest levels of recognition."
            
            tableView.estimatedRowHeight = 150
            tableView.rowHeight = UITableViewAutomaticDimension
            return cell!
        }
        else  {
            var cell = tableView.dequeueReusableCell(withIdentifier: "LeaderBoardUsersCell")
            if cell == nil {
                cell = UITableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "LeaderBoardUsersCell")
            }
            
            let dict = self.leaderArray[indexPath.row] as! Dictionary<String,AnyObject>
            
            let nameLabel = cell?.contentView.viewWithTag(1051) as! UILabel
            let address = cell?.contentView.viewWithTag(1053) as! UILabel
            let cameratype = cell?.contentView.viewWithTag(1055) as! UILabel
            let points = cell?.contentView.viewWithTag(1056) as! UILabel
            let medal = cell?.contentView.viewWithTag(1057) as! UIImageView
            let profile = cell?.contentView.viewWithTag(1052) as! UIImageView
            
            profile.contentMode = UIViewContentMode.scaleAspectFill
            nameLabel.text = dict["UserName"] as? String
            address.text = "\(dict["City_name"] as! String),\(dict["State_name"] as! String))"
            cameratype.text = dict["camera_no"] as? String
            points.text = dict["TotalPoint"] as? String
            
            profile.image = nil
            profile.image = UIImage.init(named: "profilePlaceHolder")
            if (( dict["ProfileImage"]?.isKind(of: NSNull ) ) == false)
            {
            let url =  dict["ProfileImage"] as! String
            
            profile.sd_setImage(with: URL.init(string: url), placeholderImage: UIImage.init(named: "profilePlaceHolder"))
            }
            profile.layer.borderColor = UIColor.white.cgColor
            profile.layer.cornerRadius = 50.0
            profile.layer.borderWidth = 3.5
            profile.clipsToBounds = true
            
            medal.contentMode = UIViewContentMode.scaleAspectFit
            
            if dict["LoyaltyLevelName"] as! String == "Silver" {
                medal.image = UIImage.init(named: "silverBig")
            }
            else  if dict["LoyaltyLevelName"] as! String == "Gold" {
                medal.image = UIImage.init(named: "goldBig")
            }
            else{
                medal.image = UIImage.init(named: "platinumBig")
            }

            tableView.estimatedRowHeight = 40
            tableView.rowHeight = UITableViewAutomaticDimension

            
            return cell!
        }
    }
}
