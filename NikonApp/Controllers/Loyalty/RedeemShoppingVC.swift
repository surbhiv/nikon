//
//  RedeemShoppingVC.swift
//  NikonApp
//
//  Created by Surbhi on 30/12/16.
//  Copyright © 2016 Neuronimbus. All rights reserved.
//

import UIKit

class RedeemShoppingVC: UIViewController, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource {

    @IBOutlet weak var prod_Image: UIImageView!
    @IBOutlet weak var prod_Name: UILabel!
    @IBOutlet weak var prod_Detail: UILabel!
    
    @IBOutlet weak var menu_ProdName: UILabel!
    @IBOutlet weak var menu_Points: UILabel!
    @IBOutlet weak var menu_Quantity: UILabel!
    @IBOutlet weak var menu_SizeText: UITextField!

    @IBOutlet weak var menu_ProdName_2: UILabel!
    @IBOutlet weak var menu_Points_2: UILabel!
    @IBOutlet weak var withSizeView: UIView!
    @IBOutlet weak var withoutSizeView: UIView!

    
    @IBOutlet weak var pincode_label: UILabel!
    @IBOutlet weak var pincode_Text: UITextField!
    @IBOutlet weak var pincode_Line: UILabel!
    @IBOutlet weak var pincode_Line_Height: NSLayoutConstraint!
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var nameText: UITextField!
    @IBOutlet weak var nameLine: UILabel!
    @IBOutlet weak var nameLine_Height: NSLayoutConstraint!

    @IBOutlet weak var add1Label: UILabel!
    @IBOutlet weak var add1Text: UITextField!
    @IBOutlet weak var add1Line: UILabel!
    @IBOutlet weak var add1Line_Height: NSLayoutConstraint!

    @IBOutlet weak var add2Label: UILabel!
    @IBOutlet weak var add2Text: UITextField!
    @IBOutlet weak var add2Line: UILabel!
    @IBOutlet weak var add2Line_Height: NSLayoutConstraint!

    @IBOutlet weak var tounLabel: UILabel!
    @IBOutlet weak var townText: UITextField!
    @IBOutlet weak var townLine: UILabel!
    @IBOutlet weak var townLine_Height: NSLayoutConstraint!

    @IBOutlet weak var stateLabel: UILabel!
    @IBOutlet weak var stateText: UITextField!
    @IBOutlet weak var stateLine: UILabel!
    @IBOutlet weak var stateLine_Height: NSLayoutConstraint!

    @IBOutlet weak var pincodeLabel: UILabel!
    @IBOutlet weak var pincodeText: UITextField!
    @IBOutlet weak var pincodeLine: UILabel!
    @IBOutlet weak var pincodeLine_Height: NSLayoutConstraint!

    @IBOutlet weak var countryLabel: UILabel!
    @IBOutlet weak var countryText: UITextField!
    @IBOutlet weak var countryLine: UILabel!
    @IBOutlet weak var countryLine_Height: NSLayoutConstraint!

    @IBOutlet weak var mobileLabel: UILabel!
    @IBOutlet weak var mobileText: UITextField!
    @IBOutlet weak var mobileLine: UILabel!
    @IBOutlet weak var mobileLine_Height: NSLayoutConstraint!

    @IBOutlet weak var sameShippingButton: UIButton!
    
    @IBOutlet weak var nameLabel2: UILabel!
    @IBOutlet weak var nameText2: UITextField!
    @IBOutlet weak var nameLine2: UILabel!
    @IBOutlet weak var nameLine2_Height: NSLayoutConstraint!

    @IBOutlet weak var add1Label2: UILabel!
    @IBOutlet weak var add1Text2: UITextField!
    @IBOutlet weak var add1Line2: UILabel!
    @IBOutlet weak var add1Line2_Height: NSLayoutConstraint!

    @IBOutlet weak var add2Label2: UILabel!
    @IBOutlet weak var add2Text2: UITextField!
    @IBOutlet weak var add2Line2: UILabel!
    @IBOutlet weak var add2Line2_Height: NSLayoutConstraint!

    @IBOutlet weak var tounLabel2: UILabel!
    @IBOutlet weak var townText2: UITextField!
    @IBOutlet weak var townLine2: UILabel!
    @IBOutlet weak var townLine2_Height: NSLayoutConstraint!

    @IBOutlet weak var stateLabel2: UILabel!
    @IBOutlet weak var stateText2: UITextField!
    @IBOutlet weak var stateLine2: UILabel!
    @IBOutlet weak var stateLine2_Height: NSLayoutConstraint!

    @IBOutlet weak var pincodeLabel2: UILabel!
    @IBOutlet weak var pincodeText2: UITextField!
    @IBOutlet weak var pincodeLine2: UILabel!
    @IBOutlet weak var pincodeLine2_Height: NSLayoutConstraint!

    @IBOutlet weak var countryLabel2: UILabel!
    @IBOutlet weak var countryText2: UITextField!
    @IBOutlet weak var countryLine2: UILabel!
    @IBOutlet weak var countryLine2_Height: NSLayoutConstraint!

    @IBOutlet weak var mobileLabel2: UILabel!
    @IBOutlet weak var mobileText2: UITextField!
    @IBOutlet weak var mobileLine2: UILabel!
    @IBOutlet weak var mobileLine2_Height: NSLayoutConstraint!

    
    var productDictionary = Dictionary<String,AnyObject>()
    var profileDict = Helper.getDataFromNsDefault(Constants.PROFILE)
    var picker = UIPickerView()
    var sizeArray = Array<String>()
    var itemSizeSelected = String()

    //MARK: - Starting
    
    override func viewDidLoad() {
        super.viewDidLoad()
        itemSizeSelected = ""
        self.navigationController?.isNavigationBarHidden = false
        self.setNavigationBar()
        
        self.picker = UIPickerView.init()
        self.picker.dataSource = self
        self.picker.delegate = self
        self.menu_SizeText.inputView = picker
        
        DispatchQueue.main.async{
            self.setUpView()
        }
        // Do any additional setup after loading the view.
    }
    func OpenSideMenu(_ sender: AnyObject){
        
        self.view.endEditing(true)
        toggleSideMenuView()
    }
    func PopView(_ sender: AnyObject){
        
        self.navigationController?.popViewController(animated: true)
    }

    func setNavigationBar(){
        self.navigationItem.hidesBackButton = true
        let navigationViewFrame = ScreenSize.SCREEN//self.navigationController?.navigationBar.frame
        let navigationView = UIView.init(frame: CGRect(x: 0, y: 0, width: navigationViewFrame.size.width, height: 44))
        navigationView.backgroundColor = UIColor.clear
        let menuButton = UIButton.init(type: .custom)
        
        menuButton.frame = CGRect(x: ScreenSize.SCREEN_WIDTH - 45, y: 7, width: 25, height: 25)
        menuButton.setImage(UIImage.init(named: "sideMenu_Icon"), for: UIControlState())
        menuButton.addTarget(self, action: #selector(OpenSideMenu), for: .touchUpInside)
        navigationView.addSubview(menuButton)
//        self.navigationController?.navigationBar.tintColor = Colors.appPink
        
        let titleLabel = UILabel.init(frame:CGRect(x: 60,y: 0,width: ( (navigationViewFrame.size.width) - 110),height: 40))
        titleLabel.textColor = UIColor.black
        titleLabel.text = "LOYALTY"
        titleLabel.font = UIFont(name:"OpenSans-SemiBold", size: 15)!
        titleLabel.textAlignment = .center
        navigationView.addSubview(titleLabel)
        
        let logoImage = UIImageView.init(frame: CGRect(x: 25, y: 6, width: 33, height: 33))
        logoImage.image = UIImage.init(named: "nikon_login_logo")
        navigationView.addSubview(logoImage)
        
        let backButton = UIButton.init(type: .custom)
        backButton.frame = CGRect(x: 0, y: 9, width: 20, height: 20)
        backButton.backgroundColor = UIColor.white
        backButton.setImage(UIImage.init(named: "back"), for: UIControlState())
        backButton.addTarget(self, action: #selector(PopView), for: .touchUpInside)
        navigationView.addSubview(backButton)
        
        self.navigationItem.titleView = navigationView
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- SET UP VIEW
    func setUpView(){
        
        self.prod_Name.text = self.productDictionary["Name"] as? String
        let imageURL = self.productDictionary["product_image_thumb"] as! String

        self.prod_Image.sd_setImage(with: URL.init(string: imageURL), placeholderImage: UIImage.init(named: "profilePlaceTemp"))
        
        self.menu_ProdName.text = self.productDictionary["Name"] as? String
        self.menu_Points.text = String(self.productDictionary["Loyalty_points_redeem"] as! Int)
        print(self.productDictionary)
        
        self.menu_ProdName_2.text = self.productDictionary["Name"] as? String
        self.menu_Points_2.text = String(self.productDictionary["Loyalty_points_redeem"] as! Int)

        let sizeString = self.productDictionary["ItemSize"] as? String
        
        if self.productDictionary.keys.contains("ItemSize") && sizeString?.isEmpty == false {
            self.withSizeView.isHidden = false
            self.withoutSizeView.isHidden = true
            self.view.bringSubview(toFront: self.withSizeView)
            
            sizeArray = (sizeString?.components(separatedBy: ","))!
            self.menu_SizeText.text = ""//sizeArray.first
            itemSizeSelected = self.menu_SizeText.text!
        }
        else{
            
            
            itemSizeSelected = ""
            self.withSizeView.isHidden = true
            self.withoutSizeView.isHidden = false
            self.view.bringSubview(toFront: self.withoutSizeView)
        }
        
        
        let spacing : CGFloat = 10.0 // the amount of spacing to appear between image and title
        self.sameShippingButton.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, spacing);
        self.sameShippingButton.titleEdgeInsets = UIEdgeInsetsMake(0, spacing, 0, 0);
        
        
        nameText.text = "\((profileDict["User_Title"] as? String)!) \((profileDict["User_Fname"] as? String)!) \((profileDict["User_Lname"] as? String)!)"
        add1Text.text = profileDict["User_Address"] as? String
        townText.text = profileDict["City_name"] as? String
        stateText.text = profileDict["State_name"] as? String
        pincodeText.text = profileDict["User_Pincode"] as? String
        countryText.text = profileDict["User_Country"] as? String
        mobileText.text = profileDict["User_Phone"] as? String

    }
    
    //MARK:- TEXTFIELD DELEGATES
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == nameText {
            ACTIVATETextField(nameText, textLabel: nameLabel, line: nameLine, height: nameLine_Height)
        }
        else if textField == add1Text {
            ACTIVATETextField(add1Text, textLabel: add1Label, line: add1Line, height: add1Line_Height)
        }
        else if textField == add2Text {
            ACTIVATETextField(add2Text, textLabel: add2Label, line: add2Line, height: add2Line_Height)
        }
        else if textField == townText {
            ACTIVATETextField(townText, textLabel: tounLabel, line: townLine, height: townLine_Height)
        }
        else if textField == stateText {
            ACTIVATETextField(stateText, textLabel: stateLabel, line: stateLine, height: stateLine_Height)
        }
        else if textField == countryText {
            ACTIVATETextField(countryText, textLabel: countryLabel, line: countryLine, height: countryLine_Height)
        }
        else if textField == pincodeText {
            ACTIVATETextField(pincodeText, textLabel: pincodeLabel, line: pincodeLine, height: pincodeLine_Height)
        }
        else if textField == mobileText {
            ACTIVATETextField(mobileText, textLabel: mobileLabel, line: mobileLine, height: mobileLine_Height)
        }
        else if textField == nameText2 {
            ACTIVATETextField(nameText2, textLabel: nameLabel2, line: nameLine2, height: nameLine2_Height)
        }
        else if textField == add1Text2 {
            ACTIVATETextField(add1Text2, textLabel: add1Label2, line: add1Line2, height: add1Line2_Height)
        }
        else if textField == add2Text2 {
            ACTIVATETextField(add2Text2, textLabel: add2Label2, line: add2Line2, height: add2Line2_Height)
        }
        else if textField == townText2 {
            ACTIVATETextField(townText2, textLabel: tounLabel2, line: townLine2, height: townLine2_Height)
        }
        else if textField == stateText2 {
            ACTIVATETextField(stateText2, textLabel: stateLabel2, line: stateLine2, height: stateLine2_Height)
        }
        else if textField == countryText2 {
            ACTIVATETextField(countryText2, textLabel: countryLabel2, line: countryLine2, height: countryLine2_Height)
        }
        else if textField == pincodeText2 {
            ACTIVATETextField(pincodeText2, textLabel: pincodeLabel2, line: pincodeLine2, height: pincodeLine2_Height)
        }
        else if textField == mobileText2 {
            ACTIVATETextField(mobileText2, textLabel: mobileLabel2, line: mobileLine2, height: mobileLine2_Height)
        }

        if textField == pincode_Text{
            ACTIVATETextField(pincode_Text, textLabel: pincode_label, line: pincode_Line, height:pincode_Line_Height )
        }
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == nameText {
            DEACTIVATETextField(nameText, textLabel: nameLabel, line: nameLine, height: nameLine_Height)
        }
        else if textField == add1Text {
            DEACTIVATETextField(add1Text, textLabel: add1Label, line: add1Line, height: add1Line_Height)
        }
        else if textField == add2Text {
            DEACTIVATETextField(add2Text, textLabel: add2Label, line: add2Line, height: add2Line_Height)
        }
        else if textField == townText {
            DEACTIVATETextField(townText, textLabel: tounLabel, line: townLine, height: townLine_Height)
        }
        else if textField == stateText {
            DEACTIVATETextField(stateText, textLabel: stateLabel, line: stateLine, height: stateLine_Height)
        }
        else if textField == countryText {
            DEACTIVATETextField(countryText, textLabel: countryLabel, line: countryLine, height: countryLine_Height)
        }
        else if textField == pincodeText {
            DEACTIVATETextField(pincodeText, textLabel: pincodeLabel, line: pincodeLine, height: pincodeLine_Height)
        }
        else if textField == mobileText {
            DEACTIVATETextField(mobileText, textLabel: mobileLabel, line: mobileLine, height: mobileLine_Height)
        }
        else if textField == nameText2 {
            DEACTIVATETextField(nameText2, textLabel: nameLabel2, line: nameLine2, height: nameLine2_Height)
        }
        else if textField == add1Text2 {
            DEACTIVATETextField(add1Text2, textLabel: add1Label2, line: add1Line2, height: add1Line2_Height)
        }
        else if textField == add2Text2 {
            DEACTIVATETextField(add2Text2, textLabel: add2Label2, line: add2Line2, height: add2Line2_Height)
        }
        else if textField == townText2 {
            DEACTIVATETextField(townText2, textLabel: tounLabel2, line: townLine2, height: townLine2_Height)
        }
        else if textField == stateText2 {
            DEACTIVATETextField(stateText2, textLabel: stateLabel2, line: stateLine2, height: stateLine2_Height)
        }
        else if textField == countryText2 {
            DEACTIVATETextField(countryText2, textLabel: countryLabel2, line: countryLine2, height: countryLine2_Height)
        }
        else if textField == pincodeText2 {
            DEACTIVATETextField(pincodeText2, textLabel: pincodeLabel2, line: pincodeLine2, height: pincodeLine2_Height)
        }
        else if textField == mobileText2 {
            DEACTIVATETextField(mobileText2, textLabel: mobileLabel2, line: mobileLine2, height: mobileLine2_Height)
        }

        if textField == pincode_Text{
            DEACTIVATETextField(pincode_Text, textLabel: pincode_label, line: pincode_Line, height:pincode_Line_Height )
        }
    }
    
    func ACTIVATETextField(_ textf : UITextField,textLabel : UILabel , line: UILabel, height : NSLayoutConstraint)
    {
        UIView.animate(withDuration: 1.0, delay: 0, options: UIViewAnimationOptions.transitionFlipFromBottom, animations: {
            
            textLabel.isHidden = false
            textf.attributedPlaceholder = NSAttributedString(string: "")
            Helper.activatetextField(line,height: height,textlabel: textLabel)
            
            },
       completion: { (true) in
            textLabel.isHidden = false
        })
    }
    
    func DEACTIVATETextField(_ textf : UITextField,textLabel : UILabel , line: UILabel, height : NSLayoutConstraint)
    {
        if textf.text == ""
        {
            UIView.animate(withDuration: 1.0, animations: {
                textLabel.isHidden = true
                
                Helper.deActivatetextField(line,height: height,textlabel: textLabel)
                
                let mess = textLabel.text!
                
                textf.attributedPlaceholder = NSAttributedString(string: mess ,  attributes: [NSForegroundColorAttributeName : Colors.textPlaceHolderGray, NSFontAttributeName : UIFont(name: "OpenSans", size: 14)!])
                
                }, completion: nil)
        }
    }

    
    //MARK:- BUTTON ACTIONS

    @IBAction func CheckForPinCodeAvailability_Pressed(_ sender: AnyObject) {
        
        self.pincode_Text.resignFirstResponder()
        if self.pincode_Text.text! != "" {
            if Reachability.isConnectedToNetwork()
            {
                let method = "CheckPincode?pincode=\(self.pincode_Text.text!)"
                Constants.appDelegate.startIndicator()
                Server.getRequestWithURL(Constants.BASEURL+method, completionHandler: { (response) in
                    
                    if response[Constants.STATE] != nil && response[Constants.STATE] as! String == Constants.SUCC {
                        
                        DispatchQueue.main.async
                        {
                            Constants.appDelegate.stopIndicator()
                            var mess = String()
                            if response[Constants.MESS] != nil{
                                mess = response[Constants.MESS] as! String
                            }
                            else{
                                mess = response["Message"] as! String
                            }
                            
                            let alert = UIAlertController.init(title: "Nikon", message: mess, preferredStyle: UIAlertControllerStyle.alert)
                            self.present(alert, animated: true, completion: {
                                self.perform(#selector(self.dismissAlert), with: alert, afterDelay: 0.5)
                            })
                        }
                    }
                    else {
                        
                        Constants.appDelegate.stopIndicator()
                        var mess = String()
                        if response[Constants.MESS] != nil{
                            mess = response[Constants.MESS] as! String
                        }
                        else{
                            mess = response["Message"] as! String
                        }
                        
                        let alert = UIAlertController.init(title: "Nikon", message: mess, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                            self.dismiss(animated: true, completion: nil)
                        }))
                        self.present(alert, animated: true, completion: nil)
                    }
                })
            }
            else
            {
                Constants.appDelegate.stopIndicator()
                CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
            }
        }
    }
    
    func dismissAlert(){
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func SameShippingAddressButtonPressed(_ sender: AnyObject) {
        
        if sameShippingButton.isSelected == true{
            sameShippingButton.isSelected = false
        }
        else{
            sameShippingButton.isSelected = true
        }
        
        if sameShippingButton.isSelected == true{
            
            nameText2.text = nameText.text
            add1Text2.text = add1Text.text
            add2Text2.text = add2Text.text
            townText2.text = townText.text
            stateText2.text = stateText.text
            pincodeText2.text = pincodeText.text
            countryText2.text = countryText.text
            mobileText2.text = mobileText.text
        }
        else{
            nameText2.text = ""
            add1Text2.text = ""
            add2Text2.text = ""
            townText2.text = ""
            stateText2.text = ""
            pincodeText2.text = ""
            countryText2.text = ""
            mobileText2.text = ""
        }
    }

    @IBAction func ConfirmPressed(_ sender: AnyObject) {
        
//        RedeemGoodies  post
//        int GoodiesId,int WorkshopId, string GoodiesName,int Loyalty_points_redeem, int UserID,string BFullName, string BAddressLine1,string BAddressLine2,
//        string BCIty, string BState,string BPinCode,string BCountry, string BMobile,string SFullName,string SAddressLine1,string SAddressLine2,
//        string SCIty, string SState,string SPinCode,string SCountry,string SMobile, string ItemSize, int Product_market_Price, int Member_master_id, string Order_id
        nameText.resignFirstResponder()
        add1Text.resignFirstResponder()
        add2Text.resignFirstResponder()
        townText.resignFirstResponder()
        stateText.resignFirstResponder()
        pincodeText.resignFirstResponder()
        countryText.resignFirstResponder()
        mobileText.resignFirstResponder()
        
        nameText2.resignFirstResponder()
        add1Text2.resignFirstResponder()
        add2Text2.resignFirstResponder()
        townText2.resignFirstResponder()
        stateText2.resignFirstResponder()
        pincodeText2.resignFirstResponder()
        countryText2.resignFirstResponder()
        mobileText2.resignFirstResponder()

        let goodieID = self.productDictionary["goodiesId"] as! Int
        let goodiesName = self.productDictionary["Name"] as! String
        let redeemPoint = self.productDictionary["Loyalty_points_redeem"] as! Int
        
        
        if nameText.text == "" || nameText2.text == "" {
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "Nikon", "message":"Please Enter Full Name."])
        }
        else if add1Text.text == "" || add1Text2.text == "" {
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "Nikon", "message":"Please Enter Address."])
        }
        else if townText.text == "" || townText2.text == "" {
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "Nikon", "message":"Please Enter Address."])
        }
        else if stateText.text == "" || stateText2.text == "" {
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "Nikon", "message":"Please Enter Address."])
        }
        else if pincodeText.text == "" || pincodeText2.text == "" {
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "Nikon", "message":"Please Enter Address."])
        }
        else if countryText.text == "" || countryText2.text == "" {
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "Nikon", "message":"Please Enter Address."])
        }
        else if mobileText.text == "" || mobileText2.text == "" {
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "Nikon", "message":"Please Enter Address."])
        }
        else if  sizeArray.count > 0 && itemSizeSelected == "" {
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "Nikon", "message":"Please Select Size."])
        }
        else  {
           
            let method = "RedeemGoodies"
            let param = "GoodiesId=\(goodieID)&WorkshopId=0&GoodiesName=\(goodiesName)&Loyalty_points_redeem=\(redeemPoint)&UserID=\(UserDefaults.standard.integer(forKey: Constants.USERID))&BFullName=\(self.nameText.text!)&BAddressLine1=\(self.add1Text.text!)&BAddressLine2=\(self.add2Text.text!)&BCIty=\(self.townText.text!)&BState=\(self.stateText.text!)&BPinCode=\(self.pincodeText.text!)&BCountry=\(self.countryText.text!)&BMobile=\(self.mobileText.text!)&SFullName=\(self.nameText2.text!)&SAddressLine1=\(self.add1Text2.text!)&SAddressLine2=\(self.add2Text2.text!)&SCIty=\(self.townText2.text!)&SState=\(self.stateText2.text!)&SPinCode=\(self.pincodeText2.text!)&SCountry=\(self.countryText2.text!)&SMobile=\(self.mobileText2.text!)&ItemSize=\(itemSizeSelected)&Product_market_Price=\(redeemPoint)&Member_master_id=0&Order_id="
            
            
            if Reachability.isConnectedToNetwork() {
                Constants.appDelegate.startIndicator()
                Server.postRequestWithURL(Constants.BASEURL+method, paramString: param) { (response) in
                    Constants.appDelegate.stopIndicator()
                    if response[Constants.STATE] != nil && response[Constants.STATE] as! String == Constants.SUCC {
                        
                        DispatchQueue.main.async(execute: {
                            let alert = UIAlertController.init(title: "Nikon", message: response["message"]! as? String, preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                                self.dismiss(animated: true, completion: nil)
                                self.popToMainScreen()
                            }))
                            self.present(alert, animated: true, completion: nil)
                        })
                    }
                    else {
                        var mess = String()
                        if response[Constants.MESS] != nil{
                            mess = response[Constants.MESS] as! String
                        }
                        else{
                            mess = response["Message"] as! String
                        }
                        CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "Nikon", "message": mess] )
                    }
                }
            }
            else {
                Constants.appDelegate.stopIndicator()
                CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
            }

        }
    }
    
    func popToMainScreen(){
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    
    
    //MARK:- PICKEVIEW

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return sizeArray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return String.init(format: sizeArray [row] , locale: nil)
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {

        self.menu_SizeText.text = sizeArray [row]
        itemSizeSelected =  sizeArray [row]
//        picker.removeFromSuperview()
//        self.menu_SizeText.resignFirstResponder()
    }
}
