//
//  EarnPointVC.swift
//  NikonApp
//
//  Created by Surbhi on 21/12/16.
//  Copyright © 2016 Neuronimbus. All rights reserved.
//

import UIKit

class EarnPointVC: UIViewController,UITableViewDataSource, UITableViewDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    
    @IBOutlet weak var earnpointTable: UITableView!
    var earnDict = Dictionary<String,AnyObject>()
    var earnArray = [AnyObject]()
    var titleString = String()
    var subHeader = String()
    var pointArray = [AnyObject]()

    override func viewDidLoad() {
        super.viewDidLoad()
        getdata()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getdata(){
            if Reachability.isConnectedToNetwork()
            {
                let method = "Loyalty"
                Constants.appDelegate.startIndicator()
                Server.getRequestWithURL(Constants.BASEURL+method, completionHandler: { (response) in

                    if response[Constants.STATE] != nil && response[Constants.STATE] as! String == Constants.SUCC {
                        self.earnDict = response as Dictionary<String,AnyObject>
                        self.earnArray = self.earnDict["PointonList"] as! Array<AnyObject>
                        self.titleString = self.earnDict["title"] as! String
                        self.subHeader = self.earnDict["Description"] as! String
                        self.pointArray = self.earnDict["GetLevelType"] as! Array<AnyObject>
                        
                        DispatchQueue.main.async
                        {
                            Constants.appDelegate.stopIndicator()
                            self.earnpointTable.isHidden = false
                            self.earnpointTable.reloadData()
                        }
                    }
                    else {
                        self.earnpointTable.isHidden = true
                        Constants.appDelegate.stopIndicator()
                        var mess = String()
                        if response[Constants.MESS] != nil{
                            mess = response[Constants.MESS] as! String
                        }
                        else{
                            mess = response["Message"] as! String
                        }
                        
                        let alert = UIAlertController.init(title: "Nikon", message: mess, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                            self.dismiss(animated: true, completion: nil)
                        }))
                        self.present(alert, animated: true, completion: nil)
                    }
                })
            }
            else
            {
                Constants.appDelegate.stopIndicator()
                CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
            }
    }
    
    //MARK:- TABLEVIEW
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if  section == 0  {
            return 1
        }
        return earnArray.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if  indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "EarnPointHeaderCell") as! EarnPointHeaderCell
            
            cell.subheaderLabel.text = self.earnDict["title"] as? String
            cell.detailLabel.text = self.earnDict["Description"] as? String
            cell.pointsView.delegate = self
            cell.pointsView.dataSource = self
            DispatchQueue.main.async
            {
                cell.pointsView.reloadData()
            }
            
            
            tableView.estimatedRowHeight = 340
            tableView.rowHeight = UITableViewAutomaticDimension

            return cell
        }
        else  {
            
            let dict = earnArray[indexPath.row] as! Dictionary<String,AnyObject>
           
            let cell = tableView.dequeueReusableCell(withIdentifier: "EarnPointOptionsCell")as! EarnPointOptionsCell
           
            cell.optionHeader.text = dict["title"] as? String
            cell.optionHeader.backgroundColor = UIColor.clear
            cell.optiondetail.text = dict["Description"] as? String
            let urlstr = dict["TypeImage"] as! String
            cell.imageOption.sd_setImage(with: URL.init(string: urlstr), placeholderImage: UIImage.init(named: "profilePlaceTemp"))
 
            
            cell.nikonUserLabel.textAlignment = NSTextAlignment.center
            cell.nikonLBL.textAlignment = NSTextAlignment.center
            cell.nonnikonLBL.textAlignment = NSTextAlignment.center
            
            let nikonRewardPINT = dict["Point1"]  as! String
            let non_nikonRewardPINT = dict["Point2"]  as! String

            if nikonRewardPINT == "" && non_nikonRewardPINT == ""{
                // hide the view
                cell.nikonUserLabel.isHidden = true
                cell.nikonLBL.isHidden = true
                cell.nonnikonLBL.isHidden = true
                cell.pointsView.isHidden = true
                
                cell.nikonUserLabelWidth.constant = 0
                cell.nonnikonLBLWidth.constant = 0
                cell.nonnikonLBLWidth.constant = 0
            }
            else if nikonRewardPINT == "" && non_nikonRewardPINT != ""{
                cell.nikonLBLWidth.constant = 0
                cell.nikonUserLabel.isHidden = false
                cell.nikonLBL.isHidden = true
                cell.nonnikonLBL.isHidden = false
                cell.pointsView.isHidden = false

                let attrStr = try! NSAttributedString(
                    data: non_nikonRewardPINT.data(using: String.Encoding.unicode, allowLossyConversion: true)!,
                    options: [ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                    documentAttributes: nil)
                
                let newattr = NSMutableAttributedString.init(string: " ")
                newattr.append(attrStr)

                cell.nonnikonLBL.attributedText = newattr
                
                
                cell.nikonUserLabelWidth.constant = 125
                cell.nonnikonLBLWidth.constant = cell.pointsView.frame.size.width - 125
            }
            else if nikonRewardPINT != "" && non_nikonRewardPINT == ""{
                cell.nonnikonLBLWidth.constant = 0
                cell.nikonUserLabel.isHidden = false
                cell.nikonLBL.isHidden = false
                cell.nonnikonLBL.isHidden = true
                cell.pointsView.isHidden = false

                let attrStr = try! NSAttributedString(
                    data: nikonRewardPINT.data(using: String.Encoding.unicode, allowLossyConversion: true)!,
                    options: [ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                    documentAttributes: nil)
                
                let newattr = NSMutableAttributedString.init(string: " ")
                newattr.append(attrStr)
                
                cell.nikonLBL.attributedText = newattr
                
                cell.nikonUserLabelWidth.constant = 125
                cell.nikonLBLWidth.constant = cell.pointsView.frame.size.width - 125
            }
            else{
                
                cell.nikonUserLabelWidth.constant = (ScreenSize.SCREEN_WIDTH - 15.0) / 3.0
                cell.nonnikonLBLWidth.constant = (ScreenSize.SCREEN_WIDTH - 15.0) / 3
                cell.nikonLBLWidth.constant = (ScreenSize.SCREEN_WIDTH - 15.0) / 3

                cell.nikonUserLabel.isHidden = false
                cell.nikonLBL.isHidden = false
                cell.nonnikonLBL.isHidden = false
                cell.pointsView.isHidden = false

                let attrStr = try! NSAttributedString(
                    data: non_nikonRewardPINT.data(using: String.Encoding.unicode, allowLossyConversion: true)!,
                    options: [ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                    documentAttributes: nil)
                let newattr1 = NSMutableAttributedString.init(string: " ")
                newattr1.append(attrStr)

                cell.nonnikonLBL.attributedText = newattr1
                cell.nonnikonLBL.textColor = UIColor.white
                
                let attrStr2 = try! NSAttributedString(
                    data: nikonRewardPINT.data(using: String.Encoding.unicode, allowLossyConversion: true)!,
                    options: [ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                    documentAttributes: nil)
                let newattr2 = NSMutableAttributedString.init(string: " ")
                newattr2.append(attrStr2)
                cell.nikonLBL.attributedText = newattr2
                

            }
            
            
            tableView.estimatedRowHeight = 320
            tableView.rowHeight = UITableViewAutomaticDimension

            return cell
        }
        
    }

    //MARK:- EARNPOINT COllection
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: (collectionView.bounds.size.width)/3, height: 110)
    }

    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return pointArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "pointCell", for: indexPath)
        let typeImage = cell.contentView.viewWithTag(1101) as! UIImageView
        let typeName = cell.contentView.viewWithTag(1102) as! UILabel
        let levelType = pointArray[indexPath.item]["Level_Name"] as! String
        let pointLevel = pointArray[indexPath.item]["Points"] as! Int
        typeName.text = "\(pointLevel) POINTS"

        if  levelType == "Silver" {
            typeImage.image = UIImage.init(named: "silverBig")
        }
        else if levelType == "Gold" {
            typeImage.image = UIImage.init(named: "goldBig")
        }
        else
        {
            typeImage.image = UIImage.init(named: "platinumBig")
        }

        return cell
    }
}
