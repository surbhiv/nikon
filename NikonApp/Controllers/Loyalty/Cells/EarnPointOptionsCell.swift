//
//  EarnPointOptionsCell.swift
//  NikonApp
//
//  Created by Surbhi on 21/12/16.
//  Copyright © 2016 Neuronimbus. All rights reserved.
//

import UIKit

class EarnPointOptionsCell: UITableViewCell {

    @IBOutlet weak var nikonUserLabel: UILabel!
    @IBOutlet weak var nikonLBL: UILabel!
    @IBOutlet weak var nonnikonLBL: UILabel!
    @IBOutlet weak var pointsView: UIView!

    @IBOutlet weak var nikonUserLabelWidth: NSLayoutConstraint!
    @IBOutlet weak var nikonLBLWidth: NSLayoutConstraint!
    @IBOutlet weak var nonnikonLBLWidth: NSLayoutConstraint!

    
    @IBOutlet weak var imageOption: UIImageView!
    @IBOutlet weak var optionHeader: UILabel!
    @IBOutlet weak var optiondetail: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
