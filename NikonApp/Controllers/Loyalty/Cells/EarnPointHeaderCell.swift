//
//  EarnPointHeaderCell.swift
//  NikonApp
//
//  Created by Surbhi on 21/12/16.
//  Copyright © 2016 Neuronimbus. All rights reserved.
//

import UIKit

class EarnPointHeaderCell: UITableViewCell {

    @IBOutlet weak var earnpointHeader: UIButton!
    @IBOutlet weak var subheaderLabel: UILabel!
    @IBOutlet weak var pointsView: UICollectionView!
    @IBOutlet weak var detailLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
