//
//  RedeemGoodiesVC.swift
//  NikonApp
//
//  Created by Surbhi on 21/12/16.
//  Copyright © 2016 Neuronimbus. All rights reserved.
//

import UIKit

class RedeemGoodiesVC: UIViewController,UITableViewDataSource, UITableViewDelegate {

    
    @IBOutlet weak var redeemTable: UITableView!
    var redeemArray = [AnyObject]()
    var myRedeemPoints = Int()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getdata()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- GET DATA
    func getdata(){
        if Reachability.isConnectedToNetwork()
        {
            let method = "Goodies?UsrerId=\(UserDefaults.standard.integer(forKey: Constants.USERID))"
            Constants.appDelegate.startIndicator()
            Server.getRequestWithURL(Constants.BASEURL+method, completionHandler: { (response) in
                
                if response[Constants.STATE] != nil && response[Constants.STATE] as! String == Constants.SUCC {
                    self.redeemArray = response["GoodiesList"] as! Array<AnyObject>
                    self.myRedeemPoints = response["AvailableRedeempoint"] as! Int
                                        
                    DispatchQueue.main.async
                    {
                        Constants.appDelegate.stopIndicator()
                        self.redeemTable.isHidden = false
                        self.redeemTable.reloadData()
                    }
                }
                else {
                    self.redeemTable.isHidden = true
                    Constants.appDelegate.stopIndicator()
                    var mess = String()
                    if response[Constants.MESS] != nil{
                        mess = response[Constants.MESS] as! String
                    }
                    else{
                        mess = response["Message"] as! String
                    }
                    DispatchQueue.main.async
                    {
                    let alert = UIAlertController.init(title: "Nikon", message: mess, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                        self.dismiss(animated: true, completion: nil)
                    }))
                    self.present(alert, animated: true, completion: nil)
                    }
                }
            })
        }
        else
        {
            Constants.appDelegate.stopIndicator()
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
    }

    
    //MARK:- TABLEVIEW
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 5
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 5
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return redeemArray.count + 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if  indexPath.section == 0 {
            var cell = tableView.dequeueReusableCell(withIdentifier: "RedeemHeaderCell")
            if cell == nil {
                cell = UITableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "RedeemHeaderCell")
            }
            
            let detailLabel = cell?.contentView.viewWithTag(1065) as! UILabel
            detailLabel.text = "You can use your points and get these Goodies."
            
            tableView.rowHeight = 60
            return cell!
        }
        else  {
            var cell = tableView.dequeueReusableCell(withIdentifier: "RedeemTypeCell")
            if cell == nil {
                cell = UITableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "RedeemTypeCell")
            }
            cell?.contentView.layer.cornerRadius = 8.0
            cell?.contentView.clipsToBounds = true
            
            let dict = self.redeemArray[indexPath.section - 1] as! Dictionary<String,AnyObject>
            
            let headerLabel = cell?.contentView.viewWithTag(1062) as! UILabel
            let points = cell?.contentView.viewWithTag(1063) as! UILabel
            let imag = cell?.contentView.viewWithTag(1061) as! UIImageView
            let redeembutton = cell?.contentView.viewWithTag(1064) as! UIButton
            let redeemPoints =  dict["Loyalty_points_redeem"] as! Int
             let outOfStockLbl = cell?.contentView.viewWithTag(1065) as! UILabel
            if ((dict["ItemStock"] as! Int) == 0 ) {
                outOfStockLbl.isHidden = false
            }
            else
            {
                outOfStockLbl.isHidden = true
            }
            
            if myRedeemPoints >= redeemPoints && ((dict["ItemStock"] as! Int) != 0 ){
            
                redeembutton.backgroundColor = Colors.appYello
                redeembutton.addTarget(self, action: #selector(redeemPressed), for: .touchUpInside)
            }
            else{
                redeembutton.backgroundColor = UIColor.white
            }
            
            headerLabel.text = dict["Name"] as? String
            points.text = String(redeemPoints)
            
            imag.sd_setImage(with: URL.init(string: dict["product_image_thumb"] as! String), placeholderImage: UIImage.init(named: "product_placeholder.jpg"))
            imag.setShowActivityIndicator(true)
            imag.setIndicatorStyle(.gray)
            
            tableView.rowHeight = 148
            
            return cell!
        }
    }
    
    @IBAction func redeemPressed(_ sender: AnyObject){
        
        
        if (UserDefaults.standard.integer(forKey: Constants.USERID) != 0)
        {
            var indexPath: IndexPath!
        
            if let button = sender as? UIButton {
                if let cell = button.superview?.superview?.superview?.superview as? UITableViewCell {
                    indexPath = redeemTable.indexPath(for: cell)
                }
            }
        
        
        
            let dict = self.redeemArray[indexPath.section - 1] as! Dictionary<String,AnyObject>
            let redeemPoints =  dict["Loyalty_points_redeem"] as! Int
            if myRedeemPoints >= redeemPoints && ((dict["ItemStock"] as! Int) != 0 ){
            
                let destViewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "RedeemShoppingVC") as! RedeemShoppingVC
                destViewController.productDictionary = dict
                self.navigationController?.pushViewController(destViewController, animated: true)
            }
            else
            {
            
            }

        }
        else
        {
            self.loginAlert()
        
        }
    }
    
    //MARK:- LoginAlert
    
    func loginAlert()  {
        let alert = UIAlertController.init(title: "Nikon", message: "Please login to continue.", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
            
            DispatchQueue.main.async
            {
                let destinationController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "LoginVC")as! LoginVC
                destinationController.isWantTodismissVC = true
                self.present(destinationController, animated: true, completion: nil);
            }
        }))
        alert.addAction(UIAlertAction.init(title: "Cancel", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
            self.dismiss(animated: true, completion: nil)
        }))
        self.present(alert, animated: true, completion: nil)
    }

    
}
