//
//  LoyaltyVC.swift
//  NikonApp
//
//  Created by Surbhi on 20/12/16.
//  Copyright © 2016 Neuronimbus. All rights reserved.
//

import UIKit

class LoyaltyVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var menuColl: UICollectionView!
    @IBOutlet weak var collectionheight: NSLayoutConstraint!
    @IBOutlet weak var termsButton: UIButton!
    @IBOutlet weak var contentheight: NSLayoutConstraint!

    var isFromMenu = Bool()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigationBar()
        // Do any additional setup after loading the view.
        termsButton.layer.cornerRadius = 2.0
        termsButton.clipsToBounds = true
        
        if DeviceType.IS_IPHONE_6P {
            contentheight.constant = 850
        }
        else{
            contentheight.constant = 820
        }
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func OpenSideMenu(_ sender: AnyObject){
        
        self.view.endEditing(true)
        toggleSideMenuView()
    }

    func setNavigationBar(){
        self.navigationItem.hidesBackButton = true
        
        let navigationViewFrame = self.navigationController?.navigationBar.frame
        let navigationView = UIView.init(frame: navigationViewFrame!)
        navigationView.backgroundColor = UIColor.white
        let menuButton = UIButton.init(type: .custom)
        menuButton.frame = CGRect(x: ScreenSize.SCREEN_WIDTH - 45, y: 7, width: 25, height: 25)
        menuButton.setImage(UIImage.init(named: "sideMenu_Icon"), for: UIControlState())
        menuButton.addTarget(self, action: #selector(OpenSideMenu), for: .touchUpInside)
        navigationView.addSubview(menuButton)
        
//        self.navigationController?.navigationBar.tintColor = Colors.appPink
        
        let titleLabel = UILabel.init(frame:CGRect(x: ( ScreenSize.SCREEN_WIDTH - 150)/2,y: 0,width: 150,height: 44))
        titleLabel.textColor = UIColor.black
        titleLabel.text="LOYALTY"
        titleLabel.font = UIFont(name:"OpenSans-SemiBold", size: 15)!
        titleLabel.textAlignment = .center
        navigationView.addSubview(titleLabel)
        
        
        
        if isFromMenu == false
        {
            let backButton = UIButton.init(type: .custom)
            backButton.frame = CGRect(x: 0, y: 9, width: 20, height: 20)
            backButton.backgroundColor = UIColor.white
            backButton.setImage(UIImage.init(named: "back"), for: UIControlState())
            backButton.addTarget(self, action: #selector(PopView), for: .touchUpInside)
            navigationView.addSubview(backButton)
            
            let logoImage = UIImageView.init(frame: CGRect(x: 25, y: 6, width: 33, height: 33))
            logoImage.image = UIImage.init(named: "nikon_login_logo")
            navigationView.addSubview(logoImage)

        }
        else{
            let logoImage = UIImageView.init(frame: CGRect(x: 0, y: 6, width: 33, height: 33))
            logoImage.image = UIImage.init(named: "nikon_login_logo")
            navigationView.addSubview(logoImage)

        }
        
        self.navigationItem.titleView = navigationView
    }
    func PopView(_ sender: AnyObject){
        
        self.navigationController?.popViewController(animated: true)
    }

    //MARK:- COllection View
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: (collectionView.bounds.size.width - 5)/2, height: 115)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "LoyaltyCell", for: indexPath)
        cell.layer.cornerRadius = 2.0
        cell.clipsToBounds = true
        
        let typeImage = cell.contentView.viewWithTag(1031) as! UIImageView
        let nameLabel = cell.contentView.viewWithTag(1032) as! UILabel
        nameLabel.numberOfLines = 0
        typeImage.image = nil
        typeImage.contentMode = UIViewContentMode.scaleAspectFill
        
        if indexPath.item == 0 {
            cell.backgroundColor = Colors.textPlaceHolderGray
            nameLabel.text = "EARN POINT"
            typeImage.image = UIImage.init(named: "earnPointIcon")
        }
        else if indexPath.item == 1 {
            cell.backgroundColor = Colors.indicatorYello

            nameLabel.text = "LEADERBOARD"
            typeImage.image = UIImage.init(named: "leaderBoardIon")
        }
        else if indexPath.item == 2 {
            cell.backgroundColor = Colors.indicatorYello

            nameLabel.text = "REDEEM WORKSHOP"
            typeImage.image = UIImage.init(named: "workshop_LoyaltyIcon")
        }
        else if indexPath.item == 3 {
            cell.backgroundColor = Colors.termsGray

            nameLabel.text = "REDEEM GOODIES"
            typeImage.image = UIImage.init(named: "goodiesIcon")
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        if indexPath.item != 2 {
            
            let destViewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "LoyaltyScrollVC") as! LoyaltyScrollVC

            if indexPath.item == 0 {
                destViewController.selectedSection = 0
            }
            else if indexPath.item == 1 {
                destViewController.selectedSection = 1
            }
            else {
                destViewController.selectedSection = 2
            }
            
            self.navigationController?.pushViewController(destViewController, animated: true)
        }
        else
        {
            let destViewController = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "WorkshopVC")
            self.navigationController?.pushViewController(destViewController, animated: true)
            
//            let alert = UIAlertController.init(title: "", message: "Coming Soon", preferredStyle: UIAlertControllerStyle.Alert)
//            self.presentViewController(alert, animated: true, completion: {
//                self.performSelector(#selector(self.dismissAlert), withObject: alert, afterDelay: 0.5)
//            })
        }
    }

    func dismissAlert(){
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func TermsPressed(_ sender: AnyObject){
        
        let destViewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "WebVC") as! WebVC
        destViewController.isPush = true
        destViewController.title = "TERMS & CONDITIONS"
        self.navigationController?.pushViewController(destViewController, animated: true)
    }
}
