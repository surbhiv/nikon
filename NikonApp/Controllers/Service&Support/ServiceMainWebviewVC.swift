//
//  ServiceMainWebviewVC.swift
//  NikonApp
//
//  Created by Hitesh Dhawan on 06/03/17.
//  Copyright © 2017 Neuronimbus. All rights reserved.
//

import UIKit

class ServiceMainWebviewVC: UIViewController,UIWebViewDelegate {

    @IBOutlet weak var webview: UIWebView!
    
    var strURL = String()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        webview.loadRequest(URLRequest.init(url: URL.init(string: self.strURL)!))
    }

    func webViewDidStartLoad(_ webView: UIWebView) {
         Constants.appDelegate.startIndicator()
    }
    func webViewDidFinishLoad(_ webView: UIWebView) {
        Constants.appDelegate.stopIndicator()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
