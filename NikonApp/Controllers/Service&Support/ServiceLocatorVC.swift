//
//  ServiceLocatorVC.swift
//  NikonApp
//
//  Created by Hitesh Dhawan on 06/03/17.
//  Copyright © 2017 Neuronimbus. All rights reserved.
//

import UIKit

class ServiceLocatorVC: UIViewController,CLLocationManagerDelegate,UITableViewDelegate,UITableViewDataSource,UIPickerViewDelegate,UIPickerViewDataSource,UITextFieldDelegate ,GMSMapViewDelegate {

    
    @IBOutlet weak var googleMapView: GMSMapView!
    @IBOutlet weak var serviceLocatorTable: UITableView!
    
    @IBOutlet weak var segmentStore: UISegmentedControl!
    @IBOutlet weak var allserviceTable: UITableView!
    let locationManager = CLLocationManager()
    @IBOutlet weak var radiusTextField: UITextField!
    
    //ALL STORE
     @IBOutlet weak var centerTextField: UITextField!
    @IBOutlet weak var stateTextField: UITextField!
    @IBOutlet weak var cityTextField: UITextField!
    
    var latStr = String()
    var longStr = String()
    var radiusStr = String()
    var centerShownTextStr = String()
    var serviceCenterStr = String()
    var CollectionCenterStr = String()
    var stateStr = String()
    var cityStr = String()
   
    var pageSize = Int()
    var pageNumber = Int()
    var totalPage = Int()
    
    var serviceDealerDataArray = [AnyObject]()
    var allServiceDealerDataArray = [AnyObject]()
    var marker = GMSMarker()
    var picker = UIPickerView()
    let kmArray = ["20 Km","50 Km","100 Km","150 Km","200 Km"]
    var didFindLocation = Bool()
    var isLoadingData = Bool()
    let centerArray = ["SELECT CENTER","All Centers","Authorized Service Centers","Authorized Collection Centers"]
    var stateArray = [AnyObject]()
    var allServiceTempArray = [AnyObject]()
    var cityArray = [AnyObject]()
    
    var markerCount = Int() // used to setting gmsmarker accessibilityLabel


    override func viewDidLoad() {
        super.viewDidLoad()

        let segAttributes: NSDictionary = [
            NSForegroundColorAttributeName: UIColor.black,
            NSFontAttributeName: UIFont(name: "OpenSans", size: 16)!
        ]
        
        segmentStore.setTitleTextAttributes(segAttributes as! [AnyHashable: Any], for: UIControlState())
        // Do any additional setup after loading the view.
        self.setUpView()
    }
    //MARK:-SETUPVIEW
    func setUpView()  {
        self.picker = UIPickerView.init()
        self.picker.dataSource = self
        self.picker.delegate = self
        self.radiusTextField.inputView = self.picker
        self.radiusTextField.delegate = self
        self.stateTextField.inputView = self.picker
        self.stateTextField.delegate = self
        self.cityTextField.inputView = self.picker
        self.cityTextField.delegate = self
        self.centerTextField.inputView = self.picker
        
        
        radiusStr = "20"
        serviceCenterStr = ""
        CollectionCenterStr = ""
        stateStr = ""
        cityStr = ""
       
        pageSize = 20
        pageNumber = 1
        totalPage = 0
        markerCount = 0
        
        locationManager.delegate = self
        didFindLocation = false
        isLoadingData = false
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
        googleMapView.delegate = self
        
        
    }
    
    // MARK: - CLLocationManagerDelegate
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        if status == .authorizedWhenInUse {
            
            
            locationManager.startUpdatingLocation()
            
            
            googleMapView.isMyLocationEnabled = true
            googleMapView.settings.myLocationButton = true
        }
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            googleMapView.camera = GMSCameraPosition(target: location.coordinate, zoom: 8, bearing: 0, viewingAngle: 0)
            self.latStr = String(location.coordinate.latitude)
            self.longStr = String(location.coordinate.longitude)
//            self.latStr = "28.6296"
//            self.longStr = "77.0802"
            DispatchQueue.main.async
            {
                self.locationManager.stopUpdatingLocation()
                if self.didFindLocation != true
                {
                    self.pageNumber = 1
                    self.didFindLocation = true
                    self.getServiceData()
                }
                
            }
        }
        
    }
    
    //MARK: GMSMapViewDelegate
    func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {        // Get a reference for the custom overlay
        let index:Int! = Int(marker.accessibilityLabel!)
        let customInfoWindow = Bundle.main.loadNibNamed("customViewForMarker", owner: self, options: nil)?[0] as! customViewForMarker
        customInfoWindow.storeNameLbl.text = serviceDealerDataArray[index]["name"] as? String
        customInfoWindow.storeAddressLbl.text = "\(serviceDealerDataArray[index]["street"] as! String) +\(serviceDealerDataArray[index]["town"] as! String),\(serviceDealerDataArray[index]["state"] as! String)"
        customInfoWindow.storeTimeLbl.text = serviceDealerDataArray[index]["hours"] as? String
        customInfoWindow.storeNumberLbl.text = serviceDealerDataArray[index]["tel"] as? String
        self.view.bringSubview(toFront: customInfoWindow.getDirectionBtn)
        customInfoWindow.getDirectionBtn.tag = index
        customInfoWindow.getDirectionBtn.addTarget(self, action: #selector(getDirection), for: .touchUpInside)
        
        return customInfoWindow
        
        
    }
    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
        
        let index:Int! = Int(marker.accessibilityLabel!)
        let btn = UIButton.init()
        btn.tag = index
        self .getDirection(btn)
        
        
    }
    //MARK:- SEGMENT ACTION
    
    @IBAction func segmentClicked(_ sender: AnyObject) {
        if self.segmentStore.selectedSegmentIndex == 1
        {
            self.allserviceTable.isHidden = false
            self.radiusStr = "0"
            self.latStr = ""
            self.longStr = ""
            serviceCenterStr = "False"
            CollectionCenterStr = "False"
            
            if allServiceDealerDataArray.count > 0
            {
                allServiceDealerDataArray.removeAll()
            }
            self.allserviceTable.reloadData()
            //self.getCityNameWithState()
             self.getServiceData()
            
        }
        else
        {
            let tempArr = self.radiusTextField.text?.components(separatedBy: " ")
            self.radiusStr = tempArr![0]
            serviceCenterStr = ""
            CollectionCenterStr = ""
            stateStr = ""
            cityStr = ""
            self.allserviceTable.isHidden = true
            didFindLocation = false
            //self.getServiceData()
            locationManager.startUpdatingLocation()
        }
    }

    //MARK:- Search Action
    @IBAction func searchButtonClicked(_ sender: AnyObject)
    {
        self.getServiceData1()
    }
    
    //MARK:- API
    func getServiceData()  {
        if Reachability.isConnectedToNetwork()
        {
            self.pageNumber = 1
            
            self.googleMapView.clear()
            
            var method = "GetServiceCentre?CurrentLat=\(self.latStr)&CurrentLong=\(self.longStr)&RadiusDistanceInKM=\(self.radiusStr)&State=\(stateStr)&City=\(cityStr)&service_center=\(serviceCenterStr)&collection_center=\(CollectionCenterStr)&Limit=0"
            method = method.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!

            Constants.appDelegate.startIndicator()
            Server.getRequestWithURL(Constants.BASEURL+method, completionHandler: { (response) in
                
                
                if response[Constants.STATE] != nil && response[Constants.STATE] as! String == Constants.SUCC
                {

                    DispatchQueue.main.async
                    {
                        Constants.appDelegate.stopIndicator()
                        
                        if self.serviceDealerDataArray.count > 0
                        {
                            self.serviceDealerDataArray.removeAll()
                        }
                        if self.allserviceTable.isHidden == true
                        {
                            self.serviceDealerDataArray  = response["ServicentreData"] as! NSArray as [AnyObject]
                            
                            self.markerCount = 0
                            for obj in self.serviceDealerDataArray
                            {
                                
                                let lat = Double(obj["Latitude"] as! String)!
                                let long = Double(obj["Longitude"] as! String)!
                                let position = CLLocationCoordinate2D(latitude: lat, longitude: long)
                                let newMarker = GMSMarker(position: position)
                                // newMarker.title = "\(lat)"
                                newMarker.map = self.googleMapView
                                newMarker.infoWindowAnchor = CGPoint(x: 0.5, y: 0.2)
                                newMarker.accessibilityLabel = "\(self.markerCount)"
                                newMarker.icon = GMSMarker.markerImage(with: UIColor.black)

                                self.markerCount = self.markerCount+1
                            }
                            self.serviceLocatorTable.reloadData()
                        }
                        else
                        {
                            self.allServiceDealerDataArray  = response["ServicentreData"] as! NSArray as [AnyObject]
                            self.allServiceTempArray = response["ServicentreData"] as! NSArray as [AnyObject]
                            var tempArray = [AnyObject]()
                            for obj in self.allServiceDealerDataArray
                            {
                                
                                let lat = Double(obj["Latitude"] as! String)!
                                let long = Double(obj["Longitude"] as! String)!
                                let position = CLLocationCoordinate2D(latitude: lat, longitude: long)
                                let newMarker = GMSMarker(position: position)
                                newMarker.icon = GMSMarker.markerImage(with: UIColor.black)

                                //newMarker.title = "\(lat)"
                                newMarker.map = self.googleMapView
                                
                                //filter state
                                let stateName = obj["state"] as! String
                                //let cityName = obj["town"] as! String
                                if (!(tempArray as NSArray).contains(stateName))
                                {
                                    tempArray.append(stateName as AnyObject)
                                }
                            }
                           
                            self.stateArray = tempArray.sorted { $0.localizedCaseInsensitiveCompare($1 as! String) == ComparisonResult.orderedAscending }
                           
                            self.allserviceTable.reloadData()
                        }
                        
                    }
                }
                else
                {
                    DispatchQueue.main.async
                    {
                    Constants.appDelegate.stopIndicator()
                    if self.allserviceTable.isHidden == true
                    {
                        
                        if self.serviceDealerDataArray.count > 0
                        {
                            self.serviceDealerDataArray.removeAll()
                        }
                        self.serviceLocatorTable.reloadData()
                    }
                    else
                    {
                        if self.allServiceDealerDataArray.count > 0
                        {
                            self.allServiceDealerDataArray.removeAll()
                        }
                        self.allserviceTable.reloadData()
                    }
                    }
                }
                
                
            })
        }
        else
        {
            Constants.appDelegate.stopIndicator()
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
        
        
    }
    
    func getServiceData1()  {
        if Reachability.isConnectedToNetwork()
        {
            self.pageNumber = 1
            
            self.googleMapView.clear()
//            let customAllowedSet =  NSCharacterSet(charactersInString:"&").invertedSet
//            let finalstateStr = stateStr.stringByAddingPercentEncodingWithAllowedCharacters(customAllowedSet)
//
//            let method = "GetServiceCentre?CurrentLat=\(self.latStr)&CurrentLong=\(self.longStr)&RadiusDistanceInKM=\(self.radiusStr)&State=\(finalstateStr!)&City=\(cityStr)&service_center=\(serviceCenterStr)&collection_center=\(CollectionCenterStr)&Limit=100"
            
            let escapedString = stateStr.replacingOccurrences(of: " & ", with: "*")
            
            let method = "GetServiceCentre?CurrentLat=\(self.latStr)&CurrentLong=\(self.longStr)&RadiusDistanceInKM=\(self.radiusStr)&State=\(escapedString)&City=\(cityStr)&service_center=\(serviceCenterStr)&collection_center=\(CollectionCenterStr)&Limit=0"
            
//            let method = "GetServiceCentre?CurrentLat=\(self.latStr)&CurrentLong=\(self.longStr)&RadiusDistanceInKM=\(self.radiusStr)&State=\(escapedString)&City=\(cityStr)&service_center=\(serviceCenterStr)&collection_center=\(CollectionCenterStr)&Limit=0"
            //            method = method.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!
            
//            method = method.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!
            
            Constants.appDelegate.startIndicator()
            Server.getRequestWithURL(Constants.BASEURL+method, completionHandler: { (response) in
                
                Constants.appDelegate.stopIndicator()
                if response[Constants.STATE] != nil && response[Constants.STATE] as! String == Constants.SUCC
                {
                    //print(response)
                    //let responseDict = response as! NSDictionary
                    
                    DispatchQueue.main.async
                    {
                        
                        
                        //                        let totalCount = Int(response["RecordCount"] as! String)!
                        //                        if totalCount%self.pageSize == 0
                        //                        {
                        //                            self.totalPage = totalCount/self.pageSize
                        //
                        //                        }
                        //                        else
                        //                        {
                        //                            self.totalPage = (totalCount/self.pageSize) + 1
                        //                        }
                        //                        self.pageNumber = self.pageNumber+1
                        //                        print(self.pageNumber)
                        if self.serviceDealerDataArray.count > 0
                        {
                            self.serviceDealerDataArray.removeAll()
                        }
                        if self.allserviceTable.isHidden == true
                        {
                            self.serviceDealerDataArray  = response["ServicentreData"] as! NSArray as [AnyObject]
                            
                            
                            self.markerCount = 0
                            for obj in self.serviceDealerDataArray
                            {
                                
                                let lat = Double(obj["Latitude"] as! String)!
                                let long = Double(obj["Longitude"] as! String)!
                                let position = CLLocationCoordinate2D(latitude: lat, longitude: long)
                                let newMarker = GMSMarker(position: position)
                                // newMarker.title = "\(lat)"
                                newMarker.map = self.googleMapView
                                newMarker.infoWindowAnchor = CGPoint(x: 0.5, y: 0.2)
                                newMarker.accessibilityLabel = "\(self.markerCount)"
                                newMarker.icon = GMSMarker.markerImage(with: UIColor.black)
                                
                                self.markerCount = self.markerCount+1
                                
                                
                            }
                            self.serviceLocatorTable.reloadData()
                            
                        }
                        else
                        {
                            self.allServiceDealerDataArray  = response["ServicentreData"] as! NSArray as [AnyObject]
//                            var tempArray = [AnyObject]()
                            for obj in self.allServiceDealerDataArray
                            {
                                
                                let lat = Double(obj["Latitude"] as! String)!
                                let long = Double(obj["Longitude"] as! String)!
                                let position = CLLocationCoordinate2D(latitude: lat, longitude: long)
                                let newMarker = GMSMarker(position: position)
                                newMarker.icon = GMSMarker.markerImage(with: UIColor.black)
                                
                                //newMarker.title = "\(lat)"
                                newMarker.map = self.googleMapView
                                
                                //filter state
//                                let stateName = obj["state"] as! String
//                                //let cityName = obj["town"] as! String
//                                if (!(tempArray as NSArray).containsObject(stateName))
//                                {
//                                    tempArray.append(stateName)
//                                }
//                                
//                                
//                                
                            }
//
//                            //                            self.stateArray = tempArray.sort { $0.localizedCaseInsensitiveCompare($1 as! String) == NSComparisonResult.OrderedAscending }
//                            self.stateArray = tempArray.sort { $0.localizedCaseInsensitiveCompare($1 as! String) == NSComparisonResult.OrderedAscending }
                            
                            self.allserviceTable.reloadData()
                        }
                        
                    }
                }
                else
                {
                    DispatchQueue.main.async
                    {
                        Constants.appDelegate.stopIndicator()
                        if self.allserviceTable.isHidden == true
                        {
                            
                            if self.serviceDealerDataArray.count > 0
                            {
                                self.serviceDealerDataArray.removeAll()
                            }
                            self.serviceLocatorTable.reloadData()
                            
                        }
                        else
                        {
                            if self.allServiceDealerDataArray.count > 0
                            {
                                self.allServiceDealerDataArray.removeAll()
                            }
                            self.allserviceTable.reloadData()
                        }
                    }
                }
                
                
            })
        }
        else
        {
            Constants.appDelegate.stopIndicator()
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
        
        
    }

    
    /*
    func getStoreDataWithPagination()  {
        if Reachability.isConnectedToNetwork()
        {
            let method = "GetDealer?CurrentLat=\(self.latStr)&CurrentLong=\(self.longStr)&RadiusDistanceInKM=\(self.radiusStr)&State=\(stateStr)&City=\(cityStr)&binoculars=\(binocularsStr)&experience_zones=\(experience_zonesStr)&distributor=\(distributorStr)&authorized_dealer=\(authorized_dealerStr)&PageSize=\(String(pageSize))&PageNumber=\(String(pageNumber))"
            
            Constants.appDelegate.startIndicator()
            Server.getRequestWithURL(Constants.BASEURL+method, completionHandler: { (response) in
                
                
                if response[Constants.STATE] != nil && response[Constants.STATE] as! String == Constants.SUCC {
                    print(response)
                    //let responseDict = response as! NSDictionary
                    
                    dispatch_async(dispatch_get_main_queue())
                    {
                        Constants.appDelegate.stopIndicator()
                        let tempArr = response["DealerData"] as! NSArray as [AnyObject]
                        self.pageNumber = self.pageNumber+1
                        print(self.pageNumber)
                        self.isLoadingData = false
                        for obj in tempArr//for (var j = 0; j < tempArr.count; j++)
                        {
                            
                            
                            if self.allserviceTable.hidden == true
                            {
                                let lat = Double(obj["Latitude"] as! String)!
                                let long = Double(obj["Longitude"] as! String)!
                                let position = CLLocationCoordinate2D(latitude: lat, longitude: long)
                                let newMarker = GMSMarker(position: position)
                                // newMarker.title = "\(lat)"
                                newMarker.infoWindowAnchor = CGPointMake(0.5, 0.2)
                                newMarker.accessibilityLabel = "\(self.markerCount)"
                                self.markerCount = self.markerCount+1
                                
                                newMarker.map = self.googleMapView
                                self.serviceDealerDataArray.append(obj)
                                self.serviceLocatorTable.reloadData()
                                
                            }
                            else
                            {
                                self.allserviceDealerDataArray.append(obj)
                                self.allserviceTable.reloadData()
                                
                            }
                        }
                        
                        
                    }
                }
                else
                {
                    Constants.appDelegate.stopIndicator()
                }
                
                
            })
        }
        else
        {
            Constants.appDelegate.stopIndicator()
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
        
        
    }


*/
    
    //MARK:- TABLEVIEW
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.allserviceTable.isHidden == false
        {
            return allServiceDealerDataArray.count
        }
        return serviceDealerDataArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == serviceLocatorTable
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "StoreLocatorCustomCell", for: indexPath) as! StoreLocatorCustomCell
            cell.storeNameLbl.text = serviceDealerDataArray[indexPath.row]["name"] as? String
            cell.storeAddressLbl.text = "\(serviceDealerDataArray[indexPath.row]["street"] as! String) \(serviceDealerDataArray[indexPath.row]["town"] as! String),\(serviceDealerDataArray[indexPath.row]["state"] as! String)"
            cell.storeTimeLbl.text = serviceDealerDataArray[indexPath.row]["hours"] as? String
            
            let telePhoneArr = (serviceDealerDataArray[indexPath.row]["tel"] as? String)?.components(separatedBy: " / ")
            let newTelephoneStr = telePhoneArr?.joined(separator: "\n")
            
            let attrs = [ NSFontAttributeName : UIFont(name: Fonts.regFONTname, size: (DeviceType.IS_IPAD ? 17.0 : 15.0))!,
                          NSForegroundColorAttributeName : UIColor.black,
                          NSUnderlineStyleAttributeName : 1] as [String : Any]
            
            let attributedSTR = NSMutableAttributedString.init(string: newTelephoneStr!, attributes: attrs)
            cell.storePhoneText.attributedText = attributedSTR

            //cell.storeNumberLbl.text = serviceDealerDataArray[indexPath.row]["tel"] as? String
            let myDoble = Double(serviceDealerDataArray[indexPath.row]["Distance"]as! String)
            // String(format: "%.2f", myDouble)
            cell.storeDistance.text = String(format: "%.2f KM", myDoble!)
           // cell.storeDistance.text = serviceDealerDataArray[indexPath.row]["Distance"] as? String
            cell.getDirectionBtn.tag = indexPath.row
            cell.getDirectionBtn.addTarget(self, action: #selector(getDirection), for: .touchUpInside)
            
//            let status = serviceDealerDataArray[indexPath.row]["experience_zones"] as! String
//            if status == "true"
//            {
//                cell.backgroundColor = UIColor.init(red: 250.0/255.0, green: 245.0/255.0, blue: 218.0/255.0, alpha: 1)
//            }
//            else
//            {
//                cell.backgroundColor = UIColor.whiteColor()
//            }
            cell.shadowView.layer.shadowColor = UIColor.black.cgColor
            cell.shadowView.layer.shadowOpacity = 1
            cell.shadowView.layer.shadowOffset = CGSize.zero
            cell.shadowView.layer.shadowRadius = 5
            
            serviceLocatorTable.estimatedRowHeight = 300
            serviceLocatorTable.rowHeight = UITableViewAutomaticDimension
            
            cell.selectionStyle = .none
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "StoreLocatorCustomCell", for: indexPath) as! StoreLocatorCustomCell
            cell.storeNameLbl.text = allServiceDealerDataArray[indexPath.row]["name"] as? String
            cell.storeAddressLbl.text = "\(allServiceDealerDataArray[indexPath.row]["street"] as! String) \(allServiceDealerDataArray[indexPath.row]["town"] as! String),\(allServiceDealerDataArray[indexPath.row]["state"] as! String)"
            cell.storeTimeLbl.text = allServiceDealerDataArray[indexPath.row]["hours"] as? String
           
            let telePhoneArr = (allServiceDealerDataArray[indexPath.row]["tel"] as? String)?.components(separatedBy: " / ")
            let newTelephoneStr = telePhoneArr?.joined(separator: "\n")
            
            let attrs = [ NSFontAttributeName : UIFont(name: Fonts.regFONTname, size: (DeviceType.IS_IPAD ? 17.0 : 15.0))!,
                          NSForegroundColorAttributeName : UIColor.black,
                          NSUnderlineStyleAttributeName : 1] as [String : Any]
            
            let attributedSTR = NSMutableAttributedString.init(string: newTelephoneStr!, attributes: attrs)
            cell.storePhoneText.attributedText = attributedSTR

            
            //cell.storeNumberLbl.text = allServiceDealerDataArray[indexPath.row]["tel"] as? String
            //cell.storeDistance.text = allServiceDealerDataArray[indexPath.row]["Distance"] as? String
            cell.getDirectionBtn.tag = indexPath.row
            cell.getDirectionBtn.addTarget(self, action: #selector(getDirection), for: .touchUpInside)
            
            cell.shadowView.layer.shadowColor = UIColor.black.cgColor
            cell.shadowView.layer.shadowOpacity = 1
            cell.shadowView.layer.shadowOffset = CGSize.zero
            cell.shadowView.layer.shadowRadius = 5
            
            allserviceTable.estimatedRowHeight = 204
            allserviceTable.rowHeight = UITableViewAutomaticDimension
            
            cell.selectionStyle = .none
            return cell
            
            
        }
        
    }
    //MARK:- DIRECTION Action
    func getDirection(_ sender : UIButton)
    {
        let index = sender.tag
        if UIApplication.shared.canOpenURL(URL(string: "comgooglemaps://")!)
        {
            
            var addSre = String()
            
            if self.allserviceTable.isHidden == true
            {
                addSre = "comgooglemaps://?daddr=\(self.serviceDealerDataArray[index]["Latitude"]as! String),\(self.serviceDealerDataArray[index]["Longitude"]as! String)&directionsmode=transit"
            }
            else
            {
                addSre = "comgooglemaps://?daddr=\(self.allServiceDealerDataArray[index]["Latitude"]as! String),\(self.allServiceDealerDataArray[index]["Longitude"]as! String)&directionsmode=transit"
                
            }
            
            UIApplication.shared.openURL(URL(string: addSre)!)
        }
        else
        {
            
            let finalURLStr = "http://maps.google.com/maps?saddr=\(self.latStr),\(self.longStr)&daddr=\(self.serviceDealerDataArray[index]["Latitude"]as! String),\(self.serviceDealerDataArray[index]["Longitude"]as! String)"
            UIApplication.shared.openURL(URL(string:finalURLStr )!)
        }
    }
    
    //MARK: - PICKEVIEW for date
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if self.centerTextField.isFirstResponder
        {
            return centerArray.count
        }
        else if self.radiusTextField.isFirstResponder
        {
            return kmArray.count
        }
        else if stateTextField.isFirstResponder
        {
            return stateArray.count
        }
        else
        {
            return cityArray.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if self.centerTextField.isFirstResponder
        {
             return String.init(format: centerArray [row] , locale: nil)
        }
        else if self.radiusTextField.isFirstResponder
        {
            
            return String.init(format: kmArray [row] , locale: nil)
        }
        else if self.stateTextField.isFirstResponder
        {
            return String.init(format: stateArray [row] as! String, locale: nil)
        }
        else
        {
            return String.init(format: cityArray [row] as! String, locale: nil)
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if self.radiusTextField.isFirstResponder
        {
            self.radiusTextField.text = String.init(format: kmArray [row] , locale: nil)
            let tempArr = kmArray[row].components(separatedBy: " ")
            self.radiusStr = tempArr[0]
        }
        else if self.stateTextField.isFirstResponder
        {
            self.stateStr = stateArray [row] as! String
            self.stateTextField.text = self.stateStr
            self.cityStr = ""
            self.cityTextField.text = self.cityStr
        }
        else if self.centerTextField.isFirstResponder
        {
            self.centerShownTextStr = centerArray [row]
            if self.centerShownTextStr == "SELECT CENTER"
            {
                
                self.serviceCenterStr = ""
                self.CollectionCenterStr = ""
            }
            if row == 1
            {
                self.serviceCenterStr = "True"
                self.CollectionCenterStr = "True"
                
            }
            if row == 2 {
                self.serviceCenterStr = "True"
                self.CollectionCenterStr = "False"
            }
            if row == 3 {
                self.serviceCenterStr = "False"
                self.CollectionCenterStr = "True"
            }
            self.centerTextField.text = self.centerShownTextStr
        }
        else
        {
            if stateStr != ""
            {
                if cityArray.count > 0
                {
            self.cityStr = cityArray[row]as! String
            self.cityTextField.text = self.cityStr
                }
            }
        }
      
    }
    
    // MARK: TEXTFIELD DELEGATE
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        textField .resignFirstResponder()
        if textField == radiusTextField{
            self.getServiceData()
        }
        if textField == self.stateTextField
        {
            let pre = NSPredicate(format: "SELF['state']==%@", self.stateStr)
            var tempCityArray = [AnyObject]()
            
//             tempCityArray = (self.allServiceDealerDataArray as NSArray).filteredArrayUsingPredicate(pre)
             tempCityArray = (self.allServiceTempArray as NSArray).filtered(using: pre) as [AnyObject]
            var stringCityArray = [String]()
            for obj in tempCityArray
            {
                if (!stringCityArray.contains(obj["town"]as! String))
                {
                    stringCityArray.append(obj["town"]as! String)
                }
            }
            self.cityArray = stringCityArray.sorted { $0.localizedCaseInsensitiveCompare($1 ) == ComparisonResult.orderedAscending } as [AnyObject]
            
        }
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == self.cityTextField
        {
            if stateStr == ""
            {
                textField .resignFirstResponder()
                CustomAlertController.showAlert(on: self, with: Simple, andAttributes: ["title": "", "message":"Please select state."])
            }
        }
    }
    


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
