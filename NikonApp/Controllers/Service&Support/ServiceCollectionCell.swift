//
//  ServiceCollectionCell.swift
//  NikonApp
//
//  Created by Hitesh Dhawan on 06/03/17.
//  Copyright © 2017 Neuronimbus. All rights reserved.
//

import UIKit

class ServiceCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var serviceImageView: UIImageView!
    @IBOutlet weak var findOutBtn: UIButton!
}
