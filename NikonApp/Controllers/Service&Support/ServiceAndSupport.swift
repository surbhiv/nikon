//
//  ServiceAndSupport.swift
//  NikonApp
//
//  Created by Hitesh Dhawan on 06/03/17.
//  Copyright © 2017 Neuronimbus. All rights reserved.
//

import UIKit

class ServiceAndSupport: UIViewController,UICollectionViewDelegate, UICollectionViewDataSource, UIScrollViewDelegate, UICollectionViewDelegateFlowLayout{

    @IBOutlet weak var supportImageView: UIImageView!
    @IBOutlet weak var serviceCollection: UICollectionView!
    @IBOutlet weak var servicePageControl: UIPageControl!
    
    var serviceArray = [String]()
    var timer = Timer()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationController?.isNavigationBarHidden = false
        self.setNavigationBar()
        serviceArray = ["https://www.nikonschool.in/UploadImage/AppImages/find-out-what-you-need.png","https://www.nikonschool.in/UploadImage/AppImages/customer-support.png","https://www.nikonschool.in/UploadImage/AppImages/warranty-policy.png","https://www.nikonschool.in/UploadImage/AppImages/authorized-service-centers.png"]
        supportImageView.sd_setImage(with: URL.init(string: "https://www.nikonschool.in/UploadImage/AppImages/product-support.png"), placeholderImage: UIImage.init(named: "product_placeholder.jpg"))
        supportImageView.setIndicatorStyle(.gray)
        supportImageView.setShowActivityIndicator(true)
        self.addTimer()

        
    }

    //MARK:- NAVIGATION
    func OpenSideMenu(_ sender: AnyObject){
        
        self.view.endEditing(true)
        toggleSideMenuView()
    }
    func PopView(_ sender: AnyObject){
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func setNavigationBar(){
        self.navigationItem.hidesBackButton = true
        let navigationViewFrame = ScreenSize.SCREEN//self.navigationController?.navigationBar.frame
        let navigationView = UIView.init(frame: CGRect(x: 0, y: 0, width: navigationViewFrame.size.width, height: 44))
        navigationView.backgroundColor = UIColor.clear
        let menuButton = UIButton.init(type: .custom)
        
        menuButton.frame = CGRect(x: ScreenSize.SCREEN_WIDTH - 45, y: 7, width: 25, height: 25)
        menuButton.setImage(UIImage.init(named: "sideMenu_Icon"), for: UIControlState())
        menuButton.addTarget(self, action: #selector(OpenSideMenu), for: .touchUpInside)
        navigationView.addSubview(menuButton)

        
        let titleLabel = UILabel.init(frame:CGRect(x: 60,y: 0,width: ( (navigationViewFrame.size.width) - 110),height: 40))
        titleLabel.textColor = UIColor.black
        titleLabel.text = "SERVICE & SUPPORT"
        titleLabel.font = UIFont.init(name:"OpenSans-SemiBold", size: 15)// UIFont(name:"OpenSans-SemiBold", size: 15)!
        titleLabel.textAlignment = .center
        navigationView.addSubview(titleLabel)
        
        let logoImage = UIImageView.init(frame: CGRect(x: 25, y: 6, width: 33, height: 33))
        logoImage.image = UIImage.init(named: "nikon_login_logo")
        navigationView.addSubview(logoImage)
        
        let backButton = UIButton.init(type: .custom)
        backButton.frame = CGRect(x: 0, y: 9, width: 20, height: 20)
        backButton.backgroundColor = UIColor.white
        backButton.setImage(UIImage.init(named: "back"), for: UIControlState())
        backButton.addTarget(self, action: #selector(PopView), for: .touchUpInside)
        navigationView.addSubview(backButton)
        
        
        self.navigationItem.titleView = navigationView
        
    }

    //MARK:- COllection View
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        
            return CGSize(width: collectionView.bounds.size.width ,height: 170)
      
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return serviceArray.count
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ServiceCollectionCell", for: indexPath) as! ServiceCollectionCell
        cell.serviceImageView.sd_setImage(with: URL.init(string: self.serviceArray[indexPath.row]), placeholderImage: UIImage.init(named: "product_placeholder.jpg"))
        cell.serviceImageView.setIndicatorStyle(.gray)
        cell.serviceImageView.setShowActivityIndicator(true)
        cell.findOutBtn.tag = indexPath.row
        cell.findOutBtn.addTarget(self, action: #selector(findOutMoreServiceClicked), for: .touchUpInside)
            return cell
            
           }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
           }

    
    //MARK:- button action
    func findOutMoreServiceClicked(_ sender : UIButton)  {
        
        let destViewController = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "ServiceAndSupportScrollVC") as! ServiceAndSupportScrollVC
        if sender.tag == 0 {
            destViewController.selectedIndex = 5
        }
        if sender.tag == 1 {
            destViewController.selectedIndex = 0
        }
        if sender.tag == 2 {
            destViewController.selectedIndex = 4
        }
        if sender.tag == 3 {
            destViewController.selectedIndex = 2
        }
        self.navigationController?.pushViewController(destViewController, animated: true)

        
    }
    
    @IBAction func serviceAdvisoryButtonClicked(_ sender: AnyObject) {
        let destViewController = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "ServiceAndSupportScrollVC") as! ServiceAndSupportScrollVC
        destViewController.selectedIndex = 3
        self.navigationController?.pushViewController(destViewController, animated: true)
        
    }
    @IBAction func supportFindOutMoreButtonClicked(_ sender: AnyObject) {
        
         let destViewController = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "ServiceAndSupportScrollVC") as! ServiceAndSupportScrollVC
         destViewController.selectedIndex = 1
        self.navigationController?.pushViewController(destViewController, animated: true)
        
    }
    //MARK: - AUTO SCROLLING
    func addTimer()
    {
        timer = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(nextPage), userInfo: nil, repeats: true)
        RunLoop.current.add(timer, forMode: RunLoopMode.commonModes)
    }
    
    func nextPage()
    {
        var ar = serviceCollection.indexPathsForVisibleItems
        let MaxSections = self.serviceArray.count
        
        var currentIndexPathReset = ar[0]
        if(currentIndexPathReset.item >= MaxSections - 1)
        {
            currentIndexPathReset = IndexPath(item:0, section:0)
            servicePageControl.currentPage = 0
            serviceCollection.scrollToItem(at: currentIndexPathReset, at: UICollectionViewScrollPosition.left, animated: false)
            
        }
        else
        {
            let newIndex = currentIndexPathReset.item + 1
            servicePageControl.currentPage = newIndex
            
            serviceCollection.scrollToItem(at: IndexPath.init(item: newIndex, section: 0), at: UICollectionViewScrollPosition.left, animated: true)
        }
        
        
        
    }
    
    func removeTimer()
    {
        // stop NSTimer
        timer.invalidate()
    }
    
    // UIScrollView' delegate method
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView)
    {
        removeTimer()
    }
    
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView,
                                  willDecelerate decelerate: Bool)
    {
        addTimer()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
