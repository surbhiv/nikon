//
//  ProductCatalougeCollectionReusableView.swift
//  NikonApp
//
//  Created by Hitesh Dhawan on 15/02/17.
//  Copyright © 2017 Neuronimbus. All rights reserved.
//

import UIKit

class ProductCatalougeCollectionReusableView: UICollectionReusableView {
        
    @IBOutlet weak var productCatalougeName: UILabel!
    @IBOutlet weak var productCatalougeDescription: UILabel!
    
    }
