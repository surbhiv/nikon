//
//  ProductCatalougeCollectionVC.swift
//  NikonApp
//
//  Created by Hitesh Dhawan on 14/02/17.
//  Copyright © 2017 Neuronimbus. All rights reserved.
//

import UIKit

class ProductCatalougeCollectionVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout,UISearchBarDelegate {

    @IBOutlet weak var productSearchBar: UISearchBar!
    @IBOutlet weak var productCatalouge_Collection: UICollectionView!
    var productCatalougeArray = [AnyObject]()
    var productReuse = ProductCatalougeCollectionReusableView()
    var productCatalougeNameString = String()
    var productCatalougeDescriptionString = String()
    var productIdString = String()
    
    // for search
    var filteredArray = [AnyObject]()
    
    var shouldShowSearchResults = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        productSearchBar.showsCancelButton = true
        
        self.navigationController?.isNavigationBarHidden = false
        self.setNavigationBar()
        self.getProductCatalougeData()

    }
    //MARK:- NAVIGATION
    func OpenSideMenu(_ sender: AnyObject){
        
        self.view.endEditing(true)
        toggleSideMenuView()
    }
    func PopView(_ sender: AnyObject){
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func setNavigationBar(){
        self.navigationItem.hidesBackButton = true
        let navigationViewFrame = ScreenSize.SCREEN//self.navigationController?.navigationBar.frame
        let navigationView = UIView.init(frame: CGRect(x: 0, y: 0, width: navigationViewFrame.size.width, height: 44))
        navigationView.backgroundColor = UIColor.clear
        let menuButton = UIButton.init(type: .custom)
        
        menuButton.frame = CGRect(x: ScreenSize.SCREEN_WIDTH - 45, y: 7, width: 25, height: 25)
        menuButton.setImage(UIImage.init(named: "sideMenu_Icon"), for: UIControlState())
        menuButton.addTarget(self, action: #selector(OpenSideMenu), for: .touchUpInside)
        navigationView.addSubview(menuButton)
//        self.navigationController?.navigationBar.tintColor = Colors.appPink
        
        let titleLabel = UILabel.init(frame:CGRect(x: 60,y: 0,width: ( (navigationViewFrame.size.width) - 110),height: 40))
        titleLabel.textColor = UIColor.black
        titleLabel.text = "PRODUCTS"
        titleLabel.font = UIFont.init(name:"OpenSans-SemiBold", size: 15)// UIFont(name:"OpenSans-SemiBold", size: 15)!
        titleLabel.textAlignment = .center
        navigationView.addSubview(titleLabel)
        
        let logoImage = UIImageView.init(frame: CGRect(x: 25, y: 6, width: 33, height: 33))
        logoImage.image = UIImage.init(named: "nikon_login_logo")
        navigationView.addSubview(logoImage)
        
        let backButton = UIButton.init(type: .custom)
        backButton.frame = CGRect(x: 0, y: 9, width: 20, height: 20)
        backButton.backgroundColor = UIColor.white
        backButton.setImage(UIImage.init(named: "back"), for: UIControlState())
        backButton.addTarget(self, action: #selector(PopView), for: .touchUpInside)
        navigationView.addSubview(backButton)
        
        
        self.navigationItem.titleView = navigationView
        
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //MARK:- GETPRODUCT CATALOUGE DATA
    //todo 1 may 2017
    /*
    func getProductCatalougeData(){
        if Reachability.isConnectedToNetwork()
        {
            
        
            let method = "products?category=\(productIdString)"
            
            
            Constants.appDelegate.startIndicator()
            Server.getRequestWithURL_PRODUCTCATALOGUE(Constants.PRODUCTCATALOUGEBASEURL+method, completionHandler: { (response) in
                
               // if response[Constants.STATE] != nil && response[Constants.STATE] as! String == Constants.SUCC {
                    
                    let responseDict = response["products"] as! Dictionary<String,AnyObject>
                if (responseDict["product"] != nil)
                {
                    self.productCatalougeArray = responseDict["product"] as! Array<AnyObject>
                    
                }
                    dispatch_async(dispatch_get_main_queue())
                    {
                        Constants.appDelegate.stopIndicator()
                        self.productCatalouge_Collection .reloadData()
                    }
                //}
                
            })
        }
        else
        {
            Constants.appDelegate.stopIndicator()
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
    }
    */
    
    func getProductCatalougeData(){
        if Reachability.isConnectedToNetwork()
        {
            
            
//            let method = "products?category=\(productIdString)"
            let method = "NindApi?Method=products&Value=\(productIdString)"

            //http://api.nikonschool.in/api/NindApi?Method=products&Value=
            
            Constants.appDelegate.startIndicator()
            Server.getRequestWithURL(Constants.BASEURL+method, completionHandler: { (response) in
                
                Constants.appDelegate.stopIndicator()
                 if response[Constants.STATE] != nil && response[Constants.STATE] as! String == Constants.SUCC {
                
                    let tempDict = response["MethodResult"]
                let result = Helper.convertStringToDictionary(tempDict! as! String)
                
                if (result!["products"] != nil)
                {
                    if (result!["products"]!["product"] != nil)
                    {
                        self.productCatalougeArray = result!["products"]!["product"] as! Array<AnyObject>
                    }
                    
                }
               
               
                DispatchQueue.main.async
                {
                    
                    self.productCatalouge_Collection .reloadData()
                }
                }
                else
                 {
                    
                }
                
            })
        }
        else
        {
            Constants.appDelegate.stopIndicator()
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
    }


    
    //MARK:- COllection View
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: (ScreenSize.SCREEN_WIDTH - 20)/2,height: ((ScreenSize.SCREEN_WIDTH  - 20)/2) + 110)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if shouldShowSearchResults {
            return filteredArray.count
        }
        else
        {
            return productCatalougeArray.count
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        let fon = UIFont.init(name: "OpenSans", size: 12.0)

        return CGSize(width: self.view.frame.width, height: Helper.getHeightForText(productCatalougeDescriptionString, fon: fon!)+50)  // Header size
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "productCatalougeCell", for: indexPath)
        
        let productImage = cell.contentView .viewWithTag(11001) as! UIImageView
        let productName = cell.contentView .viewWithTag(11002) as! UILabel
        let productDisc = cell.contentView .viewWithTag(11003) as! UILabel

        if shouldShowSearchResults
        {
            var imageURL = self.filteredArray[indexPath.row]["overview_image"] as! String
            imageURL = imageURL.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            productImage.sd_setImage(with: URL.init(string: "http://api.nikon-asia.com/\(imageURL)"), placeholderImage: UIImage.init(named: "product_placeholder.jpg"))
            productImage.setShowActivityIndicator(true)
            productImage.setIndicatorStyle(.gray)
            
            productName.text = self.filteredArray[indexPath.row]["formal_title"] as? String
            
            if (self.filteredArray[indexPath.row]["overview"]! != nil){
                productDisc.text = Helper.removeHTMLTagFromString((self.filteredArray[indexPath.row]["overview"] as? String)!)
            }
        }
        else
        {

            var imageURL = self.productCatalougeArray[indexPath.row]["overview_image"] as! String
            imageURL = imageURL.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            productImage.sd_setImage(with: URL.init(string: "http://api.nikon-asia.com/\(imageURL)"), placeholderImage: UIImage.init(named: "product_placeholder.jpg"))
            productImage.setShowActivityIndicator(true)
            productImage.setIndicatorStyle(.gray)
        
            productName.text = self.productCatalougeArray[indexPath.row]["formal_title"] as? String
        
            if (self.productCatalougeArray[indexPath.row]["overview"]! != nil){
                productDisc.text = Helper.removeHTMLTagFromString((self.productCatalougeArray[indexPath.row]["overview"] as? String)!)
            }
        }
//        let viewDetailButton = cell.contentView .viewWithTag(11004) as? UIButton
//        viewDetailButton?.tag = indexPath.row
//        viewDetailButton?.addTarget(self, action: #selector(viewDetailButtonClicked), forControlEvents:.TouchUpInside)
//        
    
       
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       
        productSearchBar.resignFirstResponder()
        let desination = Constants.mainStoryboard.instantiateViewController(withIdentifier: "ProductCatalougeDetailVCViewController") as! ProductCatalougeDetailVCViewController
        var productId = NSNumber()
        if shouldShowSearchResults
        {
            productId = self.filteredArray[indexPath.row]["id"] as! NSNumber
            
        }
        else
        {
            productId = self.productCatalougeArray[indexPath.row]["id"] as! NSNumber
        }

        let str = "\(productId)"

        desination.productId = str//String(self.productCatalougeArray[indexPath.row]["id"] as! Int)
        
        self.navigationController?.pushViewController(desination, animated: true)
        

    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
         var reusableView = UICollectionReusableView()
        if kind == UICollectionElementKindSectionHeader
        {
            
            let hview = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "ProductCatalougeCollectionReusableView", for: indexPath) as! ProductCatalougeCollectionReusableView
            //hview.backgroundColor = UIColor.redColor()
            hview.productCatalougeName.text = productCatalougeNameString
            hview.productCatalougeDescription.text = productCatalougeDescriptionString
            

            reusableView = hview
        }
        return reusableView
    }
    
    
    
    //MARK:-BUTTON ACTION
    func viewDetailButtonClicked(_ sender: UIButton){
        let index =  sender.tag
        
        let desination = Constants.mainStoryboard.instantiateViewController(withIdentifier: "ProductCatalougeDetailVCViewController") as! ProductCatalougeDetailVCViewController
        desination.productId = String(self.productCatalougeArray[index]["id"] as! Int)
        
        self.navigationController?.pushViewController(desination, animated: true)
        
        
    }
    
   //MARK:- Search bar delegate
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        
        //searchBar.showsCancelButton = true
        //productCatalouge_Collection.reloadData()
    }
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        
        searchBar.resignFirstResponder()

    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = ""
        searchBar.resignFirstResponder()
        searchBar.showsCancelButton = false
        shouldShowSearchResults = false
        productCatalouge_Collection.reloadData()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        shouldShowSearchResults = true

        if searchText == ""
        {
            searchBar.showsCancelButton = false
            searchBar.text = ""
            shouldShowSearchResults = false
        }
        else
        {
            searchBar.showsCancelButton = true
            let pre = NSPredicate(format: "SELF['formal_title'] contains[c] %@", searchText)
            filteredArray = (self.productCatalougeArray as NSArray).filtered(using: pre) as [AnyObject]
            shouldShowSearchResults = true
        }
        productCatalouge_Collection.reloadData()
    }
    
    @IBAction func tapClick(_ sender: AnyObject) {

        productSearchBar.resignFirstResponder()
    }
}
