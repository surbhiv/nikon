//
//  ProductImageZoomVC.swift
//  NikonApp
//
//  Created by Hitesh Dhawan on 05/04/17.
//  Copyright © 2017 Neuronimbus. All rights reserved.
//

import UIKit

class ProductImageZoomVC: UIViewController {

    var imageURLStr = String()
    
    @IBOutlet weak var productScroll: UIScrollView!
    @IBOutlet weak var productImageView: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        productScroll.minimumZoomScale = 1.0
        productScroll.maximumZoomScale = 5.0
        productScroll.alwaysBounceVertical = false
        productScroll.alwaysBounceHorizontal = false
        productScroll.showsVerticalScrollIndicator = true
        productScroll.flashScrollIndicators()
        
        productImageView.sd_setImage(with: URL.init(string: imageURLStr))
    }

    func viewForZoomingInScrollView(_ scrollView: UIScrollView) -> UIView?
    {
        return productImageView
    }
    @IBAction func crossBtnClick(_ sender: AnyObject)
    {
        self.view .removeFromSuperview()
        self .removeFromParentViewController()

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
