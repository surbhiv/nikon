//
//  ProductCatalougeVC.swift
//  NikonApp
//
//  Created by Hitesh Dhawan on 09/02/17.
//  Copyright © 2017 Neuronimbus. All rights reserved.
//

import UIKit


class ProductCatalougeVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    //MARK:- IBOUTLET
    @IBOutlet weak var productCatalougeTableView: UITableView!
    
    //MARK:- VAR DECLRATION
    
    var dataArray = [AnyObject]()
    var isFromMenu = Bool()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationController?.isNavigationBarHidden = false
        self.setNavigationBar()
        self.getProductCatalougeData()
    }
    
    //MARK:- NAVIGATION
    func OpenSideMenu(_ sender: AnyObject){
        
        self.view.endEditing(true)
        toggleSideMenuView()
    }
    func PopView(_ sender: AnyObject){
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func setNavigationBar(){
        self.navigationItem.hidesBackButton = true
        let navigationViewFrame = ScreenSize.SCREEN//self.navigationController?.navigationBar.frame
        let navigationView = UIView.init(frame: CGRect(x: 0, y: 0, width: navigationViewFrame.size.width, height: 44))
        navigationView.backgroundColor = UIColor.clear
        let menuButton = UIButton.init(type: .custom)
        
        menuButton.frame = CGRect(x: ScreenSize.SCREEN_WIDTH - 45, y: 7, width: 25, height: 25)
        menuButton.setImage(UIImage.init(named: "sideMenu_Icon"), for: UIControlState())
        menuButton.addTarget(self, action: #selector(OpenSideMenu), for: .touchUpInside)
        navigationView.addSubview(menuButton)
//        self.navigationController?.navigationBar.tintColor = Colors.appPink
        
        let titleLabel = UILabel.init(frame:CGRect(x: 60,y: 0,width: ( (navigationViewFrame.size.width) - 110),height: 40))
        titleLabel.textColor = UIColor.black
        titleLabel.text = "PRODUCT CATALOGUE"
        titleLabel.font = UIFont.init(name:"OpenSans-SemiBold", size: 15)// UIFont(name:"OpenSans-SemiBold", size: 15)!
        titleLabel.textAlignment = .center
        navigationView.addSubview(titleLabel)
        
        let logoImage = UIImageView.init(frame: CGRect(x: 25, y: 6, width: 33, height: 33))
        logoImage.image = UIImage.init(named: "nikon_login_logo")
        navigationView.addSubview(logoImage)
        
        if isFromMenu == false
        {
            let backButton = UIButton.init(type: .custom)
            backButton.frame = CGRect(x: 0, y: 9, width: 20, height: 20)
            backButton.backgroundColor = UIColor.white
            backButton.setImage(UIImage.init(named: "back"), for: UIControlState())
            backButton.addTarget(self, action: #selector(PopView), for: .touchUpInside)
            navigationView.addSubview(backButton)
        }
        
        self.navigationItem.titleView = navigationView
        
    }
    //MARK:- GETPRODUCT DATA
    func getProductCatalougeData(){
        if Reachability.isConnectedToNetwork()
        {
            
            
            let method = "GetProduct"
            
            
            Constants.appDelegate.startIndicator()
            Server.getRequestWithURL(Constants.BASEURL+method, completionHandler: { (response) in
                
                if response[Constants.STATE] != nil && response[Constants.STATE] as! String == Constants.SUCC {
                    
                    self.dataArray = response["Productlist"] as! Array<AnyObject>
                    DispatchQueue.main.async
                    {
                        Constants.appDelegate.stopIndicator()
                        self.productCatalougeTableView .reloadData()
                    }
                }
                else {
                    Constants.appDelegate.stopIndicator()
                    
                    var mess = String()
                    if response[Constants.MESS] != nil{
                        mess = response[Constants.MESS] as! String
                    }
                    else{
                        mess = response["Message"] as! String
                    }
                    DispatchQueue.main.async
                    {
                    let alert = UIAlertController.init(title: "Nikon", message: mess, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                        self.dismiss(animated: true, completion: nil)
                    }))
                    self.present(alert, animated: true, completion: nil)
                    }
                }
            })
        }
        else
        {
            Constants.appDelegate.stopIndicator()
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
    }


    //MARK:- TABLEVIEW DELEGATE
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return dataArray.count
        
    }
    
    
     //MARK:- TABLEVIEW Datasource
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProductCatalougeCell", for: indexPath)
        
        // get image 
        let productImage = cell.contentView .viewWithTag(10001) as! UIImageView
       // productImage.backgroundColor = UIColor.redColor()
        productImage.image = nil
         DispatchQueue.main.async{
            var imageURL = self.dataArray[indexPath.row]["AppCustomImage"] as! String
            imageURL = imageURL.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            //productImage.sd_setImageWithURL(NSURL.init(string: self.dataArray[indexPath.row]["AppCustomImage"] as! String))
            productImage.sd_setImage(with: URL.init(string: self.dataArray[indexPath.row]["AppCustomImage"] as! String), placeholderImage: UIImage.init(named: "product_placeholder.jpg"))
            productImage.setShowActivityIndicator(true)
            productImage.setIndicatorStyle(.gray)
        }
        tableView.estimatedRowHeight = 300
        tableView.rowHeight = UITableViewAutomaticDimension
        cell.selectionStyle = .none

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
         let desination = Constants.mainStoryboard.instantiateViewController(withIdentifier: "ProductCatalougeCollectionVC") as! ProductCatalougeCollectionVC
        desination.productCatalougeNameString = self.dataArray[indexPath.row]["Name"] as! String
        desination.productCatalougeDescriptionString = self.dataArray[indexPath.row]["Description"] as! String
        desination.productIdString = self.dataArray[indexPath.row]["id"] as! String
        self.navigationController?.pushViewController(desination, animated: true)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
