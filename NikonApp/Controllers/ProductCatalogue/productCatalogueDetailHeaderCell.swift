//
//  productCatalogueDetailHeaderCell.swift
//  NikonApp
//
//  Created by Hitesh Dhawan on 16/02/17.
//  Copyright © 2017 Neuronimbus. All rights reserved.
//

import UIKit

class productCatalogueDetailHeaderCell: UITableViewCell {

    //MARK: OUTLETS
    
    @IBOutlet weak var titleLabelButtonWidth: NSLayoutConstraint!
    @IBOutlet weak var titleLabelBtn: UIButton!
    @IBOutlet weak var productimage: UIImageView!
    @IBOutlet weak var productImageHeight: NSLayoutConstraint!
   

    @IBOutlet weak var productImageCollection: UICollectionView!
    
    @IBOutlet weak var productColorCollection: UICollectionView!
    @IBOutlet weak var productColorCollectionHeight: NSLayoutConstraint!
    
    @IBOutlet weak var productDetailTextView: UITextView!
     @IBOutlet weak var productImageCollectionHeight: NSLayoutConstraint!
    @IBOutlet weak var productDetailTextViewHeight: NSLayoutConstraint!
    
    //MARKS: START
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
