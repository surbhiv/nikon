//
//  ProductCatalougeDetailVCViewController.swift
//  NikonApp
//
//  Created by Hitesh Dhawan on 15/02/17.
//  Copyright © 2017 Neuronimbus. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class ProductCatalougeDetailVCViewController: UIViewController ,  UITableViewDataSource, UITableViewDelegate,UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var productTable: UITableView!
    @IBOutlet var imageTapGesture: UITapGestureRecognizer!
    
    var collectionViewForProductImages: UICollectionView!
    var colorCollectionView: UICollectionView!

    var titleLabel = UILabel.init()
    var selectedIndex = Int()
    
    var productId = String()
    var productlargeImageStr = String()
    var productOverviewStr = String()
    var responseDictionary = [String:Any]()
    var otherArray = ["STORE LOCATOR","REGISTER THIS PRODUCT","GET SUPPORT","CAMERA & LENS COMPATIBILITY","OVERVIEW","TECH SPECS"]
    fileprivate var expandedSections = NSMutableIndexSet()
    var productCatalougeDetailArray = [AnyObject]()
    var productSampleImageArray = [AnyObject]()
    var productColorImageArray = [AnyObject]()
    var productSpecificationArray = [AnyObject]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationController?.isNavigationBarHidden = false
        //imageTapGesture.addTarget(self, action: #selector(ImageTapped))
        self.setNavigationBar()
        self.getProductCatalougeDetailData()
    }
    @IBAction func imageTapped(_ sender: AnyObject) {
        let topview = self.parent
        let  destVC  = Constants.mainStoryboard.instantiateViewController(withIdentifier: "ProductImageZoomVC")as! ProductImageZoomVC
        destVC.imageURLStr = productlargeImageStr
        topview!.addChildViewController(destVC)
        destVC.didMove(toParentViewController: topview)
        topview?.view.addSubview(destVC.view)
    }
//    func ImageTapped(tapGestureRecognizer: UITapGestureRecognizer)  {
//        let topview = self.parentViewController
//        let  destVC  = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("ProductImageZoomVC")as! ProductImageZoomVC
//        topview!.addChildViewController(destVC)
//        destVC.didMoveToParentViewController(topview)
//        topview?.view.addSubview(destVC.view)
//
//
//    }
    //MARK:- NAVIGATION
    func OpenSideMenu(_ sender: AnyObject){
        
        self.view.endEditing(true)
        toggleSideMenuView()
    }
    func PopView(_ sender: AnyObject){
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func setNavigationBar(){
        self.navigationItem.hidesBackButton = true
        let navigationViewFrame = ScreenSize.SCREEN//self.navigationController?.navigationBar.frame
        let navigationView = UIView.init(frame: CGRect(x: 0, y: 0, width: navigationViewFrame.size.width, height: 44))
        navigationView.backgroundColor = UIColor.clear
        let menuButton = UIButton.init(type: .custom)
        
        menuButton.frame = CGRect(x: ScreenSize.SCREEN_WIDTH - 45, y: 7, width: 25, height: 25)
        menuButton.setImage(UIImage.init(named: "sideMenu_Icon"), for: UIControlState())
        menuButton.addTarget(self, action: #selector(OpenSideMenu), for: .touchUpInside)
        navigationView.addSubview(menuButton)
//        self.navigationController?.navigationBar.tintColor = Colors.appPink
        
        titleLabel = UILabel.init(frame:CGRect(x: 60,y: 0,width: ( (navigationViewFrame.size.width) - 110),height: 44))
        titleLabel.textColor = UIColor.black
        titleLabel.text = "PRODUCT"
        titleLabel.font = UIFont.init(name:"OpenSans-SemiBold", size: 15)// UIFont(name:"OpenSans-SemiBold", size: 15)!
        titleLabel.textAlignment = .center
        navigationView.addSubview(titleLabel)
        
        let logoImage = UIImageView.init(frame: CGRect(x: 25, y: 6, width: 33, height: 33))
        logoImage.image = UIImage.init(named: "nikon_login_logo")
        navigationView.addSubview(logoImage)
        
        let backButton = UIButton.init(type: .custom)
        backButton.frame = CGRect(x: 0, y: 9, width: 20, height: 20)
        backButton.backgroundColor = UIColor.white
        backButton.setImage(UIImage.init(named: "back"), for: UIControlState())
        backButton.addTarget(self, action: #selector(PopView), for: .touchUpInside)
        navigationView.addSubview(backButton)
        self.navigationItem.titleView = navigationView
    }

    
    //MARK:- GETPRODUCT CATALOUGE DATA
    /*
    func getProductCatalougeDetailData(){
        if Reachability.isConnectedToNetwork()
        {
            let method = "product?id=\(productId)"
            Constants.appDelegate.startIndicator()
            Server.getRequestWithURL_PRODUCTCATALOGUE(Constants.PRODUCTCATALOUGEBASEURL+method, completionHandler: { (response) in
                
                Constants.appDelegate.stopIndicator()

                let responseDict = response["products"] as! Dictionary<String,AnyObject>
                self.responseDictionary = responseDict
                self.productCatalougeDetailArray .append(self.responseDictionary["product"]!["product_images"]!!)
                if self.responseDictionary["product"]!["product_images"]!!["product_view"] != nil
                {
//                    let productSampleImageArray1 = self.responseDictionary["product"]!["product_images"]!!["product_view"] as? NSArray
//                    if productSampleImageArray1 != nil{
//                        self.productlargeImageStr = "http://api.nikon-asia.com/\(productSampleImageArray1![0]["image_large"] as! String)"
//                    }
                    
                    if ((self.responseDictionary["product"]!["product_images"]!?.isKindOfClass(NSDictionary)) == true) {
                        if (self.responseDictionary["product"]!["product_images"]!!["product_view"]!?.isKindOfClass(NSArray) == true)
                        {
                            let productSampleImageArray1 = self.responseDictionary["product"]!["product_images"]!!["product_view"] as! NSArray
                            self.productSampleImageArray = productSampleImageArray1 as [AnyObject]
                            self.productlargeImageStr = "http://api.nikon-asia.com/\(self.productSampleImageArray[0]["image_large"] as! String)"
                            
                        }
                        if (self.responseDictionary["product"]!["product_images"]!!["product_view"]!?.isKindOfClass(NSDictionary) == true)
                        {
                            let newDict = self.responseDictionary["product"]!["product_images"]!!["product_view"]! as! NSDictionary
                            self.productSampleImageArray.append(newDict)
                             self.productlargeImageStr = "http://api.nikon-asia.com/\(self.productSampleImageArray[0]["image_large"] as! String)"
                        }
                        
                    }

                    
                }
                if self.responseDictionary["product"]!["overview"]! != nil
                {
                    self.productOverviewStr = self.responseDictionary["product"]!["overview"] as! String
                }
                else
                {
                        self.productOverviewStr = ""
                }
                if ((self.responseDictionary["product"]!["specifications"]!?.isKindOfClass(NSDictionary)) == true) {
                    if (self.responseDictionary["product"]!["specifications"]!!["specification"]!?.isKindOfClass(NSArray) == true)
                    {
                        let specificationArray = self.responseDictionary["product"]!["specifications"]!!["specification"] as! NSArray
                                            if specificationArray.count > 0
                                            {
                                                self.productSpecificationArray = specificationArray as [AnyObject]
                                            }
                    }
                }
//                if self.responseDictionary["product"]!["specifications"]!!["specifications"]! != nil
//                {
//                    let specificationArray = self.responseDictionary["product"]!["specifications"]!!["specifications"] as! NSArray
//                    if specificationArray.count > 0
//                    {
//                        self.productSpecificationArray = specificationArray as [AnyObject]
//                    }
//                }
                
                dispatch_async(dispatch_get_main_queue())
                {
                    self.productTable.reloadData()
                }
            })
        }
        else
        {
            Constants.appDelegate.stopIndicator()
            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
    }
    */
    
    func getProductCatalougeDetailData(){
        if Reachability.isConnectedToNetwork()
        {
            //let method = "product?id=\(productId)"
            let method = "NindApi?Method=productdetails&Value=\(productId)"
            Constants.appDelegate.startIndicator()
            Server.getRequestWithURL(Constants.BASEURL+method, completionHandler: { (response) in
                
                Constants.appDelegate.stopIndicator()
                if response[Constants.STATE] != nil && response[Constants.STATE] as! String == Constants.SUCC
                {
                    let tempDict = response["MethodResult"]
                    let result = Helper.convertStringToDictionary(tempDict! as! String)

                    guard   let responseDict = result!["products"] as? [String:Any] else {
                        return
                    }
                    self.responseDictionary = responseDict
                    
                    guard let productCatlogArr =  responseDict["product"] as? [String:Any] else {
                        return
                    }
                    guard let prodCatlogDetailArr = productCatlogArr["product_images"] as? [String:Any] else{
                        return
                    }

                    self.productCatalougeDetailArray.append(prodCatlogDetailArr as AnyObject)
                    if prodCatlogDetailArr["product_view"] != nil
                    {
                        let productSampleImageArray1 = prodCatlogDetailArr["product_view"] as? [[String:Any]]
                   if productSampleImageArray1 != nil{
                    self.productlargeImageStr = "http://api.nikon-asia.com/\(productSampleImageArray1?.first!["image_large"] as! String)"
                   }

                        if let productSampleImageArray1 = (prodCatlogDetailArr["product_view"] as? [Any])
                        {
                self.productSampleImageArray = productSampleImageArray1 as [AnyObject]
                            self.productlargeImageStr = "http://api.nikon-asia.com/\(self.productSampleImageArray.first!["image_large"] as! String)"

                        }
                    
                    
                        if  let newDict = (prodCatlogDetailArr["product_view"] as? [String:Any])
                        {
                            self.productSampleImageArray.append(newDict as AnyObject)
                            self.productlargeImageStr = "http://api.nikon-asia.com/\(self.productSampleImageArray.first!["image_large"] as! String)"
                        }

                }   ////// prod  cat image
                
                 self.productOverviewStr = productCatlogArr["overview"] as? String ?? ""
               
                    
                if let prodSpecDict = productCatlogArr["specifications"] as? [String:Any] {
                    
                    if let specificationArray =  prodSpecDict["specification"] as? [Any]
                    {
                        if specificationArray.count > 0
                        {
                            self.productSpecificationArray = specificationArray as [AnyObject]
                        }
                    }
                }
                    
                    DispatchQueue.main.async
                    {
                         self.productTable.reloadData()
                    }
                    DispatchQueue.main.async
                    {
                        self.perform(#selector(self.reloadTableData), with: nil, afterDelay: 0.5)
                    }

            }
                else
                {

                }
            })
        }
        else
        {
            Constants.appDelegate.stopIndicator()
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
    }

    //MARK:- reload tabledata
    func reloadTableData()  {
        
            self.productTable.reloadData()
       
    }
    //MARK:- COllection View
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collectionViewForProductImages {
            
            return productSampleImageArray.count
        }
        else {
            
            return productColorImageArray.count
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == collectionViewForProductImages {
            
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "productCatalougeImagesDetail", for: indexPath)
        
        let productSmallImage = cell.contentView.viewWithTag(12011) as! UIImageView
        var newURL = self.productSampleImageArray[indexPath.row]["image_large"] as! String
        newURL = newURL.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        productSmallImage.sd_setImage(with: URL.init(string: "http://api.nikon-asia.com/\(newURL)"), placeholderImage: UIImage.init(named: "product_placeholder.jpg"))
            productSmallImage.setShowActivityIndicator(true)
            productSmallImage.setIndicatorStyle(.gray)
            if selectedIndex == indexPath.row {
                productSmallImage.layer.borderWidth = 1
                productSmallImage.layer.borderColor = UIColor.black.cgColor
            }
            else
            {
                productSmallImage.layer.borderWidth = 0
            }
        return cell
        }
        else
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "productColor", for: indexPath)
            let productColorImage = cell.contentView .viewWithTag(12002) as! UIImageView
            productColorImage.sd_setImage(with: URL.init(string: "http://api.nikon-asia.com/\(self.productColorImageArray[indexPath.row]["image_swatch"] as! String)"))
            productColorImage.layer.borderWidth = 0.4
            productColorImage.layer.borderColor = UIColor.lightGray.cgColor
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == collectionViewForProductImages {
            self.productlargeImageStr = "http://api.nikon-asia.com/\(self.productSampleImageArray[indexPath.row]["image_large"] as! String)"
        
            self.selectedIndex = indexPath.row
        }
        else{

            let imageStr = self.productColorImageArray[indexPath.row]["image_small"] as! String
            var index = Int()
            index = 0
            for dict in self.productSampleImageArray {
             index = index+1
            if imageStr == dict["image_small"] as! String {
                break
            }
        }
        
        collectionViewForProductImages.scrollToItem(at: IndexPath.init(item: index-1, section: 0), at:.left , animated: false)
        self.productlargeImageStr = "http://api.nikon-asia.com/\(self.productSampleImageArray[index-1]["image_large"] as! String)"
       selectedIndex = index-1
        }
        DispatchQueue.main.async
        {
            self.productTable.reloadData()
        }
    }
 
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //MARK: TABLEVIEW
    func numberOfSections(in tableView: UITableView) -> Int {
        if (self.productCatalougeDetailArray.count > 0) {
            return self.productCatalougeDetailArray.count+6
        }
        else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 6 {
            if expandedSections.contains(section) {
               return self.productSpecificationArray.count + 1
            }
            return 1
        }
        if section == 5{
            if expandedSections.contains(section) {
                return 2
            }
            return 1
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell =  tableView.dequeueReusableCell(withIdentifier: "productCatalogueDetailHeaderCell", for: indexPath) as! productCatalogueDetailHeaderCell
            //UNCOMMENT
            if let titleDict = (self.responseDictionary["product"] as? [String:Any]), let titleStr = titleDict["formal_title"] as? String  {
                titleLabel.text = titleStr
                cell.titleLabelBtn.setTitle(titleStr, for: UIControlState())

            }
            
            var productColorAray = NSArray()
            if let product = (self.responseDictionary["product"] as? [String:Any]) , let colors = product["colors"] as? [String:Any], let varient = colors["variant"] as? [Any] {
                
                productColorAray = varient as NSArray
                if (productColorAray.count > 0)
            {
                self.productColorImageArray = productColorAray as [AnyObject]
            }
            else{
                cell.productColorCollectionHeight.constant = 0
            }
            }else{
                cell.productColorCollectionHeight.constant = 0

            }

            
          
            let productPriceStr = (self.responseDictionary["product"] as? [String:Any])?["price"] as? String ?? ""
            // self.responseDictionary["product"]!["price"]!! as! String

            if productPriceStr == ""
            {
                cell.productDetailTextViewHeight.constant = 0
            }
            
            
            cell.productimage.image = nil
            
            if self.productlargeImageStr == ""{
                 cell.productImageHeight.constant = 0
                 cell.productImageCollectionHeight.constant = 0
                 tableView.estimatedRowHeight = 50
                 tableView.rowHeight = UITableViewAutomaticDimension
                
            }

            
             self.productlargeImageStr = self.productlargeImageStr.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
                cell.productimage.sd_setImage(with: URL.init(string: self.productlargeImageStr), placeholderImage: UIImage.init(named: "product_placeholder.jpg"))
                cell.productimage.setShowActivityIndicator(true)
                cell.productimage.setIndicatorStyle(.gray)
            do
            {
                let atributedStr = try NSAttributedString.init(data: productPriceStr.data(using: String.Encoding.unicode)!, options: [ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType ], documentAttributes: nil)
                cell.productDetailTextView.attributedText = atributedStr
            }
            catch
            {
                print("error occure")
                
            }

            
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped))
            
            
            cell.productimage.addGestureRecognizer(tapGestureRecognizer)
            
            
            DispatchQueue.main.async{
                self.collectionViewForProductImages = cell.productImageCollection
                self.collectionViewForProductImages.delegate = self
                self.collectionViewForProductImages.dataSource = self
                self.collectionViewForProductImages.reloadData()
                self.view.bringSubview(toFront: self.collectionViewForProductImages)
                
                self.colorCollectionView = cell.productColorCollection
                self.colorCollectionView.delegate = self
                self.colorCollectionView.dataSource = self
                self.colorCollectionView.reloadData()
                self.view.bringSubview(toFront: self.colorCollectionView)
            }
            
            tableView.estimatedRowHeight = 430
            tableView.rowHeight = UITableViewAutomaticDimension

           
            return cell
        }
        else
        {
            let cell =  tableView.dequeueReusableCell(withIdentifier: "productHeadingCell", for: indexPath)
            if indexPath.row == 0 {
                
                let titleLabel = cell.contentView.viewWithTag(121202) as! UILabel
                let image = cell.contentView.viewWithTag(121203) as! UIImageView
                titleLabel.text = self.otherArray[indexPath.section-1]
                image.image = UIImage.init(named: "plus")
                
                if expandedSections.contains(indexPath.section) {
                    image.image = UIImage.init(named: "minus")
                }
                
                tableView.estimatedRowHeight = 50
                tableView.rowHeight = UITableViewAutomaticDimension

            return cell
            }
            else
            {
                if indexPath.section == 6 {
                    
                    let cell =  tableView.dequeueReusableCell(withIdentifier: "productCatalougeSpecification", for: indexPath)
                        let nameLabel = cell.contentView.viewWithTag(12301) as! UILabel
                        nameLabel.text = self.productSpecificationArray[indexPath.row-1]["name"] as? String
                        
                        let discriptionLabel = cell.contentView.viewWithTag(12302) as! UILabel
                    var description = self.productSpecificationArray[indexPath.row-1]["description"]! as! String
                    description = description.replacingOccurrences(of: "Â°", with: "°")
                       // discriptionLabel.text = String(description!)
                    
                    do {
                        let str = try NSAttributedString(data: description.data(using: String.Encoding.unicode, allowLossyConversion: true)!, options: [NSDocumentTypeDocumentAttribute : NSHTMLTextDocumentType], documentAttributes: nil)
                        discriptionLabel.attributedText = str
                        
                        
                    } catch {
                        print(error)
                    }

                    
                    tableView.estimatedRowHeight = 50
                    tableView.rowHeight = UITableViewAutomaticDimension

                    return cell;
                    
                    }
                if indexPath.section == 5 {
                    
                    let cell =  tableView.dequeueReusableCell(withIdentifier: "productDetailCell", for: indexPath)
                    let overViewText = cell.contentView.viewWithTag(121201) as! UITextView
                    overViewText.allowsEditingTextAttributes = false
                    do
                    {
                        let atributedStr = try NSAttributedString.init(data: self.productOverviewStr.data(using: String.Encoding.unicode)!, options: [ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType ], documentAttributes: nil)
                        overViewText.attributedText = atributedStr
                    }
                    catch
                    {
                        print("error occure")
                        
                    }

                    cell.contentView.addSubview(overViewText)
                    
                    tableView.estimatedRowHeight = 50
                    tableView.rowHeight = UITableViewAutomaticDimension
                    return cell;
                }
            }
        return cell
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 5 || indexPath.section == 6 {
            let currentlyExpanded : Bool = expandedSections.contains(indexPath.section)
            if currentlyExpanded == true {
                expandedSections.remove(indexPath.section)
                UIView.animate(withDuration: 0.5, animations: {
                    self.productTable.reloadSections(IndexSet.init(integer: indexPath.section), with: UITableViewRowAnimation.automatic)
                    
                    self.productTable.scrollToRow(at: IndexPath.init(row: 0, section: indexPath.section), at: UITableViewScrollPosition.top, animated: false)
                })
            }
            else {
                var otherindex = Int()
                
                if expandedSections.count > 0 {
                    otherindex = expandedSections.firstIndex
                    expandedSections.remove(otherindex)
                    self.productTable.reloadSections(IndexSet.init(integer: otherindex), with: UITableViewRowAnimation.automatic)
                }
                expandedSections.add(indexPath.section)
                UIView.animate(withDuration: 0.5, animations: {
                    self.productTable.reloadSections(IndexSet.init(integersIn: NSRange.init(location: 5, length: 2).toRange() ?? 0..<0), with: UITableViewRowAnimation.none)
                    self.productTable.scrollToRow(at: IndexPath.init(row: indexPath.row, section: indexPath.section), at: UITableViewScrollPosition.top, animated: false)

                })
            }
        }
        else{
            
            if  indexPath.section == 0{
                return
            }
            if indexPath.section == 1
            {
                let destViewController = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "StoreLocatorVC")
                self.navigationController?.pushViewController(destViewController, animated: true)
                
            }
            else
            {
            let desination = Constants.mainStoryboard.instantiateViewController(withIdentifier: "ProductCalalougeWebview") as! ProductCalalougeWebview
            if indexPath.section == 2 {
                desination.titleString = "PRODUCT REGISTER"
                desination.URLString = "http://www.nikon.co.in/en_IN/about/contact_us?"

            }
            if indexPath.section == 3 {
                desination.titleString = "GET SUPPORT"
                desination.URLString = "http://www.nikon.co.in/en_IN/service/customer_support?"
                
            }
            if indexPath.section == 4 {
                desination.titleString = "COMPATIBILITY"
                desination.URLString = "http://www.nikon.co.in/na/NSG_home?ctry=SG/lang=en_SG"
                
            }
            
            
            self.navigationController?.pushViewController(desination, animated: true)
            }

        }
    }
    
    
    @IBAction func leftArrowClicked(_ sender: AnyObject) {
        if (selectedIndex <= self.productSampleImageArray.count-1) && (selectedIndex > 0) {
            
            self.collectionViewForProductImages.scrollToItem(at: IndexPath.init(item: selectedIndex, section: 0), at:.right , animated: false)
            self.productlargeImageStr = "http://api.nikon-asia.com/\(self.productSampleImageArray[selectedIndex-1]["image_large"] as! String)"
            selectedIndex = selectedIndex-1
            
            DispatchQueue.main.async
            {
                self.productTable.reloadData()
            }
        }
    }
    @IBAction func rightArrowClick(_ sender: AnyObject) {
        
        if selectedIndex < self.productSampleImageArray.count-1 {
       
    self.collectionViewForProductImages.scrollToItem(at: IndexPath.init(item: selectedIndex, section: 0), at:.left , animated: false)
        self.productlargeImageStr = "http://api.nikon-asia.com/\(self.productSampleImageArray[selectedIndex+1]["image_large"] as! String)"
            
        selectedIndex = selectedIndex+1

    DispatchQueue.main.async
    {
    self.productTable.reloadData()
    }
        }
      
    }
    
}
