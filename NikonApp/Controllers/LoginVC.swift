//
//  ViewController.swift
//  NikonApp
//
//  Created by Surbhi on 14/12/16.
//  Copyright © 2016 Neuronimbus. All rights reserved.
//

import UIKit
import GoogleSignIn
import Firebase
import FBSDKCoreKit
import FBSDKLoginKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func >= <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l >= r
  default:
    return !(lhs < rhs)
  }
}



class LoginVC: UIViewController , UIScrollViewDelegate,GIDSignInDelegate,GIDSignInUIDelegate,sendEmailId{
	func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
		
	}



    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var headerTextLabel: UILabel!

    @IBOutlet weak var headerScroll: UIScrollView!
    @IBOutlet weak var detailScroll: UIScrollView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var widthConstraint: NSLayoutConstraint!
    @IBOutlet weak var mainContentViewHeight: NSLayoutConstraint!
    
    
    
    fileprivate var indicatorLabel : UILabel!
    fileprivate var ControllerArray = [AnyObject]()
    fileprivate var headerArray = ["SIGN IN","REGISTER"]
    var signIn = LoginView()
    var signUp = RegisterView()
    // use for social
    var userFirstName = String()
    var userLastName = String()
    var userSocialId = String()
    var userSocialEmail = String()
    var option = String()
    var isWantTodismissVC = Bool()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setHeaderScroll()
        self.setUpScrollView()
        self.hideKeyboardWhenTappedAround()
		mainContentViewHeight.constant = 750
    }
    

    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        sideMenuController()?.sideMenu?.allowPanGesture = false
         sideMenuController()?.sideMenu?.allowLeftSwipe = false
         sideMenuController()?.sideMenu?.allowRightSwipe = false

    }
    override func viewDidDisappear(_ animated: Bool) {
         sideMenuController()?.sideMenu?.allowPanGesture = true
        sideMenuController()?.sideMenu?.allowLeftSwipe = true
        sideMenuController()?.sideMenu?.allowRightSwipe = true
    }
    
    // MARK: - Label ScrollView
    
    func setHeaderScroll() {
        var scrollContentCount = 0;
        var frame : CGRect = CGRect(x: 0, y: 0, width: 0, height: 0)
        var labelx : CGFloat = 0.0
        let width  : CGFloat = 110
        let firstLabelWidth : CGFloat = 110
        
        indicatorLabel = UILabel.init(frame: CGRect(x: labelx, y: 38, width: firstLabelWidth, height: 3))
        indicatorLabel.backgroundColor = Colors.appYello//UIColor.yellowColor()
        indicatorLabel.text = "";
        headerScroll.addSubview(indicatorLabel)
        
        for arrayIndex in 0 ..< headerArray.count {
            frame.origin.x = labelx
            // set scorllview properties
            frame.origin.y = 0;
            frame.size.width=width;
            frame.size.height=39;
            
            let accountLabelButton = UIButton.init(type: UIButtonType.custom)
            accountLabelButton.addTarget(self, action:#selector(HeaderlabelSelected), for: UIControlEvents.touchUpInside)
            
            accountLabelButton.frame = frame;
            accountLabelButton.tag = arrayIndex;
            accountLabelButton.setAttributedTitle(NSAttributedString(string: headerArray[arrayIndex],  attributes: [NSFontAttributeName : UIFont.init(name: "Helvetica-Bold", size: 15.0)!, NSForegroundColorAttributeName : UIColor.white]), for: UIControlState())
            
            if DeviceType.IS_IPHONE_5 || DeviceType.IS_IPHONE_4_OR_LESS {
                accountLabelButton.setAttributedTitle(NSAttributedString(string: headerArray[arrayIndex],  attributes: [NSForegroundColorAttributeName : UIColor.white,NSFontAttributeName : UIFont.init(name: "Helvetica-Bold", size: 14.0)!]), for: UIControlState())
            }
            accountLabelButton.titleLabel?.textAlignment = NSTextAlignment.center
            
            headerScroll.addSubview(accountLabelButton)
            
            if arrayIndex == 0 {
                accountLabelButton.alpha=1.0;
            }
            else {
                accountLabelButton.alpha=0.7;
            }
            
            labelx=labelx+width;
            scrollContentCount = scrollContentCount + 1;
        }
        
        // set scroll content size
        
        DispatchQueue.main.async(execute: {
            self.headerScroll.contentSize = CGSize(width: frame.width, height: 40)
        })
        automaticallyAdjustsScrollViewInsets = false;
        detailScroll.delegate=self;
    }
    
    
    func HeaderlabelSelected(_ sender: UIButton) {
        detailScroll.tag = sender.tag
        
        if sender.tag == 0 {
            mainContentViewHeight.constant = 750
        }
        else{
            mainContentViewHeight.constant = 850
        }
        
        
        let xAxis : CGFloat = ScreenSize.SCREEN_WIDTH * CGFloat(sender.tag)
        detailScroll.setContentOffset(CGPoint(x: xAxis, y: 0), animated: true)
        
        let buttonArray = NSMutableArray()
        
        for view in headerScroll.subviews {
            if view.isKind(of: UIButton.self) {
                let labelButton = view as! UIButton
                buttonArray.add(labelButton)
                
                if labelButton.tag == sender.tag {
                    labelButton.alpha = 1.0
                }
                else {
                    labelButton.alpha = 0.7
                }
            }
        }
        
        
        
        let labelButton = buttonArray.object(at: sender.tag) as! UIButton
        var frame : CGRect = labelButton.frame
        frame.origin.y = 38
        frame.size.height = 3
        
        UIView.animate(withDuration: 0.2, animations: {
            if sender.tag == 0 {
                self.indicatorLabel.frame =  CGRect(x: 0,y: 38, width: 110, height: 3)
                self.headerLabel.text = "Login"
//                self.headerTextLabel.text = "Login here to become a part of the Nikon Family"
            }
            else {
                self.indicatorLabel.frame =  CGRect(x: 110 * CGFloat(sender.tag) ,y: 38, width: 110, height: 3)
                self.headerLabel.text = "Register"
//                self.headerTextLabel.text = "Register here to become a part of the Nikon Family"
            }
            
            self.headerScroll.scrollRectToVisible(self.indicatorLabel.frame, animated: false)
            
            }, completion: nil)
    }
    
    // MARK: -  Main Scrollview
    func setUpScrollView() {
        for arrayIndex in headerArray {
            if (arrayIndex == "SIGN IN")  {
                signIn = Constants.mainStoryboard.instantiateViewController(withIdentifier: "LoginView") as! LoginView
                ControllerArray.append(signIn)
                
            }
            if (arrayIndex == "REGISTER") {
                signUp = Constants.mainStoryboard.instantiateViewController(withIdentifier: "RegisterView") as! RegisterView
                ControllerArray.append(signUp)
            }
        }
        
        setViewControllers(ControllerArray as NSArray, animated: false)
    }
    
    
    //  Add and remove view controllers
    func setViewControllers(_ viewControllers : NSArray, animated : Bool) {
        if self.childViewControllers.count > 0 {
            for vC in self.childViewControllers {
                vC.willMove(toParentViewController: nil)
                vC.removeFromParentViewController()
            }
        }
        
        for vC in viewControllers {
            self.addChildViewController(vC as! UIViewController)
            (vC as AnyObject).didMove(toParentViewController: self)
        }
        
        //TODO animations
        if ((detailScroll) != nil) {
            reloadPages()
        }
    }
    
    func reloadPages() {
        var scrollContentCount = 0;
        
        for arrayIndex in 0 ..< headerArray.count {
            // set scorllview properties
            var frame : CGRect = CGRect(x: 0, y: 0, width: 0, height: 0)
            frame.origin.x = ScreenSize.SCREEN_WIDTH * CGFloat(arrayIndex)
            frame.origin.y = 0;
            frame.size = ScreenSize.SCREEN.size
            
            detailScroll.isPagingEnabled = true
            let contentWidth : CGFloat = ScreenSize.SCREEN_WIDTH * CGFloat(scrollContentCount+1)
            widthConstraint.constant = contentWidth;
            
            let vC = self.childViewControllers[arrayIndex]
            addView(vC.view, contentView: contentView, frame: frame)
            
            
            if arrayIndex == 0 {
                signIn.forgotPass.addTarget(self, action: #selector(ForgotPasswordPressed), for: .touchUpInside)//ForgotVC
                signIn.loginButton.addTarget(self, action: #selector(SignInPressed), for: .touchUpInside)
                signIn.fbLogin.addTarget(self, action: #selector(FBLogInPressed), for: .touchUpInside)
                signIn.googleLogin.addTarget(self, action: #selector(GoogleSignInPressed), for: .touchUpInside)
                
                signIn.termsButton.addTarget(self, action: #selector(TermsPressed), for: .touchUpInside)
                signIn.privacyButton.addTarget(self, action: #selector(PrivacyPressed), for: .touchUpInside)
                signIn.guestBtn.layer.borderColor = Colors.appYello.cgColor
                signIn.guestBtn.layer.borderWidth = 0.7
                signIn.guestBtn.addTarget(self, action: #selector(ContinueAsGuestPressed), for: .touchUpInside)
                
            }
            else if arrayIndex == 1
            {
                signUp.registerButton.addTarget(self, action: #selector(CreateAccountPressed), for: .touchUpInside)
            }
            
            scrollContentCount = scrollContentCount + 1;
        }
        
        DispatchQueue.main.async(execute: {
            self.detailScroll.contentSize = CGSize(width: self.widthConstraint.constant, height: self.detailScroll.frame.size.height)
        })
        detailScroll.delegate = self
    }
    
    // adding views to scrollview
    func addView(_ view : UIView, contentView : UIView, frame : CGRect) {
        view.frame = frame
        view.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(view)
        
        
//        let newH = mainContentViewHeight.constant - 200
		
        
        DispatchQueue.main.async(execute: {
            contentView.addConstraint(NSLayoutConstraint.init(item: view, attribute: NSLayoutAttribute.leading, relatedBy: NSLayoutRelation.equal, toItem: contentView, attribute:  NSLayoutAttribute.leading, multiplier: 1.0, constant: frame.origin.x))
            contentView.addConstraint(NSLayoutConstraint.init(item: view, attribute: NSLayoutAttribute.width, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute:  NSLayoutAttribute.notAnAttribute, multiplier: 1.0, constant: frame.size.width))
            contentView.addConstraint(NSLayoutConstraint.init(item: view, attribute: NSLayoutAttribute.top, relatedBy: NSLayoutRelation.equal, toItem: contentView, attribute:  NSLayoutAttribute.top, multiplier: 1.0, constant:0))
            contentView.addConstraint(NSLayoutConstraint.init(item: view, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute:  NSLayoutAttribute.notAnAttribute, multiplier: 1.0, constant: 568))
        })
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    //MARK: - SCROLL Delegate
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        if scrollView == self.detailScroll {
            let pageWidth = ScreenSize.SCREEN_WIDTH
            let page = (Int)(floor((self.detailScroll.contentOffset.x - pageWidth / 2) / pageWidth) + 1);

            let but = UIButton.init()
            but.tag = page
            
            self.HeaderlabelSelected(but)
        }
    }
    
    // MARK:- FOrot pass
    @IBAction func ForgotPasswordPressed(_ sender: AnyObject) {
        let forgot = Constants.mainStoryboard.instantiateViewController(withIdentifier: "ForgotVC") as! ForgotVC
//        forgot.isForgetView = true
        forgot.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        self.present(forgot, animated: true, completion: nil)
    }
    
        //MARK: - LOGIN Action
    @IBAction func FBLogInPressed(_ sender: AnyObject)
    {
        if Reachability.isConnectedToNetwork() {
            
            DispatchQueue.main.async
            {
                let logManager = FBSDKLoginManager.init()
                
                logManager.loginBehavior = FBSDKLoginBehavior.web
                logManager.logOut()
                logManager.logIn(withReadPermissions: ["public_profile", "email"], from: self) { (result, error) in
                    if error != nil {
                        CustomAlertController.showAlert(on: self, with: Simple, andAttributes: ["title": "Nikon", "message" : "Some error Occurred !"])
                    }
                    else if result?.isCancelled == true {
                    }
                    else {
                        if result?.token.tokenString != "" || result?.token != nil {
                            self.fetchUserInfo()
                        }
                    }
                }
            }
        }
        else {
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }

    }
    func fetchUserInfo()
    {
        
        if Reachability.isConnectedToNetwork() {
            
            Constants.appDelegate.startIndicator()
            let req = FBSDKGraphRequest.init(graphPath: "me" , parameters: ["fields": "id, name, picture.width(500).height(500), email, gender"])
            DispatchQueue.main.async
            {
                req?.start { (connection, result, error) in
                    if(error == nil) {
                        
                        let resDict = result as! Dictionary<String,AnyObject>
                        self.userFirstName = resDict["name"]as! String
                        self.userSocialId = resDict["id"]as! String
                        if (resDict["email"] != nil)
                        {
                            self.userSocialEmail = resDict["email"]as! String
                        }
                        self.option = "facebook"
                        self.checkUserAuthonticationWithUserInfo(resDict as NSDictionary , option: self.option as NSString)
                    }
                    else
                    {
                       Constants.appDelegate.stopIndicator()
                    }
                }
            }
        }
        else {
            
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
        
    }
    func checkUserAuthonticationWithUserInfo(_ result : NSDictionary , option : NSString
        ) {
        
        if Reachability.isConnectedToNetwork()
        {
            
            let userId = result["id"] as! String
            let method = "CheckSocialUser?SocialId=\(userId)&option=\(option)&device_token=\(UserDefaults.standard.object(forKey: Constants.USERDEVICETOKEN)!)&device_type=IOS&device_id=\(AppDelegate.getUniqueDeviceIdentifierAsString())&device_version=\(iOSVersion.SYS_VERSION_FLOAT)"
            Constants.appDelegate.startIndicator()
            
            print("DEVICE TOKE - \(Constants.USERDEVICETOKEN)")
            
            Server.getRequestWithURL(Constants.BASEURL+method, completionHandler: { (response) in
                
                if response[Constants.STATE] != nil && response[Constants.STATE] as! String == Constants.SUCC {
                    
                    DispatchQueue.main.async
                    {
                        
                        UserDefaults.standard.set(response["emp_id"], forKey: Constants.USERID)
                        UserDefaults.standard.set(response["pwd_status"], forKey: Constants.USERHAVEPASSWORD)
                        Constants.appDelegate.stopIndicator()
                        DispatchQueue.main.async(execute: {

                            self.goToDashboard()
                        })
                    }
                }
                else {
                    Constants.appDelegate.stopIndicator()

                    DispatchQueue.main.async
                    {
                        if self.userSocialEmail == ""
                        {
                        let emailId = Constants.mainStoryboard.instantiateViewController(withIdentifier: "EmailIdVC") as! EmailIdVC
                        emailId.sendEmailDelegate = self
                        emailId.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                        self.present(emailId, animated: true, completion: nil)
                        }
                        else
                        {
                            self.registerSocialId(self.option as NSString)
                        }
                    }

                }
            })
        }
        UserDefaults.standard.synchronize()

    }
    
    func getEmailIdStr(_ str : String){
       // print(str)
        self.userSocialEmail = str
        self.registerSocialId(self.option as NSString)
    }
    func registerSocialId(_ option : NSString)  {
         Constants.appDelegate.startIndicator()
        Server.RegisterToAppWithSocialID(self.userFirstName, lname: self.userLastName, Id: self.userSocialId, email: self.userSocialEmail, type: option as String, completionHandler: { (response) in
            
            if response[Constants.STATE] != nil && response[Constants.STATE] as! String == Constants.SUCC {
                
                DispatchQueue.main.async
                {
                    
                    UserDefaults.standard.set(response["emp_id"], forKey: Constants.USERID)
                    Constants.appDelegate.stopIndicator()
                    DispatchQueue.main.async(execute: {
                        self.goToDashboard()
                    })
                }
            }
            else {
                Constants.appDelegate.stopIndicator()
                
                var mess = String()
                if response[Constants.MESS] != nil{
                    mess = response[Constants.MESS] as! String
                }
                else{
                    mess = response["Message"] as! String
                }
                
                let alert = UIAlertController.init(title: "Nikon", message: mess, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                    self.dismiss(animated: true, completion: nil)
                }))
                self.present(alert, animated: true, completion: nil)
            }
            
        })
    }

    @IBAction func GoogleSignInPressed(_ sender: AnyObject) {
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().delegate = self
        //GIDSignIn.sharedInstance().signOut()
        GIDSignIn.sharedInstance().clientID = "200218119340-esm2bi55517emd7lbfc4c3283e9lbdc7.apps.googleusercontent.com"
        //GIDSignIn.sharedInstance().clientID = FIRApp.defaultApp()?.options.clientID
        GIDSignIn.sharedInstance().scopes.append("https://www.googleapis.com/auth/plus.login")
        GIDSignIn.sharedInstance().scopes.append("https://www.googleapis.com/auth/plus.me")
        GIDSignIn.sharedInstance().signIn()

    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
                withError error: NSError!) {
        if (error == nil) {
            // Perform any operations on signed in user here.
            
            let userId = user.userID                  // For client-side use only!
//            let idToken = user.authentication.idToken // Safe to send to the server
            let fullName = user.profile.name
//            let givenName = user.profile.givenName
//            let familyName = user.profile.familyName
            let email = user.profile.email
            if email == nil {
                self.userSocialEmail = ""
            }
            else
            {
                self.userSocialEmail = email!
            }
            
            self.userFirstName = fullName!
           
            self.userSocialId = userId!
            self.userLastName = ""
            let dictSend = ["id" : userId ]
            self.option = "gmail"
            self.checkUserAuthonticationWithUserInfo(dictSend as NSDictionary, option: self.option as NSString)
            //GIDSignIn.sharedInstance().signOut()
            // ...
        } else {
            print("\(error.localizedDescription)")
        }
    }
    func sign(_ signIn: GIDSignIn!,
                present viewController: UIViewController!) {
        self.present(viewController, animated: true, completion: nil)
    }
    
    // Dismiss the "Sign in with Google" view
    func sign(_ signIn: GIDSignIn!,
                dismiss viewController: UIViewController!) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func SignInPressed(_ sender: AnyObject) {
        
        signIn.emailtext.resignFirstResponder()
        signIn.passText.resignFirstResponder()
        
        if signIn.emailtext.text == "" {
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "Nikon", "message": "Email Id cannot be empty."] )
        }
        else if !Helper.isValidEmail(signIn.emailtext.text!) {
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "Nikon", "message": "Enter a valid email id."] )
        }
        else if signIn.passText.text == "" {
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "Nikon", "message": "Password cannot be empty."] )
        }
        else if signIn.passText.text?.characters.count < 6
        {
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "Nikon", "message": "Password length should be minimum 6."] )
            
        }
        
        else {
            if Reachability.isConnectedToNetwork() {
                Constants.appDelegate.startIndicator()
                Server.LoginToApp(signIn.emailtext.text!, pass: signIn.passText.text!, completionHandler: { (response) in
                    
                    if response[Constants.STATE] as! String == Constants.SUCC {
                         let dict = NSMutableDictionary()
                        for item in response
                        {
                            var value = item.1
                            
                            if  (value as? String)?.count == 0 || (value as? NSNull) != nil
                            {
                                value = "" as AnyObject
                            }
                            dict.setValue(value, forKey: item.0)
                        }
                        Helper.saveDataInNsDefault(dict, key: Constants.PROFILE)
                        UserDefaults.standard.set(response["emp_id"], forKey: Constants.USERID)
                        UserDefaults.standard.set(response["pwd_status"], forKey: Constants.USERHAVEPASSWORD)
                        DispatchQueue.main.async(execute: {
//                            let mess = response[Constants.MESS] as! String
//                            
//
//                            let alert = UIAlertController.init(title: "Nikon", message: mess, preferredStyle: UIAlertControllerStyle.Alert)
//                            alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.Default, handler: { (action:UIAlertAction) in
//                                
//                                self.getUserDetail(String(response["emp_id"] as! Int))
//                            }))
//                            self.presentViewController(alert, animated: true, completion: nil)
                             self.goToDashboard()
                        })
                    }
                    else {
                        Constants.appDelegate.stopIndicator()
                        DispatchQueue.main.async(execute: {
                            let mess = response[Constants.MESS] as! String
                            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "Nikon", "message": mess] )
                        })
                    }
                })
            }
            else {
                CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "Nikon", "message": Messages.internetError] )
            }
        }
    }
    
    // Register
    
    

    @IBAction func CreateAccountPressed(_ sender: AnyObject) {
        
        signUp.emailtext.resignFirstResponder()
        signUp.passText.resignFirstResponder()
        signUp.fnameText.resignFirstResponder()
        signUp.lnameText.resignFirstResponder()
        signUp.confirmPassText.resignFirstResponder()
        signUp.mobiletext.resignFirstResponder()
        signUp.dayText.resignFirstResponder()

        if signUp.fnameText.text == "" || signUp.lnameText.text == ""{
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "Nikon", "message": "First Name and Last Name field cannot be blank."] )
        }
        else if signUp.emailtext.text == "" {
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "Nikon", "message": "Email Id cannot be blank."] )
        }
        
        else if !Helper.isValidEmail(signUp.emailtext.text!) {
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "Nikon", "message": "Enter a valid email id."] )
        }

//        else if signUp.mobiletext.text == "" {
//            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "Nikon", "message": "Mobile No. cannot be blank."] )
//        }
        else if signUp.mobiletext.text?.characters.count >= 1 && !Helper.isValidPhoneNumber(signUp.mobiletext.text!)
        {
    
//            if !Helper.isValidPhoneNumber(signUp.mobiletext.text!)
//            {
                CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "Nikon", "message": "Enter a valid phone no."] )
            //}
        }
        else if signUp.passText.text == "" {
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "Nikon", "message": "Password cannot be blank."] )
        }
        else if !Helper.isValidPassword(signUp.passText.text!)
        {
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "Nikon", "message": "Enter password with atleast 1 alphabet, 1 number and length should be minimum 6"] )
        }
        else if signUp.confirmPassText.text != signUp.passText.text{
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "Nikon", "message": "Confirm password does not match."] )
        }
//        else if signUp.genderSelected == ""
//        {
//            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "Nikon", "message": "Please select gender"] )
//        }
//        else if signUp.dayText.text == ""
//        {
//            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "Nikon", "message": "Please enter DOB"] )
//        }
        else {
            if Reachability.isConnectedToNetwork() {
                
                var dobStr = ""
                let datestr = signUp.dayText.text!
                if (datestr != ""){
                    dobStr = Helper.sendingDateString("\(signUp.dayText.text!)")
                }
                else
                {
                    dobStr = "01/01/1900"//Helper.sendingDateString("01 Jan, 1900")
                }
                
                var phoneStr = ""
                let mobileStr = signUp.mobiletext.text!
                if (mobileStr != ""){
                    phoneStr = signUp.mobiletext.text!
                }
                else
                {
                    phoneStr = ""
                }
                
              var genderStr = ""
                if (signUp.genderSelected != ""){
                    genderStr = signUp.genderSelected
                }
                else
                {
                    genderStr = ""
                }

                
                Constants.appDelegate.startIndicator()
                Server.RegisterToApp(signUp.fnameText.text!, lname: signUp.lnameText.text!, mobile: phoneStr, email: signUp.emailtext.text!, pass: signUp.passText.text!, gender: genderStr , dob: dobStr, completionHandler: { (response) in
                    Constants.appDelegate.stopIndicator()
                    if response[Constants.STATE] != nil && response[Constants.STATE] as! String == Constants.SUCC {
                        
                       // NSUserDefaults.standardUserDefaults().setObject(response, forKey: Constants.PROFILE)
                        
                        let dict = NSMutableDictionary()
                        for item in response
                        {
                            var value = item.1
                            if  value as? Int == 0  //(value.isEmpty)
                            {
                                value = "" as AnyObject
                            }
                            dict.setValue(value, forKey: item.0)
                        }
                        Helper.saveDataInNsDefault(dict, key: Constants.PROFILE)
                        //NSUserDefaults.standardUserDefaults().setObject(response["emp_id"], forKey: Constants.USERID)

                        DispatchQueue.main.async(execute: {
                            
//                            
                            
                            let mess = response[Constants.MESS] as! String
                            let alert = UIAlertController.init(title: "Nikon", message: mess, preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                            
                                //self.getUserDetail(String(response["emp_id"] as! Int))
                                self.removeAllText()
                            }))
                            self.present(alert, animated: true, completion: nil)
                             //self.goToDashboard()
                        })
                    }
                    else {
                        DispatchQueue.main.async(execute: {
                            var mess = String()
                            if response[Constants.MESS] != nil{
                                mess = response[Constants.MESS] as! String
                            }
                            else if response[Constants.MESS] == nil
                            {
                                mess = "Oops some error occured."
                            }
                            else{
                                mess = response["Message"] as! String
                            }
                            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "Nikon", "message": mess] )
                        })
                    }
                })
            }
            else {
                CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "Nikon", "message": Messages.internetError] )
            }
        }
    }
    //MARK:- CONTINUE AS GUEST
    @IBAction func ContinueAsGuestPressed(_ sender: AnyObject) {
        FIRAnalytics.logEvent(withName: kFIREventSelectContent, parameters: [kFIRParameterItemID: "id-Splash", kFIRParameterItemName: "App Lanched", kFIRParameterContentType: ""])
        if isWantTodismissVC == true
        {
            self.dismiss(animated: false, completion: {
                
                self.isWantTodismissVC = false
                let vc = Constants.mainStoryboard.instantiateViewController(withIdentifier: "Dashboard")
                self.sideMenuController()?.setContentViewController(vc)

            })
//
            
            
        }
        else
        {
        
//        let sideMenuObj = sideMenuController()!.sideMenu!.menuViewController as! MenuViewController
        let vc = Constants.mainStoryboard.instantiateViewController(withIdentifier: "Dashboard")
        sideMenuController()?.setContentViewController(vc)
        }
        

    }
    
    //MARK:- GET USER DETAIL API
    func getUserDetail(_ userId : NSString)  {
        if Reachability.isConnectedToNetwork()
        {
            let method = "getuserprofile?emp_id=\(userId)"
            
            Constants.appDelegate.startIndicator()
            Server.getRequestWithURL(Constants.BASEURL+method, completionHandler: { (response) in
                
                
                // if response[Constants.STATE] != nil && response[Constants.STATE] as! String == Constants.SUCC {
                let responseDict = response //as NSDictionary
                
                DispatchQueue.main.async
                {
                    Constants.appDelegate.stopIndicator()
                    let userName = "\(responseDict["User_Fname"] as! String) \(responseDict["User_Lname"] as! String)"

                    UserDefaults.standard .set(userName, forKey: Constants.USERNAME)
                    UserDefaults.standard .set(Helper.getDOBStringFromDate(response["User_Dob"] as! String), forKey: Constants.USERDOB)
                    
                    UserDefaults.standard .synchronize()
                    
                    self.goToDashboard()
                    
                }
                
                
            })
        }
        else
        {
            Constants.appDelegate.stopIndicator()
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
        

    }
    //MARK:- REMOVE ALL TEXTFIELD VALUE
    
    func removeAllText() {
        
        signUp.fnameText.text = ""
        signUp.lnameText.text = ""
        signUp.emailtext.text = ""
        signUp.mobiletext.text = ""
        signUp.passText.text = ""
        signUp.confirmPassText.text = ""
        signUp.dayText.text = ""
        signUp.maleBtn.isSelected = false
        signUp.femaleBtn.isSelected = false
        
        let but = UIButton.init()
        but.tag = 0
        
        self.HeaderlabelSelected(but)

       // signUp.passText.text = ""
        
    }
    //MARK:- GO TO DASHBOARD
    func goToDashboard(){

        
        if isWantTodismissVC == false
        {
            FIRAnalytics.logEvent(withName: kFIREventSelectContent, parameters: [kFIRParameterItemID: "id-Splash", kFIRParameterItemName: "App Lanched", kFIRParameterContentType: ""])

            let sideMenuObj = sideMenuController()!.sideMenu!.menuViewController as! MenuViewController
            let vc = Constants.mainStoryboard.instantiateViewController(withIdentifier: "Dashboard")
            let user = "\(UserDefaults.standard.value(forKey: Constants.USERID)!)"
            sideMenuObj.getUserDetail(user as NSString)
            sideMenuController()?.setContentViewController(vc)
        }
        else
        {
            let sideMenuObj = sideMenuController()!.sideMenu!.menuViewController as! MenuViewController
            let user = "\(UserDefaults.standard.value(forKey: Constants.USERID)!)"
            sideMenuObj.getUserDetail(user as NSString)
            
            isWantTodismissVC = false
            Constants.appDelegate.stopIndicator()
            self.dismiss(animated: true, completion: {
                
            })
        }
        }
    
//    func goToDashboard(){
//        
//        let home = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("Dashboard")
//        let destinationController = ENSideMenuNavigationController.init(menuViewController: MenuViewController(), contentViewController: home)
//        Constants.appDelegate.window?.makeKeyAndVisible()
//        Constants.appDelegate.window?.rootViewController = destinationController
//        sideMenuController()?.setContentViewController(home)
//    }
    
    @IBAction func TermsPressed(_ button: UIButton, forEvent event: UIEvent){
        
        let vc = Constants.mainStoryboard.instantiateViewController(withIdentifier: "WebVC") as! WebVC
        vc.title = "TERMS & CONDITIONS"
        vc.isPush = true
        vc.isTermsType = "LOGIN"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func PrivacyPressed(_ button: UIButton, forEvent event: UIEvent){
        
        let destViewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "WebVC") as! WebVC
        destViewController.isPush = true
        destViewController.title = "PRIVACY POLICY"
        self.navigationController?.pushViewController(destViewController, animated: true)

    }

}


