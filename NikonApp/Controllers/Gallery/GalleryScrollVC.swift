//
//  GalleryScrollVC.swift
//  NikonApp
//
//  Created by Surbhi on 09/01/17.
//  Copyright © 2017 Neuronimbus. All rights reserved.
//

import UIKit

class GalleryScrollVC: UIViewController, UIScrollViewDelegate, UITextFieldDelegate  {

    //MARK:- IBOUTLETS
    //SCROLL
    @IBOutlet weak var headerScroll: UIScrollView!
    @IBOutlet weak var detailScroll: UIScrollView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var widthConstraint: NSLayoutConstraint!
    fileprivate var indicatorLabel : UILabel!
    
    
    //MARK:- DECLARED VARIABLES
    fileprivate var ControllerArray = [AnyObject]()
    fileprivate var headerArray = ["WORKSHOP GALLERY","PRO GALLERY","USER GALLERY","MENTOR GALLERY"]
    var selectedSection : Int = 0
    var workSHOP = workshopCollectionVC()
    var prO = proCollectionVC()
    var usER = UserCollectionVC()
    var menTOR = MentorCollectionVC()
    var autocompleteNames = [String]()
    
    //MARK:- START
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        self.setNavigationBar()
        self.setHeaderScroll()
        self.setUpScrollView()
        
        
        
        
        
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: - Navigation
    
    func OpenSideMenu(_ sender: AnyObject){
        
        self.view.endEditing(true)
        toggleSideMenuView()
    }
    
    func PopView(_ sender: AnyObject){
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func setNavigationBar(){
        self.navigationItem.hidesBackButton = true
        let navigationViewFrame = ScreenSize.SCREEN
        let navigationView = UIView.init(frame: CGRect(x: 0, y: 0, width: navigationViewFrame.size.width, height: 44))
        navigationView.backgroundColor = UIColor.clear
        let menuButton = UIButton.init(type: .custom)
        
        menuButton.frame = CGRect(x: ScreenSize.SCREEN_WIDTH - 45, y: 12, width: 30, height: 30)
        menuButton.setImage(UIImage.init(named: "sideMenu_Icon"), for: UIControlState())
        menuButton.addTarget(self, action: #selector(OpenSideMenu), for: .touchUpInside)
        navigationView.addSubview(menuButton)
        self.navigationController?.navigationBar.tintColor = UIColor.black
        
        let titleLabel = UILabel.init(frame:CGRect(x: 60,y: 0,width: ( (navigationViewFrame.size.width) - 110),height: 44))
        titleLabel.textColor = UIColor.black
        titleLabel.text = "GALLERY"
        titleLabel.font = UIFont(name:"OpenSans-SemiBold", size: 15)!
        titleLabel.textAlignment = .center
        navigationView.addSubview(titleLabel)
        
         let logoImage = UIImageView.init(frame: CGRect(x: 25, y: 6, width: 33, height: 33))
        logoImage.image = UIImage.init(named: "nikon_login_logo")
        navigationView.addSubview(logoImage)
        
        let backButton = UIButton.init(type: .custom)
        backButton.frame = CGRect(x: 0, y: 12, width: 20, height: 20)
        backButton.backgroundColor = UIColor.white
        backButton.setImage(UIImage.init(named: "back"), for: UIControlState())
        backButton.addTarget(self, action: #selector(PopView), for: .touchUpInside)
        navigationView.addSubview(backButton)
        
        self.navigationItem.titleView = navigationView
        
    }
    
    
    // MARK: - Label ScrollView
    
    func setHeaderScroll() {
        var scrollContentCount = 0;
        var frame : CGRect = CGRect(x: 0, y: 0, width: 0, height: 0)
        let width  : CGFloat = ScreenSize.SCREEN_WIDTH / 2.20
        var labelx : CGFloat = width * 0
        
        let firstLabelWidth : CGFloat = ScreenSize.SCREEN_WIDTH / 2.20
        
        indicatorLabel = UILabel.init(frame: CGRect(x: labelx + CGFloat(self.selectedSection), y: 38, width: firstLabelWidth, height: 3))
        indicatorLabel.backgroundColor = Colors.appYello
        indicatorLabel.text = "";
        headerScroll.addSubview(indicatorLabel)
        
        for arrayIndex in 0 ..< headerArray.count {
            frame.origin.x = labelx
            // set scorllview properties
            frame.origin.y = 0;
            frame.size.width=width;
            frame.size.height=39;
            
            let accountLabelButton = UIButton.init(type: UIButtonType.custom)
            accountLabelButton.addTarget(self, action:#selector(HeaderlabelSelected), for: UIControlEvents.touchUpInside)
            
            accountLabelButton.frame = frame;
            accountLabelButton.tag = arrayIndex;

            headerScroll.addSubview(accountLabelButton)
            
            if arrayIndex == self.selectedSection {
                accountLabelButton.setAttributedTitle(NSAttributedString(string: headerArray[arrayIndex],  attributes: [NSFontAttributeName : UIFont.init(name: "OpenSans-SemiBold", size: 15)!, NSForegroundColorAttributeName : UIColor.white]), for: UIControlState())
                
                if DeviceType.IS_IPHONE_5 || DeviceType.IS_IPHONE_4_OR_LESS {
                    accountLabelButton.setAttributedTitle(NSAttributedString(string: headerArray[arrayIndex],  attributes: [NSForegroundColorAttributeName :  UIColor.white,NSFontAttributeName : UIFont.init(name: "OpenSans-SemiBold", size: 14)!]), for: UIControlState())
                }
                accountLabelButton.titleLabel?.textAlignment = NSTextAlignment.center

            }
            else {
                accountLabelButton.setAttributedTitle(NSAttributedString(string: headerArray[arrayIndex],  attributes: [NSFontAttributeName : UIFont.init(name: "OpenSans-Light", size: 15)!, NSForegroundColorAttributeName : UIColor.white]), for: UIControlState())
                
                if DeviceType.IS_IPHONE_5 || DeviceType.IS_IPHONE_4_OR_LESS {
                    accountLabelButton.setAttributedTitle(NSAttributedString(string: headerArray[arrayIndex],  attributes: [NSForegroundColorAttributeName :  UIColor.white,NSFontAttributeName : UIFont.init(name: "OpenSans-Light", size: 14)!]), for: UIControlState())
                }
                accountLabelButton.titleLabel?.textAlignment = NSTextAlignment.center

            }
            
            labelx=labelx+width;
            scrollContentCount = scrollContentCount + 1;
        }
        
        // set scroll content size
        
        DispatchQueue.main.async(execute: {
            self.headerScroll.contentSize = CGSize(width: width * 4, height: 40)
            self.indicatorLabel.frame =  CGRect(x: (ScreenSize.SCREEN_WIDTH/2.20) * CGFloat(self.selectedSection) ,y: 38, width: ScreenSize.SCREEN_WIDTH/2.20, height: 3)
            //self.headerScroll.scrollRectToVisible(CGRectMake((ScreenSize.SCREEN_WIDTH/2.20) * CGFloat(self.selectedSection), 0, width * 3, 40), animated: false)
            self.headerScroll.scrollRectToVisible(self.indicatorLabel.frame, animated: false)
            if (self.selectedSection == 0)
            {
                self.headerScroll.setContentOffset(CGPoint(x: self.indicatorLabel.frame.origin.x , y: 0), animated: true)
            }
            else{
                self.headerScroll.setContentOffset(CGPoint(x: self.indicatorLabel.frame.origin.x-ScreenSize.SCREEN_WIDTH/2.20 , y: 0), animated: true)
                
            }

            self.detailScroll.scrollRectToVisible(CGRect(x: ScreenSize.SCREEN_WIDTH * CGFloat(self.selectedSection), y: 0, width: ScreenSize.SCREEN_WIDTH, height: self.detailScroll.frame.size.height), animated: false)
            
        })
        automaticallyAdjustsScrollViewInsets = false;
        detailScroll.delegate=self;
    }
    
    
    func HeaderlabelSelected(_ sender: UIButton) {
        detailScroll.tag = sender.tag
        if selectedSection == sender.tag {
            return
        }
        

        selectedSection = sender.tag
        let xAxis : CGFloat = ScreenSize.SCREEN_WIDTH * CGFloat(sender.tag)
        detailScroll.setContentOffset(CGPoint(x: xAxis, y: 0), animated: true)
        
        let buttonArray = NSMutableArray()
        for view in headerScroll.subviews {
            
            if view.isKind(of: UIButton.self) {
                let labelButton = view as! UIButton
                buttonArray.add(labelButton)
                
                if labelButton.tag == sender.tag {
                    labelButton.setAttributedTitle(NSAttributedString(string: headerArray[labelButton.tag],  attributes: [NSFontAttributeName : UIFont.init(name: "OpenSans-SemiBold", size: 15)!, NSForegroundColorAttributeName : UIColor.white]), for: UIControlState())
                    
                    if DeviceType.IS_IPHONE_5 || DeviceType.IS_IPHONE_4_OR_LESS {
                        labelButton.setAttributedTitle(NSAttributedString(string: headerArray[labelButton.tag],  attributes: [NSForegroundColorAttributeName :  UIColor.white,NSFontAttributeName : UIFont.init(name: "OpenSans-SemiBold", size: 14.0)!]), for: UIControlState())
                    }
                }
                else {
                    labelButton.setAttributedTitle(NSAttributedString(string: headerArray[labelButton.tag],  attributes: [NSFontAttributeName : UIFont.init(name: "OpenSans-Light", size: 15)!, NSForegroundColorAttributeName : UIColor.white]), for: UIControlState())
                    
                    if DeviceType.IS_IPHONE_5 || DeviceType.IS_IPHONE_4_OR_LESS {
                        labelButton.setAttributedTitle(NSAttributedString(string: headerArray[labelButton.tag],  attributes: [NSForegroundColorAttributeName :  UIColor.white,NSFontAttributeName : UIFont.init(name: "OpenSans-Light", size: 14.0)!]), for: UIControlState())
                    }
                }
            }
        }
        
        let labelButton = buttonArray.object(at: sender.tag) as! UIButton
        var frame : CGRect = labelButton.frame
        frame.origin.y = 38
        frame.size.height = 3
        
        UIView.animate(withDuration: 0.2, animations: {
            if sender.tag == 0 {
                self.indicatorLabel.frame =  CGRect(x: 0,y: 38, width: ScreenSize.SCREEN_WIDTH/2.20, height: 3)
            }
            else {
                self.indicatorLabel.frame =  CGRect(x: (ScreenSize.SCREEN_WIDTH/2.20) * CGFloat(sender.tag) ,y: 38, width: ScreenSize.SCREEN_WIDTH/2.20, height: 3)
            }
            
            self.headerScroll.scrollRectToVisible(self.indicatorLabel.frame, animated: false)
            if (sender.tag == 0)
            {
                self.headerScroll.setContentOffset(CGPoint(x: self.indicatorLabel.frame.origin.x , y: 0), animated: true)
            }
            else{
                self.headerScroll.setContentOffset(CGPoint(x: self.indicatorLabel.frame.origin.x-ScreenSize.SCREEN_WIDTH/2.20 , y: 0), animated: true)
                
            }
            }, completion: nil)
    }
    
    // MARK: -  Main Scrollview
    func setUpScrollView() {
        for arrayIndex in headerArray {//
            
            if (arrayIndex == "WORKSHOP GALLERY")  {
                workSHOP  = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "workshopCollectionVC") as! workshopCollectionVC
                ControllerArray.append(workSHOP)
            }
            if (arrayIndex == "PRO GALLERY") {
                prO = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "proCollectionVC") as! proCollectionVC
                ControllerArray.append(prO)
            }
            if (arrayIndex == "USER GALLERY"){
                usER = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "UserCollectionVC") as! UserCollectionVC
                ControllerArray.append(usER)
            }
            if (arrayIndex == "MENTOR GALLERY"){
                menTOR = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "MentorCollectionVC") as! MentorCollectionVC
                ControllerArray.append(menTOR)
            }

        }
        setViewControllers(ControllerArray as NSArray, animated: false)
    }
    
    
    //  Add and remove view controllers
    func setViewControllers(_ viewControllers : NSArray, animated : Bool) {
        if self.childViewControllers.count > 0 {
            for vC in self.childViewControllers {
                vC.willMove(toParentViewController: nil)
                vC.removeFromParentViewController()
            }
        }
        
        for vC in viewControllers {
            self.addChildViewController(vC as! UIViewController)
            (vC as AnyObject).didMove(toParentViewController: self)
        }
        
        //TODO animations
        if ((detailScroll) != nil) {
            reloadPages()
        }
    }
    
    func reloadPages() {
        var scrollContentCount = 0;
        
        for arrayIndex in 0 ..< headerArray.count {
            // set scorllview properties
            var frame : CGRect = CGRect(x: 0, y: 0, width: 0, height: 0)
            frame.origin.x = ScreenSize.SCREEN_WIDTH * CGFloat(arrayIndex)
            frame.origin.y = 0;
            frame.size = ScreenSize.SCREEN.size
            
            detailScroll.isPagingEnabled = true
            let contentWidth : CGFloat = ScreenSize.SCREEN_WIDTH * CGFloat(scrollContentCount+1)
            widthConstraint.constant = contentWidth;
            
            let vC = self.childViewControllers[arrayIndex]
            addView(vC.view, contentView: contentView, frame: frame)
            scrollContentCount = scrollContentCount + 1;
        }
        
        DispatchQueue.main.async(execute: {
            self.detailScroll.contentSize = CGSize(width: self.widthConstraint.constant, height: self.detailScroll.frame.size.height)
        })
        detailScroll.delegate = self
    }
    
    // adding views to scrollview
    func addView(_ view : UIView, contentView : UIView, frame : CGRect) {
        view.frame = frame
        view.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(view)
        
        DispatchQueue.main.async(execute: {
            contentView.addConstraint(NSLayoutConstraint.init(item: view, attribute: NSLayoutAttribute.leading, relatedBy: NSLayoutRelation.equal, toItem: contentView, attribute:  NSLayoutAttribute.leading, multiplier: 1.0, constant: frame.origin.x + 5))
            contentView.addConstraint(NSLayoutConstraint.init(item: view, attribute: NSLayoutAttribute.width, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute:  NSLayoutAttribute.notAnAttribute, multiplier: 1.0, constant: frame.size.width - 10))
            contentView.addConstraint(NSLayoutConstraint.init(item: view, attribute: NSLayoutAttribute.top, relatedBy: NSLayoutRelation.equal, toItem: contentView, attribute:  NSLayoutAttribute.top, multiplier: 1.0, constant:5))
            
            contentView.addConstraint(NSLayoutConstraint.init(item: view, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute:  NSLayoutAttribute.notAnAttribute, multiplier: 1.0, constant: frame.size.height - 109))
        })
    }
    
    //MARK: - SCROLL Delegate
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        if scrollView == self.detailScroll {

            let pageWidth = ScreenSize.SCREEN_WIDTH
            let page = (Int)(floor((self.detailScroll.contentOffset.x - pageWidth / 2.20) / pageWidth) + 1);
            
            let but = UIButton.init()
            but.tag = page
            
            self.HeaderlabelSelected(but)
        }
    }
}
