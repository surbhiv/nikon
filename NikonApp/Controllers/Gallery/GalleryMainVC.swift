//
//  GalleryMainVC.swift
//  NikonApp
//
//  Created by Surbhi on 09/01/17.
//  Copyright © 2017 Neuronimbus. All rights reserved.
//

import UIKit

class GalleryMainVC: UIViewController{

    
    @IBOutlet weak var workshop: UIButton!
    @IBOutlet weak var pro: UIButton!
    @IBOutlet weak var user: UIButton!
    @IBOutlet weak var mentor: UIButton!

    var isFromMenu = Bool()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        DispatchQueue.main.async{
            self.setNavigationBar()
        }
        // Do any additional setup after loading the view.
        
        var heigh = CGFloat()
        if DeviceType.IS_IPHONE_6 || DeviceType.IS_IPHONE_6P {
           heigh = (ScreenSize.SCREEN_HEIGHT-70)/4
        }
        else if DeviceType.IS_IPHONE_5{
            heigh = (ScreenSize.SCREEN_HEIGHT-80)/4
        }
        else{
            heigh = (ScreenSize.SCREEN_HEIGHT-70)/4
        }
        
        
        let layer1 = CAShapeLayer.init()
        let path1 = UIBezierPath.init()
        path1.move(to: CGPoint(x: 0, y: 0))
        path1.addLine(to: CGPoint(x: 0, y: heigh + 20))
        path1.addLine(to: CGPoint(x: ScreenSize.SCREEN_WIDTH, y: heigh - 25))
        path1.addLine(to: CGPoint(x: ScreenSize.SCREEN_WIDTH, y: 0))
        path1.close()
        layer1.path = path1.cgPath
        self.workshop.layer.masksToBounds = true
        self.workshop.layer.mask = layer1
        
        let layer2 = CAShapeLayer.init()
        let path2 = UIBezierPath.init()
        path2.move(to: CGPoint(x: 0, y: 35))
        path2.addLine(to: CGPoint(x: 0, y: heigh + 15))
        path2.addLine(to: CGPoint(x: ScreenSize.SCREEN_WIDTH, y: heigh + 15))
        path2.addLine(to: CGPoint(x: ScreenSize.SCREEN_WIDTH, y: -5))
        path2.close()
        layer2.path = path2.cgPath
        self.pro.layer.masksToBounds = true
        self.pro.layer.mask = layer2
        
        let layer3 = CAShapeLayer.init()
        let path3 = UIBezierPath.init()
        path3.move(to: CGPoint(x: 0, y: 0))
        path3.addLine(to: CGPoint(x: 0, y: heigh + 20))
        path3.addLine(to: CGPoint(x: ScreenSize.SCREEN_WIDTH, y: heigh - 25))
        path3.addLine(to: CGPoint(x: ScreenSize.SCREEN_WIDTH, y: 0))
        path3.close()
        layer3.path = path3.cgPath
        self.user.layer.masksToBounds = true
        self.user.layer.mask = layer3

        let layer4 = CAShapeLayer.init()
        let path4 = UIBezierPath.init()
        path4.move(to: CGPoint(x: 0, y: 35))
        path4.addLine(to: CGPoint(x: 0, y: heigh + 15))
        path4.addLine(to: CGPoint(x: ScreenSize.SCREEN_WIDTH, y: heigh + 15))
        path4.addLine(to: CGPoint(x: ScreenSize.SCREEN_WIDTH,y: -5))
        path4.close()
        layer4.path = path4.cgPath
        self.mentor.layer.masksToBounds = true
        self.mentor.layer.mask = layer4

        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- NAVIGATION
    
    func OpenSideMenu(_ sender: AnyObject){
        
        self.view.endEditing(true)
        toggleSideMenuView()
    }
    
    func setNavigationBar(){
        
        self.navigationItem.hidesBackButton = true
        let navigationViewFrame = self.navigationController?.navigationBar.frame
        let navigationView = UIView.init(frame: navigationViewFrame!)
        navigationView.backgroundColor = UIColor.white
        let menuButton = UIButton.init(type: .custom)
        menuButton.frame = CGRect(x: ScreenSize.SCREEN_WIDTH - 45, y: 12, width: 30, height: 30)
        menuButton.setImage(UIImage.init(named: "sideMenu_Icon"), for: UIControlState())
        menuButton.addTarget(self, action: #selector(OpenSideMenu), for: .touchUpInside)
        navigationView.addSubview(menuButton)
        
//        self.navigationController?.navigationBar.tintColor = Colors.appPink
        
        let titleLabel = UILabel.init(frame:CGRect(x: ( ScreenSize.SCREEN_WIDTH - 150)/2,y: 0,width: 150,height: 44))
        titleLabel.textColor = UIColor.black
        titleLabel.text="GALLERY"
        titleLabel.font = UIFont(name:"OpenSans-SemiBold", size: 15)!
        titleLabel.textAlignment = .center
        navigationView.addSubview(titleLabel)
        
        if isFromMenu == false
        {
            let backButton = UIButton.init(type: .custom)
            backButton.frame = CGRect(x: 0, y: 9, width: 20, height: 20)
            backButton.backgroundColor = UIColor.white
            backButton.setImage(UIImage.init(named: "back"), for: UIControlState())
            backButton.addTarget(self, action: #selector(PopView), for: .touchUpInside)
            navigationView.addSubview(backButton)
            
            let logoImage = UIImageView.init(frame: CGRect(x: 25, y: 6, width: 33, height: 33))
            logoImage.image = UIImage.init(named: "nikon_login_logo")
            navigationView.addSubview(logoImage)
            
            
        }
        else
        {
            let logoImage = UIImageView.init(frame: CGRect(x: 0, y: 6, width: 33, height: 33))
            logoImage.image = UIImage.init(named: "nikon_login_logo")
            navigationView.addSubview(logoImage)
        }

        
//        let logoImage = UIImageView.init(frame: CGRectMake(25, 6, 33, 33))
//        logoImage.image = UIImage.init(named: "nikon_login_logo")
//        navigationView.addSubview(logoImage)
        
        self.navigationItem.titleView = navigationView
    }
    func PopView(_ sender: AnyObject){
        
        self.navigationController?.popViewController(animated: true)
    }

    //MARK:- BUTTON ACTIONS
    @IBAction func WorkShopPressed(_ sender: AnyObject) {
        
        let desination = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "GalleryScrollVC") as! GalleryScrollVC
        desination.selectedSection = 0
        self.navigationController?.pushViewController(desination, animated: true)
    }
    
    @IBAction func ProPressed(_ sender: AnyObject) {
        let desination = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "GalleryScrollVC") as! GalleryScrollVC
        desination.selectedSection = 1
        self.navigationController?.pushViewController(desination, animated: true)
    }
    
    
    @IBAction func UserPressed(_ sender: AnyObject) {
        let desination = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "GalleryScrollVC") as! GalleryScrollVC
        desination.selectedSection = 2
        self.navigationController?.pushViewController(desination, animated: true)
    }
    
    @IBAction func MentorPressed(_ sender: AnyObject) {
        let desination = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "GalleryScrollVC") as! GalleryScrollVC
        desination.selectedSection = 3
        self.navigationController?.pushViewController(desination, animated: true)
    }
    
    
}
