//
//  workshopCollectionVC.swift
//  NikonApp
//
//  Created by Surbhi on 09/01/17.
//  Copyright © 2017 Neuronimbus. All rights reserved.
//

import UIKit

class workshopCollectionVC: UIViewController, UITableViewDelegate, UITableViewDataSource  {

    @IBOutlet weak var work_Table: UITableView!

    var workshopArray = [AnyObject]()
    var displayArray = [AnyObject]()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.getData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- GETDATA
    func getData(){
        if Reachability.isConnectedToNetwork()
        {
            let method = "WorkshopGalleryLevel"
            Constants.appDelegate.startIndicator()
            Server.getRequestWithURL(Constants.BASEURL+method, completionHandler: { (response) in
                
                if response[Constants.STATE] != nil && response[Constants.STATE] as! String == Constants.SUCC {
                    self.workshopArray = response["GetList"] as! Array<AnyObject>
                    DispatchQueue.main.async
                    {
                        Constants.appDelegate.stopIndicator()
                        if self.workshopArray.count > 0 {
                            self.displayArray = self.workshopArray
                           self.work_Table.reloadData()
                        }
                    }
                }
                else {
                    Constants.appDelegate.stopIndicator()
                    var mess = String()
                    if response[Constants.MESS] != nil{
                        mess = response[Constants.MESS] as! String
                    }
                    else{
                        mess = response["Message"] as! String
                    }
                    DispatchQueue.main.async
                    {
                    let alert = UIAlertController.init(title: "Nikon", message: mess, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                        self.dismiss(animated: true, completion: nil)
                    }))
                    self.present(alert, animated: true, completion: nil)
                    }
                }
            })
        }
        else
        {
            Constants.appDelegate.stopIndicator()
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
    }

    //MARK:- TABLEVIEW
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 5
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 5
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.displayArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "workShopCell", for: indexPath) as! workShopCell
        cell.selectionStyle = .none
        let dict = self.displayArray[indexPath.section]
        let image = dict["Level_image"] as? String
        let updateURL = image?.replacingOccurrences(of: " ", with: "%20")
        
        cell.backImage.image = nil
        
        let str = dict["Level_name"] as? String
        var arr = str?.components(separatedBy: " ")
        let str2 = arr?.last
        arr?.removeLast()
        let str1 = arr?.joined(separator: " ")
        
        let attr1 = [ NSFontAttributeName : UIFont(name:"OpenSans", size: 14)!,
                      NSForegroundColorAttributeName : UIColor.init(red: 152.0/255.0, green: 152.0/255.0, blue: 152.0/255.0, alpha: 1.0)]
        let attr2 = [ NSFontAttributeName : UIFont(name:"OpenSans-ExtraBold", size: 14)!,
                      NSForegroundColorAttributeName :  UIColor.white]
        //UIColor.init(red: 36.0/255.0, green: 36.0/255.0, blue: 36.0/255.0, alpha: 1.0)
        
        let strAtt1 = NSAttributedString.init(string: "   \(str1!) ", attributes: attr1)
        let strAtt2 = NSAttributedString.init(string: str2!, attributes: attr2)
        let finalStr = NSMutableAttributedString()
        finalStr.append(strAtt1)
        finalStr.append(strAtt2)
        
        cell.titleLabel.attributedText = finalStr

        cell.backImage.sd_setImage(with: URL.init(string: updateURL!), placeholderImage: UIImage.init(named: "product_placeholder.jpg"))
        cell.backImage.setShowActivityIndicator(true)
        cell.backImage.setIndicatorStyle(.gray)
        
        return cell

    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dict = self.displayArray[indexPath.section] as! Dictionary<String,AnyObject>
        
        let desination = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "GalleryWorkShopSubVC") as! GalleryWorkShopSubVC
        desination.levelId = dict["Level_Id"] as! Int
        desination.namLabeltext = dict["Level_name"] as! String
        
        
        let image = dict["Level_image"] as? String
        let updateURL = image?.replacingOccurrences(of: " ", with: "%20")
        desination.imageString = updateURL!
        
        self.navigationController?.pushViewController(desination, animated: true)
    }
}
