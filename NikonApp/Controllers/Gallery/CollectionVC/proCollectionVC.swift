//
//  proCollectionVC.swift
//  NikonApp
//
//  Created by Surbhi on 09/01/17.
//  Copyright © 2017 Neuronimbus. All rights reserved.
//

import UIKit

class proCollectionVC: UIViewController,UITableViewDataSource, UITableViewDelegate  {

    @IBOutlet weak var proTable: UITableView!

    var proArray = [AnyObject]()
    var displayArray = [AnyObject]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- GETDATA
    func getData(){
        if Reachability.isConnectedToNetwork()
        {
            let method = "NikkonGallery?option=pro"
            Constants.appDelegate.startIndicator()
            Server.getRequestWithURL(Constants.BASEURL+method, completionHandler: { (response) in
                
                if response[Constants.STATE] != nil && response[Constants.STATE] as! String == Constants.SUCC {
                    self.proArray = response["gallerylist"] as! Array<AnyObject>

                    DispatchQueue.main.async
                    {
                        Constants.appDelegate.stopIndicator()
                        if self.proArray.count > 0 {
                            self.displayArray = self.proArray
                            self.proTable.reloadData()
                        }
                    }
                }
                else {
                    Constants.appDelegate.stopIndicator()
                    var mess = String()
                    if response[Constants.MESS] != nil{
                        mess = response[Constants.MESS] as! String
                    }
                    else{
                        mess = response["Message"] as! String
                    }
                    DispatchQueue.main.async
                    {
                    let alert = UIAlertController.init(title: "Nikon", message: mess, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                        self.dismiss(animated: true, completion: nil)
                    }))
                    self.present(alert, animated: true, completion: nil)
                    }
                }
            })
        }
        else
        {
            Constants.appDelegate.stopIndicator()
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
    }
    //MARK:- TABLE View
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 5
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 5
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return displayArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OtherGalleryCell", for: indexPath) as! OtherGalleryCell
        cell.selectionStyle = .none
        let dict = self.displayArray[indexPath.section] as! Dictionary<String,AnyObject>
        
        let str = dict["GalleryName"] as? String
        var arr = str?.components(separatedBy: " ")
        let str1 = arr?.first
        arr?.removeFirst()
        let str2 = arr?.joined(separator: " ")
        
        let attr2 = [ NSFontAttributeName : UIFont(name:"OpenSans", size: 14)!,
                      NSForegroundColorAttributeName : UIColor.init(red: 152.0/255.0, green: 152.0/255.0, blue: 152.0/255.0, alpha: 1.0)]
        let attr1 = [ NSFontAttributeName : UIFont(name:"OpenSans-ExtraBold", size: 14)!,
                      NSForegroundColorAttributeName :  UIColor.white]
        
//        let strAtt1 = NSAttributedString.init(string: "\(str1!) ", attributes: attr2)
//        let strAtt2 = NSAttributedString.init(string: str2!, attributes: attr1)
        let strAtt1 = NSAttributedString.init(string: "\(str1!) ", attributes: attr1)
        let strAtt2 = NSAttributedString.init(string: str2!, attributes: attr2)
        
        let finalStr = NSMutableAttributedString()
        finalStr.append(strAtt1)
        finalStr.append(strAtt2)
        
        cell.titleLabel.attributedText = finalStr
        
        cell.numberLabel.text = String(dict["totalcount"] as! Int)
        
        let image = dict["GalleryImage"] as? String
        let updateURL = image?.replacingOccurrences(of: " ", with: "%20")
        
        cell.backImage.image = nil
        cell.backImage.sd_setImage(with: URL.init(string: updateURL!), placeholderImage: UIImage.init(named: "product_placeholder.jpg"))
        cell.backImage.setShowActivityIndicator(true)
        cell.backImage.setIndicatorStyle(.gray)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dict = self.displayArray[indexPath.section] as! Dictionary<String,AnyObject>
        let desination = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "GalleryPhotoCollectionVC") as! GalleryPhotoCollectionVC
        desination.galleryyType = 1
        desination.masterID = dict["GalleryMasterID"] as! Int
        desination.subID = dict["MetorId"] as! Int
        
        desination.namLabeltext = dict["GalleryName"] as! String
        
        let image = dict["GalleryImage"] as? String
        let updateURL = image?.replacingOccurrences(of: " ", with: "%20")
        desination.userImage = updateURL!
        
        self.navigationController?.pushViewController(desination, animated: true)

    }

    
}
