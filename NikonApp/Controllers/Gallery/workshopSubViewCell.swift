//
//  OtherGalleryCell.swift
//  NikonApp
//
//  Created by Surbhi on 09/01/17.
//  Copyright © 2017 Neuronimbus. All rights reserved.
//

import UIKit

class workshopSubViewCell: UICollectionViewCell {
    
    @IBOutlet weak var backImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var numberLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
