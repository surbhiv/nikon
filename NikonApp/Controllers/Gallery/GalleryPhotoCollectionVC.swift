//
//  GalleryPhotoCollectionVC.swift
//  NikonApp
//
//  Created by Surbhi on 09/01/17.
//  Copyright © 2017 Neuronimbus. All rights reserved.
//

import UIKit

class GalleryPhotoCollectionVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {

    //MARK:- IBOUTLETS
    
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var detailCollection: UICollectionView!
    
    
    
    
    //MARK:- VARIABLE DEclaration
    var galleryyType = Int()
    var masterID = Int()
    var subID = Int()
    var levelId = Int() // needed only for workshop
    var dataArray = [AnyObject]()
    var namLabeltext = String()
    var userImage = String()
    var infoLabel: UILabel!
    
    
    //MARK:- START
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        self.setNavigationBar()
        nameLabel.text = namLabeltext
        profileImage.sd_setImage(with: URL.init(string: userImage), placeholderImage: UIImage.init(named: "profilePlaceTemp"))
        profileImage.layer.borderWidth = 0
        addInfoLabel()
        self.getData()
        //addInfoLabel()
        // Do any additional setup after loading the view.
    }

    
   
    func addInfoLabel() {
        
        //        let attributedString = NSMutableAttributedString(string: "NOTIFICATION NOT AVAILABLE!")
        //        attributedString.addAttributes([NSFontAttributeName: UIFont.systemFontOfSize(24.0)], range: NSMakeRange(0, 27))
        
        infoLabel = UILabel()
        //        infoLabel.attributedText = attributedString
        infoLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
        infoLabel.numberOfLines = 4
        infoLabel.textColor = UIColor.black
        infoLabel.font = UIFont(name: "OpenSans", size: 18)
        infoLabel.text = "No Pic Available."
        infoLabel.textAlignment = NSTextAlignment.center
        infoLabel.frame = CGRect(x: (self.view.frame.size.width/2) - 100, y: (self.view.frame.size.height/2) - 150, width: 200, height: 100)
        //        tblNotification.addSubview(infoLabel)
        self.view.addSubview(infoLabel)
        self.infoLabel.isHidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: - Navigation
    func OpenSideMenu(_ sender: AnyObject){
        
        self.view.endEditing(true)
        toggleSideMenuView()
    }
    func PopView(_ sender: AnyObject){
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func setNavigationBar(){
        self.navigationItem.hidesBackButton = true
        let navigationViewFrame = ScreenSize.SCREEN//self.navigationController?.navigationBar.frame
        let navigationView = UIView.init(frame: CGRect(x: 0, y: 0, width: navigationViewFrame.size.width, height: 44))
        navigationView.backgroundColor = UIColor.clear
        let menuButton = UIButton.init(type: .custom)
        
        menuButton.frame = CGRect(x: ScreenSize.SCREEN_WIDTH - 45, y: 12, width: 30, height: 30)
        menuButton.setImage(UIImage.init(named: "sideMenu_Icon"), for: UIControlState())
        menuButton.addTarget(self, action: #selector(OpenSideMenu), for: .touchUpInside)
        navigationView.addSubview(menuButton)
//        self.navigationController?.navigationBar.tintColor = Colors.appPink
        
        let titleLabel = UILabel.init(frame:CGRect(x: 60,y: 0,width: ( (navigationViewFrame.size.width) - 110),height: 44))
        titleLabel.textColor = UIColor.black
        titleLabel.text = "GALLERY"
        titleLabel.font = UIFont(name:"OpenSans-SemiBold", size: 15)!
        titleLabel.textAlignment = .center
        navigationView.addSubview(titleLabel)
        
         let logoImage = UIImageView.init(frame: CGRect(x: 25, y: 6, width: 33, height: 33))
        logoImage.image = UIImage.init(named: "nikon_login_logo")
        navigationView.addSubview(logoImage)
        
        let backButton = UIButton.init(type: .custom)
        backButton.frame = CGRect(x: 0, y: 12, width: 20, height: 20)
        backButton.backgroundColor = UIColor.white
        backButton.setImage(UIImage.init(named: "back"), for: UIControlState())
        backButton.addTarget(self, action: #selector(PopView), for: .touchUpInside)
        navigationView.addSubview(backButton)
        
        self.navigationItem.titleView = navigationView
    }

    //MARK:- GETDATA
    func getData(){
        if Reachability.isConnectedToNetwork()
        {
            var method = String()
            if galleryyType == 0 {
                method = "WorkshopGallery?ID=\(levelId)"
            }
            else if galleryyType == 1 {
                method = "ProGallery?GalleryMasterId=\(masterID)&MentorId=\(subID)"
            }
            else if galleryyType == 2 {
                method = "UserGallery?GalleryMasterId=\(masterID)&UserId=\(subID)"
            }
            else if galleryyType == 3 {
                method = "GetMentorGallery?GalleryMasterId=\(masterID)&MentorId=\(subID)"
            }

            Constants.appDelegate.startIndicator()
            Server.getRequestWithURL(Constants.BASEURL+method, completionHandler: { (response) in
                
                if response[Constants.STATE] != nil && response[Constants.STATE] as! String == Constants.SUCC {

                    self.dataArray = response["galleryList"] as! Array<AnyObject>
                    DispatchQueue.main.async
                    {
                        Constants.appDelegate.stopIndicator()
                        
                        self.detailCollection.isHidden = false
                        self.detailCollection.reloadData()
                        self.infoLabel.isHidden = true
                    }
                }
                else {
                    Constants.appDelegate.stopIndicator()
                    
//                    var mess = String()
//                    if response[Constants.MESS] != nil{
//                        mess = response[Constants.MESS] as! String
//                    }
//                    else{
//                        mess = response["Message"] as! String
//                    }
                    DispatchQueue.main.async
                    {
                        self.detailCollection.isHidden = true
                        self.infoLabel.isHidden = false
                        self.view.bringSubview(toFront: self.infoLabel)

//                    let alert = UIAlertController.init(title: "Nikon", message: mess, preferredStyle: UIAlertControllerStyle.Alert)
//                    alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.Default, handler: { (action:UIAlertAction) in
//                        self.dismissViewControllerAnimated(true, completion: nil)
                    }//))
//                    self.presentViewController(alert, animated: true, completion: nil)
                }
            })
        }
        else
        {
            Constants.appDelegate.stopIndicator()
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
    }
    

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: ((ScreenSize.SCREEN_WIDTH - 20)/2),height: ((ScreenSize.SCREEN_WIDTH - 20)/2 - 60))
    }

    //MARK:- UICOLLECTIONVIEW Delegate
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let dict = self.dataArray[indexPath.item]//self.dataArray[indexPath.item] as! Dictionary

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "detailCollectionCell", for: indexPath)
        let imagevw = cell.contentView.viewWithTag(1501) as! UIImageView
        
        let image = dict["Gallery_Big_Img"] as! String
        let updateURL = image.replacingOccurrences(of: " ", with: "%20")
        
        imagevw.sd_setImage(with: URL.init(string: updateURL), placeholderImage: UIImage.init(named: "product_placeholder.jpg"))
        imagevw.setShowActivityIndicator(true)
        imagevw.setIndicatorStyle(.gray)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    if (UserDefaults.standard.integer(forKey: Constants.USERID) != 0)
    {
        let dict = self.dataArray[indexPath.item]

        let desination = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "GalleryPhotodetailVC") as! GalleryPhotodetailVC
        desination.type = self.galleryyType
        desination.masterID = dict["GalleryMasterId"] as! Int
        desination.userGalleryID = dict["UserGalleryID"] as! Int
        desination.mentorID = 0

        self.navigationController?.pushViewController(desination, animated: true)
    }
    else
    {
      self.loginAlert()
    }
    }
    //MARK:- LoginAlert
    
    func loginAlert()  {
        let alert = UIAlertController.init(title: "Nikon", message: "Please login to continue.", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
            
            DispatchQueue.main.async
            {
                let destinationController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "LoginVC")as! LoginVC
                destinationController.isWantTodismissVC = true
                self.present(destinationController, animated: true, completion: nil);
            }
        }))
        alert.addAction(UIAlertAction.init(title: "Cancel", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
            self.dismiss(animated: true, completion: nil)
        }))
        self.present(alert, animated: true, completion: nil)
    }
  
}
