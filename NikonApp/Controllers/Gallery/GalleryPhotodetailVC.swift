//
//  GalleryPhotodetailVC.swift
//  NikonApp
//
//  Created by Surbhi on 09/01/17.
//  Copyright © 2017 Neuronimbus. All rights reserved.
//

import UIKit

class GalleryPhotodetailVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextViewDelegate {
    
    
    
    @IBOutlet weak var detailtable: UITableView!
    
    var masterID = Int()
    var mentorID = Int()
    var userID = Int()
    var userGalleryID = Int()
    var options = String()
    var commentType = String()
    var isLike = Int()
    var likeType = String()
    var imageFinalURL = String()

    var type = Int()// 0,1,2,3
    
    var commentArray = [AnyObject]()
    var photoDetailDict = Dictionary<String,AnyObject>()
    
    
    //UI VARIABLES
    var commentText = UITextView()
    var submitButton = UIButton()
    var likeButton = UIButton()
    var comButton = UIButton()
    var shareButton = UIButton()
    var likeLabel = UILabel()
    var commentLabel = UILabel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        self.setNavigationBar()
        
        userID = UserDefaults.standard.integer(forKey: Constants.USERID)
        
        if self.type == 0 {
            options = "workshop"
            commentType = "W"
        }else if self.type == 1{
            options = "pro"
            commentType = "M"
        }else if self.type == 2{
            options = "user"
            commentType = "U"
        }else if self.type == 3{
            options = "mentor"
            commentType = "M"
        }
        
        
        getdata()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func OpenSideMenu(_ sender: AnyObject){
        
        self.view.endEditing(true)
        toggleSideMenuView()
    }
    
    func PopView(_ sender: AnyObject){
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func setNavigationBar(){
        self.navigationItem.hidesBackButton = true
        let navigationViewFrame = ScreenSize.SCREEN//self.navigationController?.navigationBar.frame
        let navigationView = UIView.init(frame: CGRect(x: 0, y: 0, width: navigationViewFrame.size.width, height: 44))
        navigationView.backgroundColor = UIColor.clear
        let menuButton = UIButton.init(type: .custom)
        
        menuButton.frame = CGRect(x: ScreenSize.SCREEN_WIDTH - 45, y: 12, width: 30, height: 30)
        menuButton.setImage(UIImage.init(named: "sideMenu_Icon"), for: UIControlState())
        menuButton.addTarget(self, action: #selector(OpenSideMenu), for: .touchUpInside)
        navigationView.addSubview(menuButton)
//        self.navigationController?.navigationBar.tintColor = Colors.appPink
        
        let titleLabel = UILabel.init(frame:CGRect(x: 60,y: 0,width: ( (navigationViewFrame.size.width) - 110),height: 44))
        titleLabel.textColor = UIColor.black
        titleLabel.text = "GALLERY DETAILS"
        titleLabel.font = UIFont(name:"OpenSans-SemiBold", size: 15)!
        titleLabel.textAlignment = .center
        navigationView.addSubview(titleLabel)
        
         let logoImage = UIImageView.init(frame: CGRect(x: 25, y: 6, width: 33, height: 33))
        logoImage.image = UIImage.init(named: "nikon_login_logo")
        navigationView.addSubview(logoImage)
        
        let backButton = UIButton.init(type: .custom)
        backButton.frame = CGRect(x: 0, y: 12, width: 20, height: 20)
        backButton.backgroundColor = UIColor.white
        backButton.setImage(UIImage.init(named: "back"), for: UIControlState())
        backButton.addTarget(self, action: #selector(PopView), for: .touchUpInside)
        navigationView.addSubview(backButton)
        
        self.navigationItem.titleView = navigationView
        
    }
    
    //MARK:- GET DATA
    func getdata(){
        if Reachability.isConnectedToNetwork()
        {
            let method = "GalleryPopup?GalleryMasterId=\(masterID)&MentorId=\(mentorID)&UserID=\(userID)&UserGalleryID=\(userGalleryID)&options=\(options)"
            Constants.appDelegate.startIndicator()
            Server.getRequestWithURL(Constants.BASEURL+method, completionHandler: { (response) in
                
                Constants.appDelegate.stopIndicator()
                if response[Constants.STATE] != nil && response[Constants.STATE] as! String == Constants.SUCC {
                    
                   // print( response)
                    self.photoDetailDict = response
                   // print(self.photoDetailDict.count)
                    self.commentArray = self.photoDetailDict["GetComments"] as! Array<AnyObject>
                    
                    DispatchQueue.main.async
                    {
//                        if self.commentArray.count > 0{
                            self.detailtable.isHidden = false
                            self.detailtable.reloadData()
//                        }
//                        else{
//                            self.detailtable.hidden = true
//                        }
                    }
                }
                else {
                    
                    self.detailtable.isHidden = true
                    var mess = String()
                    if response[Constants.MESS] != nil{
                        mess = response[Constants.MESS] as! String
                    }
                    else{
                        mess = response["Message"] as! String
                    }
                    DispatchQueue.main.async
                    {
                    let alert = UIAlertController.init(title: "Nikon", message: mess, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                        self.dismiss(animated: true, completion: nil)
                    }))
                    self.present(alert, animated: true, completion: nil)
                    }
                }
            })
        }
        else
        {
            self.detailtable.isHidden = true
            Constants.appDelegate.stopIndicator()
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
    }
    
    
    //MARK:- TABLEVIEW
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 || section == 1 {
            if photoDetailDict.count == 0 {
                return 0
            }
            return 1
        }
        else{
            return commentArray.count
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0{
            if options == "workshop"
            {
                return 410
            }
            return 450
        }
        else if indexPath.section == 1{
            return 160
        }
        else{
            
            let dict = commentArray[indexPath.row] as! Dictionary<String,AnyObject>
            let text = dict["comments"] as? String
            
            let fon = UIFont.init(name: "OpenSans", size: 14.0)
            return 35 + self.getHeightForLabel(text!,fon: fon!)
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "PhotoDescriptionCell", for: indexPath)
            
            let PhotoImage = cell.contentView.viewWithTag(1601) as! UIImageView
            let imageURL = photoDetailDict["Gallery_Big_Img"] as? String
            var updateURL = imageURL?.replacingOccurrences(of: " ", with: "%20")
            if updateURL == nil{
                updateURL = ""
            }
            
            PhotoImage.sd_setImage(with: URL.init(string: updateURL!), placeholderImage: UIImage.init(named: "product_placeholder.jpg"))
            PhotoImage.setShowActivityIndicator(true)
            PhotoImage.setIndicatorStyle(.gray)
            imageFinalURL = updateURL!
            
            likeButton = cell.contentView.viewWithTag(1602) as! UIButton
            likeButton.addTarget(self, action: #selector(LikePhoto), for: .touchUpInside)

            likeLabel = cell.contentView.viewWithTag(1603) as! UILabel
            let likeCoun = photoDetailDict["LikeCount"] as! Int
            likeLabel.text = "\(likeCoun) Likes this"
            self.isLike = photoDetailDict["like"] as! Int
            if self.isLike == 0{
                likeButton.isSelected = false
            }
            else{
                likeButton.isSelected = true
            }
            
            comButton = cell.contentView.viewWithTag(1604) as! UIButton
            comButton.addTarget(self, action: #selector(CommentClicked), for: .touchUpInside)
            commentLabel = cell.contentView.viewWithTag(1605) as! UILabel
            let coun = commentArray.count
            commentLabel.text = "\(coun) Comments"
            
            shareButton = cell.contentView.viewWithTag(1606) as! UIButton
            shareButton.addTarget(self, action: #selector(SharePressed), for: .touchUpInside)

            
            
            let profileImage = cell.contentView.viewWithTag(1607) as! UIImageView
            let pimageURL = photoDetailDict["ProfileImage"] as? String
            var updateURL_profile = pimageURL?.replacingOccurrences(of: " ", with: "%20")
            if updateURL_profile == nil{
                updateURL_profile = ""
            }

            profileImage.sd_setImage(with: URL.init(string: updateURL_profile!), placeholderImage: UIImage.init(named: "product_placeholder.jpg"))
            profileImage.setShowActivityIndicator(true)
            profileImage.setIndicatorStyle(.gray)
            
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped))
            
            
            PhotoImage.addGestureRecognizer(tapGestureRecognizer)

            
            let name = cell.contentView.viewWithTag(1608) as! UILabel
            name.text = photoDetailDict["Username"] as? String
            let description = cell.contentView.viewWithTag(1609) as! UILabel
            description.text = photoDetailDict["Gallery_Title"] as? String
            
            
            //for lense section
            let lensView = cell.contentView.viewWithTag(1691)
            if (options != "workshop")
            {
            let lensName = cell.contentView.viewWithTag(1653) as! UILabel
            lensName.text = photoDetailDict["LenseName"] as? String
            let apartureName = cell.contentView.viewWithTag(1655) as! UILabel
            apartureName.text = photoDetailDict["Exposure"] as? String
            let shutterSpeed = cell.contentView.viewWithTag(1656) as! UILabel
            shutterSpeed.text = photoDetailDict["Shoton"] as? String
            let ISOName = cell.contentView.viewWithTag(1658) as! UILabel
            ISOName.text = "\(photoDetailDict["Iso"]!)"
                lensView?.isHidden = false
            }
            else
            {
                lensView?.isHidden = true

                for const in (lensView?.constraints)! {
                    if const.identifier == "LensViewHeight"
                    {
                        const.constant = 0.0
                    }
                }
            }
            return cell
        }
        else if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "PhotoWriteCommentCell", for: indexPath)
            
            commentText = cell.contentView.viewWithTag(1621) as! UITextView
            commentText.delegate = self
            submitButton = cell.contentView.viewWithTag(1622) as! UIButton
            commentText.text = "WRITE YOUR COMMENT ..."
            submitButton.addTarget(self, action: #selector(PostComment), for: .touchUpInside)
            return cell
        }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "PhotoCommentCell", for: indexPath)
            let dict = commentArray[indexPath.row] as! Dictionary<String,AnyObject>
            let name = cell.contentView.viewWithTag(1611) as! UILabel
            name.text = dict["user_fname"] as? String
            let comment = cell.contentView.viewWithTag(1612) as! UILabel
            comment.text = dict["comments"] as? String
            return cell
        }
    }
    
    
    func getHeightForLabel(_ text : String, fon : UIFont) -> CGFloat{
        
        let wide = ScreenSize.SCREEN_WIDTH - 20
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: wide, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.font = fon
        label.text = text
        label.sizeToFit()
        return label.frame.height
    }
    
    
    @IBAction func imageTapped(_ sender: AnyObject) {
        let topview = self.parent
        let  destVC  = Constants.mainStoryboard.instantiateViewController(withIdentifier: "ProductImageZoomVC")as! ProductImageZoomVC
        destVC.imageURLStr = imageFinalURL
        topview!.addChildViewController(destVC)
        destVC.didMove(toParentViewController: topview)
        topview?.view.addSubview(destVC.view)
    }
    //MARK:- TEXTVIEW Delegate
    func textViewDidBeginEditing(_ textView: UITextView) {
        commentText.text = ""
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if commentText.text.trimmingCharacters(
            in: CharacterSet.whitespacesAndNewlines).isEmpty == true{
            commentText.text = "WRITE YOUR COMMENT ..."
        }
    }
    
    
    //MARK:- POST Comment
    @IBAction func PostComment(_ sender: AnyObject){
        if commentText.text == "" || commentText.text == "WRITE YOUR COMMENT ..." {
            //            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "Nikon", "message":"Please enter your comment"])
        }
        else{
            let method = "PostComment"
            let param = "GalleryID=\(self.userGalleryID)&UserID=\(userID)&Comments=\(commentText.text!)&CommentType=\(commentType)"
            
            if Reachability.isConnectedToNetwork() {
                Constants.appDelegate.startIndicator()
                
                Server.postRequestWithURL(Constants.BASEURL+method, paramString: param) { (response) in
                    
                    if response[Constants.STATE] != nil && response[Constants.STATE] as! String == Constants.SUCC {
                        
                        Constants.appDelegate.stopIndicator()
                        
                        DispatchQueue.main.async(execute: {//response["message"]! as? String
                            let alert = UIAlertController.init(title: "Nikon", message: "Comment Posted Successfully !", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                                self.dismiss(animated: true, completion: nil)
                                self.getdata()
                            }))
                            self.present(alert, animated: true, completion: nil)
                        })
                    }
                    else {
                        Constants.appDelegate.stopIndicator()
                        var mess = String()
                        if response[Constants.MESS] != nil{
                            mess = response[Constants.MESS] as! String
                        }
                        else{
                            mess = response["Message"] as! String
                        }
                        CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "Nikon", "message": mess] )
                    }
                }
            }
            else {
                CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
            }
            
        }
    }
    
    //MARK:- LIKE
    @IBAction func LikePhoto(_ sender: AnyObject){
        
        let method = "PostLike"
        var likeTosend = Int()
        if isLike == 0 {
            likeTosend = 1
        }
        else{
            likeTosend = 0
        }
        
        let param = "UserGalleryID=\(self.userGalleryID)&UserID=\(userID)&Liketype=\(commentType)&IsLike=\(likeTosend)"
        
        if Reachability.isConnectedToNetwork() {
            Constants.appDelegate.startIndicator()
            
            Server.postRequestWithURL(Constants.BASEURL+method, paramString: param) { (response) in
                
                if response[Constants.STATE] != nil && response[Constants.STATE] as! String == Constants.SUCC {
                    
                    Constants.appDelegate.stopIndicator()
                    self.getdata()
                }
                else {
                    Constants.appDelegate.stopIndicator()
                    var mess = String()
                    if response[Constants.MESS] != nil{
                        mess = response[Constants.MESS] as! String
                    }
                    else{
                        mess = response["Message"] as! String
                    }
                    CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "Nikon", "message": mess] )
                }
            }
        }
        else {
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
    }
    
    //MARK:- COMMENT Clicked
    func CommentClicked(){
        self.commentText.becomeFirstResponder()
    }
    
    //MARK:- SHARE
    @IBAction func SharePressed(_ sender: AnyObject){
        
        let imageURL = photoDetailDict["Gallery_Big_Img"] as? String
        var updateURL = imageURL?.replacingOccurrences(of: " ", with: "%20")
        if updateURL == nil{
            updateURL = ""
        }
        else{
            // set up activity view controller
            let shareSheet = UIActivityViewController.init(activityItems: [updateURL!], applicationActivities: nil)//[title!,instructionString,myWebsite!]
            let excludeActivities = [UIActivityType.airDrop,UIActivityType.print,UIActivityType.assignToContact,UIActivityType.saveToCameraRoll,UIActivityType.addToReadingList]
            shareSheet.excludedActivityTypes = excludeActivities;
            
            shareSheet.popoverPresentationController?.sourceView = self.view
            shareSheet.setValue("Whirlpool Culineria", forKey: "subject")
            self.present(shareSheet, animated: true, completion: nil)
        }
        
        
    }
}
