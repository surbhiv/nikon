//
//  GalleryWorkShopSubVC.swift
//  NikonApp
//
//  Created by Surbhi on 09/01/17.
//  Copyright © 2017 Neuronimbus. All rights reserved.
//

import UIKit

class GalleryWorkShopSubVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout  {

    
    //MARK:- IBOUTLETS
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var workshopCollection: UICollectionView!

    
    //MARK:- VARIABLE DEclaration
    var levelId = Int()
    var workShopArray = [AnyObject]()
    var namLabeltext = String()
    var imageString = String()
    var infoLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false

        
        nameLabel.text = namLabeltext

        profileImage.sd_setImage(with: URL.init(string: imageString), placeholderImage: UIImage.init(named: "product_placeholder.jpg"))
        addInfoLabel()
        self.setNavigationBar()
        self.getData()

        // Do any additional setup after loading the view.
    }
    func addInfoLabel() {
        
        //        let attributedString = NSMutableAttributedString(string: "NOTIFICATION NOT AVAILABLE!")
        //        attributedString.addAttributes([NSFontAttributeName: UIFont.systemFontOfSize(24.0)], range: NSMakeRange(0, 27))
        
        infoLabel = UILabel()
        //        infoLabel.attributedText = attributedString
        infoLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
        infoLabel.numberOfLines = 4
        infoLabel.textColor = UIColor.black
        infoLabel.font = UIFont(name: "OpenSans", size: 18)
        infoLabel.text = "No Pic Available."
        infoLabel.textAlignment = NSTextAlignment.center
        infoLabel.frame = CGRect(x: (self.view.frame.size.width/2) - 100, y: (self.view.frame.size.height/2) - 150, width: 200, height: 100)
        //        tblNotification.addSubview(infoLabel)
        self.view.addSubview(infoLabel)
        self.infoLabel.isHidden = true
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Navigation
    func OpenSideMenu(_ sender: AnyObject){
        
        self.view.endEditing(true)
        toggleSideMenuView()
    }
    func PopView(_ sender: AnyObject){
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func setNavigationBar(){
        self.navigationItem.hidesBackButton = true
        let navigationViewFrame = ScreenSize.SCREEN//self.navigationController?.navigationBar.frame
        let navigationView = UIView.init(frame: CGRect(x: 0, y: 0, width: navigationViewFrame.size.width, height: 44))
        navigationView.backgroundColor = UIColor.clear
        let menuButton = UIButton.init(type: .custom)
        
        menuButton.frame = CGRect(x: ScreenSize.SCREEN_WIDTH - 45, y: 12, width: 30, height: 30)
        menuButton.setImage(UIImage.init(named: "sideMenu_Icon"), for: UIControlState())
        menuButton.addTarget(self, action: #selector(OpenSideMenu), for: .touchUpInside)
        navigationView.addSubview(menuButton)
//        self.navigationController?.navigationBar.tintColor = Colors.appPink
        
        let titleLabel = UILabel.init(frame:CGRect(x: 60,y: 0,width: ( (navigationViewFrame.size.width) - 110),height: 44))
        titleLabel.textColor = UIColor.black
        titleLabel.text = "GALLERY"
        titleLabel.font = UIFont(name:"OpenSans-SemiBold", size: 15)!
        titleLabel.textAlignment = .center
        navigationView.addSubview(titleLabel)
        
         let logoImage = UIImageView.init(frame: CGRect(x: 25, y: 6, width: 33, height: 33))
        logoImage.image = UIImage.init(named: "nikon_login_logo")
        navigationView.addSubview(logoImage)
        
        let backButton = UIButton.init(type: .custom)
        backButton.frame = CGRect(x: 0, y: 12, width: 20, height: 20)
        backButton.backgroundColor = UIColor.white
        backButton.setImage(UIImage.init(named: "back"), for: UIControlState())
        backButton.addTarget(self, action: #selector(PopView), for: .touchUpInside)
        navigationView.addSubview(backButton)
        
        self.navigationItem.titleView = navigationView
    }
    
    //MARK:- GETDATA
    func getData(){
        if Reachability.isConnectedToNetwork()
        {
            let method =  "NikkonGallery/WorkshopGallery?Level_id=\(levelId)"
            
            Constants.appDelegate.startIndicator()
            Server.getRequestWithURL(Constants.BASEURL+method, completionHandler: { (response) in
                
                if response[Constants.STATE] != nil && response[Constants.STATE] as! String == Constants.SUCC {
                    
                    self.workShopArray = response["GetList"] as! Array<AnyObject>
                    DispatchQueue.main.async
                    {
                        
                        Constants.appDelegate.stopIndicator()
                        self.workshopCollection.isHidden = false
                        self.workshopCollection.reloadData()
                        self.infoLabel.isHidden = true
                    }
                }
                else {
                    Constants.appDelegate.stopIndicator()
//                    self.workshopCollection.hidden = true
//                    var mess = String()
//                    if response[Constants.MESS] != nil{
//                        mess = response[Constants.MESS] as! String
//                    }
//                    else{
//                        mess = response["Message"] as! String
//                    }
                    DispatchQueue.main.async
                    {
                        self.workshopCollection.isHidden = true
                        self.infoLabel.isHidden = false
                        self.view.bringSubview(toFront: self.infoLabel)
                        
//                    let alert = UIAlertController.init(title: "Nikon", message: mess, preferredStyle: UIAlertControllerStyle.Alert)
//                    alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.Default, handler: { (action:UIAlertAction) in
//                        self.dismissViewControllerAnimated(true, completion: nil)
//                    }))
//                    self.presentViewController(alert, animated: true, completion: nil)
                    }
                }
            })
        }
        else
        {
            Constants.appDelegate.stopIndicator()
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
    }

    //MARK:- COllection View
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: ((ScreenSize.SCREEN_WIDTH - 20)/2),height: ((ScreenSize.SCREEN_WIDTH - 20)/2) )
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return workShopArray.count
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "workshopSubViewCell", for: indexPath) as! workshopSubViewCell
        let dict = self.workShopArray[indexPath.row] as! Dictionary<String,AnyObject>
        
        let str = dict["albumname"] as? String
        var arr = str?.components(separatedBy: " ")
        let str2 = arr?.first
        arr?.removeFirst()
        let str1 = arr?.joined(separator: " ")
        
        let attr2 = [ NSFontAttributeName : UIFont(name:"OpenSans", size: 14)!,
                      NSForegroundColorAttributeName : UIColor.white]
        let attr1 = [ NSFontAttributeName : UIFont(name:"OpenSans-ExtraBold", size: 14)!,
                      NSForegroundColorAttributeName :  UIColor.init(red: 36.0/255.0, green: 36.0/255.0, blue: 36.0/255.0, alpha: 1.0)]
        
        let strAtt1 = NSAttributedString.init(string: "\(str!) ", attributes: attr2)
        let strAtt2 = NSAttributedString.init(string: str2!, attributes: attr2)
        let finalStr = NSMutableAttributedString()
        finalStr.append(strAtt1)
//        finalStr.appendAttributedString(strAtt2)
        
        cell.titleLabel.attributedText = finalStr
        cell.numberLabel.text = String(dict["totalcount"] as! Int)
        let image = dict["albumimage"] as? String
        let updateURL = image?.replacingOccurrences(of: " ", with: "%20")
        cell.backImage.image = nil
        cell.backImage.sd_setImage(with: URL.init(string: updateURL!), placeholderImage: UIImage.init(named: "product_placeholder.jpg"))
        cell.backImage.setShowActivityIndicator(true)
        cell.backImage.setIndicatorStyle(.gray)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let dict = self.workShopArray[indexPath.row] as! Dictionary<String,AnyObject>
        
        let desination = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "GalleryPhotoCollectionVC") as! GalleryPhotoCollectionVC
        desination.galleryyType = 0
        desination.levelId = dict["ID"] as! Int
        desination.namLabeltext = dict["albumname"] as! String
        
        let image = dict["albumimage"] as? String
        let updateURL = image?.replacingOccurrences(of: " ", with: "%20")
        desination.userImage = updateURL!
        
        self.navigationController?.pushViewController(desination, animated: true)
        
    }


}
