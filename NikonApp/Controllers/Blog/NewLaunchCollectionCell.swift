//
//  NewLaunchCollectionCell.swift
//  NikonApp
//
//  Created by Surbhi on 09/01/17.
//  Copyright © 2017 Neuronimbus. All rights reserved.
//

import UIKit

class NewLaunchCollectionCell: UICollectionViewCell {
 
    @IBOutlet weak var readMoreButton: UIButton!
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var subHeaderLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var imageVw: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
