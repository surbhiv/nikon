//
//  FeaturedCollectionCell.swift
//  NikonApp
//
//  Created by Surbhi on 03/01/17.
//  Copyright © 2017 Neuronimbus. All rights reserved.
//

import UIKit

class FeaturedCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var readMoreButton: UIButton!
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var subHeaderLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var imageVw: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
}