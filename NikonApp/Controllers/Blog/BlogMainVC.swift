                                                                                                    //
//  BlogMainVC.swift
//  NikonApp
//
//  Created by Surbhi on 03/01/17.
//  Copyright © 2017 Neuronimbus. All rights reserved.
//

import UIKit

extension String {
    
    var html2AttributedString: NSAttributedString? {
        guard
            let data = data(using: String.Encoding.utf8)
            else { return nil }
        do {
            return try NSAttributedString(data: data, options: [NSDocumentTypeDocumentAttribute:NSHTMLTextDocumentType,NSCharacterEncodingDocumentAttribute:String.Encoding.utf8], documentAttributes: nil)
        } catch let error as NSError {
            print(error.localizedDescription)
            return  nil
        }
    }
    var html2String: String {
        
        var str = html2AttributedString?.string
        str = str!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        return str ?? ""
    }
}

class BlogMainVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UIScrollViewDelegate, UICollectionViewDelegateFlowLayout{

    @IBOutlet weak var topCollection: UICollectionView!
    @IBOutlet weak var topPageControl: UIPageControl!
    
    @IBOutlet weak var featuredCollection: UICollectionView!
    @IBOutlet weak var featuredPageControl: UIPageControl!
    
    @IBOutlet weak var newLaunchLabel: UILabel!
    @IBOutlet weak var loadMoreButton: UIButton!
    
    @IBOutlet weak var newLaunchCollection: UICollectionView!
    @IBOutlet weak var newLaunchPageControl: UIPageControl!

    //SCROLL
    @IBOutlet weak var headerScroll: UIScrollView!
    @IBOutlet weak var detailScroll: UIScrollView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var widthConstraint: NSLayoutConstraint!
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    @IBOutlet weak var mainView_heightConstraint: NSLayoutConstraint!

    
    @IBOutlet weak var mainCollectionHeight: NSLayoutConstraint!
    @IBOutlet weak var featuredCollectionHeight: NSLayoutConstraint!
    @IBOutlet weak var newLaunchCollectionHeight: NSLayoutConstraint!

    
    var isCompleteLoad : Bool = false
    var isFromMenu = Bool()
    
    fileprivate var indicatorLabel : UILabel!
    fileprivate var ControllerArray = [AnyObject]()
    fileprivate var headerArray = ["RECENT ARTICLES","TUTORIAL BLOG"]

    var recentVC = BlogCollectionVC()
    var tutorialVC = BlogCollectionVC()

    
//    @IBOutlet weak var contentView: UIView!
//        = Dictionary<String,AnyObject>()
//            = [AnyObject]()
    
    var timer = Timer()
    
    var mainBlogArray = [AnyObject]()
    var featuredBlogArray = [AnyObject]()
    var recentBlogArray = [AnyObject]()
    var tutorialBlogArray = [AnyObject]()
    var newLaunchBlogArray = [AnyObject]()

    var selectedIndex : Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        DispatchQueue.main.async{
            self.setNavigationBar()
        }
        
        getdata()
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func OpenSideMenu(_ sender: AnyObject){
        
        self.view.endEditing(true)
        toggleSideMenuView()
    }
    
    func setNavigationBar(){
        let navigationViewFrame = self.navigationController?.navigationBar.frame
        let navigationView = UIView.init(frame: navigationViewFrame!)
        navigationView.backgroundColor = UIColor.white
        let menuButton = UIButton.init(type: .custom)
        menuButton.frame = CGRect(x: ScreenSize.SCREEN_WIDTH - 45, y: 7, width: 25, height: 25)
        menuButton.setImage(UIImage.init(named: "sideMenu_Icon"), for: UIControlState())
        menuButton.addTarget(self, action: #selector(OpenSideMenu), for: .touchUpInside)
        navigationView.addSubview(menuButton)
        
//        self.navigationController?.navigationBar.tintColor = Colors.appPink
        
        let titleLabel = UILabel.init(frame:CGRect(x: ( ScreenSize.SCREEN_WIDTH - 150)/2,y: 0,width: 150,height: 44))
        titleLabel.textColor = UIColor.black
        titleLabel.text="BLOG"
        titleLabel.font = UIFont(name:"OpenSans-SemiBold", size: 15)!
        titleLabel.textAlignment = .center
        navigationView.addSubview(titleLabel)
        
        
//       let logoImage = UIImageView.init(frame: CGRectMake(25, 6, 33, 33))
//        logoImage.image = UIImage.init(named: "nikon_login_logo")
//        navigationView.addSubview(logoImage)
        if isFromMenu == false
        {
            let backButton = UIButton.init(type: .custom)
            backButton.frame = CGRect(x: 0, y: 9, width: 20, height: 20)
            backButton.backgroundColor = UIColor.white
            backButton.setImage(UIImage.init(named: "back"), for: UIControlState())
            backButton.addTarget(self, action: #selector(PopView), for: .touchUpInside)
            navigationView.addSubview(backButton)
            
            let logoImage = UIImageView.init(frame: CGRect(x: 25, y: 6, width: 33, height: 33))
            logoImage.image = UIImage.init(named: "nikon_login_logo")
            navigationView.addSubview(logoImage)
            
            
        }
        else
        {
            let logoImage = UIImageView.init(frame: CGRect(x: 0, y: 6, width: 33, height: 33))
            logoImage.image = UIImage.init(named: "nikon_login_logo")
            navigationView.addSubview(logoImage)
        }

        //}
        
        self.navigationItem.titleView = navigationView
    }
    func PopView(_ sender: AnyObject){
        
        self.navigationController?.popViewController(animated: true)
    }

    //MARK:- get Data
    func getdata(){
        if Reachability.isConnectedToNetwork()
        {
            let method = "GetBlog"
            Constants.appDelegate.startIndicator()
            Server.getRequestWithURL(Constants.BASEURL+method, completionHandler: { (response) in
                
                Constants.appDelegate.stopIndicator()
                if response[Constants.STATE] != nil && response[Constants.STATE] as! String == Constants.SUCC {
                    
                    self.mainBlogArray = response["MainBlog"] as! Array<AnyObject>
                    self.featuredBlogArray = response["FeaturedBlog"] as! Array<AnyObject>
                    self.recentBlogArray = response["RecentBlog"] as! Array<AnyObject>
                    self.tutorialBlogArray = response["TotorialBlog"] as! Array<AnyObject>
                    self.newLaunchBlogArray = response["NewLaunchBlog"] as! Array<AnyObject>
                    
                    DispatchQueue.main.async
                    {
                        self.UpdateView()
                        if self.mainBlogArray.count > 0{
                            self.topCollection.reloadData()
                        }
                        if self.featuredBlogArray.count > 0{
                            
                            self.featuredCollection.reloadData()
                            
                        }
                        if self.recentBlogArray.count > 0 && self.tutorialBlogArray.count > 0{
//                            self.setView()
                            self.setHeaderScroll()
                            self.setUpScrollView()

                        }
                        self.addTimer()
                    }
                }
                else {

                    var mess = String()
                    if response[Constants.MESS] != nil{
                        mess = response[Constants.MESS] as! String
                    }
                    else{
                        mess = response["Message"] as! String
                    }
                    DispatchQueue.main.async
                    {
                    let alert = UIAlertController.init(title: "Nikon", message: mess, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                        self.dismiss(animated: true, completion: nil)
                    }))
                    self.present(alert, animated: true, completion: nil)
                    }
                }
            })
        }
        else
        {
            Constants.appDelegate.stopIndicator()
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
    }
    //MARK:- SET UP VIEW CONSTRAINTS
    func UpdateView(){
        
        let height_Collections = (ScreenSize.SCREEN_WIDTH - 10)/2 + 120
        self.mainCollectionHeight.constant = height_Collections
        self.featuredCollectionHeight.constant = height_Collections
        
        if newLaunchBlogArray.count > 0 {
            self.newLaunchCollectionHeight.constant = height_Collections
        }
        else
        {
            self.newLaunchCollectionHeight.constant = 0
        }
        
         var heigth_ScrollView  = CGFloat()
        if  isCompleteLoad == false {
            heigth_ScrollView  = ScreenSize.SCREEN_WIDTH + 40.0
            self.heightConstraint.constant = heigth_ScrollView
        }
        else {
            if selectedIndex == 0 {
                heigth_ScrollView  = ((ScreenSize.SCREEN_WIDTH/2) * CGFloat((self.recentBlogArray.count + 1)/2)) + 40.0
                recentVC.blogCollection.reloadData()
            }
            else{
                heigth_ScrollView  = ((ScreenSize.SCREEN_WIDTH/2) * CGFloat((self.tutorialBlogArray.count + 1)/2)) + 40.0
                tutorialVC.blogCollection.reloadData()
            }
            
            self.heightConstraint.constant = heigth_ScrollView
        }
        
        
        
        self.mainView_heightConstraint.constant = (height_Collections * 2) + heigth_ScrollView + 120 + self.newLaunchCollectionHeight.constant
    }
    
    
    //MARK:- COllection View
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        if collectionView == topCollection || collectionView == featuredCollection || collectionView == newLaunchCollection {

            return CGSize(width: collectionView.bounds.size.width,height: 275)
        }
        else{
            return CGSize(width: (collectionView.bounds.size.width - 20)/2,height: (collectionView.bounds.size.width - 20)/2)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == topCollection{
            topPageControl.numberOfPages = self.mainBlogArray.count
            return self.mainBlogArray.count
        }
        else if collectionView == featuredCollection{
            featuredPageControl.numberOfPages = self.featuredBlogArray.count
            return self.featuredBlogArray.count
        }
        else if collectionView == newLaunchCollection{
            newLaunchPageControl.numberOfPages = self.newLaunchBlogArray.count
            return self.newLaunchBlogArray.count
        }
        else if collectionView == recentVC.blogCollection{
            if isCompleteLoad == false {
                if self.recentBlogArray.count > 4 {
                    return 4
                }
                return self.recentBlogArray.count
            }
            else
            {
                return self.recentBlogArray.count
            }
        }
        else if collectionView == tutorialVC.blogCollection{
            if isCompleteLoad == false {
                if self.tutorialBlogArray.count > 4 {
                    return 4
                }
                return self.tutorialBlogArray.count
            }
            else
            {
                return self.tutorialBlogArray.count
            }
        }
        return 0
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == topCollection {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ArticleCollectionCell", for: indexPath) as! ArticleCollectionCell
            let dict = self.mainBlogArray[indexPath.item] as! Dictionary<String,AnyObject>

            cell.headerLabel.text = dict["BlogTitle"] as? String
            let comments = dict["CommentCount"] as? Int
            cell.subHeaderLabel.text = "(\(comments!) Comments)"
            
            let detailstr = dict["BlogSummary"]
            var str = (detailstr as! String).replacingOccurrences(of: "<[^>]+>", with: "", options: .regularExpression, range: nil)
            str = str.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            cell.detailLabel.text = str.html2String

            let imageURL = dict["BlogBigImage"] as? String
            let updateURL = imageURL?.replacingOccurrences(of: " ", with: "%20")
            //Helper.loadImageFromUrl(updateURL!, view: cell.imageVw, width: 320)
            cell.imageVw.sd_setImage(with: URL.init(string: updateURL!), placeholderImage: UIImage.init(named: "product_placeholder.jpg"))
            
            cell.imageVw.setShowActivityIndicator(true)

            cell.readMoreButton.addTarget(self, action: #selector(ReadMorePressed), for: .touchUpInside)
            cell.shareButton.addTarget(self, action: #selector(SharePressed), for: .touchUpInside)
            
            return cell
            
        }
        else if collectionView == featuredCollection{
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FeaturedCollectionCell", for: indexPath) as! FeaturedCollectionCell
            let dict = self.featuredBlogArray[indexPath.item] as! Dictionary<String,AnyObject>

            cell.headerLabel.text = dict["BlogTitle"] as? String
            
            let comments = dict["CommentCount"] as? Int
            cell.subHeaderLabel.text = "(\(comments!) Comments)"
            
            let detailstr = dict["BlogSummary"]
            var str = (detailstr as! String).replacingOccurrences(of: "<[^>]+>", with: "", options: .regularExpression, range: nil)
            str = str.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            cell.detailLabel.text = str.html2String

            let imageURL = dict["BlogBigImage"] as? String

            let updateURL = imageURL?.replacingOccurrences(of: " ", with: "%20")
            //Helper.loadImageFromUrl(updateURL!, view: cell.imageVw, width: 320)
            cell.imageVw.sd_setImage(with: URL.init(string: updateURL!), placeholderImage: UIImage.init(named: "product_placeholder.jpg"))
            cell.imageVw.setShowActivityIndicator(true)
            
            cell.readMoreButton.addTarget(self, action: #selector(ReadMore_FeaturedPressed), for: .touchUpInside)
            cell.shareButton.addTarget(self, action: #selector(SharePressed_Featured), for: .touchUpInside)

            return cell

        }
        else if collectionView == newLaunchCollection{
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NewLaunchCollectionCell", for: indexPath) as! NewLaunchCollectionCell
            let dict = self.newLaunchBlogArray[indexPath.item] as! Dictionary<String,AnyObject>
            
            cell.headerLabel.text = dict["BlogTitle"] as? String
            
            let comments = dict["CommentCount"] as? Int
            cell.subHeaderLabel.text = "(\(comments!) Comments)"
            
            let detailstr = dict["BlogSummary"]
            var str = (detailstr as! String).replacingOccurrences(of: "<[^>]+>", with: "", options: .regularExpression, range: nil)
            str = str.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            cell.detailLabel.text = str.html2String
            
            let imageURL = dict["BlogBigImage"] as? String
            
            let updateURL = imageURL?.replacingOccurrences(of: " ", with: "%20")
            //Helper.loadImageFromUrl(updateURL!, view: cell.imageVw, width: 320)
            cell.imageVw.sd_setImage(with: URL.init(string: updateURL!), placeholderImage: UIImage.init(named: "product_placeholder.jpg"))
            cell.imageVw.setShowActivityIndicator(true)
            
            cell.readMoreButton.addTarget(self, action: #selector(ReadMore_FeaturedPressed), for: .touchUpInside)
            cell.shareButton.addTarget(self, action: #selector(SharePressed_Featured), for: .touchUpInside)
            
            return cell
            
        }

        else if collectionView == recentVC.blogCollection{
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BlogCollectionCell", for: indexPath)
            
            let dict = self.recentBlogArray[indexPath.item] as! Dictionary<String,AnyObject>

            let imag = cell.contentView.viewWithTag(3011) as! UIImageView
            let imageURL = dict["BlogBigImage"] as? String
            let updateURL = imageURL?.replacingOccurrences(of: " ", with: "%20")
            //Helper.loadImageFromUrl(updateURL!, view: imag, width: 150)
            imag.sd_setImage(with: URL.init(string: updateURL!), placeholderImage: UIImage.init(named: "product_placeholder.jpg"))
            imag.setShowActivityIndicator(true)

            let nameLabel = cell.contentView.viewWithTag(3012) as! UILabel
            nameLabel.text = dict["BlogTitle"] as? String
            
            return cell
        }
        else if collectionView == tutorialVC.blogCollection{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BlogCollectionCell", for: indexPath)
            
            let dict = self.tutorialBlogArray[indexPath.item] as! Dictionary<String,AnyObject>

            let imag = cell.contentView.viewWithTag(3011) as! UIImageView
            let imageURL = dict["BlogBigImage"] as? String
            let updateURL = imageURL?.replacingOccurrences(of: " ", with: "%20")
            //Helper.loadImageFromUrl(updateURL!, view: imag, width: 150)
            imag.sd_setImage(with: URL.init(string: updateURL!), placeholderImage: UIImage.init(named: "product_placeholder.jpg"))
            imag.setShowActivityIndicator(true)
            
            let nameLabel = cell.contentView.viewWithTag(3012) as! UILabel
            nameLabel.text = dict["BlogTitle"] as? String

            
            return cell
        }
        else {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FeaturedCollectionCell", for: indexPath)
            
            return cell
        }
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == recentVC.blogCollection {
            
            let desination = Constants.mainStoryboard.instantiateViewController(withIdentifier: "BlogDetailVC") as! BlogDetailVC
            let dict = recentBlogArray[indexPath.row] as! Dictionary<String,AnyObject>
            desination.blogId = dict["BlogID"] as! Int
            self.navigationController?.pushViewController(desination, animated: true)
        }
        else if collectionView == tutorialVC.blogCollection{
            let desination = Constants.mainStoryboard.instantiateViewController(withIdentifier: "BlogDetailVC") as! BlogDetailVC
            let dict = tutorialBlogArray[indexPath.row] as! Dictionary<String,AnyObject>
            desination.blogId = dict["BlogID"] as! Int
            self.navigationController?.pushViewController(desination, animated: true)
        }
    }
    
    
    //MARK:- BUTTON ACTION
    @IBAction func LoadMorePressed(_ sender: AnyObject) {
        
//        isCompleteLoad = true
//        UpdateView()
         let desination = Constants.mainStoryboard.instantiateViewController(withIdentifier: "BlogViewAllVC") as! BlogViewAllVC
       
        
//        let desination = Constants.mainStoryboard.instantiateViewControllerWithIdentifier("BlogArticleScrollVC") as! BlogArticleScrollVC
//        desination.recentBlogArray = self.recentBlogArray
//        desination.tutorialBlogArray = self.tutorialBlogArray
       self.navigationController?.pushViewController(desination, animated: true)

    }
    
    @IBAction func ReadMorePressed(_ sender: AnyObject) {
        
        var indexPath: IndexPath!
        
        if let button = sender as? UIButton {
            if let superview = button.superview {
                if let cell = superview.superview as? ArticleCollectionCell {
                    indexPath = topCollection.indexPath(for: cell)
                }
            }
        }

        let desination = Constants.mainStoryboard.instantiateViewController(withIdentifier: "BlogDetailVC") as! BlogDetailVC
        let dict = mainBlogArray[indexPath.row] as! Dictionary<String,AnyObject>
        desination.blogId = dict["BlogID"] as! Int
        self.navigationController?.pushViewController(desination, animated: true)

    }
    
    @IBAction func ReadMore_FeaturedPressed(_ sender: AnyObject) {
        
        var indexPath: IndexPath!
        
        if let button = sender as? UIButton {
            if let superview = button.superview {
                if let cell = superview.superview as? FeaturedCollectionCell {
                    indexPath = featuredCollection.indexPath(for: cell)
                }
            }
        }
        
        let desination = Constants.mainStoryboard.instantiateViewController(withIdentifier: "BlogDetailVC") as! BlogDetailVC
        let dict = featuredBlogArray[indexPath.row] as! Dictionary<String,AnyObject>
        desination.blogId = dict["BlogID"] as! Int
        self.navigationController?.pushViewController(desination, animated: true)

    }
    
    @IBAction func SharePressed(_ sender: AnyObject) {
        
        let imageURL = mainBlogArray[0]["BlogBigImage"] as? String
        var updateURL = imageURL?.replacingOccurrences(of: " ", with: "%20")
        if updateURL == nil{
            updateURL = ""
        }
        else{
            // set up activity view controller
            DispatchQueue.main.async
            {
                let shareSheet = UIActivityViewController.init(activityItems: [updateURL!], applicationActivities: nil)//[title!,instructionString,myWebsite!]
                let excludeActivities = [UIActivityType.airDrop,UIActivityType.print,UIActivityType.assignToContact,UIActivityType.saveToCameraRoll,UIActivityType.addToReadingList]
                shareSheet.excludedActivityTypes = excludeActivities;
                
                shareSheet.popoverPresentationController?.sourceView = self.view
                shareSheet.setValue("Whirlpool Culineria", forKey: "subject")
                
                self.present(shareSheet, animated: true, completion: nil)
            }
        }

    }
    
    @IBAction func SharePressed_Featured(_ sender: AnyObject) {
        
        var indexPath: IndexPath!
        
        if let button = sender as? UIButton {
            if let superview = button.superview {
                if let cell = superview.superview as? FeaturedCollectionCell {
                    indexPath = featuredCollection.indexPath(for: cell)
                }
            }
        }

        let imageURL = featuredBlogArray[indexPath.row]["BlogBigImage"] as? String
        var updateURL = imageURL?.replacingOccurrences(of: " ", with: "%20")
        if updateURL == nil{
            updateURL = ""
        }
        else{
            // set up activity view controller
            DispatchQueue.main.async
            {
            let shareSheet = UIActivityViewController.init(activityItems: [updateURL!], applicationActivities: nil)
            let excludeActivities = [UIActivityType.airDrop,UIActivityType.print,UIActivityType.assignToContact,UIActivityType.saveToCameraRoll,UIActivityType.addToReadingList]
            shareSheet.excludedActivityTypes = excludeActivities;
            
            shareSheet.popoverPresentationController?.sourceView = self.view
            shareSheet.setValue("Whirlpool Culineria", forKey: "subject")
            
            self.present(shareSheet, animated: true, completion: nil)
            }
        }

        

    }

    
    
    //MARK: - AUTO SCROLLING
    func addTimer()
    {
        timer = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(nextPage), userInfo: nil, repeats: true)
        RunLoop.current.add(timer, forMode: RunLoopMode.commonModes)
    }
    
    func nextPage()
    {
        var ar = featuredCollection.indexPathsForVisibleItems
        let MaxSections = featuredBlogArray.count
        
        var currentIndexPathReset = ar[0]
        if(currentIndexPathReset.item >= MaxSections - 1)
        {
            currentIndexPathReset = IndexPath(item:0, section:0)
            featuredPageControl.currentPage = 0
            featuredCollection.scrollToItem(at: currentIndexPathReset, at: UICollectionViewScrollPosition.left, animated: false)

        }
        else
        {
            let newIndex = currentIndexPathReset.item + 1
            featuredPageControl.currentPage = newIndex

            featuredCollection.scrollToItem(at: IndexPath.init(item: newIndex, section: 0), at: UICollectionViewScrollPosition.left, animated: true)
        }
        
        
        var ar1 = topCollection.indexPathsForVisibleItems
        let MaxSections1 = mainBlogArray.count
        
        var currentIndexPathReset1 = ar1[0]
        if(currentIndexPathReset1.item >= MaxSections1 - 1)
        {
            currentIndexPathReset1 = IndexPath(item:0, section:0)
            topPageControl.currentPage = 0
            topCollection.scrollToItem(at: currentIndexPathReset1, at: UICollectionViewScrollPosition.left, animated: false)
            
        }
        else
        {
            let newIndex = currentIndexPathReset1.item + 1
            topPageControl.currentPage = newIndex
            
            topCollection.scrollToItem(at: IndexPath.init(item: newIndex, section: 0), at: UICollectionViewScrollPosition.left, animated: true)
        }
    }
    
    func removeTimer()
    {
        // stop NSTimer
        timer.invalidate()
    }
    
    // UIScrollView' delegate method
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView)
    {
        removeTimer()
    }
    
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView,
                                  willDecelerate decelerate: Bool)
    {
        addTimer()
    }
    
    
    //MARK:- SET VIEW BOT BOTTOM
    // MARK: - Label ScrollView
    
    func setHeaderScroll() {
        var scrollContentCount = 0;
        var frame : CGRect = CGRect(x: 0, y: 0, width: 0, height: 0)
        var labelx : CGFloat = 0.0
        let width  : CGFloat = ScreenSize.SCREEN_WIDTH / 2.0
        let firstLabelWidth : CGFloat = ScreenSize.SCREEN_WIDTH / 2.0
        
        indicatorLabel = UILabel.init(frame: CGRect(x: labelx, y: 38, width: firstLabelWidth, height: 3))
        indicatorLabel.backgroundColor = UIColor.black//UIColor.yellowColor()
        indicatorLabel.text = "";
        headerScroll.addSubview(indicatorLabel)
        
        for arrayIndex in 0 ..< headerArray.count {
            frame.origin.x = labelx
            // set scorllview properties
            frame.origin.y = 0;
            frame.size.width=width;
            frame.size.height=39;
            
            let accountLabelButton = UIButton.init(type: UIButtonType.custom)
            accountLabelButton.addTarget(self, action:#selector(HeaderlabelSelected), for: UIControlEvents.touchUpInside)
            
            accountLabelButton.frame = frame;
            accountLabelButton.tag = arrayIndex;
            accountLabelButton.setAttributedTitle(NSAttributedString(string: headerArray[arrayIndex],  attributes: [NSFontAttributeName : UIFont.init(name: "OpenSans-Semibold", size: 15.0)!, NSForegroundColorAttributeName : UIColor.black]), for: UIControlState())
            
            if DeviceType.IS_IPHONE_5 || DeviceType.IS_IPHONE_4_OR_LESS {
                accountLabelButton.setAttributedTitle(NSAttributedString(string: headerArray[arrayIndex],  attributes: [NSForegroundColorAttributeName : UIColor.black,NSFontAttributeName : UIFont.init(name: "OpenSans-Semibold", size: 15.0)!]), for: UIControlState())
            }
            accountLabelButton.titleLabel?.textAlignment = NSTextAlignment.center
            
            headerScroll.addSubview(accountLabelButton)
            
            if arrayIndex == 0 {
                accountLabelButton.alpha=1.0;
            }
            else {
                accountLabelButton.alpha=0.7;
            }
            
            labelx=labelx+width;
            scrollContentCount = scrollContentCount + 1;
        }
        
        // set scroll content size
        
        DispatchQueue.main.async(execute: {
            self.headerScroll.contentSize = CGSize(width: frame.width, height: 40)
        })
        automaticallyAdjustsScrollViewInsets = false;
        detailScroll.delegate=self;
    }
    
    
    func HeaderlabelSelected(_ sender: UIButton) {
        detailScroll.tag = sender.tag
        
        let xAxis : CGFloat = ScreenSize.SCREEN_WIDTH * CGFloat(sender.tag)
        detailScroll.setContentOffset(CGPoint(x: xAxis, y: 0), animated: true)
        
        let buttonArray = NSMutableArray()
        selectedIndex = sender.tag

        
        for view in headerScroll.subviews {
            if view.isKind(of: UIButton.self) {
                let labelButton = view as! UIButton
                buttonArray.add(labelButton)
                
                
                if labelButton.tag == sender.tag {
                    labelButton.alpha = 1.0
                    
                }
                else {
                    labelButton.alpha = 0.7
                }
            }
        }
        
        let labelButton = buttonArray.object(at: sender.tag) as! UIButton
        var frame : CGRect = labelButton.frame
        frame.origin.y = 38
        frame.size.height = 3
        
        UIView.animate(withDuration: 0.2, animations: {
            if sender.tag == 0 {
                self.indicatorLabel.frame =  CGRect(x: 0,y: 38, width: ScreenSize.SCREEN_WIDTH / 2.0, height: 3)
            }
            else {
                self.indicatorLabel.frame =  CGRect(x: (ScreenSize.SCREEN_WIDTH / 2.0) * CGFloat(sender.tag) ,y: 38, width: ScreenSize.SCREEN_WIDTH / 2.0, height: 3)
            }
            
            self.headerScroll.scrollRectToVisible(self.indicatorLabel.frame, animated: false)
            
            }, completion: nil)
        
        UpdateView()
    }
    
    // MARK: -  Main Scrollview
    func setUpScrollView() {
        for arrayIndex in headerArray {
            if (arrayIndex == "RECENT ARTICLES")  {
                recentVC = Constants.mainStoryboard.instantiateViewController(withIdentifier: "BlogCollectionVC") as! BlogCollectionVC
                ControllerArray.append(recentVC)
                
            }
            if (arrayIndex == "TUTORIAL BLOG") {
                tutorialVC = Constants.mainStoryboard.instantiateViewController(withIdentifier: "BlogCollectionVC") as! BlogCollectionVC
                ControllerArray.append(tutorialVC)
            }
        }
        
        setViewControllers(ControllerArray as NSArray, animated: false)
    }
    
    
    //  Add and remove view controllers
    func setViewControllers(_ viewControllers : NSArray, animated : Bool) {
        if self.childViewControllers.count > 0 {
            for vC in self.childViewControllers {
                vC.willMove(toParentViewController: nil)
                vC.removeFromParentViewController()
            }
        }
        
        for vC in viewControllers {
            self.addChildViewController(vC as! UIViewController)
            (vC as AnyObject).didMove(toParentViewController: self)
        }
        
        //TODO animations
        if ((detailScroll) != nil) {
            reloadPages()
        }
    }
    
    func reloadPages() {
        var scrollContentCount = 0;
        
        for arrayIndex in 0 ..< headerArray.count {
            // set scorllview properties
            var frame : CGRect = CGRect(x: 0, y: 0, width: 0, height: 0)
            frame.origin.x = ScreenSize.SCREEN_WIDTH * CGFloat(arrayIndex)
            frame.origin.y = 0;
            frame.size = ScreenSize.SCREEN.size
            
            detailScroll.isPagingEnabled = true
            let contentWidth : CGFloat = ScreenSize.SCREEN_WIDTH * CGFloat(scrollContentCount+1)
            widthConstraint.constant = contentWidth;
            
            let vC = self.childViewControllers[arrayIndex]
            addView(vC.view, contentView: contentView, frame: frame)
            
            if arrayIndex == 0 {
                
                DispatchQueue.main.async{
                    self.recentVC.blogCollection.delegate = self
                    self.recentVC.blogCollection.dataSource = self
                    self.recentVC.blogCollection.reloadData()
                }
            }
            else if arrayIndex == 1
            {
                DispatchQueue.main.async{
                    self.tutorialVC.blogCollection.delegate = self
                    self.tutorialVC.blogCollection.dataSource = self
                    self.tutorialVC.blogCollection.reloadData()
                }
            }
            
            scrollContentCount = scrollContentCount + 1;
        }
        
        DispatchQueue.main.async(execute: {
            self.detailScroll.contentSize = CGSize(width: self.widthConstraint.constant, height: self.detailScroll.frame.size.height)
        })
        detailScroll.delegate = self
    }
    
    // adding views to scrollview
    func addView(_ view : UIView, contentView : UIView, frame : CGRect) {
        view.frame = frame
        contentView.addSubview(view)
//        view.translatesAutoresizingMaskIntoConstraints = false

        DispatchQueue.main.async(execute: {
            contentView.addConstraint(NSLayoutConstraint.init(item: view, attribute: NSLayoutAttribute.leading, relatedBy: NSLayoutRelation.equal, toItem: contentView, attribute:  NSLayoutAttribute.leading, multiplier: 1.0, constant: frame.origin.x))
            contentView.addConstraint(NSLayoutConstraint.init(item: view, attribute: NSLayoutAttribute.width, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute:  NSLayoutAttribute.notAnAttribute, multiplier: 1.0, constant: frame.size.width))
            contentView.addConstraint(NSLayoutConstraint.init(item: view, attribute: NSLayoutAttribute.top, relatedBy: NSLayoutRelation.equal, toItem: contentView, attribute:  NSLayoutAttribute.top, multiplier: 1.0, constant:0))
            contentView.addConstraint(NSLayoutConstraint.init(item: view, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute:  NSLayoutAttribute.notAnAttribute, multiplier: 1.0, constant: frame.size.height))
        })
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        if scrollView == self.detailScroll {
            let pageWidth = ScreenSize.SCREEN_WIDTH
            let page = (Int)(floor((self.detailScroll.contentOffset.x - pageWidth / 2) / pageWidth) + 1);
            
            let but = UIButton.init()
            but.tag = page
            
            self.HeaderlabelSelected(but)
        }
    }


}
