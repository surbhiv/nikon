//
//  BlogDetailVC.swift
//  NikonApp
//
//  Created by Surbhi on 03/01/17.
//  Copyright © 2017 Neuronimbus. All rights reserved.
//

import UIKit

class BlogDetailVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextViewDelegate {

    
    
    @IBOutlet weak var detailtable: UITableView!
    
    var blogId = Int()
    var commentArray = [AnyObject]()
    var blogDetailDict = Dictionary<String,AnyObject>()

    var commentText = UITextView()
    var submitButton = UIButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        self.setNavigationBar()
        getdata()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func OpenSideMenu(_ sender: AnyObject){
        
        self.view.endEditing(true)
        toggleSideMenuView()
    }
    
    func PopView(_ sender: AnyObject){
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func setNavigationBar(){
        self.navigationItem.hidesBackButton = true
        let navigationViewFrame = ScreenSize.SCREEN//self.navigationController?.navigationBar.frame
        let navigationView = UIView.init(frame: CGRect(x: 0, y: 0, width: navigationViewFrame.size.width, height: 44))
        navigationView.backgroundColor = UIColor.clear
        let menuButton = UIButton.init(type: .custom)
        
        menuButton.frame = CGRect(x: ScreenSize.SCREEN_WIDTH - 45, y: 7, width: 25, height: 25)
        menuButton.setImage(UIImage.init(named: "sideMenu_Icon"), for: UIControlState())
        menuButton.addTarget(self, action: #selector(OpenSideMenu), for: .touchUpInside)
        navigationView.addSubview(menuButton)
//        self.navigationController?.navigationBar.tintColor = Colors.appPink
        
        let titleLabel = UILabel.init(frame:CGRect(x: 60,y: 0,width: ( (navigationViewFrame.size.width) - 110),height: 40))
        titleLabel.textColor = UIColor.black
        titleLabel.text = "BLOG DETAILS"
        titleLabel.font = UIFont(name:"OpenSans-SemiBold", size: 15)!
        titleLabel.textAlignment = .center
        navigationView.addSubview(titleLabel)
        
        let logoImage = UIImageView.init(frame: CGRect(x: 25, y: 6, width: 33, height: 33))
        logoImage.image = UIImage.init(named: "nikon_login_logo")
        navigationView.addSubview(logoImage)
        
        let backButton = UIButton.init(type: .custom)
        backButton.frame = CGRect(x: 0, y: 9, width: 20, height: 20)
        backButton.backgroundColor = UIColor.white
        backButton.setImage(UIImage.init(named: "back"), for: UIControlState())
        backButton.addTarget(self, action: #selector(PopView), for: .touchUpInside)
        navigationView.addSubview(backButton)
        
        self.navigationItem.titleView = navigationView
        
    }
    
    //MARK:- GET DATA
    func getdata(){
        if Reachability.isConnectedToNetwork()
        {
            let method = "GetBlog?BlogId=\(blogId)"
            Constants.appDelegate.startIndicator()
            Server.getRequestWithURL(Constants.BASEURL+method, completionHandler: { (response) in
                
                Constants.appDelegate.stopIndicator()
                if response[Constants.STATE] != nil && response[Constants.STATE] as! String == Constants.SUCC {
                    self.blogDetailDict = response
                    self.commentArray = self.blogDetailDict["Comments"] as! Array<AnyObject>
                    
                    DispatchQueue.main.async
                    {
                        self.detailtable.isHidden = false
                        self.detailtable.reloadData()
                        self.detailtable.layoutIfNeeded()

                    }
                }
                else {
                    
                    self.detailtable.isHidden = true
                    var mess = String()
                    if response[Constants.MESS] != nil{
                        mess = response[Constants.MESS] as! String
                    }
                    else{
                        mess = response["Message"] as! String
                    }
                    DispatchQueue.main.async
                    {
                    
                    let alert = UIAlertController.init(title: "Nikon", message: mess, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                        self.dismiss(animated: true, completion: nil)
                    }))
                    self.present(alert, animated: true, completion: nil)
                    }
                }
            })
        }
        else
        {
            self.detailtable.isHidden = true
            Constants.appDelegate.stopIndicator()
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
    }

    
    //MARK:- TABLEVIEW
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 || section == 1 {
            if blogDetailDict.count == 0 {
                return 0
            }
            return 1
        }
        else{
            return commentArray.count
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0{

            let textV = UITextView.init(frame: CGRect(x: 8, y: 0, width: ScreenSize.SCREEN_WIDTH, height: 9999.0))
            
            let detailstr = blogDetailDict["BlogSummary"]as! String
            do
            {
                let atributedStr = try NSAttributedString.init(data: detailstr.data(using: String.Encoding.unicode)!, options: [ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType ], documentAttributes: nil)
                textV.attributedText = atributedStr
            }
            catch
            {
                print("error occure")
                
            }
            
            let contentSize = textV.sizeThatFits(textV.bounds.size)
            return 805
            
        }
        else if indexPath.section == 1{
            return 160
        }
        else{
            
            let dict = commentArray[indexPath.row] as! Dictionary<String,AnyObject>
            let text = dict["Comments"] as? String
            let fon = UIFont.init(name: "OpenSans", size: 14.0)
            return 35 + self.getHeightForLabel(text!,fon: fon!)
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "BlogDescriptionCell", for: indexPath)
            
            let blogImage = cell.contentView.viewWithTag(1401) as! UIImageView
            let title = cell.contentView.viewWithTag(1402) as! UILabel
            let name = cell.contentView.viewWithTag(1403) as! UILabel
            let date = cell.contentView.viewWithTag(1404) as! UILabel
            let comments = cell.contentView.viewWithTag(1405) as! UILabel
            let description = cell.contentView.viewWithTag(1406) as! UIWebView

            let imageURL = blogDetailDict["BlogBigImage"] as? String
            var updateURL = imageURL?.replacingOccurrences(of: " ", with: "%20")
            if updateURL == nil{
                updateURL = ""
            }
            Helper.loadImageFromUrl(updateURL!, view: blogImage, width: 320)
            
            title.text = blogDetailDict["BlogTitle"] as? String
            name.text = blogDetailDict["BlogBy"] as? String
            
            date.text = self.getTimeString((blogDetailDict["Blogdate"] as? String)!)//blogDetailDict["Blogdate"] as? String
            
            
            let commentSTR = blogDetailDict["CommentCount"] as? Int
            comments.text = "(\(commentSTR!) Comments)"
            
            let detailstr = blogDetailDict["BlogSummary"]as! String
            
            description.layer.shadowColor = UIColor.black.cgColor
            description.layer.shadowOpacity = 1
            description.layer.shadowOffset = CGSize.zero
            description.layer.shadowRadius = 3
            description.layer.masksToBounds = false
            
            description.loadHTMLString(detailstr, baseURL: nil)

            return cell
        }
        else if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "BlogWriteCommentCell", for: indexPath)
            
            commentText = cell.contentView.viewWithTag(1409) as! UITextView
            commentText.delegate = self
            submitButton = cell.contentView.viewWithTag(1410) as! UIButton
            commentText.text = "WRITE YOUR COMMENT ..."
            submitButton.addTarget(self, action: #selector(PostComment), for: .touchUpInside)

            return cell
        }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "BlogCommentCell", for: indexPath)
            let dict = commentArray[indexPath.row] as! Dictionary<String,AnyObject>
            let name = cell.contentView.viewWithTag(1407) as! UILabel
            name.text = dict["Emp_Name"] as? String
            let comment = cell.contentView.viewWithTag(1408) as! UILabel
            comment.text = dict["Comments"] as? String

            return cell
        }
        
    }
    
    
    func getHeightForLabel(_ text : String, fon : UIFont) -> CGFloat{
        
        let wide = ScreenSize.SCREEN_WIDTH - 20
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: wide, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.font = fon
        label.text = text
        label.sizeToFit()
        return label.frame.height
    }

    //MARK:- TEXTVIEW DELEGATE
    func textViewDidBeginEditing(_ textView: UITextView) {
        commentText.text = ""
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if commentText.text.trimmingCharacters(
            in: CharacterSet.whitespacesAndNewlines).isEmpty == true{
            commentText.text = "WRITE YOUR COMMENT ..."
        }
    }
    
    //MARK:- Time Calculator
    func getTimeString(_ time: String)->String{
        
        let arr = time.components(separatedBy: "T")
        let time1 = arr[0] 
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let dateObj = dateFormatter.date(from: time1)
        
        dateFormatter.dateFormat = "MM/dd/yyyy"
        let finalDate = dateFormatter.string(from: dateObj!)
        return finalDate
    }
    
    //MARK:- LoginAlert
    
    func loginAlert()  {
        let alert = UIAlertController.init(title: "Nikon", message: "Please login to continue.", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
            
            DispatchQueue.main.async
            {
                let destinationController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "LoginVC")as! LoginVC
                destinationController.isWantTodismissVC = true
                self.present(destinationController, animated: true, completion: nil);            }
        }))
        alert.addAction(UIAlertAction.init(title: "Cancel", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
            self.dismiss(animated: true, completion: nil)
        }))
        self.present(alert, animated: true, completion: nil)
    }

    //MARK:- PostComment
    @IBAction func PostComment(_ sender: AnyObject){
        if (UserDefaults.standard.integer(forKey: Constants.USERID) != 0)
        {
        
            if commentText.text == "" || commentText.text == "WRITE YOUR COMMENT ..." {
//            CustomAlertController.showAlertOnController(self, withAlertType: Simple, andAttributes:["title": "Nikon", "message":"Please enter your comment"])
            }
            else{
                let method = "GetBlog"
                let param = "BlogId=\(self.blogId)&Login_ID=\(UserDefaults.standard.integer(forKey: Constants.USERID))&Comments=\(commentText.text)"
            
                if Reachability.isConnectedToNetwork() {
                    Constants.appDelegate.startIndicator()

                    Server.postRequestWithURL(Constants.BASEURL+method, paramString: param) { (response) in
                    
                        if response[Constants.STATE] != nil && response[Constants.STATE] as! String == Constants.SUCC {
                        
                            Constants.appDelegate.stopIndicator()
                        
                            DispatchQueue.main.async(execute: {
                                let alert = UIAlertController.init(title: "Nikon", message: response["message"]! as? String, preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                                    self.dismiss(animated: true, completion: nil)
                                    self.getdata()
                                }))
                                self.present(alert, animated: true, completion: nil)
                            })
                        }
                        else {
                            Constants.appDelegate.stopIndicator()
                            var mess = String()
                            if response[Constants.MESS] != nil{
                                mess = response[Constants.MESS] as! String
                            }
                            else{
                                mess = response["Message"] as! String
                            }
                            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "Nikon", "message": mess] )
                        }
                    }
                }
                else {
                    CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
                }

            }
        }
        else
        {
            self.loginAlert()
        }
    }
}
