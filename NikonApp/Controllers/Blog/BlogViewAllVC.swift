//
//  BlogViewAllVC.swift
//  NikonApp
//
//  Created by Hitesh Dhawan on 01/04/17.
//  Copyright © 2017 Neuronimbus. All rights reserved.
//

import UIKit

class BlogViewAllVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UIScrollViewDelegate, UICollectionViewDelegateFlowLayout {

     @IBOutlet weak var viewAllCollection: UICollectionView!
    var dataArray = [AnyObject]()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationController?.isNavigationBarHidden = false
        self.setNavigationBar()
        self.getdata()
    }
    
    //MARK:- NAVIGATION
    func OpenSideMenu(_ sender: AnyObject){
        
        self.view.endEditing(true)
        toggleSideMenuView()
    }
    func PopView(_ sender: AnyObject){
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func setNavigationBar(){
        self.navigationItem.hidesBackButton = true
        let navigationViewFrame = ScreenSize.SCREEN//self.navigationController?.navigationBar.frame
        let navigationView = UIView.init(frame: CGRect(x: 0, y: 0, width: navigationViewFrame.size.width, height: 44))
        navigationView.backgroundColor = UIColor.clear
        let menuButton = UIButton.init(type: .custom)
        
        menuButton.frame = CGRect(x: ScreenSize.SCREEN_WIDTH - 45, y: 7, width: 25, height: 25)
        menuButton.setImage(UIImage.init(named: "sideMenu_Icon"), for: UIControlState())
        menuButton.addTarget(self, action: #selector(OpenSideMenu), for: .touchUpInside)
        navigationView.addSubview(menuButton)
        //        self.navigationController?.navigationBar.tintColor = Colors.appPink
        
        let titleLabel = UILabel.init(frame:CGRect(x: 60,y: 0,width: ( (navigationViewFrame.size.width) - 110),height: 40))
        titleLabel.textColor = UIColor.black
        titleLabel.text = "BLOG"
        titleLabel.font = UIFont.init(name:"OpenSans-SemiBold", size: 15)// UIFont(name:"OpenSans-SemiBold", size: 15)!
        titleLabel.textAlignment = .center
        navigationView.addSubview(titleLabel)
        
        let logoImage = UIImageView.init(frame: CGRect(x: 25, y: 6, width: 33, height: 33))
        logoImage.image = UIImage.init(named: "nikon_login_logo")
        navigationView.addSubview(logoImage)
        
        let backButton = UIButton.init(type: .custom)
        backButton.frame = CGRect(x: 0, y: 9, width: 20, height: 20)
        backButton.backgroundColor = UIColor.white
        backButton.setImage(UIImage.init(named: "back"), for: UIControlState())
        backButton.addTarget(self, action: #selector(PopView), for: .touchUpInside)
        navigationView.addSubview(backButton)
        
        
        self.navigationItem.titleView = navigationView
        
    }
    
    func getdata(){
        if Reachability.isConnectedToNetwork()
        {
            let method = "GetBlog/GetAllBlog?Status=1&Publish=1&SearchText="
            Constants.appDelegate.startIndicator()
            Server.getRequestWithURL(Constants.BASEURL+method, completionHandler: { (response) in
                
                Constants.appDelegate.stopIndicator()
                if response[Constants.STATE] != nil && response[Constants.STATE] as! String == Constants.SUCC {
                    
                    self.dataArray = response["MainBlog"] as! Array<AnyObject>

                    DispatchQueue.main.async
                    {
                        self.viewAllCollection.reloadData()
                    }
                }
                else {
                    
                    var mess = String()
                    if response[Constants.MESS] != nil{
                        mess = response[Constants.MESS] as! String
                    }
                    else{
                        mess = response["Message"] as! String
                    }
                    
                    DispatchQueue.main.async
                    {
                    let alert = UIAlertController.init(title: "Nikon", message: mess, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                        self.dismiss(animated: true, completion: nil)
                    }))
                    self.present(alert, animated: true, completion: nil)
                    }
                }
            })
        }
        else
        {
            Constants.appDelegate.stopIndicator()
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: (collectionView.bounds.size.width - 20)/2,height: (collectionView.bounds.size.width - 20)/2)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataArray.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
       
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BlogCollectionCell", for: indexPath)
        
        let dict = self.dataArray[indexPath.item] as! Dictionary<String,AnyObject>
        
        let imag = cell.contentView.viewWithTag(3011) as! UIImageView
        let imageURL = dict["BlogBigImage"] as? String
        let updateURL = imageURL?.replacingOccurrences(of: " ", with: "%20")
        //Helper.loadImageFromUrl(updateURL!, view: imag, width: 150)
        imag.sd_setImage(with: URL.init(string: updateURL!), placeholderImage: UIImage.init(named: "product_placeholder.jpg"))
        imag.setShowActivityIndicator(true)
        
        let nameLabel = cell.contentView.viewWithTag(3012) as! UILabel
        nameLabel.text = dict["BlogTitle"] as? String
        
        return cell

    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
            let desination = Constants.mainStoryboard.instantiateViewController(withIdentifier: "BlogDetailVC") as! BlogDetailVC
            let dict = dataArray[indexPath.row] as! Dictionary<String,AnyObject>
            desination.blogId = dict["BlogID"] as! Int
            self.navigationController?.pushViewController(desination, animated: true)
        }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
