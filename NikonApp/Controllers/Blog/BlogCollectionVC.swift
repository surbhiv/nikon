//
//  BlogCollectionVC.swift
//  NikonApp
//
//  Created by Surbhi on 03/01/17.
//  Copyright © 2017 Neuronimbus. All rights reserved.
//

import UIKit

class BlogCollectionVC: UIViewController {

    
    @IBOutlet weak var blogCollection: UICollectionView!
//    @IBOutlet weak var blogCollectionHeight: NSLayoutConstraint!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.translatesAutoresizingMaskIntoConstraints = false
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
