//
//  StoreLocatorCustomCell.swift
//  NikonApp
//
//  Created by Hitesh Dhawan on 01/03/17.
//  Copyright © 2017 Neuronimbus. All rights reserved.
//

import UIKit

class StoreLocatorCustomCell: UITableViewCell {

    @IBOutlet weak var storeNameLbl: UILabel!
    @IBOutlet weak var getDirectionBtn: UIButton!
    @IBOutlet weak var storeAddressLbl: UILabel!
    @IBOutlet weak var storeTimeLbl: UILabel!
    @IBOutlet weak var storeNumberLbl: UILabel!
    @IBOutlet weak var storeDistance: UILabel!
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var storePhoneText: UITextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
            }


    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
