//
//  StoreLocatorVC.swift
//  NikonApp
//
//  Created by Hitesh Dhawan on 01/03/17.
//  Copyright © 2017 Neuronimbus. All rights reserved.
//

import UIKit

class StoreLocatorVC: UIViewController,CLLocationManagerDelegate,UITableViewDelegate,UITableViewDataSource,UIPickerViewDelegate,UIPickerViewDataSource,UITextFieldDelegate ,GMSMapViewDelegate{

    @IBOutlet weak var googleMapView: GMSMapView!
    @IBOutlet weak var storeLocatorTable: UITableView!
    
    @IBOutlet weak var segmentStore: UISegmentedControl!
    @IBOutlet weak var allStoreTable: UITableView!
    let locationManager = CLLocationManager()
    @IBOutlet weak var radiusTextField: UITextField!
    @IBOutlet weak var customViewForAnnotation: UIView!
    
    //ALL STORE
    @IBOutlet weak var stateTextField: UITextField!
    @IBOutlet weak var cityTextField: UITextField!
    @IBOutlet weak var binocularsSwitch: UISwitch!
    @IBOutlet weak var distributorDealerSwitch: UISwitch!
    @IBOutlet weak var authorisedDealerSwitch: UISwitch!
    @IBOutlet weak var experiencedDealerSwitch: UISwitch!
    @IBOutlet weak var switchView: UIView!
    
    
    var latStr = String()
    var longStr = String()
    var radiusStr = String()
    var stateStr = String()
    var cityStr = String()
    var binocularsStr = String()
    var experience_zonesStr = String()
    var distributorStr = String()
    var authorized_dealerStr = String()
    var pageSize = Int()
    var pageNumber = Int()
    var totalPage = Int()
    var storeDealerDataArray = [AnyObject]()
    var allStoreDealerDataArray = [AnyObject]()
    var marker = GMSMarker()
    var picker = UIPickerView()
    let kmArray = ["5 Km","10 Km","15 Km","20 Km","25 Km","30 Km"]
    var didFindLocation = Bool()
    var isLoadingData = Bool()
    var stateArray = [AnyObject]()
    var cityArray = [AnyObject]()
    var cityNameByStateArray = [AnyObject]()
    var markerCount = Int() // used to setting gmsmarker accessibilityLabel
    
    
    //MARK:- START
    override func viewDidLoad() {
        super.viewDidLoad()

        let segAttributes: NSDictionary = [
            NSForegroundColorAttributeName: UIColor.black,
            NSFontAttributeName: UIFont(name: "OpenSans", size: 16)!
        ]
        
        segmentStore.setTitleTextAttributes(segAttributes as! [AnyHashable: Any], for: UIControlState())
        // Do any additional setup after loading the view.
        self.setUpView()
    }
    //MARK:-SETUPVIEW
    func setUpView()  {
        self.picker = UIPickerView.init()
        self.picker.dataSource = self
        self.picker.delegate = self
        self.radiusTextField.inputView = self.picker
        self.radiusTextField.delegate = self
        self.stateTextField.inputView = self.picker
        self.stateTextField.delegate = self
        self.cityTextField.inputView = self.picker
        self.cityTextField.delegate = self
        
        self.navigationController?.isNavigationBarHidden = false
        self.setNavigationBar()
        
        radiusStr = "5"
        stateStr = ""
        cityStr = ""
        binocularsStr = ""
        experience_zonesStr = ""
        distributorStr = ""
        authorized_dealerStr = ""
        pageSize = 20
        pageNumber = 1
        totalPage = 0
        markerCount = 0
        
        locationManager.delegate = self
        didFindLocation = false
        isLoadingData = false
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
        googleMapView.delegate = self
        
        switchView.layer.borderWidth = 1
        switchView.layer.borderColor = UIColor.lightGray.cgColor
        
        self.getStateName()
        
    }

    //MARK:- NAVIGATION
    func OpenSideMenu(_ sender: AnyObject){
        
        self.view.endEditing(true)
        toggleSideMenuView()
    }
    func PopView(_ sender: AnyObject){
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func setNavigationBar(){
        self.navigationItem.hidesBackButton = true
        let navigationViewFrame = ScreenSize.SCREEN//self.navigationController?.navigationBar.frame
        let navigationView = UIView.init(frame: CGRect(x: 0, y: 0, width: navigationViewFrame.size.width, height: 44))
        navigationView.backgroundColor = UIColor.clear
        let menuButton = UIButton.init(type: .custom)
        
        menuButton.frame = CGRect(x: ScreenSize.SCREEN_WIDTH - 45, y: 7, width: 25, height: 25)
        menuButton.setImage(UIImage.init(named: "sideMenu_Icon"), for: UIControlState())
        menuButton.addTarget(self, action: #selector(OpenSideMenu), for: .touchUpInside)
        navigationView.addSubview(menuButton)
//        self.navigationController?.navigationBar.tintColor = Colors.appPink
        
        let titleLabel = UILabel.init(frame:CGRect(x: 60,y: 0,width: ( (navigationViewFrame.size.width) - 110),height: 40))
        titleLabel.textColor = UIColor.black
        titleLabel.text = "STORE LOCATOR"
        titleLabel.font = UIFont.init(name:"OpenSans-SemiBold", size: 15)// UIFont(name:"OpenSans-SemiBold", size: 15)!
        titleLabel.textAlignment = .center
        navigationView.addSubview(titleLabel)
        
        let logoImage = UIImageView.init(frame: CGRect(x: 25, y: 6, width: 33, height: 33))
        logoImage.image = UIImage.init(named: "nikon_login_logo")
        navigationView.addSubview(logoImage)
        
        let backButton = UIButton.init(type: .custom)
        backButton.frame = CGRect(x: 0, y: 9, width: 20, height: 20)
        backButton.backgroundColor = UIColor.white
        backButton.setImage(UIImage.init(named: "back"), for: UIControlState())
        backButton.addTarget(self, action: #selector(PopView), for: .touchUpInside)
        navigationView.addSubview(backButton)
        
        
        self.navigationItem.titleView = navigationView
        
    }
    //MARK:- SWITCH
    @IBAction func experiencedDealerSwitchChanged(_ sender: AnyObject) {
        if experiencedDealerSwitch.isOn == true {
            self.experience_zonesStr = "true"
        }
        else
        {
            self.experience_zonesStr = "false"
        }
    }
    @IBAction func authorisedDealerSwitchChanged(_ sender: AnyObject) {
        if authorisedDealerSwitch.isOn == true {
            self.authorized_dealerStr = "true"
        }
        else
        {
            self.authorized_dealerStr = "false"
        }
    }
    @IBAction func distributorDealerSwitchChanged(_ sender: AnyObject) {
        if distributorDealerSwitch.isOn == true {
            self.distributorStr = "true"
        }
        else
        {
            self.distributorStr = "false"
        }
    }
    @IBAction func binocularsSwitchChanged(_ sender: AnyObject) {
        if binocularsSwitch.isOn == true {
            self.binocularsStr = "true"
        }
        else
        {
            self.binocularsStr = "false"
        }
    }
    
    @IBAction func searchButtonClicked(_ sender: AnyObject)
    {
        self.getStoreData()
        
    }
    //MARK:- SEGMENT ACTION
    
    @IBAction func segmentClicked(_ sender: AnyObject) {
        if self.segmentStore.selectedSegmentIndex == 1
        {
            self.stateTextField.text = ""
            self.cityTextField.text  = ""
            self.allStoreTable.isHidden = false
            self.radiusStr = "0"
            self.latStr = ""
            self.longStr = ""
            binocularsStr = "false"
            experience_zonesStr = "false"
            distributorStr = "false"
            authorized_dealerStr = "false"
            
            authorisedDealerSwitch.isOn = false
            binocularsSwitch.isOn = false
            distributorDealerSwitch.isOn = false
            experiencedDealerSwitch.isOn = false
            if allStoreDealerDataArray.count > 0
            {
                allStoreDealerDataArray.removeAll()
                allStoreTable.reloadData()
            }
            self.getCityNameWithState()
           // self.getStoreData()
            
        }
        else
        {
            stateStr = ""
            cityStr = ""
            binocularsStr = ""
            experience_zonesStr = ""
            distributorStr = ""
            authorized_dealerStr = ""
            let tempArr = self.radiusTextField.text?.components(separatedBy: " ")
            self.radiusStr = tempArr![0]
            self.allStoreTable.isHidden = true
            didFindLocation = false
            locationManager.startUpdatingLocation()
        }
    }

    // MARK: - CLLocationManagerDelegate
   
        func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
           
            if status == .authorizedWhenInUse {
                
              
                locationManager.startUpdatingLocation()
                
             
                googleMapView.isMyLocationEnabled = true
                googleMapView.settings.myLocationButton = true
            }
        }
        
    
        func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
            if let location = locations.first {
                googleMapView.camera = GMSCameraPosition(target: location.coordinate, zoom: 8, bearing: 0, viewingAngle: 0)
                self.latStr = String(location.coordinate.latitude)
                self.longStr = String(location.coordinate.longitude)
//                self.latStr = "28.6296"
//                self.longStr = "77.0802"
                DispatchQueue.main.async
                {
                self.locationManager.stopUpdatingLocation()
                    if self.didFindLocation != true
                    {
                        self.pageNumber = 1
                        self.didFindLocation = true
                        self.getStoreData()
                    }
                
                }
            }
            
        }
    
    //MARK: GMSMapViewDelegate
   func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {        // Get a reference for the custom overlay
        let index:Int! = Int(marker.accessibilityLabel!)
        let customInfoWindow = Bundle.main.loadNibNamed("customViewForMarker", owner: self, options: nil)?[0] as! customViewForMarker
        customInfoWindow.storeNameLbl.text = storeDealerDataArray[index]["name"] as? String
        customInfoWindow.storeAddressLbl.text = "\(storeDealerDataArray[index]["street"] as! String) +\(storeDealerDataArray[index]["town"] as! String),\(storeDealerDataArray[index]["state"] as! String)"
        customInfoWindow.storeTimeLbl.text = storeDealerDataArray[index]["hours"] as? String
        customInfoWindow.storeNumberLbl.text = storeDealerDataArray[index]["tel"] as? String
        self.view.bringSubview(toFront: customInfoWindow.getDirectionBtn)
        customInfoWindow.getDirectionBtn.tag = index
        customInfoWindow.getDirectionBtn.addTarget(self, action: #selector(getDirection), for: .touchUpInside)

        return customInfoWindow
    }
    
    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
        
        let index:Int! = Int(marker.accessibilityLabel!)
        let btn = UIButton.init()
        btn.tag = index
        self .getDirection(btn)
        
        
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        
    }
    //MARK:-API METHOD
    
    func getStateName(){
        if Reachability.isConnectedToNetwork()
        {
            
            
            let method = "GetStateCity"
            
            
            Constants.appDelegate.startIndicator()
            Server.getRequestWithURL(Constants.BASEURL+method, completionHandler: { (response) in
                
                if response[Constants.STATE] != nil && response[Constants.STATE] as! String == Constants.SUCC {
                    
                    self.stateArray = response["statelist"] as! Array<AnyObject>
                    DispatchQueue.main.async
                    {
                        Constants.appDelegate.stopIndicator()
                        //self.getCityName("52")
                        
                    }
                }
                else {
                    Constants.appDelegate.stopIndicator()
                    
                    var mess = String()
                    if response[Constants.MESS] != nil{
                        mess = response[Constants.MESS] as! String
                    }
                    else{
                        mess = response["Message"] as! String
                    }
                    DispatchQueue.main.async
                    {
                    let alert = UIAlertController.init(title: "Nikon", message: mess, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                        self.dismiss(animated: true, completion: nil)
                    }))
                    self.present(alert, animated: true, completion: nil)
                    }
                }
            })
        }
        else
        {
            Constants.appDelegate.stopIndicator()
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
    }
    
    func getCityNameWithState(){
        if Reachability.isConnectedToNetwork()
        {
            
            
            let method = "GetDealer?Type=city"
            
            
            Constants.appDelegate.startIndicator()
            Server.getRequestWithURL(Constants.BASEURL+method, completionHandler: { (response) in
                
                if response[Constants.STATE] != nil && response[Constants.STATE] as! String == Constants.SUCC {
                    
                    self.cityArray = response["DealerCityData"] as! Array<AnyObject>
                    DispatchQueue.main.async
                    {
                        Constants.appDelegate.stopIndicator()
                        
                    }
                }
                else {
                    Constants.appDelegate.stopIndicator()
                    
                    var mess = String()
                    if response[Constants.MESS] != nil{
                        mess = response[Constants.MESS] as! String
                    }
                    else{
                        mess = response["Message"] as! String
                    }
                    DispatchQueue.main.async
                    {
                    let alert = UIAlertController.init(title: "Nikon", message: mess, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                        self.dismiss(animated: true, completion: nil)
                    }))
                    self.present(alert, animated: true, completion: nil)
                    }
                }
            })
        }
        else
        {
            Constants.appDelegate.stopIndicator()
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
    }
    func getStoreData()  {
        if Reachability.isConnectedToNetwork()
        {
            self.pageNumber = 1
            self.googleMapView.clear()
            

            let method = "GetDealer?CurrentLat=\(self.latStr)&CurrentLong=\(self.longStr)&RadiusDistanceInKM=\(self.radiusStr)&State=\(stateStr)&City=\(cityStr)&binoculars=\(binocularsStr)&experience_zones=\(experience_zonesStr)&distributor=\(distributorStr)&authorized_dealer=\(authorized_dealerStr)&PageSize=\(String(pageSize))&PageNumber=\(String(pageNumber))"
            
            Constants.appDelegate.startIndicator()
            Server.getRequestWithURL(Constants.BASEURL+method, completionHandler: { (response) in
                
                
                 if response[Constants.STATE] != nil && response[Constants.STATE] as! String == Constants.SUCC
                 {
                    DispatchQueue.main.async
                    {
                        Constants.appDelegate.stopIndicator()
                    
                        let totalCount = Int(response["RecordCount"] as! String)!
                        if totalCount%self.pageSize == 0
                        {
                            self.totalPage = totalCount/self.pageSize
                        
                        }
                        else
                        {
                            self.totalPage = (totalCount/self.pageSize) + 1
                        }
                        self.pageNumber = self.pageNumber+1
                        
                        if self.allStoreTable.isHidden == true
                        {
                            if self.storeDealerDataArray.count > 0
                            {
                                self.storeDealerDataArray.removeAll()
                            }
                            self.storeDealerDataArray  = response["DealerData"] as! NSArray as [AnyObject]
                        
                            self.markerCount = 0
                            for obj in self.storeDealerDataArray
                            {
                            
                                let lat = Double(obj["Latitude"] as! String)!
                                let long = Double(obj["Longitude"] as! String)!
                                let position = CLLocationCoordinate2D(latitude: lat, longitude: long)
                                let newMarker = GMSMarker(position: position)
                               // newMarker.title = "\(lat)"
                                newMarker.map = self.googleMapView
                                newMarker.icon = GMSMarker.markerImage(with: UIColor.black)
                                newMarker.infoWindowAnchor = CGPoint(x: 0.5, y: 0.2)
                                newMarker.accessibilityLabel = "\(self.markerCount)"
                                self.markerCount = self.markerCount+1
                            
                            }
                            self.storeLocatorTable.reloadData()
                            
                        self.perform(#selector(self.reloadNearestStoreData), with: nil, afterDelay: 0.5)
                            

                        }
                        else
                        {
                            self.allStoreDealerDataArray  = response["DealerData"] as! NSArray as [AnyObject]
                            
                            for obj in self.allStoreDealerDataArray
                            {
                                
                                let lat = Double(obj["Latitude"] as! String)!
                                let long = Double(obj["Longitude"] as! String)!
                                let position = CLLocationCoordinate2D(latitude: lat, longitude: long)
                                let newMarker = GMSMarker(position: position)
                                //newMarker.title = "\(lat)"
                                newMarker.icon = GMSMarker.markerImage(with: UIColor.black)

                                newMarker.map = self.googleMapView
                               
                                
                            }
                            self.allStoreTable.reloadData()
                            self.perform(#selector(self.reloadAllStoreData), with: nil, afterDelay: 0.5)

                        }
                    
                    }
                }
                else
                 {
                    DispatchQueue.main.async
                    {
                    Constants.appDelegate.stopIndicator()
                    if self.allStoreTable.isHidden == true
                    {

                        if self.storeDealerDataArray.count > 0
                        {
                            self.storeDealerDataArray.removeAll()
                        }
                        self.storeLocatorTable.reloadData()

                        
                    }
                    else
                    {
                        if self.allStoreDealerDataArray.count > 0
                        {
                            self.allStoreDealerDataArray.removeAll()
                        }
                        self.allStoreTable.reloadData()
                    }
                    }
                }

                
            })
        }
        else
        {
            Constants.appDelegate.stopIndicator()
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
        
        
    }
    
    func getStoreDataWithPagination()  {
        if Reachability.isConnectedToNetwork()
        {
            let method = "GetDealer?CurrentLat=\(self.latStr)&CurrentLong=\(self.longStr)&RadiusDistanceInKM=\(self.radiusStr)&State=\(stateStr)&City=\(cityStr)&binoculars=\(binocularsStr)&experience_zones=\(experience_zonesStr)&distributor=\(distributorStr)&authorized_dealer=\(authorized_dealerStr)&PageSize=\(String(pageSize))&PageNumber=\(String(pageNumber))"
            
            Constants.appDelegate.startIndicator()
            Server.getRequestWithURL(Constants.BASEURL+method, completionHandler: { (response) in
                
                
                if response[Constants.STATE] != nil && response[Constants.STATE] as! String == Constants.SUCC {
                    
                    DispatchQueue.main.async
                    {
                        Constants.appDelegate.stopIndicator()
                       let tempArr = response["DealerData"] as! NSArray as [AnyObject]
                        self.pageNumber = self.pageNumber+1

                        self.isLoadingData = false
                        for obj in tempArr//for (var j = 0; j < tempArr.count; j++)
                        {
                            if self.allStoreTable.isHidden == true
                            {
                                let lat = Double(obj["Latitude"] as! String)!
                                let long = Double(obj["Longitude"] as! String)!
                                let position = CLLocationCoordinate2D(latitude: lat, longitude: long)
                                let newMarker = GMSMarker(position: position)
                               // newMarker.title = "\(lat)"
                                newMarker.icon = GMSMarker.markerImage(with: UIColor.black)
                                newMarker.infoWindowAnchor = CGPoint(x: 0.5, y: 0.2)
                                newMarker.accessibilityLabel = "\(self.markerCount)"
                                self.markerCount = self.markerCount+1

                                newMarker.map = self.googleMapView
                                self.storeDealerDataArray.append(obj)
                                self.storeLocatorTable.reloadData()
                            }
                            else
                            {
                                self.allStoreDealerDataArray.append(obj)
                                self.allStoreTable.reloadData()

                            }
                        }
                    }
                }
                else
                {
                    Constants.appDelegate.stopIndicator()
                }
            })
        }
        else
        {
            Constants.appDelegate.stopIndicator()
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
    }

    func reloadAllStoreData()  {
        self.allStoreTable.reloadData()
    }
    
    func reloadNearestStoreData()  {
        self.storeLocatorTable.reloadData()
    }


    //MARK:- TABLEVIEW
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.allStoreTable.isHidden == false
        {
            return allStoreDealerDataArray.count
        }
        return storeDealerDataArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == storeLocatorTable
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "StoreLocatorCustomCell", for: indexPath) as! StoreLocatorCustomCell
            cell.storeNameLbl.text = storeDealerDataArray[indexPath.row]["name"] as? String
            cell.storeAddressLbl.text = "\(storeDealerDataArray[indexPath.row]["street"] as! String) \(storeDealerDataArray[indexPath.row]["town"] as! String),\(storeDealerDataArray[indexPath.row]["state"] as! String)"
            cell.storeTimeLbl.text = storeDealerDataArray[indexPath.row]["hours"] as? String
            
            let telePhoneArr = (storeDealerDataArray[indexPath.row]["tel"] as? String)?.components(separatedBy: " / ")
            let newTelephoneStr = telePhoneArr?.joined(separator: "\n")
            
            let attrs = [ NSFontAttributeName : UIFont(name: Fonts.regFONTname, size: (DeviceType.IS_IPAD ? 17.0 : 15.0))!,
                          NSForegroundColorAttributeName : UIColor.black,
                          NSUnderlineStyleAttributeName : 1] as [String : Any]
            
            let attributedSTR = NSMutableAttributedString.init(string: newTelephoneStr!, attributes: attrs)
            cell.storePhoneText.attributedText = attributedSTR
            
            //cell.storeNumberLbl.text = storeDealerDataArray[indexPath.row]["tel"] as? String
            let myDoble = Double(storeDealerDataArray[indexPath.row]["Distance"]as! String)
           // String(format: "%.2f", myDouble)
            cell.storeDistance.text = String(format: "%.2f KM", myDoble!)
            cell.getDirectionBtn.tag = indexPath.row
            cell.getDirectionBtn.addTarget(self, action: #selector(getDirection), for: .touchUpInside)

            let status = storeDealerDataArray[indexPath.row]["experience_zones"] as! String
            if status == "True" || status == "true"
            {
//                cell.backgroundColor = UIColor.init(red: 250.0/255.0, green: 245.0/255.0, blue: 218.0/255.0, alpha: 1)
                cell.shadowView.backgroundColor = UIColor.init(red: 250.0/255.0, green: 245.0/255.0, blue: 218.0/255.0, alpha: 1)
            }
            else
            {
                cell.shadowView.backgroundColor = UIColor.white
            }
            cell.shadowView.layer.shadowColor = UIColor.black.cgColor
            cell.shadowView.layer.shadowOpacity = 1
            cell.shadowView.layer.shadowOffset = CGSize.zero
            cell.shadowView.layer.shadowRadius = 3
            storeLocatorTable.estimatedRowHeight = 300
            storeLocatorTable.rowHeight = UITableViewAutomaticDimension
        
            cell.selectionStyle = .none
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "StoreLocatorCustomCell", for: indexPath) as! StoreLocatorCustomCell
            cell.storeNameLbl.text = allStoreDealerDataArray[indexPath.row]["name"] as? String
            cell.storeAddressLbl.text = "\(allStoreDealerDataArray[indexPath.row]["street"] as! String) \(allStoreDealerDataArray[indexPath.row]["town"] as! String),\(allStoreDealerDataArray[indexPath.row]["state"] as! String)"
            cell.storeTimeLbl.text = allStoreDealerDataArray[indexPath.row]["hours"] as? String
            let telePhoneArr = (allStoreDealerDataArray[indexPath.row]["tel"] as? String)?.components(separatedBy: " / ")
            let newTelephoneStr = telePhoneArr?.joined(separator: "\n")
            
            let attrs = [ NSFontAttributeName : UIFont(name: Fonts.regFONTname, size: (DeviceType.IS_IPAD ? 17.0 : 15.0))!,
                          NSForegroundColorAttributeName : UIColor.black,
                          NSUnderlineStyleAttributeName : 1] as [String : Any]
            
            let attributedSTR = NSMutableAttributedString.init(string: newTelephoneStr!, attributes: attrs)
            cell.storePhoneText.attributedText = attributedSTR
            
           // cell.storeNumberLbl.text = allStoreDealerDataArray[indexPath.row]["tel"] as? String
            //cell.storeDistance.text = allStoreDealerDataArray[indexPath.row]["Distance"] as? String
            cell.getDirectionBtn.tag = indexPath.row
            cell.getDirectionBtn.addTarget(self, action: #selector(getDirection), for: .touchUpInside)
            
            let status = allStoreDealerDataArray[indexPath.row]["experience_zones"] as! String
            if status == "True" || status == "true"
            {
                //                cell.backgroundColor = UIColor.init(red: 250.0/255.0, green: 245.0/255.0, blue: 218.0/255.0, alpha: 1)
                cell.shadowView.backgroundColor = UIColor.init(red: 250.0/255.0, green: 245.0/255.0, blue: 218.0/255.0, alpha: 1)
            }
            else
            {
                cell.shadowView.backgroundColor = UIColor.white
            }
            cell.shadowView.layer.shadowColor = UIColor.black.cgColor
            cell.shadowView.layer.shadowOpacity = 1
            cell.shadowView.layer.shadowOffset = CGSize.zero
            cell.shadowView.layer.shadowRadius = 5

            
            allStoreTable.estimatedRowHeight = 204
            allStoreTable.rowHeight = UITableViewAutomaticDimension
           
            cell.selectionStyle = .none
            return cell

            
        }

    }
    //MARK:- DIRECTION Action
    func getDirection(_ sender : UIButton)
    {
            let index = sender.tag
            if UIApplication.shared.canOpenURL(URL(string: "comgooglemaps://")!)
            {
           
                var addSre = String()
           
                if self.allStoreTable.isHidden == true
                {
                    addSre = "comgooglemaps://?daddr=\(self.storeDealerDataArray[index]["Latitude"]as! String),\(self.storeDealerDataArray[index]["Longitude"]as! String)&directionsmode=transit"
                }
                else
                {
                    addSre = "comgooglemaps://?daddr=\(self.allStoreDealerDataArray[index]["Latitude"]as! String),\(self.allStoreDealerDataArray[index]["Longitude"]as! String)&directionsmode=transit"
                
                }
           
                UIApplication.shared.openURL(URL(string: addSre)!)
            }
            else
            {
               
                let finalURLStr = "http://maps.google.com/maps?saddr=\(self.latStr),\(self.longStr)&daddr=\(self.storeDealerDataArray[index]["Latitude"]as! String),\(self.storeDealerDataArray[index]["Longitude"]as! String)"
                UIApplication.shared.openURL(URL(string:finalURLStr )!)
            }
    }
    
    //MARK: - PICKEVIEW for date
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
         if self.radiusTextField.isFirstResponder
         {
            return kmArray.count
         }
         else if stateTextField.isFirstResponder
         {
            return stateArray.count
         }
        else
         {
            return cityNameByStateArray.count
         }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if self.radiusTextField.isFirstResponder
        {

            return String.init(format: kmArray [row] , locale: nil)
        }
        else if self.stateTextField.isFirstResponder
        {
            return String.init(format: stateArray [row]["State_Name"] as! String, locale: nil)
        }
        else
        {
            return String.init(format: cityNameByStateArray [row]["town"] as! String, locale: nil)
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
         if self.radiusTextField.isFirstResponder
         {
            self.radiusTextField.text = String.init(format: kmArray [row] , locale: nil)
            let tempArr = kmArray[row].components(separatedBy: " ")
            self.radiusStr = tempArr[0]
         }
         else if self.stateTextField.isFirstResponder
         {
            self.stateStr = stateArray [row]["State_Name"] as! String
            self.stateTextField.text = self.stateStr
         }
        else
         {
            if stateStr != ""
            {
            self.cityStr = cityNameByStateArray[row]["town"]as! String
            self.cityTextField.text = self.cityStr
            }
         }
        
        
            
        
        
    }
    //MARK:-SCROLLING DELEGATE
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if allStoreTable.isHidden == true
        {
            if (storeLocatorTable.contentOffset.y >= (self.storeLocatorTable.contentSize.height-self.storeLocatorTable.bounds.size.height))
            {
                if self.pageNumber <= self.totalPage && isLoadingData == false{
                    isLoadingData = true
                    self.getStoreDataWithPagination()
                }
            }
        }
        else
        {
            if (allStoreTable.contentOffset.y >= (self.allStoreTable.contentSize.height-self.allStoreTable.bounds.size.height-290))
            {
                if self.pageNumber <= self.totalPage && isLoadingData == false{
                    isLoadingData = true
                    self.getStoreDataWithPagination()
                }
            }
        }
    
        
    }
    // MARK: TEXTFIELD DELEGATE
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        textField .resignFirstResponder()
        if textField == radiusTextField{
        self.getStoreData()
        }
        if textField == self.stateTextField
        {
            let pre = NSPredicate(format: "SELF['state']==%@", self.stateStr)
            cityNameByStateArray  = (self.cityArray as NSArray).filtered(using: pre) as [AnyObject]        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
