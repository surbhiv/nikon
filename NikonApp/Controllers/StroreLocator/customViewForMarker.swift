//
//  customViewForMarker.swift
//  NikonApp
//
//  Created by Hitesh Dhawan on 04/03/17.
//  Copyright © 2017 Neuronimbus. All rights reserved.
//

import UIKit

class customViewForMarker: UIView {

    @IBOutlet weak var storeNameLbl: UILabel!
    @IBOutlet weak var getDirectionBtn: UIButton!
    @IBOutlet weak var storeAddressLbl: UILabel!
    @IBOutlet weak var storeTimeLbl: UILabel!
    @IBOutlet weak var storeNumberLbl: UILabel!
   
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}
