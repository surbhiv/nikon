//
//  NewContestWebVC.swift
//  NikonApp
//
//  Created by Arnab Maity on 10/02/20.
//  Copyright © 2020 Neuronimbus. All rights reserved.
//

import UIKit

class NewContestWebVC: UIViewController {

	@IBOutlet weak var contestWebView: UIWebView!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		contestWebView.delegate = self
		let url = URL(string: "https://www.capturewithnikon.in/contest/")
		let request = URLRequest(url: url!)
		contestWebView.loadRequest(request)
	}
}

extension NewContestWebVC: UIWebViewDelegate {
	func webViewDidFinishLoad(_ webView: UIWebView) {
		print("Finished loading")
	}
}
