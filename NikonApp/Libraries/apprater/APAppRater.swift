//
//  APAppRater.swift
//  AppRaterSample
//
//  Created by Keith Elliott on 2/9/16.
//  Copyright © 2016 GittieLabs. All rights reserved.
//

import UIKit

let AP_APP_LAUNCHES = "com.gittielabs.applaunches"
let AP_APP_LAUNCHES_CHANGED = "com.gittielabs.applaunches.changed"
let AP_INSTALL_DATE = "com.gittielabs.install_date"
let AP_APP_RATING_SHOWN = "com.gittielabs.app_rating_shown"

@objc open class APAppRater: NSObject, UIAlertViewDelegate {
    var application: UIApplication!
    var userdefaults = UserDefaults()
    let requiredLaunchesBeforeRating = 0
    open var appId: String!
    
    @objc open static var sharedInstance = APAppRater()
    
    //MARK: - Initialize
    override init() {
        super.init()
        setup()
    }
    
    func setup(){
//         NSNotificationCenter.defaultCenter().addObserver(self, selector: "appDidFinishLaunching:" , name: UIApplicationDidFinishLaunchingNotification, object: nil)
        
//        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(appDidFinishLaunching), name: UIApplicationDidFinishLaunchingNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(appDidFinishLaunching), name: NSNotification.Name(rawValue: "RatingViewCanBeShowm"), object: UIApplication.shared)

    }
    
    //MARK: - NSNotification Observers
    func appDidFinishLaunching(_ notification: Notification){
        if let _application = notification.object as? UIApplication{
            self.application = _application
//            displayRatingsPromptIfRequired()
            //nikon-india/id1228039296?ls=1&mt=8
//            let url = NSURL(string: "itms-apps://itunes.apple.com/app/id\(self.appId)")
            let url = URL(string: "itms-apps://itunes.apple.com/app/nikon-india/id1228039296?ls=1&mt=8")
            UIApplication.shared.openURL(url!)
            self.setAppRatingShown()

        }
    }
    
    //MARK: - App Launch count
    func getAppLaunchCount() -> Int {
        let launches = userdefaults.integer(forKey: AP_APP_LAUNCHES)
        return launches
    }
    
    func incrementAppLaunches(){
        var launches = userdefaults.integer(forKey: AP_APP_LAUNCHES)
        launches += 1
        userdefaults.set(launches, forKey: AP_APP_LAUNCHES)
        userdefaults.synchronize()
    }
    
    func resetAppLaunches(){
        userdefaults.set(0, forKey: AP_APP_LAUNCHES)
        userdefaults.synchronize()
    }
    
    //MARK: - First Launch Date
    func setFirstLaunchDate(){
        userdefaults.setValue(Date(), forKey: AP_INSTALL_DATE)
        userdefaults.synchronize()
    }
    
    func getFirstLaunchDate()->Date{
        if let date = userdefaults.value(forKey: AP_INSTALL_DATE) as? Date{
            return date
        }
    
        return Date()
    }
    
    //MARK: App Rating Shown
    func setAppRatingShown(){
        userdefaults.set(true, forKey: AP_APP_RATING_SHOWN)
        userdefaults.synchronize()
    }
    
    func hasShownAppRating()->Bool{
        let shown = userdefaults.bool(forKey: AP_APP_RATING_SHOWN)
        return shown
    }
    
    //MARK: - Rating the App
    fileprivate func displayRatingsPromptIfRequired(){
        let appLaunchCount = getAppLaunchCount()
        let nothanks = hasShownAppRating()
        
        if appLaunchCount >= self.requiredLaunchesBeforeRating && nothanks == false {
//            if #available(iOS 8.0, *) {
                // show App Ratings
                rateTheApp()
//            }
//            else{
//               rateTheAppOldVersion()
//            }
        }
        
        incrementAppLaunches()
    }
    
    @available(iOS 8.0, *)
    fileprivate func rateTheApp(){
        let app_name = Bundle(for: type(of: application.delegate!)).infoDictionary!["CFBundleName"] as? String
        let message = "Do you love the \(app_name!) app?  Please rate us!"
        let rateAlert = UIAlertController(title: "Rate Us", message: message, preferredStyle: .alert)
        let goToItunesAction = UIAlertAction(title: "Rate Us", style: .default, handler: { (action) -> Void in
            let url = URL(string: "itms-apps://itunes.apple.com/app/id\(self.appId)")
            UIApplication.shared.openURL(url!)
            
            self.setAppRatingShown()
        })
        
        let neverRate = UIAlertAction(title: "No Thanks", style: .default, handler: { (action) -> Void in
            
            self.setAppRatingShown()
        })

        
        let cancelAction = UIAlertAction(title: "Not Now", style: .cancel, handler: { (action) -> Void in
           self.resetAppLaunches()
        })
        
        rateAlert.addAction(goToItunesAction)
        rateAlert.addAction(cancelAction)
        rateAlert.addAction(neverRate)

        DispatchQueue.main.async(execute: { () -> Void in
            let window = self.application.windows[0]
            window.rootViewController?.present(rateAlert, animated: true, completion: nil)
        })
    
    }
    
    fileprivate func rateTheAppOldVersion(){
        let app_name = Bundle(for: type(of: application.delegate!)).infoDictionary!["CFBundleName"] as? String
        let message = "Do you love the \(app_name!) app?  Please rate us!"
        let alert = UIAlertView(title: "Rate Us", message: message, delegate: self, cancelButtonTitle: "Not Now", otherButtonTitles: "Rate Us")
        alert.show()
    }
    
    //MARK: - Alert Views
    @objc open func alertViewCancel(_ alertView: UIAlertView) {
        // reset app launch count
        self.resetAppLaunches()
    }
    
    @objc open func alertView(_ alertView: UIAlertView, didDismissWithButtonIndex buttonIndex: Int) {
        setAppRatingShown()
        
        let url = URL(string: "itms-apps://itunes.apple.com/app/id\(self.appId)")
        UIApplication.shared.openURL(url!)
    }
}
