//
//  AppDelegate.swift
//  NikonApp
//
//  Created by Surbhi on 14/12/16.
//  Copyright © 2016 Neuronimbus. All rights reserved.
//

import UIKit
import Firebase
//import Siren
import Fabric
import Crashlytics
//import GGLSignIn
import GoogleSignIn
import Google
import FBSDKCoreKit
import FBSDKLoginKit
import IQKeyboardManagerSwift



@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,GIDSignInDelegate {

    var window: UIWindow?
    var activityView1  = UIView()
    var imageForR = UIImageView ()
    var load = UILabel()


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
		
		//keyboard
		IQKeyboardManager.shared.enable=true
		
        //Fabric
        
        Fabric.with([Crashlytics.self])
        
        self.createIndicator()
         UIApplication.shared.applicationIconBadgeNumber = 0
        registerForPushNotifications(application)
        //App Rating
        let appRater = APAppRater.sharedInstance
        let apID : String = (Bundle.main.object(forInfoDictionaryKey: kCFBundleIdentifierKey as String) as? String) ?? "" //CFBundleIdentifier
        appRater.appId = apID
        
		FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        var configureError: NSError?
        GGLContext.sharedInstance().configureWithError(&configureError)
        assert(configureError == nil, "Error configuring Google services: \(configureError)")
        // change done on 10 april 2017
        GIDSignIn.sharedInstance().clientID = FIRApp.defaultApp()?.options.clientID
         GMSServices.provideAPIKey("AIzaSyDYkJs0dr4D8KmezSLKkYjE8Kx25qktCwE")
         FIRApp.configure()
		
        return true
    }
// register for push notification
    func registerForPushNotifications(_ application: UIApplication) {
        let notificationSettings = UIUserNotificationSettings(
            types: [.badge, .sound, .alert], categories: nil)
        UIApplication.shared.applicationIconBadgeNumber = 0
        application.registerUserNotificationSettings(notificationSettings)
    }
    func application(_ application: UIApplication, didRegister notificationSettings: UIUserNotificationSettings) {
        if notificationSettings.types != UIUserNotificationType() {
            application.registerForRemoteNotifications()
        }
        else
        {
             UserDefaults.standard.set("notFound", forKey: Constants.USERDEVICETOKEN)
            
        }
        print("Device Token:", "Empty Token")

    }
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let tokenChars = (deviceToken as NSData).bytes.bindMemory(to: CChar.self, capacity: deviceToken.count)
        var tokenString = ""
        
        for i in 0..<deviceToken.count {
            tokenString += String(format: "%02.2hhx", arguments: [tokenChars[i]])
        }
        
        print("Device Token:", tokenString)
        if tokenString == "" || tokenString.isEmpty == true {
            tokenString = "notFound"
        }
        print("TOKEN STRING")
        print(tokenString)
        UserDefaults.standard.set(tokenString, forKey: Constants.USERDEVICETOKEN)
    
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Failed to register:", error)
        UserDefaults.standard.set("error", forKey: Constants.USERDEVICETOKEN)
        print("Device Token:", "Empty Token")

    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void)
    {
        let aps = userInfo["aps"] as! [String: AnyObject]
        
        let message = aps["alert"]! as! String
        let rateAlert = UIAlertController(title: "Alert", message: message, preferredStyle: .alert)
        let goToItunesAction = UIAlertAction(title: "Dismiss", style: .cancel, handler: { (action) -> Void in
            
        })
        
        rateAlert.addAction(goToItunesAction)
        DispatchQueue.main.async(execute: { () -> Void in
            
            self.window!.rootViewController?.present(rateAlert, animated: true, completion: nil)
        })
    }

    //MARK:- UNIQUE Identifier
    static func getUniqueDeviceIdentifierAsString() -> String {
        
        let appName = Bundle.main.infoDictionary![kCFBundleNameKey as String] as! String
        
        if let appUUID = SSKeychain.password(forService: appName, account: "com.nikonIndia") {//"com.neuronimbus.MyRecipeApp"
            return appUUID
        }
        else {
            
            let appUUID = UIDevice.current.identifierForVendor?.uuidString
            SSKeychain.setPassword(appUUID, forService: appName, account: "com.nikonIndia")//"com.neuronimbus.MyRecipeApp"
            return appUUID!
        }
    }
	
	
	
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool
    {
        return FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
    }
	
	
	
    func application(_ application: UIApplication,
                     open url: URL, options: [UIApplicationOpenURLOptionsKey: Any]) -> Bool {
        return GIDSignIn.sharedInstance().handle(url, sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String, annotation: options[UIApplicationOpenURLOptionsKey.annotation])
    }
	
	
	
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
                withError error: Error!) {
        if (error == nil) {
            // Perform any operations on signed in user here.
//            let userId = user.userID                  // For client-side use only!
//            let idToken = user.authentication.idToken // Safe to send to the server
//            let fullName = user.profile.name
//            let givenName = user.profile.givenName
//            let familyName = user.profile.familyName
//            let email = user.profile.email
            // ...
        } else {
            print("\(error.localizedDescription)")
        }
    }
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user:GIDGoogleUser!,
                withError error: NSError!) {
        // Perform any operations when the user disconnects from app here.
        // ...
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
//        Siren.sharedInstance.checkVersion(.Immediately)
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        FBSDKAppEvents.activateApp()
//        Siren.sharedInstance.checkVersion(.Daily)
    }

    func applicationWillTerminate(_ application: UIApplication) {
    }

    
    
    //MARK:- INDICATOR VIEW
    func createIndicator()
    {
        activityView1 = UIView.init(frame: CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: ScreenSize.SCREEN_HEIGHT))
        activityView1.backgroundColor = UIColor.clear
        
        let back = UIImageView.init(frame: CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: ScreenSize.SCREEN_HEIGHT))
        back.backgroundColor = UIColor.black
        back.alpha = 0.5
        activityView1.addSubview(back)
        
        let bgView = UIView.init(frame: CGRect(x: (ScreenSize.SCREEN_WIDTH - 150)/2,y: (ScreenSize.SCREEN_HEIGHT - 150)/2, width: 150,height: 150))
        bgView.backgroundColor = UIColor.darkGray
        bgView.layer.cornerRadius = 18.0
        bgView.layer.shadowColor = UIColor.lightGray.cgColor
        bgView.clipsToBounds = true
        
        load = UILabel.init(frame: CGRect(x: 25, y: (bgView.frame.size.height - 30)/2, width: 100, height: 30))
        load.text = "LOADING..."
        load.textColor = UIColor.white
        load.font = UIFont.boldSystemFont(ofSize: 14.0)
        load.textAlignment = NSTextAlignment.center
        
        imageForR = UIImageView.init(frame: CGRect(x: 20 ,y: 20, width: 110,height: 110))
        imageForR.image = UIImage.init(named: "mouse.png")
        imageForR.backgroundColor = UIColor.clear
        
        bgView.addSubview(load)
        bgView.addSubview(imageForR)
        
        activityView1.addSubview(bgView)
        print(self.window)
        self.window?.addSubview(activityView1)
        self.window?.sendSubview(toBack: activityView1)
    }
    
    func startIndicator()
    {
        
        DispatchQueue.main.async {
            self.window!.addSubview(self.activityView1)
            self.rotate360Degrees()
        }
    }
    
    func stopIndicator()
    {
        DispatchQueue.main.async {
            self.imageForR.layer.removeAllAnimations()
            self.window?.willRemoveSubview(self.activityView1)
            self.activityView1.removeFromSuperview()
        }
    }
    
    func rotate360Degrees(_ duration: CFTimeInterval = 3.5, completionDelegate: AnyObject? = nil)
    {
        let rotateAnimation = CABasicAnimation(keyPath: "transform.rotation")
        rotateAnimation.fromValue = 0.0
        rotateAnimation.toValue = CGFloat(M_PI * -2.0)
        rotateAnimation.duration = duration
        rotateAnimation.repeatCount = 25
        
        if let delegate: AnyObject = completionDelegate {
            rotateAnimation.delegate = delegate as! CAAnimationDelegate
        }
        imageForR.layer.add(rotateAnimation, forKey: nil)
    }
    
    //MARK:- SIREN - APp version Updation
//    func setupSiren() {
//        
//        let siren = Siren.sharedInstance
//        
//        // Optional
//        siren.delegate = self
//        
//        // Optional
//        siren.debugEnabled = true
//        
//        // Optional - Defaults to .Option
//        //        siren.alertType = .Option // or .Force, .Skip, .None
//        // Optional - Can set differentiated Alerts for Major, Minor, Patch, and Revision Updates (Must be called AFTER siren.alertType, if you are using siren.alertType)
//        siren.majorUpdateAlertType = .Option
//        siren.minorUpdateAlertType = .Option
//        siren.patchUpdateAlertType = .Option
//        siren.revisionUpdateAlertType = .Option
//        siren.forceLanguageLocalization = SirenLanguageType.English
//        siren.alertType = SirenAlertType.Skip
//        
//        // Optional - Sets all messages to appear in Spanish. Siren supports many other languages, not just English and Russian.
//        //        siren.forceLanguageLocalization = .Russian
//        // Optional - Set this variable if your app is not available in the U.S. App Store. List of codes: https://developer.apple.com/library/content/documentation/LanguagesUtilities/Conceptual/iTunesConnect_Guide/Appendices/AppStoreTerritories.html
//        //        siren.countryCode = ""
//        // Optional - Set this variable if you would only like to show an alert if your app has been available on the store for a few days. The number 5 is used as an example.
//        //        siren.showAlertAfterCurrentVersionHasBeenReleasedForDays = 5
//        // Required
//        siren.checkVersion(SirenVersionCheckType.Daily)
//        
//        
//    }

}
//extension AppDelegate: SirenDelegate
//{
//    func sirenDidShowUpdateDialog(alertType: SirenAlertType) {
//        print(#function, alertType)
//    }
//    
//    func sirenUserDidCancel() {
//        print(#function)
//    }
//    
//    func sirenUserDidSkipVersion() {
//        print(#function)
//    }
//    
//    func sirenUserDidLaunchAppStore() {
//        print(#function)
//    }
//    
//    func sirenDidFailVersionCheck(error: NSError) {
//        print(#function, error)
//    }
//    
//    func sirenLatestVersionInstalled() {
//        print(#function, "Latest version of app is installed")
//    }
//    
//    /**
//     This delegate method is only hit when alertType is initialized to .None
//     */
//    func sirenDidDetectNewVersionWithoutAlert(message: String) {
//        print(#function, "\(message)")
//    }
//}

